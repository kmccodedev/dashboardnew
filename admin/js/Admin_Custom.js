﻿//==========================================================================================================================================
/* File Name: Admin_Custom.js */
/* File Created: July 11, 2016 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================
//TODO : Sevice URL
var BaseURL = "https://optimise.marstongroup.co.uk";
var ServiceURL = BaseURL+"/OptimiseAPI/";

//==========================================================================================================================================
// TODO: Home page link
var HomeUrl = BaseURL+"/sign-in.html";
var VideoUrl = BaseURL+"/Intro.html";
var SCUrl = BaseURL+"/Dashboard_SC.html";
var MSHomeUrl = BaseURL+"/Dashboard_MS.html";
var HCHomeUrl = BaseURL+"/Dashboard_HC.html";
var OptimiseImageUrl = BaseURL+"/Admin/OptimiseActionImage.html";
var AdminCaseUrl = BaseURL+"/CaseDetail.html";
var CaseTemplateDownload = BaseURL+"/Template/CaseUploadTemplate.xlsx";

// TODO: Menu link
//==========================================================================================================================================
var TeamAdminUrl=BaseURL+"/Admin/ITadmin.html";
var ReturnActionUrl = BaseURL+"/Admin/PendingAction.html";
var CaseSearchUrl = BaseURL+"/Admin/CaseSearch.html";
var ApproveRequestUrl = BaseURL+"/Admin/AssignRequest.html";
var HPICheckRequestUrl = BaseURL+"/Admin/HPICheckRequest.html";
var PaymentReportUrl = BaseURL+"/Admin/Reports.html";
var CaseuploadUrl = BaseURL+"/Admin/CaseUpload.html";
var CGAdminUrl = BaseURL+"/Admin/CGadmin.html";
var ClampedImageUrl = BaseURL+"/Admin/ClampedImage.html";
var BailedImageSearchUrl = BaseURL+"/Admin/BailedImage.html";
var CaseReturnUrl = BaseURL+"/Admin/CaseReturn.html";
var SCAdminUrl = BaseURL+"/Admin/SCadmin.html";
//==========================================================================================================================================




var Email = /^[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/;
var Mobile = /^[0-9-+]+$/;
//----------------------------------------------------------------------------------------------------------------------

var DataType = 'json';
var markers = [];
var map;
var latLng;
var marker;
var markerCluster;
var infowindow;
var mcOptions;
var options;
var center;

$(document).ready(function () {
    if ($.trim($.session.get('OfficerID')).length > 0) {
    }
    else {
        window.location.href = HomeUrl;
    }
    history.pushState(null, document.title, location.href);
    window.addEventListener('popstate', function (event) {
        history.pushState(null, document.title, location.href);
    });

    $('#lblLoginOfficer').html($.session.get('OfficerName'));

     if ($.session.get('RoleType') == 5) {
        $('#Alogo').attr('href', '#');
        $('#popup2Trigger').hide();
    }
    else
        $('#Alogo').attr('href', ($.session.get('CompanyID') == 1 ? MSHomeUrl : HCHomeUrl));

    if ($.session.get("timervalue") != undefined) {
        Timer();
    }

    $(".Numeric").keydown(function (e) {
        $(this).attr('style', 'border-color: none');
        if (e.shiftKey == true) {
            if (e.shiftKey == true && e.keyCode != 16) {
                $(this).attr('style', 'border:2px solid #7A4073');
                $(this).attr('placeholder', 'Enter numeric values');
                return false;
            }
            return false;
        }
        if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 35 || e.keyCode == 36 ||
                     e.keyCode == 37 || e.keyCode == 39 || e.keyCode == 45 || e.keyCode == 46 || e.keyCode == 144 || e.keyCode == 59 || e.ctrlKey == true) {
            $(this).attr('style', 'border-color: none');
            $(this).removeAttr('placeholder');
            return true;
        }
        else {
            $(this).attr('style', 'border:2px solid #7A4073');
            $(this).attr('placeholder', 'Enter numeric values');
            return false;
        }
    });
});

function AlertMessage(title, message) {
    $("#DAlert").dialog({ "title": title });
    $('#DAlert').html(message);
    $('#DAlert').dialog({
        modal: true,
        resizable: false,
        width: "25%",
        buttons: {
            "Close": function () {
                $(this).dialog("close");
            }
        }
    });
}

function Logout() {
    $('#DivLogout').dialog({
        modal: true,
        resizable: false,
        width: "22%",
        "title": "Confirm",
        buttons: {
            "Yes": function () {
                $.ajax({
                    type: 'PUT',
                    url: ServiceURL + 'api/v1/officers/' + $.session.get('OfficerID') + '/Logout',
                    headers: {
                        "Authorization": $.session.get('TokenAuthorization'),
                        'Content-Type': 'application/json'
                    },
                    dataType: DataType,
                    beforeSend: function () {
                    },
                    success: function (data) {
                        if (data == true) {
                            $.session.clear();
                            window.location.href = HomeUrl;
                        }
                    },
                    complete: function () { },
                    error: function (xhr, textStatus, errorThrown) {
                        alert('Error ' + errorThrown);
                    }
                });

                $(this).dialog("close");
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });
}

function parameterPush(paramData, paramKey, paramValue) {
    if ($.trim(paramValue) != '') {
        var myObj = jQuery.parseJSON('{ "' + paramKey + '": ' + paramValue + '}');
        $.extend(true, paramData, myObj);
    }
    return paramData;
}

function Timer() {
    setInterval(function () {
        var timervalue = parseInt($.session.get("timervalue"));
        timervalue = timervalue + 1;
        $.session.set("timervalue", timervalue);
        if (timervalue > 15) {
            CheckToken();
        }
    }, 60000);
}

function CheckToken() {
    $.ajax({
        type: 'POST',
        url: ServiceURL + 'token',
        data: {
            "grant_type": "password",
            "username": $.session.get('OfficerID'),
            "password": $.session.get('Password')
        },
        dataType: DataType,
        success: function (data, textStatus, xhr) {
            $.session.set('Token', data.access_token);
            $.session.set('TokenAuthorization', data.token_type + ' ' + data.access_token);
            $.session.set("timervalue", 0);
        },
        error: function (xhr, textStatus, errorThrown) {
            //    alert('Error');
        }
    });
}

function zeropadded(l_value) {
    var l_temp = "" + (l_value + 100);
    return l_temp.substr(1);
}


//TODO: Page link

function TeamAdmin() {
    window.location.href = TeamAdminUrl;
}

function ReturnAction() {
    window.location.href = ReturnActionUrl;
}

function CaseSearchAction() {
    window.location.href = CaseSearchUrl;
}

function PaymentReport() {
    window.location.href = PaymentReportUrl;
}

function CaseUpload() {
    window.location.href = CaseuploadUrl;
}

function ClampedImageSearch() {
    window.location.href = ClampedImageUrl;
}

function BailedImageSearch() {
    window.location.href = BailedImageSearchUrl;
}

function GetManageCaseReturn() {
    window.location.href = CaseReturnUrl;
}
