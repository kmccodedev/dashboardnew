﻿var l_date;
var PFlag = false, PPFlag = false, IFlag = false;
var SFlag = false, EFlag = false;
$(document).ready(function () {
    if ($.trim($.session.get('OfficerID')).length > 0) {
    }
    else {
        window.location.href = HomeUrl;
    }
    $(':text').val('');
    $('select').prop('selectedIndex', 0);

    var l_curdate = new Date();
    l_date = l_curdate.getFullYear() + zeropadded(l_curdate.getMonth() + 1) + zeropadded(l_curdate.getDate()) + zeropadded(l_curdate.getHours()) + zeropadded(l_curdate.getMinutes()) + zeropadded(l_curdate.getSeconds());

    $('#txtToDate,#txtFromDate').attr('readonly', 'readonly');
    $('#txtFromDate,#txtToDate').attr('disabled', false);
    //format: 'Y/m/d',    
    date = new Date();
    TodayDate = $.datepicker.formatDate('yy/mm/dd', date) + ' ' + date.getHours() + ':' + date.getMinutes();
    TodayDateOnly = $.datepicker.formatDate('yy-mm-dd', date);

    $('#txtFromDate').datetimepicker({
        format: 'Y/m/d',
        maxDate: TodayDateOnly,
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('#txtToDate').val() ? $('#txtToDate').val() : false
            })
        },
        timepicker: false
    }).datepicker("setDate", TodayDateOnly);

    $('#txtToDate').datetimepicker({
        format: 'Y/m/d',
        maxDate: TodayDateOnly,
        onShow: function (ct) {
            this.setOptions({
                //minDate: $('#txtFromDate').val().length>0 ? $('#txtFromDate').val() : false
                minDate: $('#txtFromDate').val()
            })
        },
        timepicker: false
    }).datepicker("setDate", TodayDateOnly);

    $('#btnSearch').click(function () {
        var pass = 0;
        $('#selReport').each(function () {
            var $this = $(this);
            if ($this.is('select')) {
                if ($this.val() == '0') {
                    pass = 1;
                    $('#' + $this.attr('id')).attr('style', 'border:2px solid #7A4073;');
                }
            }
            $.session.set('RPTFlag', 0);
        });
        if (pass == 0) {
            GetPaymentReport();
        }
    });

    $('#btnPending').click(function () {
        $.session.set('RPTFlag', 1);
        GetPaymentReport();
    });

    $('#btnClearOfficer').click(function () {
        $(':text').val('');
        $('select').prop('selectedIndex', 0);
        $('#lblReport').html('');
        $('#selReport').css("border", "1px solid #D7D7D7");
        $('#DPaymentReport,#DPendingPaymentreport,#DOfficerIMEI,#DSurrenderLog,#DePaying,#btnPending').hide();
        $('#txtFromDate,#txtToDate').attr('disabled', false);
        PFlag = false; PPFlag = false; IFlag = false;
        SFlag = false; EFlag = false;
    });

    $('#selReport').change(function () {
        $('#btnPending').hide();
        $('#selReport').css("border", "1px solid #D7D7D7");
        switch ($(this).val()) {
            case '1':
                $('#txtFromDate,#txtToDate').val("").attr('disabled', false);
                $('#lblReport').html('Payment Report');
                $('#btnPending').show();
                break;
            case '2':
                $('#txtFromDate,#txtToDate').val("").attr('disabled', true);
                $('#lblReport').html('Officer IMEI Report');
                break;
            case '3':
                $('#txtFromDate,#txtToDate').val("").attr('disabled', false);
                $('#lblReport').html('Surrender Log Report');
                break;
            case '4':
                $('#txtFromDate,#txtToDate').val("").attr('disabled', true);
                $('#lblReport').html('ePaying Report');
                break;
        }
    });
});

function GetPaymentReport() {
    var FromDate = "", ToDate = "";
    FromDate = $.trim($('#txtFromDate').val().replace(/[^0-9\/]/g, ''));
    if (FromDate.length == 10) {
        var dateF = FromDate.split('/');
        FromDate = dateF[0] + '-' + dateF[1] + '-' + dateF[2];
    }
    else {
        FromDate = ""
    }

    ToDate = $.trim($('#txtToDate').val().replace(/[^0-9\/]/g, ''));
    if (ToDate.length == 10) {
        var dateT = ToDate.split('/');
        ToDate = dateT[0] + '-' + dateT[1] + '-' + dateT[2];
    }
    else {
        ToDate = ""
    }

    $('#DPaymentReport,#DPendingPaymentreport,#DOfficerIMEI,#DSurrenderLog,#DePaying').hide();
    switch ($('#selReport').val()) {
        case '1':
            //0- View
            //1- Pending

            if ($.session.get('RPTFlag') == 0) {
                $.ajax({
                    type: 'GET',
                    url: ServiceURL + 'api/v1/cases/' + (FromDate == '' ? '1-1-1900' : FromDate) + '/' + (ToDate == '' ? '1-1-1900' : ToDate) + '/PaymentReport',
                    headers: {
                        "Authorization": $.session.get('TokenAuthorization'),
                        'Content-Type': 'application/json'
                    },
                    dataType: 'json',
                    beforeSend: function () {
                        $.loader({
                            className: "blue-with-image",
                            content: ''
                        });
                        if (PFlag == true)
                            $('#tblPaymentReport').DataTable().destroy();
                    },
                    success: function (data) {
                        if (!(data == '' || data == null || data == '[]')) {
                            PFlag = true;
                            $('#tbodyPaymentReport').empty();
                            $('#DPaymentReport').show();
                            $.each(data, function (key, value) {
                                $('#tbodyPaymentReport').append('<tr><td>' + value.Officer + '</td><td>' + value.OfficerName + '</td>' +
                                                                    '<td>' + value.CaseNumber + '</td><td>' + value.DateActioned + '</td>' +
                                                                    '<td>' + value.Cash + '</td><td>' + value.Card + '</td><td>' + value.ADMname + '</td>' +
                                                                    '<td>' + value.CashupStatus + '</td><td>' + value.CashierStatus + '</td><td>' + value.PType + '</td></tr>');
                            });
                            $('#tblPaymentReport').DataTable({
                                "sScrollY": "auto",
                                "bFilter": false,
                                "bPaginate": true,
                                "bDestroy": true,
                                "bSort": false,
                                "sPaginationType": "full_numbers",
                                "bLengthChange": false,
                                "sInfo": true,
                                "iDisplayLength": 10,
                                dom: 'Bfrtip',
                                buttons: [
                                      {
                                          'extend': 'excelHtml5',
                                          'title': 'PaymentRPT_' + l_date
                                      }
                                ]
                            });
                        }
                        else {
                            AlertMessage('Info!', 'No data found');
                        }
                    },
                    complete: function () {
                        $.loader('close');
                    },
                    error: function () { }
                });
            }
            else {
                $.ajax({
                    type: 'GET',
                    url: ServiceURL + 'api/v1/cases/' + (FromDate == '' ? '1-1-1900' : FromDate) + '/' + (ToDate == '' ? '1-1-1900' : ToDate) + '/PendingPaymentReport',
                    headers: {
                        "Authorization": $.session.get('TokenAuthorization'),
                        'Content-Type': 'application/json'
                    },
                    dataType: 'json',
                    beforeSend: function () {
                        $.loader({
                            className: "blue-with-image",
                            content: ''
                        });
                        if (PPFlag == true)
                            $('#tblPaymentPendingReport').DataTable().destroy();
                    },
                    success: function (data) {
                        if (!(data == '' || data == null || data == '[]')) {
                            PPFlag = true;
                            $('#tbodyPaymentPendingReport').empty();
                            $('#DPendingPaymentreport').show();
                            $.each(data, function (key, value) {
                                $('#tbodyPaymentPendingReport').append('<tr><td>' + value.Officer + '</td><td>' + value.OfficerName + '</td>' +
                                                                    '<td>' + value.CaseNumber + '</td><td>' + value.DateActioned + '</td>' +
                                                                    '<td>' + value.Cash + '</td><td>' + value.Card + '</td><td>' + value.ADMname + '</td>' +
                                                                    '<td>' + value.CashupStatus + '</td><td>' + value.CashierStatus + '</td><td>' + value.PType + '</td></tr>');
                            });
                            $('#tblPaymentPendingReport').DataTable({
                                "sScrollY": "auto",
                                "bFilter": false,
                                "bPaginate": true,
                                "bDestroy": true,
                                "bSort": false,
                                "sPaginationType": "full_numbers",
                                "bLengthChange": false,
                                "sInfo": true,
                                "iDisplayLength": 10,
                                dom: 'Bfrtip',
                                buttons: [
                                      {
                                          'extend': 'excelHtml5',
                                          'title': 'PendingPaymentRPT_' + l_date
                                      }
                                ]
                            });
                        }
                        else {
                            AlertMessage('Info!', 'No data found');
                        }
                    },
                    complete: function () {
                        $.loader('close');
                    },
                    error: function () { }
                });
            }
            break;
        case '2':
            $.ajax({
                type: 'GET',
                url: ServiceURL + 'api/v1/cases/' + (FromDate == "" ? '1-1-1900' : FromDate) + '/' + (ToDate == "" ? '1-1-1900' : ToDate) + '/OfficerIMEIReport',
                headers: {
                    "Authorization": $.session.get('TokenAuthorization'),
                    'Content-Type': 'application/json'
                },
                dataType: 'json',
                beforeSend: function () {
                    $.loader({
                        className: "blue-with-image",
                        content: ''
                    });
                    if (IFlag == true)
                        $('#tblOfficerIMEI').DataTable().destroy();
                },
                success: function (data) {
                    if (!(data == '' || data == null || data == '[]')) {
                        IFlag = true;
                        $('#DOfficerIMEI').show();
                        $('#tbodyOfficerIMEI').empty();
                        $.each(data, function (key, value) {
                            $('#tbodyOfficerIMEI').append('<tr><td>' + value.OfficerID + '</td><td>' + value.OfficerName + '</td>' +
                                                                '<td>' + value.IMEInumber + '</td><td>' + value.ManagerID + '</td>' +
                                                                '<td>' + value.ManagerName + '</td><td>' + value.LastActionedDate + '</td></tr>');
                        });
                        $('#tblOfficerIMEI').DataTable({
                            "sScrollY": "auto",
                            "bFilter": false,
                            "bPaginate": true,
                            "bDestroy": true,
                            "bSort": false,
                            "sPaginationType": "full_numbers",
                            "bLengthChange": false,
                            "sInfo": true,
                            "iDisplayLength": 10,
                            dom: 'Bfrtip',
                            buttons: [
                                  {
                                      'extend': 'excelHtml5',
                                      'title': 'OfficerIMEIRPT_' + l_date
                                  }
                            ]
                        });
                    }
                    else {
                        AlertMessage('Info!', 'No data found');
                    }
                },
                complete: function () {
                    $.loader('close');
                },
                error: function () { }
            });
            break;
        case '3':
            $.ajax({
                type: 'GET',
                url: ServiceURL + 'api/v1/cases/' + (FromDate == "" ? TodayDateOnly : FromDate) + '/' + (ToDate == "" ? TodayDateOnly : ToDate) + '/SurrenderLogReport',
                headers: {
                    "Authorization": $.session.get('TokenAuthorization'),
                    'Content-Type': 'application/json'
                },
                dataType: 'json',
                beforeSend: function () {
                    $.loader({
                        className: "blue-with-image",
                        content: ''
                    });
                    if (SFlag == true)
                        $('#tblSurrenderLog').DataTable().destroy();
                },
                success: function (data) {
                    if (!(data == '' || data == null || data == '[]')) {
                        SFlag = true;
                        $('#DSurrenderLog').show();
                        $('#tbodySurrenderLog').empty();
                        $.each(data, function (key, value) {
                            $('#tbodySurrenderLog').append('<tr><td>' + value.OfficerId + '</td><td>' + value.caseNumber + '</td>' +
                                                                '<td>' + value.SDate + '</td><td>' + value.STime + '</td>' +
                                                                '<td>' + value.Location + '</td><td>' + value.ContactNo + '</td><td>' + value.ActionDate + '</td>' +
                                                                '<td>' + value.DefendantName + '</td><td>' + value.ClientReference + '</td><td>' + value.Warranttype + '</td></tr>');
                        });
                        $('#tblSurrenderLog').DataTable({
                            "sScrollY": "auto",
                            "bFilter": false,
                            "bPaginate": true,
                            "bDestroy": true,
                            "bSort": false,
                            "sPaginationType": "full_numbers",
                            "bLengthChange": false,
                            "sInfo": true,
                            "iDisplayLength": 10,
                            dom: 'Bfrtip',
                            buttons: [
                                  {
                                      'extend': 'excelHtml5',
                                      'title': 'SurrenderLogRPT_' + l_date
                                  }
                            ]
                        });
                    }
                    else {
                        AlertMessage('Info!', 'No data found');
                    }
                },
                complete: function () {
                    $.loader('close');
                },
                error: function () { }
            });
            break;
        case '4':
            $.ajax({
                type: 'GET',
                url: ServiceURL + 'api/v1/cases/' + (FromDate == "" ? '1-1-1900' : FromDate) + '/' + (ToDate == "" ? '1-1-1900' : ToDate) + '/EpayingReport',
                headers: {
                    "Authorization": $.session.get('TokenAuthorization'),
                    'Content-Type': 'application/json'
                },
                dataType: 'json',
                beforeSend: function () {
                    $.loader({
                        className: "blue-with-image",
                        content: ''
                    });
                    if (EFlag == true)
                        $('#tblePaying').DataTable().destroy();
                },
                success: function (data) {
                    if (!(data == '' || data == null || data == '[]')) {
                        EFlag = true;
                        $('#DePaying').show();
                        $('#tbodyePaying').empty();
                        $.each(data, function (key, value) {
                            $('#tbodyePaying').append('<tr><td>' + value.OfficerID + '</td><td>' + value.GiroNo + '</td>' +
                                                                '<td>' + value.Paying_Date + '</td><td>' + value.Processed_On + '</td>' +
                                                                '<td>' + value.WarrantNo + '</td><td>' + value.ScreenPrint + '</td>' +
                                                                '<td>' + value.Total_Cash + '</td><td>' + value.ReceiptNo + '</td></tr>');
                        });
                        $('#tblePaying').DataTable({
                            "sScrollY": "auto",
                            "bFilter": false,
                            "bPaginate": true,
                            "bDestroy": true,
                            "bSort": false,
                            "sPaginationType": "full_numbers",
                            "bLengthChange": false,
                            "sInfo": true,
                            "iDisplayLength": 10,
                            dom: 'Bfrtip',
                            buttons: [
                                  {
                                      'extend': 'excelHtml5',
                                      'title': 'ePayingRPT_' + l_date
                                  }
                            ]
                        });
                    }
                    else {
                        AlertMessage('Info!', 'No data found');
                    }
                },
                complete: function () {
                    $.loader('close');
                },
                error: function () { }
            });
            break;
    }
}