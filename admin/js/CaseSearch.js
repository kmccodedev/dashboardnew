﻿//==========================================================================================================================================
/* File Name: PendingAction.js */
/* File Created: March 19, 2014 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================
var mID;
var MgrID;
var DNotes;
var Tabledata = '';
var dFlag = false;
$(document).ready(function () {
    if ($.trim($.session.get('OfficerID')).length > 0) {
    }
    else {
        window.location.href = HomeUrl;
    }
    mID = $.session.get('OfficerID');
    $(':text').val('');

    if (mID == 9999 || mID == 8998) {
        $('#ManagerSelect').show();
        $('#ManagerNameLabel').hide();
        GetManagers();
    }
    else {
        $('#ManagerNameLabel').html($.session.get('OfficerName')).show();
        $('#ManagerSelect').hide();
        GetOfficerManager(mID);
    }

    date = new Date(Date.now() - (5 * 24 * 60 * 60 * 1000));
    l_limitedDate = $.datepicker.formatDate('yy/mm/dd', date);
    date = new Date();
    l_todayDate = $.datepicker.formatDate('yy/mm/dd', date);

    $('#ActionTypeSelect').prop("selectedIndex", -1);

    $('#ToDateText,#FromDateText').attr('readonly', 'readonly');
    //format: 'Y/m/d',
    $('#FromDateText').datetimepicker({
        //format: 'd/m/Y',
        format: 'Y/m/d',
        minDate: l_limitedDate,
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('#ToDateText').val() ? $('#ToDateText').val() : false
            })
        },
        timepicker: false
    });

    $('#ToDateText').datetimepicker({
        format: 'Y/m/d',
        maxDate: l_todayDate,
        onShow: function (ct) {
            this.setOptions({
                minDate: $('#FromDateText').val() ? $('#FromDateText').val() : false
            })
        },
        timepicker: false
    });

    $('#ClearButton').click(function () {
        $(':text').val('');
        if (mID == 9999 || mID == 8998) {
            $("#OfficerSelect").empty();
        }
        $('#OfficerFindText').show();
        $("#chkColumbusAction").prop("checked", false);
        $('input[name=Type][value=0]').prop('checked', true);

        $('#page-content-wrapper select').prop('selectedIndex', 0);
        $('#ActionTypeSelect').prop("selectedIndex", -1);
        $('input[name=rdBW][value=2]').prop('checked', true);
        $('#selNotFilming').val(0).attr("selected", "selected");
        $('#DCase').hide();
        $('#selRecordingNotSuccess,#selNotFilming').hide();
        $('#lblReason').html('');
        dFlag = false;
    });

    $('input[name=Type][value=0]').prop('checked', true);

    $('#FindButton').click(function () {
        if (mID == 9999 || mID == 8998)
            MgrID = $('#ManagerSelect').val();
        else
            MgrID = mID;

        GetCaseActionsSearchForAdmin(MgrID,
            $("#OfficerSelect option:selected").map(function () { return this.value; }).get().join(','),
            $('#FromDateText').val().replace(/[^0-9\/]/g, ''), $('#ToDateText').val().replace(/[^0-9\/]/g, ''),
            (($('#ManagerSelect').val() == '0' && (mID == 9999 || mID == 8998)) ? $('#CaseNoText').val().replace(/[^a-zA-Z0-9]/g, '') + '|' + mID : $('#CaseNoText').val().replace(/[^a-zA-Z0-9]/g, '')),
            $("#ActionTypeSelect option:selected").map(function () { return this.value; }).get().join(','),
            ($("#chkColumbusAction").is(':checked') == true ? 1 : 0),
            $('input[name=rdBW]:checked').val(),
            $('input[name=rdBW]:checked').val() == '1' ? $('#selRecordingNotSuccess option:selected').text() : '',
            $('input[name=rdBW]:checked').val() == '0' ? $('#selNotFilming option:selected').text() : '',
            $('input[name=Type]:checked').val());
    });

    $('input[type=radio][name=rdBW]').change(function () {
        if (this.value == 0) {
            $('#selRecordingNotSuccess').hide();
            $('#selNotFilming').show();
            $('#selRecordingNotSuccess').val(0).attr("selected", "selected");
            $('#lblReason').html('Reason for not filming');
        }
        else if (this.value == 1) {
            $('#selNotFilming').hide();
            $('#selRecordingNotSuccess').show();
            $('#selNotFilming').val(0).attr("selected", "selected");
            $('#lblReason').html('Reason for recoding was not successful');
        }
        else {
            $('#selRecordingNotSuccess,#selNotFilming').hide();
            $('#lblReason').html('');
        }
    });
    $('input[name=rdBW][value=2]').prop('checked', true);
    $('#selRecordingNotSuccess,#selNotFilming').hide();
    $('#lblReason').html('');
    //----------------------------------------------------------------------------------------------------------------------
    // Keyup function

    $('#OfficerFindText').keyup(function () {
        if ($.trim($('#OfficerFindText').val()) != '') {
            GetOfficersOnSearch($('#OfficerFindText').val());
        }
        else {
            GetOfficersOnSearch($('#OfficerFindText').val());
        }
    });

    //----------------------------------------------------------------------------------------------------------------------
});


//----------------------------------------------------------------------------------------------------------------------
// TOP Panel list box

//TODO: Manager list
function GetManagers() {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/managers/' + mID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () { },
        success: function (data) {
            $('#ManagerSelect').empty();
            var ManagerList = '<option value="0">Select all managers</option>';
            $.each(data, function (key, value) {
                ManagerList = ManagerList + '<option value="' + value.OfficerID + '">' + value.OfficerName + '</option>';
            });
            $('#ManagerSelect').html(ManagerList);
            $('#ManagerSelect').change(function () {
                GetOfficerManager($('#ManagerSelect').val());
                if ($('#ManagerSelect').val() == 0) {
                    $('#OfficerFindText').val('');
                    $('#OfficerFindText').show();
                }
                else
                    $('#OfficerFindText').hide();
            });
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

//TODO: officer list for selected manager
function GetOfficerManager(ManagerID) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + $.trim(ManagerID) + '/OfficersManager',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () { },
        success: function (data) {
            $("#OfficerSelect").empty();
            if (data != undefined) {
                if (data != "[]" && data != null) {
                    var OfficerList = '<option value="">Select all officers</option>';
                    $.each(data, function (key, value) {
                        OfficerList = OfficerList + '<option value="' + value.OfficerId + '">' + value.OfficerName + '</option>';
                    });
                    $('#OfficerSelect').html(OfficerList);
                }
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

//TODO: Officers On Search list
function GetOfficersOnSearch(SearchText) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + SearchText + '/SearchOfficers',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () { },
        success: function (data) {
            $("#OfficerSelect").empty();
            if (data != undefined) {
                if (data != "[]" && data != null) {
                    var OfficerList = '<option value="">Select all officers</option>';
                    $.each(data, function (key, value) {
                        OfficerList = OfficerList + '<option value="' + value.OfficerID + '">' + value.OfficerName + '</option>';
                    });
                    $('#OfficerSelect').html(OfficerList);
                }
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

//----------------------------------------------------------------------------------------------------------------------
//TODO
function GetCaseActionsSearchForAdmin(ManagerID, OfficerID, FromDatep, ToDatep, CaseNo, Type, IsApprove, IsBWV, ReasonUnsuccessful, ReasonNotFilming, Category) {
    var Filename = "";
    var Titlename = "";
    var l_curdate = new Date();
    var l_date = l_curdate.getFullYear() + zeropadded(l_curdate.getMonth() + 1) + zeropadded(l_curdate.getDate()) + zeropadded(l_curdate.getHours()) + zeropadded(l_curdate.getMinutes()) + zeropadded(l_curdate.getSeconds());

    if (Category == 0) {
        Titlename = "Case_" + l_date;
        Filename = "Normal case";
    }
    else {
        Titlename = "Special case" + l_date;
        Filename = "Special case";
    }

    var Parameter = "";
    var FromDate = "", ToDate = "";
    if ($.trim(FromDatep) != "")
        FromDate = FromDatep.replace('/', '-').replace('/', '-');

    if ($.trim(ToDatep) != "")
        ToDate = ToDatep.replace('/', '-').replace('/', '-');

    if ($.trim(ManagerID) != "")
        Parameter = 'ManagerId=' + $.trim(ManagerID);

    if ($.trim(OfficerID) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'OfficerId=' + $.trim(OfficerID);
        }
        else
            Parameter = 'OfficerId=' + $.trim(OfficerID);
    }

    if ($.trim(FromDate) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'FromDate=' + FromDate;
        }
        else
            Parameter = 'FromDate=' + FromDate;
    }

    if ($.trim(ToDate) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'ToDate=' + ToDate;
        }
        else
            Parameter = 'ToDate=' + ToDate;
    }

    if ($.trim(CaseNo) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'CaseNo=' + CaseNo;
        }
        else
            Parameter = 'CaseNo=' + CaseNo;
    }

    if ($.trim(Type) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'ActionType=' + Type;
        }
        else
            Parameter = 'ActionType=' + Type;
    }

    if ($.trim(IsApprove) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'IsApproved=' + IsApprove;
        }
        else
            Parameter = 'IsApproved=' + IsApprove;
    }

    if ($.trim(IsBWV) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'IsBWV=' + IsBWV;
        }
        else
            Parameter = 'IsBWV=' + IsBWV;
    }

    if ($.trim(ReasonNotFilming) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'ReasonNotFilming=' + ReasonNotFilming;
        }
        else
            Parameter = 'ReasonNotFilming=' + ReasonNotFilming;
    }

    if ($.trim(ReasonUnsuccessful) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'ReasonUnsuccessful=' + ReasonUnsuccessful;
        }
        else
            Parameter = 'ReasonUnsuccessful=' + ReasonUnsuccessful;
    }

    if ($.trim(Category) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'Category=' + Category;
        }
        else
            Parameter = 'Category=' + Category;
    }


    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/CaseActionsSearchForDataExport?' + Parameter,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('.Hidecase').hide();
            $.loader({
                className: "blue-with-image",
                content: ''
            });
            if (dFlag == true)
                $('#tblGridView').DataTable().destroy();
        },
        success: function (data) {
            if (!(data == '' || data == null || data == '[]')) {
                dFlag = true;
                $('#DCase').show();
                $('#tbodyGridView').empty();
                $.each(data, function (key, value) {
                    $('#tbodyGridView').append('<tr><td>' + value.ID + '</td>' +
                                        '<td>' + value.OfficerID + '</td>' +
                                        '<td>' + value.OfficerName + '</td>' +
                                        '<td>' + value.CaseNumber + '</td>' +
                                        '<td>' + value.DateActioned + '</td>' +
                                        '<td class="Hidecase">' + value.GpsLatitude + '</td>' +
                                        '<td class="Hidecase">' + value.GpsLongitude + '</td>' +
                                        '<td class="Hidecase">' + value.GpsHDOP + '</td>' +
                                        '<td>' + value.ManagerName + '</td>' +
                                        '<td class="Hidecase">' + value.Notes + '</td>' +
                                        '<td class="Hidecase">' + value.ContactName + '</td>' +
                                        '<td class="Hidecase">' + value.DateReceived + '</td>' +
                                        '<td class="Hidecase">' + value.ActionText + '</td>' +
                                        '<td>' + value.IsBWV + '</td>' +
                                        '<td class="Hidecase">' + value.ReasonNotfilming + '</td>' +
                                        '<td class="Hidecase">' + value.EndConfirmation + '</td>' +
                                        '<td class="Hidecase">' + value.SurrenderDate + '</td>' +
                                        '<td class="Hidecase">' + value.SurrenderTime + '</td>' +
                                        '<td class="Hidecase">' + value.Location + '</td>' +
                                        '<td class="Hidecase">' + value.ContactNo + '</td>' +
                                        '<td class="Hidecase">' + value.ContactPostcode + '</td>' +
                                        '</tr>');
                });
                $('.Hidecase').hide();
                $('#tblGridView').DataTable({
                    "sScrollY": "auto",
                    "bFilter": false,
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "bLengthChange": false,
                    "sInfo": true,
                    "iDisplayLength": 5,
                    dom: 'Bfrtip',
                    buttons: [
                          {
                              'extend': 'excelHtml5',
                              'title': Titlename
                          }
                    ]
                });
            }
            else {
                AlertMessage(Filename, 'No data found!');
                $('#DCase').hide();
                dFlag = false;
            }
        },
        complete: function () {
            $.loader('close');
        },
        error: function (xhr, textStatus, errorThrown) {
            AlertMessage('Opps!', Filename + ' The result has been exceeded the excel sheet row limit. Please refine your search!');
            $.loader('close');
        }
    });
}