﻿/*
Created By   : R.Santhakumar
Created Date : 28-11-2016
Description  : Special cases javascript
*/

var CaseFlag;
var mID;
$(document).ready(function () {
    mID = getCookie("OfficerID");
    GetManager(mID);
    $('#DSearchPanel select').attr('style', 'width:160px;');

    $('#txtIssueDate,#txtAssignedOn,#txtExpiresOn,#txtCHOccured,#txtCNOccured,#txtCOOffenceDate,#txtCSDate,#txtCVResponseDate').datetimepicker({
        format: 'd/m/Y H:i:s',
        startDate: '+1971/05/01'//or 1986/12/08
        //timepicker: true
    });

    $('#btnGetSpecialCase').click(function () {
        GetCaseHeaderSearch_SC();
    });
    $('#btnClearSpecialCase').click(function () {
        $('#DSearchPanel select').prop('selectedIndex', 0);
        $('#DCaseType,#DSpecialCase').hide();
        // $('#DPause').hide();
    });
});

function GetManager(ManagerID) {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetManagerDetailsAdmin?ManagerID=" + ManagerID + "&" + getCookie('BrowserId'), // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#selManager').empty();
        },
        complete: function () { },
        success: function (msg) {//On Successfull service call  
            var result = JSON.parse(msg);
            var ManagerList = '<option value="0">Select Manager</option>';
            $.each(result, function (key, value) {
                if (key == 0)
                    GetADMList(0);

                ManagerList = ManagerList + '<option value="' + $.trim(value.OfficerID) + '">' + value.OfficerName + '</option>';
            });
            $('#selManager').html(ManagerList);

            $('#selManager').change(function () {
                GetADMList($('#selManager').val());
            });
        },
        error: function () { }
    });
}

function GetADMList(MgrID) {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetADMList?ManagerID=" + MgrID + "&" + getCookie('BrowserId'), // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#selADM').empty();
        },
        complete: function () { },
        success: function (msg) {//On Successfull service call  
            var result = JSON.parse(msg);
            var ADMList = '<option value="0">Select ADM</option>';
            if (MgrID == 0) {
                GetOfficerADM(0);
            }
            $.each(result, function (key, value) {
                if (key == 0)
                    GetOfficerADM(0);
                ADMList = ADMList + '<option value="' + value.ADMID + '">' + value.ADMName + '</option>';
            });
            $('#selADM').html(ADMList);

            $('#selADM').change(function () {
                GetOfficerADM($('#selADM').val());
            });
        },
        error: function () { }
    });
}

function GetOfficerADM(ADMID) {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetOfficerList?ADMID=" + ADMID + "&" + getCookie('BrowserId'), // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $("#selOfficer").empty();
        },
        complete: function () { },
        success: function (msg) {//On Successfull service call
            var OfficerList = '<option value="0">Select Officer</option>';
            var result = JSON.parse(msg);
            $.each(result, function (key, value) {
                OfficerList = OfficerList + '<option value="' + value.OfficerID + '">' + value.OfficerName + '</option>';
            });
            $('#selOfficer').html(OfficerList);
        },
        error: function () {
        } // When Service call fails
    });
}

function GetCaseHeaderSearch_SC() {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetCaseHeaderSearch_SC?CaseNumber=" + $('#txtCaseNumber').val() +
                          "&Postcode=" + $('#txtPostcode').val() +
                          "&VRM=" + $('#txtVRM').val() +
                          "&ManagerID=" + $('#selManager').val() +
                          "&ADMID=" + $('#selADM').val() +
                          "&OfficerID=" + $('#selOfficer').val(), // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: false,
        timeout: 20000,
        beforeSend: function () {
            $('#DCaseType,#DSpecialCase').show();
            $.loader({
                className: "blue-with-image",
                content: ''
            });
        },
        complete: function () {
            $.loader('close');
        },
        success: function (msg) {//On Successfull service call 
            var result = JSON.parse(msg);
            $('#tblSCCase').dataTable().fnClearTable();
            if (result != '') {
                $.each(result, function (key, value) {
                    //[{"Id":1,"caseNumber":"245856","ClientName":"Marston","DebtName":"DebtName1",
                    //    "ContactFirstLine":"ContactFirstLine","ContactAddress":"ContactAddress",
                    //    "ContactPostCode":"cm16","WarrantFirstLine":"WarrantFirstLine","WarrantAddress":"WarrantAddress",
                    //    "WarrantPostCode":"x18","CaseStatus":"Open","IssueDate":"\/Date(1337900400000)\/",
                    //    "OfficerID":114,"AssignedOn":"\/Date(1337900400000)\/","caseTypeId":"1","Brand":null,
                    //    "PreviousCaseNo":null,"CreatedDate":"\/Date(1480079239147)\/","ModifiedDate":null,"Category":null,
                    //    "CreatedBy":null},
                    //h.caseNumber, h.ClientName, h.ContactPostCode, h.OfficerID, v.VRM
                    $('#tbodySCCase').append('<tr><td>' + value.caseNumber + '</td>' +
                                                 '<td>' + value.ClientName + '</td>' +
                                                 '<td>' + value.ContactPostCode + '</td>' +
                                                 '<td>' + value.OfficerID + '</td>' +
                                                 '<td>' + value.VRM + '</td>' +
                                                 '<td><img class="imgEdit" src="images/Select_G_24_24.png" alt="View" title="view" style="cursor:pointer;padding-right:5px;" onclick="ViewCaseHeader(' + "'" + value.caseNumber + "'" + ')"/></td></tr>');
                });
                $('#tblSCCase').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "bLengthChange": false,
                    "bFilter": false,
                    "sInfo": true,
                    "iDisplayLength": 10
                });
            }
            else {
                $('#tblSCCase').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": false,
                    "bDestroy": true,
                    "bSort": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bInfo": false,
                    "iDisplayLength": 1
                });
            }
        },
        error: function () {
        }
    });
}

function ViewCaseHeader(CaseNo) {
    $('#pType,#pCaseNumber').show();
    $('#DSpecialCase,#DSearchPanel').hide();
    $('#lblPanelCaseno').html(CaseNo);
    CaseType(0);
}

function CaseType(Type) {
    $('#DCaseHeader,#DCaseDetail,#DCaseHistory,#DCaseNote,#DCaseOffence,#DCaseStatement,#DCaseVRM').hide();
    $('#LI0,#LI1,#LI2,#LI3,#LI4,#LI5,#LI6,#LI7').attr('style', 'background-color: #');

    CaseFlag = Type;
    switch (Type) {
        case 0://'Case Header':
            $('#LI0').attr('style', 'background-color: #613663;');
            $('#DCaseHeader').show();
            $('#pHeaderName').html('Case header list');
            GetCasenumberSearch_SC($.trim($('#lblPanelCaseno').html()), Type);
            break;
        case 1://'Case Detail':
            $('#LI1').attr('style', 'background-color: #613663;');
            $('#DCaseDetail').show();
            $('#pHeaderName').html('Case detail list');
            GetCasenumberSearch_SC($.trim($('#lblPanelCaseno').html()), Type);
            break;
        case 2:// 'Case History':
            $('#LI2').attr('style', 'background-color: #613663;');
            $('#DCaseHistory').show();
            $('#pHeaderName').html('Case history list');
            GetCasenumberSearch_SC($.trim($('#lblPanelCaseno').html()), Type);
            break;
        case 3://'Case Note':
            $('#LI3').attr('style', 'background-color: #613663;');
            $('#DCaseNote').show();
            $('#pHeaderName').html('Case note list');
            GetCasenumberSearch_SC($.trim($('#lblPanelCaseno').html()), Type);
            break;
        case 4: //'Case Offence Detail':
            $('#LI4').attr('style', 'background-color: #613663;');
            $('#DCaseOffence').show();
            $('#pHeaderName').html('Case offence detail list');
            GetCasenumberSearch_SC($.trim($('#lblPanelCaseno').html()), Type);
            break;
        case 5: //'Case Statement':
            $('#LI5').attr('style', 'background-color: #613663;');
            $('#DCaseStatement').show();
            $('#pHeaderName').html('Case statement list');
            GetCasenumberSearch_SC($.trim($('#lblPanelCaseno').html()), Type);
            break;
        case 6: // 'Case VRM Detail':
            $('#LI6').attr('style', 'background-color: #613663;');
            $('#DCaseVRM').show();
            $('#pHeaderName').html('Case vrm detail list');
            GetCasenumberSearch_SC($.trim($('#lblPanelCaseno').html()), Type);
            break;
        case 7: // back
            $('#LI7').attr('style', 'background-color: #613663;');
            $('#pHeaderName').html('Case  list');
            $('#DCaseHeader,#DCaseDetail,#DCaseHistory,#DCaseNote,#DCaseOffence,#DCaseStatement,#DCaseVRM').hide();
            $('#DSpecialCase,#DSearchPanel').show();
            $('#pType,#pCaseNumber').hide();
            // GetCaseHeader_SC();
            break;
    }
}

function GetCasenumberSearch_SC(CaseNo, Flag) {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetCasenumberSearch_SC?CaseNumber=" + CaseNo + "&Flag=" + Flag, // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: false,
        timeout: 20000,
        beforeSend: function () {
            $.loader({
                className: "blue-with-image",
                content: ''
            });
        },
        complete: function () {
            $.loader('close');
        },
        success: function (msg) {//On Successfull service call  
            var result = JSON.parse(msg);
            $('#tblCaseHeader').dataTable().fnClearTable();
            $('#tblCaseDetail').dataTable().fnClearTable();
            $('#tblCaseHistory').dataTable().fnClearTable();
            $('#tblCaseNote').dataTable().fnClearTable();
            $('#tblCaseOffence').dataTable().fnClearTable();
            $('#tblCaseStatement').dataTable().fnClearTable();
            $('#tblCaseVRM').dataTable().fnClearTable();
            if (result != '') {
                $.each(result, function (key, value) {
                    switch (Flag) {
                        case 0:
                            $('#tbodyCaseHeader').append('<tr><td>' + value.caseNumber + '</td>' +
                               '<td>' + value.ClientName + '</td>' +
                               '<td>' + value.DebtName + '</td>' +
                               '<td>' + value.OfficerID + '</td>' +
                               '<td>' + value.CaseStatus + '</td>' +
                               '<td style="display:none">' + value.Id + '</td>' +
                               '<td style="display:none">' + value.ContactFirstLine + '</td>' +
                               '<td style="display:none">' + value.ContactAddress + '</td>' +
                               '<td style="display:none">' + value.ContactPostCode + '</td>' +
                               '<td style="display:none">' + value.WarrantFirstLine + '</td>' +
                               '<td style="display:none">' + value.WarrantAddress + '</td>' +
                               '<td style="display:none">' + value.WarrantPostCode + '</td>' +
                               '<td style="display:none">' + value.IssueDate + '</td>' +
                               '<td style="display:none">' + value.AssignedOn + '</td>' +
                               '<td>' +
                               '<img class="imgEdit" src="images/Edit_24_24.png" alt="Edit" title="Edit" style="cursor:pointer;padding-right:5px;" onclick="UpdateCaseHeader()"/> ' +
                               '<img class="imgUnRegister" src="images/Delete_24_24.png" alt="UnRegister" title="Un Register" style="cursor:pointer;padding-right:5px;" onclick="UnRegister(0,' + $.trim(value.Id) + ',' + value.caseNumber + ')" /></td>' +
                               '</tr>');
                            break;
                        case 1:
                            //[{"Id":1,"caseNumber":"245856","OutstandingBalance":150.00,"OriginalBalance":270.00,
                            //    "expiresOn":"\/Date(1400972400000)\/","clientCaseReference":"114","ArrangementStartDate":null,
                            //    "ArrangementPaymentFrequency":null,"ArrangenentEndDate":null,"AssignableDate":null,
                            //    "CreatedDate":"\/Date(1480079239150)\/","ModifiedDate":null},
                            $('#tbodyCaseDetail').append('<tr><td>' + value.caseNumber + '</td>' +
                                     '<td>' + value.OutstandingBalance + '</td>' +
                                     '<td>' + value.OriginalBalance + '</td>' +
                                     '<td>' + value.expiresOn + '</td>' +
                                     '<td>' + value.clientCaseReference + '</td>' +
                                     '<td style="display:none;">' + value.Id + '</td>' +
                                     '<td><img class="imgEdit" src="images/Edit_24_24.png" alt="Edit" title="Edit" style="cursor:pointer;padding-right:5px;" onclick="UpadteCaseDetail()"/> </td></tr>');
                            //'<img class="imgUnRegister" src="images/Delete_24_24.png" alt="UnRegister" title="Un Register" style="cursor:pointer;padding-right:5px;" onclick="UnRegister(' + Flag + ',' + value.Id + ',' + value.caseNumber + ')" />
                            break;
                        case 2:
                            //[{"Id":1,"caseNumber":"245856","comment":"Comment for rsk 245856","occured":"\/Date(1337900400000)\/",
                            //"CreatedDate":"\/Date(1480079239153)\/","ModifiedDate":"\/Date(1480079239153)\/"},
                            $('#tbodyCaseHistory').append('<tr><td>' + value.caseNumber + '</td>' +
                                    '<td>' + value.comment + '</td>' +
                                    '<td>' + value.occured + '</td>' +
                                     '<td style="display:none;">' + value.Id + '</td>' +
                                    '<td>' +
                                    '<img class="imgEdit" src="images/Edit_24_24.png" alt="Edit" title="Edit" style="cursor:pointer;padding-right:5px;" onclick="CaseHistory(2)"/> ' +
                                    '<img class="imgUnRegister" src="images/Delete_24_24.png" alt="UnRegister" title="Un Register" style="cursor:pointer;padding-right:5px;" onclick="UnRegister(' + Flag + ',' + value.Id + ',' + value.caseNumber + ')" /></td>' +
                                    '</tr>');
                            break;
                        case 3:
                            //[{"Id":1,"caseNumber":"245856","ctext":"Comment for rsk 245856","occured":"\/Date(1337900400000)\/",
                            //    "CreatedDate":"\/Date(1480079239160)\/","ModifiedDate":"\/Date(1480079239160)\/"},
                            $('#tbodyCaseNote').append('<tr><td>' + value.caseNumber + '</td>' +
                                    '<td>' + value.ctext + '</td>' +
                                    '<td>' + value.occured + '</td>' +
                                    '<td style="display:none;">' + value.Id + '</td>' +
                                    '<td>' +
                                    '<img class="imgEdit" src="images/Edit_24_24.png" alt="Edit" title="Edit" style="cursor:pointer;padding-right:5px;" onclick="CaseNote(2)"/> ' +
                                    '<img class="imgUnRegister" src="images/Delete_24_24.png" alt="UnRegister" title="Un Register" style="cursor:pointer;padding-right:5px;" onclick="UnRegister(' + Flag + ',' + value.Id + ',' + value.caseNumber + ')" /></td>' +
                                    '</tr>');
                            break;
                        case 4:
                            //[{"Id":1,"caseNumber":"245856","offencedate":"\/Date(1337900400000)\/","offenceLocation":"offenceLocation",
                            //    "Notes":"Offence notes by rsk","description":"Offence description","CreatedDate":"\/Date(1480079239163)\/","ModifiedDate":"\/Date(1480079239163)\/"},
                            $('#tbodyCaseOffence').append('<tr><td>' + value.caseNumber + '</td>' +
                                   '<td>' + value.offencedate + '</td>' +
                                   '<td>' + value.offenceLocation + '</td>' +
                                   '<td>' + value.Notes + '</td>' +
                                   '<td>' + value.description + '</td>' +
                                   '<td style="display:none;">' + value.Id + '</td>' +
                                   '<td>' +
                                   '<img class="imgEdit" src="images/Edit_24_24.png" alt="Edit" title="Edit" style="cursor:pointer;padding-right:5px;" onclick="CaseOffence(2)"/> ' +
                                   '<img class="imgUnRegister" src="images/Delete_24_24.png" alt="UnRegister" title="Un Register" style="cursor:pointer;padding-right:5px;" onclick="UnRegister(' + Flag + ',' + value.Id + ',' + value.caseNumber + ')" /></td>' +
                                   '</tr>');
                            break;
                        case 5:
                            //[{"Id":1,"caseNumber":"245856","DATE":"\/Date(1337900400000)\/","Description":"Statement description",
                            //"Type":"1","Debit":10.00,"Credit":2.00,"Vat":5.00,"Total":17.00,"STATUS":"1","Balance":10.00,
                            //"CreatedDate":"\/Date(1480079239167)\/","ModifiedDate":"\/Date(1480079239167)\/"},
                            $('#tbodyCaseStatement').append('<tr><td>' + value.caseNumber + '</td>' +
                                   '<td>' + value.DATE + '</td>' +
                                   '<td>' + value.Description + '</td>' +
                                   '<td>' + value.Type + '</td>' +
                                   '<td>' + value.Debit + '</td>' +
                                   '<td>' + value.Credit + '</td>' +
                                   '<td>' + value.Vat + '</td>' +
                                   '<td>' + value.Total + '</td>' +
                                   '<td>' + value.STATUS + '</td>' +
                                   '<td>' + value.Balance + '</td>' +
                                   '<td style="display:none;">' + value.Id + '</td>' +
                                   '<td>' +
                                   '<img class="imgEdit" src="images/Edit_24_24.png" alt="Edit" title="Edit" style="cursor:pointer;padding-right:5px;" onclick="CaseStatement(2)"/> ' +
                                   '<img class="imgUnRegister" src="images/Delete_24_24.png" alt="UnRegister" title="Un Register" style="cursor:pointer;padding-right:5px;" onclick="UnRegister(' + Flag + ',' + value.Id + ',' + value.caseNumber + ')" /></td>' +
                                   '</tr>');
                            break;
                        case 6:
                            //[{"Id":1,"caseNumber":"245856","VRM":"sd45bvbv","ResponseStatus":"1",
                            //"ResponseDate":"\/Date(1337900400000)\/","VehicleMake":"Honda","VehicleModel":"XUV",
                            //"VehicleColour":"Black","KeeperDetails":"1","CreatedDate":"\/Date(1480079239173)\/",
                            //"ModifiedDate":"\/Date(1480079239173)\/"},
                            $('#tbodyCaseVRM').append('<tr><td>' + value.caseNumber + '</td>' +
                                '<td>' + value.VRM + '</td>' +
                                '<td>' + value.ResponseStatus + '</td>' +
                                '<td>' + value.ResponseDate + '</td>' +
                                '<td>' + value.VehicleMake + '</td>' +
                                '<td>' + value.VehicleModel + '</td>' +
                                '<td>' + value.VehicleColour + '</td>' +
                                '<td>' + value.KeeperDetails + '</td>' +
                                '<td style="display:none;">' + value.Id + '</td>' +
                                '<td>' +
                                '<img class="imgEdit" src="images/Edit_24_24.png" alt="Edit" title="Edit" style="cursor:pointer;padding-right:5px;" onclick="CaseVRM(2)"/> ' +
                                '<img class="imgUnRegister" src="images/Delete_24_24.png" alt="UnRegister" title="Un Register" style="cursor:pointer;padding-right:5px;" onclick="UnRegister(' + Flag + ',' + value.Id + ',' + value.caseNumber + ')" /></td>' +
                                '</tr>');
                            break;
                    }
                });
                $('#tblCaseHeader,#tblCaseDetail,#tblCaseHistory,#tblCaseNote,#tblCaseOffence,#tblCaseStatement,#tblCaseVRM').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": false,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "bLengthChange": false,
                    "bFilter": false,
                    "sInfo": true,
                    "iDisplayLength": 10
                });

                $("#tblCaseHeader").delegate("tbody tr", "click", function (key, event) {
                    // $('#lblCaseNumber,#lblPanelCaseno,#lblCHCaseNumber,#lblCNCaseNumber,#lblCOCaseNumber,#lblCSCaseNumber,#lblCVCaseNumber').html(this.cells[0].innerHTML);
                    $('#lblCaseNumber').html(this.cells[0].innerHTML);
                    $('#txtClientName').val(this.cells[1].innerHTML);
                    $('#txtDebtName').val(this.cells[2].innerHTML);
                    $('#lblOfficerID').html(this.cells[3].innerHTML);
                    $('#selCaseStatus option:selected').text(this.cells[4].innerHTML);
                    $('#lblCaseHeaderID').html(this.cells[5].innerHTML);
                    $('#txtContactFirstLine').val(this.cells[6].innerHTML);
                    $('#txtContactAddress').val(this.cells[7].innerHTML);
                    $('#txtContactPostCode').val(this.cells[8].innerHTML);
                    $('#txtWarrantFirstLine').val(this.cells[9].innerHTML);
                    $('#txtWarrantAddress').val(this.cells[10].innerHTML);
                    $('#txtWarrantPostCode').val(this.cells[11].innerHTML);
                    $('#txtIssueDate').val(this.cells[12].innerHTML);
                    $('#txtAssignedOn').val(this.cells[13].innerHTML);
                });

                $("#tblCaseDetail").delegate("tbody tr", "click", function (key, event) {
                    $('#lblCDCaseNumber').html(this.cells[0].innerHTML);
                    $('#txtOutstandingBalance').val(this.cells[1].innerHTML);
                    $('#lblCDoriginalBalance').html(this.cells[2].innerHTML);
                    $('#txtExpiresOn').val(this.cells[3].innerHTML);
                    $('#txtClientCaseReference').val(this.cells[4].innerHTML);
                    $('#lblCDID').html(this.cells[5].innerHTML);
                });

                $("#tblCaseHistory").delegate("tbody tr", "click", function (key, event) {
                    $('#lblCHCaseNumber').html(this.cells[0].innerHTML);
                    $('#txtCHComment').val(this.cells[1].innerHTML);
                    $('#txtCHOccured').val(this.cells[2].innerHTML);
                    $('#lblCHID').html(this.cells[3].innerHTML);
                });

                $("#tblCaseNote").delegate("tbody tr", "click", function (key, event) {
                    $('#lblCNCaseNumber').html(this.cells[0].innerHTML);
                    $('#txtCNText').val(this.cells[1].innerHTML);
                    $('#txtCNOccured').val(this.cells[2].innerHTML);
                    $('#lblCNID').html(this.cells[3].innerHTML);
                });

                $("#tblCaseOffence").delegate("tbody tr", "click", function (key, event) {
                    $('#lblCOCaseNumber').html(this.cells[0].innerHTML);
                    $('#txtCOOffenceDate').val(this.cells[1].innerHTML);
                    $('#txtCOoffenceLocation').val(this.cells[2].innerHTML);
                    $('#txtCONotes').val(this.cells[3].innerHTML);
                    $('#txtCOdescription').val(this.cells[4].innerHTML);
                    $('#lblCOID').html(this.cells[5].innerHTML);
                });

                $("#tblCaseStatement").delegate("tbody tr", "click", function (key, event) {
                    $('#lblCSCaseNumber').html(this.cells[0].innerHTML);
                    $('#txtCSDate').val(this.cells[1].innerHTML);
                    $('#txtCSDescription').val(this.cells[2].innerHTML);
                    $('#txtCSType').val(this.cells[3].innerHTML);
                    $('#txtCSDebit').val(this.cells[4].innerHTML);
                    $('#txtCSCredit').val(this.cells[5].innerHTML);
                    $('#txtCSVat').val(this.cells[6].innerHTML);
                    $('#txtCSTotal').val(this.cells[7].innerHTML);
                    $('#txtCSStatus').val(this.cells[8].innerHTML);
                    $('#txtCSBalance').val(this.cells[9].innerHTML);
                    $('#lblCSID').html(this.cells[10].innerHTML);
                });

                $("#tblCaseVRM").delegate("tbody tr", "click", function (key, event) {
                    $('#lblCVCaseNumber').html(this.cells[0].innerHTML);
                    $('#txtCVVrm').val(this.cells[1].innerHTML);
                    $('#txtCVResponseStatus').val(this.cells[2].innerHTML);
                    $('#txtCVResponseDate').val(this.cells[3].innerHTML);
                    $('#txtCVVehicleMake').val(this.cells[4].innerHTML);
                    $('#txtCVVehicleModel').val(this.cells[5].innerHTML);
                    $('#txtCVVehicleColour').val(this.cells[6].innerHTML);
                    $('#txtCVKeeperDetails').val(this.cells[7].innerHTML);
                    $('#lblCVID').html(this.cells[8].innerHTML);
                });

            }
            else {
                $('#tblCaseDetail,#tblCaseHistory,#tblCaseNote,#tblCaseOffence,#tblCaseStatement,#tblCaseVRM').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "bLengthChange": false,
                    "bFilter": false,
                    "sInfo": true,
                    "iDisplayLength": 10
                });
            }
        },
        error: function () {
        }
    });
}

function UpdateCaseHeader() {
    $('#DCHDialog').dialog({
        modal: true,
        resizable: false,
        width: "65%",
        buttons: {
            "Update": function () {
                $.ajax({
                    type: "GET",
                    url: serviceUrl + "/UpdateCaseHeader_SC?CaseNumber=" + $.trim($('#lblCaseNumber').html()) +
                            "&ClientName=" + $.trim($('#txtClientName').val()) + "&DebtName=" + $('#txtDebtName').val() +
                            "&ContactFirstLine=" + $.trim($('#txtContactFirstLine').val()) + "&ContactAddress=" + $('#txtContactAddress').val() +
                            "&ContactPostCode=" + $.trim($('#txtContactPostCode').val()) + "&WarrantFirstLine=" + $('#txtWarrantFirstLine').val() +
                            "&WarrantAddress=" + $.trim($('#txtWarrantAddress').val()) + "&WarrantPostCode=" + $('#txtWarrantPostCode').val() +
                            "&CaseStatus=" + $.trim($('#selCaseStatus').val()) + "&IssueDate=" + $('#txtIssueDate').val() +
                            "&OfficerID=" + $.trim($('#lblOfficerID').html()) + "&AssignedOn=" + $('#txtAssignedOn').val() +
                            "&Id=" + $.trim($('#lblCaseHeaderID').html()), // Location of the service
                    contentType: "application/javascript", // content type sent to server
                    dataType: "jsonp", //Expected data format from server       
                    processdata: false, //True or False      
                    async: false,
                    timeout: 20000,
                    beforeSend: function () {
                        $.loader({
                            className: "blue-with-image",
                            content: ''
                        });
                    },
                    complete: function () {
                        $.loader('close');
                        GetCasenumberSearch_SC($.trim($('#lblCaseNumber').html()), CaseFlag);
                    },
                    success: function (msg) {//On Successfull service call  
                        AlertMessage("Info", "Case header updated successfully!");
                    },
                    error: function () {
                    }
                });
                $(this).dialog("close");
            },
            "Cancel": function () {
                $('#DCHDialog :text').val('');
                $(this).dialog("close");
            }
        }
    });
}

function UpadteCaseDetail() {
    $('#DCDDialog').dialog({
        modal: true,
        resizable: false,
        width: "65%",
        buttons: {
            "Update": function () {
                $.ajax({
                    type: "GET",
                    url: serviceUrl + "/UpdateCaseDetail_SC?CaseNumber=" + $.trim($('#lblCDCaseNumber').html()) +
                            "&OutstandingBalance=" + $.trim($('#txtOutstandingBalance').val()) + "&OriginalBalance=" + $('#lblCDoriginalBalance').html() +
                            "&expiresOn=" + $.trim($('#txtExpiresOn').val()) + "&clientCaseReference=" + $('#txtClientCaseReference').val() +
                            "&Id=" + $.trim($('#lblCDID').html()), // Location of the service
                    contentType: "application/javascript", // content type sent to server
                    dataType: "jsonp", //Expected data format from server       
                    processdata: false, //True or False      
                    async: false,
                    timeout: 20000,
                    beforeSend: function () {
                        $.loader({
                            className: "blue-with-image",
                            content: ''
                        });
                    },
                    complete: function () {
                        $.loader('close');
                        GetCasenumberSearch_SC($.trim($('#lblCDCaseNumber').html()), CaseFlag);
                    },
                    success: function (msg) {//On Successfull service call  
                        AlertMessage("Info", "Case detail updated successfully!");
                    },
                    error: function () {
                    }
                });
                $(this).dialog("close");
            },
            "Cancel": function () {
                $('#DCDDialog :text').val('');
                $(this).dialog("close");
            }
        }
    });
}

function CaseHistory(Type) {
    var Button;
    if (Type == 1) {
        $('#DCHistoryDialog :text').val('');
        $('#lblCHID').html('0');
        $('#txtCHComment').val('');
        Button = "Add";
    }
    else
        Button = "Update";

    $('#DCHistoryDialog').dialog({
        modal: true,
        resizable: false,
        width: "65%",
        buttons: [{
            text: Button,
            click: function () {
                InsertUpdateCaseHistory();
                $(this).dialog("close");
            }
        },
            {
                text: "Cancel",
                click: function () {
                    $('#DCHDialog :text').val('');
                    $(this).dialog('close');
                }
            }]
    });
}

function InsertUpdateCaseHistory() {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/InsertUpdateCaseHistory_SC?CaseNumber=" + $.trim($('#lblCHCaseNumber').html()) +
            "&Comment=" + $.trim($('#txtCHComment').val()) + "&Occured=" + $('#txtCHOccured').val() +
            "&Id=" + $.trim($('#lblCHID').html()), // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: false,
        timeout: 20000,
        beforeSend: function () {
            $.loader({
                className: "blue-with-image",
                content: ''
            });
        },
        complete: function () {
            $.loader('close');
            GetCasenumberSearch_SC($.trim($('#lblCHCaseNumber').html()), CaseFlag);
        },
        success: function (msg) {//On Successfull service call  
            if ($.trim($('#lblCHID').html()) == '0')
                AlertMessage("Info", "Case history added successfully!");
            else
                AlertMessage("Info", "Case history updated successfully!");
        },
        error: function () {
        }
    });
}

function CaseNote(Type) {
    var Button;
    if (Type == 1) {
        $('#DCNDialog :text').val('');
        $('#lblCNID').html('0');
        $('#txtCNText').val('');
        Button = "Add";
    }
    else
        Button = "Update";

    $('#DCNDialog').dialog({
        modal: true,
        resizable: false,
        width: "65%",
        buttons: [{
            text: Button,
            click: function () {
                InsertUpdateCaseNote();
                $(this).dialog("close");
            }
        },
       {
           text: "Cancel",
           click: function () {
               $('#DCNDialog :text').val('');
               $(this).dialog('close');
           }
       }]
    });
}

function InsertUpdateCaseNote() {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/InsertUpdateCaseNote_SC?CaseNumber=" + $.trim($('#lblCNCaseNumber').html()) +
            "&Comment=" + $.trim($('#txtCNText').val()) + "&Occured=" + $('#txtCNOccured').val() +
            "&Id=" + $.trim($('#lblCNID').html()), // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: false,
        timeout: 20000,
        beforeSend: function () {
            $.loader({
                className: "blue-with-image",
                content: ''
            });
        },
        complete: function () {
            $.loader('close');
            GetCasenumberSearch_SC($.trim($('#lblCNCaseNumber').html()), CaseFlag);
        },
        success: function (msg) {//On Successfull service call  
            if ($.trim($('#lblCNID').html()) == '0')
                AlertMessage("Info", "Case note added successfully!");
            else
                AlertMessage("Info", "Case note updated successfully!");
        },
        error: function () {
        }
    });
}

function CaseOffence(Type) {
    var Button;
    if (Type == 1) {
        $('#DCODialog :text').val('');
        $('#lblCOID').html('0');
        $('#txtCONotes,#txtCOdescription').val('');
        Button = "Add";
    }
    else
        Button = "Update";

    $('#DCODialog').dialog({
        modal: true,
        resizable: false,
        width: "65%",
        buttons: [{
            text: Button,
            click: function () {
                InsertUpdateCaseOffence();
                $(this).dialog("close");
            }
        },
      {
          text: "Cancel",
          click: function () {
              $('#DCODialog :text').val('');
              $(this).dialog('close');
          }
      }]
    });
}

function InsertUpdateCaseOffence() {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/InsertUpdateCaseOffence_SC?CaseNumber=" + $.trim($('#lblCOCaseNumber').html()) +
            "&OffenceDate=" + $('#txtCOOffenceDate').val() + "&OffenceLocation=" + $.trim($('#txtCOoffenceLocation').val()) +
            "&Notes=" + $('#txtCONotes').val() + "&Description=" + $('#txtCOdescription').val() +
            "&Id=" + $.trim($('#lblCOID').html()), // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: false,
        timeout: 20000,
        beforeSend: function () {
            $.loader({
                className: "blue-with-image",
                content: ''
            });
        },
        complete: function () {
            $.loader('close');
            GetCasenumberSearch_SC($.trim($('#lblCOCaseNumber').html()), CaseFlag);
        },
        success: function (msg) {//On Successfull service call  
            if ($.trim($('#lblCOID').html()) == '0')
                AlertMessage("Info", "Case offence added successfully!");
            else
                AlertMessage("Info", "Case offence updated successfully!");
        },
        error: function () {
        }
    });
}

function CaseStatement(Type) {
    var Button;
    if (Type == 1) {
        $('#DCSDialog :text').val('');
        $('#lblCSID').html('0');
        Button = "Add";
    }
    else
        Button = "Update";

    $('#DCSDialog').dialog({
        modal: true,
        resizable: false,
        width: "65%",
        buttons: [{
            text: Button,
            click: function () {
                InsertUpdateCaseStatement();
                $(this).dialog("close");
            }
        },
      {
          text: "Cancel",
          click: function () {
              $('#DCSDialog :text').val('');
              $(this).dialog('close');
          }
      }]
    });
}

function InsertUpdateCaseStatement() {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/InsertUpdateCaseStatement_SC?CaseNumber=" + $.trim($('#lblCSCaseNumber').html()) +
            "&Date=" + $('#txtCSDate').val() + "&Description=" + $.trim($('#txtCSDescription').val()) +
            "&Type=" + $('#txtCSType').val() + "&Debit=" + $('#txtCSDebit').val() +
            "&Credit=" + $('#txtCSCredit').val() + "&Vat=" + $('#txtCSVat').val() +
            "&Total=" + $('#txtCSTotal').val() + "&Status=" + $('#txtCSStatus').val() +
            "&Balance=" + $('#txtCSBalance').val() + "&Id=" + $.trim($('#lblCSID').html()), // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: false,
        timeout: 20000,
        beforeSend: function () {
            $.loader({
                className: "blue-with-image",
                content: ''
            });
        },
        complete: function () {
            $.loader('close');
            GetCasenumberSearch_SC($.trim($('#lblCSCaseNumber').html()), CaseFlag);
        },
        success: function (msg) {//On Successfull service call  
            if ($.trim($('#lblCSID').html()) == '0')
                AlertMessage("Info", "Case statement added successfully!");
            else
                AlertMessage("Info", "Case statement updated successfully!");
        },
        error: function () {
        }
    });
}

function CaseVRM(Type) {
    var Button;
    if (Type == 1) {
        $('#DCVDialog :text').val('');
        $('#lblCVID').html('0');
        Button = "Add";
    }
    else
        Button = "Update";

    $('#DCVDialog').dialog({
        modal: true,
        resizable: false,
        width: "65%",
        buttons: [{
            text: Button,
            click: function () {
                InsertUpdateCaseVRM();
                $(this).dialog("close");
            }
        },
      {
          text: "Cancel",
          click: function () {
              $('#DCVDialog :text').val('');
              $(this).dialog('close');
          }
      }]
    });
}

function InsertUpdateCaseVRM() {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/InsertUpdateCaseVRM_SC?CaseNumber=" + $.trim($('#lblCVCaseNumber').html()) +
            "&Vrm=" + $('#txtCVVrm').val() + "&ResponseStatus=" + $.trim($('#txtCVResponseStatus').val()) +
            "&ResponseDate=" + $('#txtCVResponseDate').val() + "&VehicleMake=" + $('#txtCVVehicleMake').val() +
            "&VehicleModel=" + $('#txtCVVehicleModel').val() + "&VehicleColour=" + $('#txtCVVehicleColour').val() +
            "&KeeperDetails=" + $('#txtCVKeeperDetails').val() + "&Id=" + $.trim($('#lblCVID').html()), // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: false,
        timeout: 20000,
        beforeSend: function () {
            $.loader({
                className: "blue-with-image",
                content: ''
            });
        },
        complete: function () {
            $.loader('close');
            GetCasenumberSearch_SC($.trim($('#lblCVCaseNumber').html()), CaseFlag);
        },
        success: function (msg) {//On Successfull service call  
            if ($.trim($('#lblCVID').html()) == '0')
                AlertMessage("Info", "Case vrm added successfully!");
            else
                AlertMessage("Info", "Case vrm updated successfully!");
        },
        error: function () {
        }
    });
}

function UnRegister(Flag, CaseID, CaseNumber) {
    var Title;
    switch (Flag) {
        case 0:
            Title = 'Case header';
            break;
        case 1:
            Title = 'Case detail';
            break;
        case 2:
            Title = 'Case history';
            break;
        case 3:
            Title = 'Case note';
            break;
        case 4:
            Title = 'Case offence detail';
            break;
        case 5:
            Title = 'Case statement';
            break;
        case 6:
            Title = 'Case vrm detail';
            break;
    }
    $('#DAlert').html('Are you sure to delete this case request <b>' + CaseNumber + '</b>?');
    $('#DAlert').dialog({
        modal: true,
        resizable: false,
        width: "30%",
        "title": Title,
        buttons: {
            "Yes": function () {
                $.ajax({
                    type: "GET",
                    url: serviceUrl + "/DeleteCase_SC?ID=" + CaseID + "&Flag=" + Flag, // Location of the service
                    contentType: "application/javascript", // content type sent to server
                    dataType: "jsonp", //Expected data format from server       
                    processdata: false, //True or False      
                    async: true,
                    timeout: 20000,
                    beforeSend: function () { },
                    complete: function () {
                        AlertMessage("Info", "Case request removed successfully!");
                    },
                    success: function (msg) {//On Successfull service call  
                        if (Flag == 0)
                            GetCaseHeader_SC();
                        else
                            GetCasenumberSearch_SC(CaseNumber, Flag);
                    },
                    error: function () { }
                });
                $('#DAlert').dialog('close');
            },
            "No": function () {
                $('#DAlert').dialog('close');
            }
        }
    });
}






