﻿//==========================================================================================================================================
/* File Name: CaseUpload.js */
/* File Created: Aug 16, 2016 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================
var CFlag = false;
$(document).ready(function () {
    if ($.trim($.session.get('OfficerID')).length > 0) {
    }
    else {
        window.location.href = HomeUrl;
    }
    $(':text').val('');
    $('#lblLoginOfficer').html($.session.get('OfficerName'));

    $('#btnUpload').click(function () {
        GetCaseUpload();
    });

    $('#btnClear').click(function () {
        $('#fileInput').val('');
        $('#DErrorMsg,#btnClear').hide();
    });

    $('#btnDownloadSCcase').click(function () {
        GetCaseDownloadSC();
    });

    $("#fileInput").change(function () {
        var fileExtension = ['xlsx', 'xls'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Allowed formats are : " + fileExtension.join(', '));
            $("#fileInput").val('');
        }
    });
});

function GetCaseTemplateDownload() {
    window.location.href = CaseTemplateDownload;
}

function GetCaseUpload() {
    var formData = new FormData();
    formData.append('userfile', $('#fileInput')[0].files[0]);
    if (!($('#fileInput').val() == "" || $('#fileInput').val() == null)) {
        $.ajax({
            type: 'POST',
            url: ServiceURL + 'api/v1/cases/CaseUpload',
            headers: {
                "Authorization": $.session.get('TokenAuthorization')
            },
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $('#tbodyGridView').empty();
                $.loader({
                    className: "blue-with-image",
                    content: ''
                });
                if (CFlag == true) {
                    $('#tblGridView').DataTable().destroy();
                }
            },
            success: function (data) {
                if (!(data == '' || data == null || data == '[]')) {
                    CFlag = true;
                    $('#DErrorMsg,#btnClear').show();
                    $('#tbodyGridView').empty();
                    $.each(data, function (key, value) {
                        $('#tbodyGridView').append('<tr><td><label id="lblErrorType' + key + '" style="font-weight:normal; color:red;">' + value.ErrorType + '</label></td>' +
                                                        '<td>' + value.RowNo + '</td>' +
                                                        '<td>' + value.ErrorMessage + '</td>' +
                                                        '<td>' + value.SheetName + '</td></tr>');
                    });

                    $('#tblGridView').DataTable({
                        "sScrollY": "auto",
                        "bFilter": false,
                        "bPaginate": true,
                        "bDestroy": true,
                        "bSort": false,
                        "sPaginationType": "full_numbers",
                        "bLengthChange": false,
                        "sInfo": true,
                        "iDisplayLength": 5,
                        dom: 'Bfrtip',
                        buttons: [
                            //'copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5'
                            'excelHtml5'
                        ]
                    });
                }
                else {
                    //$('#tblGridView').DataTable({
                    //    "sScrollY": "auto",
                    //    "bPaginate": true,
                    //    "bDestroy": true,
                    //    "bSort": false,
                    //    "sPaginationType": "full_numbers",
                    //    "bLengthChange": false,
                    //    "bFilter": false,
                    //    "sInfo": true,
                    //    "iDisplayLength": 5
                    //});
                    $('#DErrorMsg,#btnClear').hide();
                    AlertMessage('Info!', 'Case uploaded successfully');
                }
            },
            complete: function () {
                $.loader('close');
            },
            error: function (xhr, textStatus, errorThrown) {
                $.loader('close');
            }
        });
    }
    else {
        AlertMessage('Info!', 'Please upload case');
    }
}

function GetCaseDownloadSC() {
    var l_curdate = new Date();
    var l_date = l_curdate.getFullYear() + zeropadded(l_curdate.getMonth() + 1) + zeropadded(l_curdate.getDate()) + zeropadded(l_curdate.getHours()) + zeropadded(l_curdate.getMinutes()) + zeropadded(l_curdate.getSeconds());

    $.loader({
        className: "blue-with-image",
        content: ''
    });
    var url = ServiceURL + 'api/v1/GetCaseDownloadSC/Excel';
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'blob';
    xhr.setRequestHeader('Authorization', $.session.get('TokenAuthorization'));
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.onload = function (e) {
        if (this.status == 200) {
            var blob = new Blob([this.response], { type: 'application/vnd.ms-excel' });
            var downloadUrl = URL.createObjectURL(blob);
            var a = document.createElement("a");
            a.href = downloadUrl;
            a.download = "SpecialCases_" + l_date + ".xlsx";
            document.body.appendChild(a);
            a.click();
            $.loader('close');
        } else if (this.status == 401) {
            AlertMessage('Info!', 'Invalid token');
            $.loader('close');
        } else {
            AlertMessage('Info!', 'Unable to download excel.');
            $.loader('close');
        }
    };
    xhr.send();
}

