﻿/*
Created By  : R.Santhakumar
Created Date: 2-12-2016
Description : Manage case return
*/

var mID;
$(document).ready(function () {
    if ($.trim($.session.get('OfficerID')).length > 0) {
    }
    else {
        window.location.href = HomeUrl;
    }
    mID = $.session.get('OfficerID');
    GetManager(mID);
    GetReturnCodeList_ES();
    GetClientListForCR_ES();

    $('#btnGetPause').click(function () {
        GetManagerPauseDetail_ES();
    });

    $('#DPauseForm select').attr('style', 'width:160px;');

    $('#btnClearPause').click(function () {
        $('#DPauseForm select').prop('selectedIndex', 0);
        $('#DPause').hide();
    });

    $('#btnPauseRecord').click(function () {
        var id = '';
        $('[name=PauseAll]:checked').each(function (key, value) {
            var Data = $(this).val();
            if ($.isEmptyObject(id)) {
                id += Data;
            }
            else {
                id += ',' + Data;
            }
        });
        if (!(id == '' || id == undefined || id == null)) {
            UpdatePauseRequest_ES(id);
        }
        else {
            AlertMessage('Info', 'Select any one request to pause?');
        }
    });
});


function GetManager(ManagerID) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/managers/' + ManagerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#selPauseManager').empty();
        },
        success: function (data) {
            var ManagerList = '<option value="0">Select Manager</option>';
            $.each(data, function (key, value) {
                if (key == 0)
                    GetADMList(0);

                ManagerList = ManagerList + '<option value="' + $.trim(value.OfficerID) + '">' + value.OfficerName + '</option>';
            });
            $('#selPauseManager').html(ManagerList);

            $('#selPauseManager').change(function () {
                GetADMList($.trim($('#selPauseManager').val()));
            });
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function GetADMList(MgrID) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + MgrID + '/ADMList',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#selPauseADM').empty();
        },
        success: function (data) {
            var ADMList = '<option value="0">Select ADM</option>';
            if (MgrID == 0) {
                GetOfficerList(0);
            }
            $.each(data, function (key, value) {
                //if (key == 0)
                //    GetOfficerList(0);
                ADMList = ADMList + '<option value="' + $.trim(value.ADMID) + '">' + value.ADMName + '</option>';
            });
            $('#selPauseADM').html(ADMList);
            $('#selPauseADM').change(function () {
                GetOfficerList($.trim($('#selPauseADM').val()));
            });
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

function GetOfficerList(ADMID) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + ADMID + '/OfficerList',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $("#selPauseOfficer").empty();
        },
        success: function (data) {
            var OfficerList = '<option value="0">Select Officer</option>';
            $.each(data, function (key, value) {
                OfficerList = OfficerList + '<option value="' + $.trim(value.OfficerID) + '">' + value.OfficerName + '</option>';
            });
            $('#selPauseOfficer').html(OfficerList);
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function GetReturnCodeList_ES() {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/ReturnCodes',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#selPauseReturncode').empty();
        },
        success: function (data) {
            var ReturncodeList = '<option value="0">Select Returncode</option>';
            if (!(data == '' || data == null || data == '[]')) {
                $.each(data, function (key, value) {
                    ReturncodeList = ReturncodeList + '<option value="' + $.trim(value) + '">' + value + '</option>';
                });
            }
            $('#selPauseReturncode').html(ReturncodeList);
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

function GetClientListForCR_ES() {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/ClientList',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#selPauseClient').empty();
        },
        success: function (data) {
            var ClientNameList = '<option>Select Client</option>';
            if (!(data == '' || data == null || data == '[]')) {
                $.each(data, function (key, value) {
                    ClientNameList = ClientNameList + '<option>' + value + '</option>';
                });
            }
            $('#selPauseClient').html(ClientNameList);
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

//======================================

function GetManagerPauseDetail_ES() {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/ManagerPause?ManagerID=' + $('#selPauseManager').val() + '&ADMID=' + $('#selPauseADM').val() +
            '&OfficerID=' + $('#selPauseOfficer').val() + '&ReturnCode=' + $('#selPauseReturncode').val() + '&ClientName=' + ($('#selPauseClient option:selected').text() != 'Select Client' ? $('#selPauseClient option:selected').text() : '0'),
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        //data: {
        //    "ManagerID": $('#selPauseManager').val(),
        //    "ADMID": $('#selPauseADM').val(),
        //    "OfficerID": $('#selPauseOfficer').val(),
        //    "ReturnCode": $('#selPauseReturncode').val(),
        //    "ClientName": $('#selPauseClient option:selected').text()
        //},
        beforeSend: function () {
            $('#DPause').show();
            $.loader({
                className: "blue-with-image",
                content: ' '
            });
        },
        success: function (data) {
            if (data != undefined) {
                if (!(data == "[]" || data == null || data == "")) {
                    $('#btnPauseRecord').show();
                    $('#tbodyManagerPause').empty();
                    $.each(data, function (key, value) {
                        $('#tbodyManagerPause').append('<tr><td id="tdCheckList' + key + '" class="Checklist">' +
                                                    '<input type="checkbox" name="PauseAll" class="action" id="Chk' + key + '" value="' + value.TicketrefId + '" ></td>' +
                                                '<td>' + value.TicketNumber + '</td><td>' + value.CreatedDate + '</td>' +
                                                '<td>' + value.CaseNumber + '</td><td>' + value.Officer + '</td>' +
                                                '<td>' + value.ClientName + '</td><td>' + value.returncode + '</td>' +
                                                '</tr>');
                    });
                }
                else {
                    $('#tbodyManagerPause').empty().append('<tr><td colspan="7" align="center">No data found.</td></tr>');
                    $('#btnPauseRecord').hide();
                }
            }
        },
        complete: function () {
            $.loader('close');
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function CheckboxSelectAll() {
    if (!$('#chkAll').is(':checked'))
        $('.action').prop('checked', false);
    else
        $('.action').prop('checked', 'checked');
}

function UpdatePauseRequest_ES(TicketRefID) {
    $('#DPauseAlert').html('Are you sure you want to pause these requests?');
    $('#DPauseAlert').dialog({
        modal: true,
        resizeable: false,
        width: "25%",
        "title": "Confirm!",
        buttons: {
            "Yes": function () {
                $.ajax({
                    type: 'PUT',
                    url: ServiceURL + 'api/v1/officers/' + TicketRefID + '/update/' + $.session.get('OfficerID') + '/PauseRequest',
                    headers: {
                        "Authorization": $.session.get('TokenAuthorization'),
                        'Content-Type': 'application/json'
                    },
                    dataType: 'json',
                    beforeSend: function () {
                    },
                    success: function (data) {
                        GetManagerPauseDetail_ES();
                    },
                    complete: function () {
                        $('#DPauseAlert').dialog("close");
                        AlertMessage('Info', 'Request has been paused successfully!');
                    },
                    error: function (xhr, textStatus, errorThrown) {
                    }
                });
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });
}