﻿//==========================================================================================================================================
/* File Name: ClampedImage.js */
/* File Created: oct 4, 2016 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================
$(document).ready(function () {
    if ($.trim($.session.get('OfficerID')).length > 0) {
    }
    else {
        window.location.href = HomeUrl;
    }
    $('#txtFromDate,#txtToDate').attr('readonly', 'readonly');

    $('#txtFromDate').datetimepicker({
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('#txtToDate').val() ? $('#txtToDate').val() : false
            })
        },
        timepicker: false
    });
    $('#txtToDate').datetimepicker({
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('#txtFromDate').val() ? $('#txtFromDate').val() : false
            })
        },
        timepicker: false
    });

    $('#btnSearchClamp').click(function () {
        $('#DClampedImage').show();
        GetClampedImageSearchForAdmin();
    });

    $('#btnClearClamp').click(function () {
        $(':text').val('');
        $('#DClampedImage').hide();
    });
});

function GetClampedImageSearchForAdmin() {
    var Parameter = "";
    var FromDate, ToDate = "";
    FromDate = $.trim($('#txtFromDate').val().replace(/[^0-9\/]/g, ''));
    if (FromDate.length == 10) {
        var dateF = FromDate.split('/');
        FromDate = dateF[2] + '-' + dateF[1] + '-' + dateF[0];
    }
    else {
        FromDate = "";
    }

    ToDate = $.trim($('#txtToDate').val().replace(/[^0-9\/]/g, ''));
    if (ToDate.length == 10) {
        var dateT = ToDate.split('/');
        ToDate = dateT[2] + '-' + dateT[1] + '-' + dateT[0];
    }
    else {
        ToDate = "";
    }

    if ($.trim($('#txtCaseNumber').val().replace(/[^a-zA-Z0-9]/g, '')) != "") {
        Parameter = 'CaseNumber=' + $('#txtCaseNumber').val().replace(/[^a-zA-Z0-9]/g, '');
    }

    if ($.trim($('#txtOfficerID').val().replace(/[^a-zA-Z0-9]/g, '')) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'OfficerID=' + $('#txtOfficerID').val().replace(/[^a-zA-Z0-9]/g, '');
        }
        else
            Parameter = 'OfficerID=' + $('#txtOfficerID').val().replace(/[^a-zA-Z0-9]/g, '');
    }

    if ($.trim($('#txtVRM').val().replace(/[^a-zA-Z0-9]/g, '')) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'VRM=' + $('#txtVRM').val().replace(/[^a-zA-Z0-9]/g, '');
        }
        else
            Parameter = 'VRM=' + $('#txtVRM').val().replace(/[^a-zA-Z0-9]/g, '');
    }

    if (FromDate != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'FromDate=' + FromDate;
        }
        else
            Parameter = 'FromDate=' + FromDate;
    }

    if (ToDate != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'ToDate=' + ToDate;
        }
        else
            Parameter = 'ToDate=' + ToDate;
    }

    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/ClampedImageSearch?' + Parameter,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#tbodyClamped').empty();
            $('#tblClamped').dataTable().fnClearTable();
            $.loader({
                className: "blue-with-image",
                content: ' '
            });
        },
        success: function (data) {
            if (data != '') {
                $('#tdClampImage').show();
                $.each(data, function (key, value) {
                    $('#tbodyClamped').append('<tr><td>' + value.CaseNumber + '</td>' +
                                                  '<td>' + value.OfficerName + ' - ' + value.Officer + '</td>' +
                                                  '<td>' + value.VRMNo + '</td>' +
                                                  '<td>' + value.DateActioned + '</td>' +
                                                  '<td>' +
                                                  '<a href="' + OptimiseImageUrl + '" target="_blank" ><img style="padding-right:10px;" src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" onclick="ExpandClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ',' + "'" + value.VRMNo + "'" + ')"/></a>' +
                                                  '<img id="imgClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="clampUnselect" alt="View" title="View clamped image" style="cursor:pointer;padding-right:10px;"  onclick="ViewClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ',' + "'" + value.VRMNo + "'" + ')" />' +
                                                  '</td></tr>');
                    if (key == 0) {
                        ViewClampedImage(key, value.Officer, value.CaseNumber, value.VRMNo);
                        $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'clampSelect');
                    }
                });
                $('#tblClamped').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "aLengthMenu": [[5, 10, 50, 100, -1], [5, 10, 50, 100, "All"]],
                    "bLengthChange": false,
                    "bFilter": false,
                    "sInfo": true,
                    "iDisplayLength": 5
                });
            }
            else {
                $('#tdClampImage').hide();
                $('#tblClamped').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": false,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "aLengthMenu": [[5, 10, 50, 100, -1], [5, 10, 50, 100, "All"]],
                    "bLengthChange": false,
                    "bFilter": false,
                    "sInfo": true,
                    "iDisplayLength": 5
                });
            }
        },
        complete: function () {
            $.loader('close');
        },
        error: function (jqxhr, settings, ex) {
        }
    });
}

function ViewClampedImage(key, OfficerID, CaseNo, VrmNo) {
    $.session.set('AdminImageType', 'C');
    $.session.set('AdminClampCaseNo', CaseNo);
    $.session.set('AdminClampOfficerID', OfficerID);
    $.session.set('AdminClampVRMNo', VrmNo);

    $('#iframeClampImage').attr('src', OptimiseImageUrl);

    $('.clampSelect').removeClass('clampSelect').addClass('clampUnselect').attr('src', 'images/ViewImage_24_24_2.png');

    if ($('#imgClampImage' + key).attr('src') == 'images/ViewImage_24_24_2.png')
        $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'clampSelect');
    else
        $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_2.png').attr('class', 'clampUnselect');
}


function ExpandClampedImage(key, OfficerID, CaseNo, VrmNo) {
    $.session.set('AdminImageType', 'C');
    $.session.set('AdminClampCaseNo', CaseNo);
    $.session.set('AdminClampOfficerID', OfficerID);
    $.session.set('AdminClampVRMNo', VrmNo);

    $('#iframeClampImage').attr('src', OptimiseImageUrl);

    $('.clampSelect').removeClass('clampSelect').addClass('clampUnselect').attr('src', 'images/ViewImage_24_24_2.png');

    if ($('#imgClampImage' + key).attr('src') == 'images/ViewImage_24_24_2.png')
        $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'clampSelect');
    else
        $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_2.png').attr('class', 'clampUnselect');
}