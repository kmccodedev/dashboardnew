
//==========================================================================================================================================
/* File Name: AssignRequest.js */
/* File Created: Sep 21, 2015 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================

$(document).ready(function () {
    if ($.trim($.session.get('OfficerID')).length > 0) {
    }
    else {
        window.location.href = HomeUrl;
    }
    $(':text').val('');
    $('#txtFromDate,#txtToDate').attr('readonly', 'readonly');

    $('#txtFromDate').datetimepicker({
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('#txtToDate').val() ? $('#txtToDate').val() : false
            })
        },
        timepicker: false
    });

    $('#txtToDate').datetimepicker({
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('#txtFromDate').val() ? $('#txtFromDate').val() : false
            })
        },
        timepicker: false
    });


    $('#chkAll').prop('checked', false);
    GetHPICheckRequestListForAdmin();

    $('#btnProcess').click(function () {
        var id = '';
        $('[name=HPICheckAll]:checked').each(function (key, value) {
            var Data = $(this).val();

            if ($.isEmptyObject(id)) {
                id += Data;
            }
            else {
                id += ',' + Data;
            }
        });
        if (!(id == '' || id == undefined || id == null)) {
            UpdateHPICheckRequestMessageForAdmin(id);
        }
        else {
            alert('Select any one HPI Check request!');
        }
    });

    $('#btnFindAction').click(function () {
        GetHPICheckRequestListForAdmin();
    });

});

function GetHPICheckRequestListForAdmin() {

    var Parameter = "";
    if ($.trim($('#txtFromDate').val()) != "") {
        var dateF = $('#txtFromDate').val().split('/');
        var FromDate = dateF[2] + '-' + dateF[1] + '-' + dateF[0];
    }
    if ($.trim($('#txtToDate').val()) != "") {
        var dateT = $('#txtToDate').val().split('/');
        var ToDate = dateT[2] + '-' + dateT[1] + '-' + dateT[0];
    }

    if ($.trim($('#txtOfficerID').val()) != "") {
        Parameter = 'OfficerId=' + $('#txtOfficerID').val();
    }

    if ($.trim($('#txtCaseNo').val()) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'CaseNo=' + $('#txtCaseNo').val();
        }
        else
            Parameter = 'CaseNo=' + $('#txtCaseNo').val();
    }

    if ($.trim($('#txtVRMNo').val()) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'VrmNo=' + $('#txtVRMNo').val();
        }
        else
            Parameter = 'VrmNo=' + $('#txtVRMNo').val();
    }

    if ($.trim($('#txtFromDate').val()) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'FromDate=' + FromDate;
        }
        else
            Parameter = 'FromDate=' + FromDate;
    }

    if ($.trim($('#txtToDate').val()) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'ToDate=' + ToDate;
        }
        else
            Parameter = 'ToDate=' + ToDate;
    }
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/HPICheckRequestList?' + Parameter,
        //+ ($('#txtOfficerID').val() !="" ? ('OfficerId=' + $('#txtOfficerID').val()) : + '&CaseNo=' + $('#txtCaseNo').val() +
        //'&VrmNo=' + $('#txtVRMNo').val() + '&FromDate=' + FromDate + '&ToDate=' + ToDate,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $.loader({
                className: "blue-with-image",
                content: ''
            });
        },
        success: function (data) {
            $('#HPICheckTBody').empty();
            if (!(data == '' || data == null || data == '[]')) {
                $('#btnProcess').show();
                $.each(data, function (key, value) {
                    $('#HPICheckTBody').append('<tr>' +
                                          '<td id="tdCheckList' + key + '" class="Checklist">' +
                                               '<input type="checkbox" name="HPICheckAll" class="action" id="Chk' + key + '" value="' + value.Id + '" ></td>' +
                                          '<td id="tdOfficer' + key + '" >' + value.Officer + '</td>' +
                                          '<td>' + value.CaseNumber + '</td>' +
                                          '<td>' + value.VRMNo + '</td>' +
                                          '<td>' + value.RequestedDate + '</td>' +
                                          '</tr>');
                });
            }
            else {
                $('#btnProcess').hide();
                $('#HPICheckTBody').append('<tr><td colspan="5" align="center">No data found...</td></tr>');
            }
        },
        complete: function () { $.loader('close'); },
        error: function (xhr, textStatus, errorThrown) {
            // alert('Error');
        }
    });
}

function UpdateHPICheckRequestMessageForAdmin(HPID) {
    $.ajax({
        type: 'PUT',
        url: ServiceURL + 'api/v1/officers/' + HPID + '/' + $.session.get('ManagerID') + '/' + ($('#txtResult').val() == "" ? 'Unknown' : $('#txtResult').val()) + '/UpdateHPICheckRequest',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: DataType,
        beforeSend: function () {
        },
        success: function (data) {
            if (data == true) {
                GetHPICheckRequestListForAdmin();
            }
        },
        complete: function () {
            $('#txtResult').val('');
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function CheckboxSelectAll() {
    if (!$('#chkAll').is(':checked')) {
        $('.action').prop('checked', false);
    }
    else {
        $('.action').prop('checked', 'checked');
    }
}
