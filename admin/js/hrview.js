﻿var serviceUrl = "http://intranet.atom8itsolutions.com:8196/OAService/MarstonOfficeToolService.svc";
var RedirectUrl = "http://intranet.atom8itsolutions.com:8196/TestOAM/sign-in.html";

$(document).ready(function () {

    $(':text').val('');
    $('#lblLoginOfficer').html(getCookie('OfficerName'));

    $('#btnSearch').click(function () {
        GetOfficerLogViewforHR();
    });
    $('#txtToDate,#txtFromDate').attr('readonly', 'readonly');

    $('#txtToDate').datetimepicker({
        timepicker: false,
        format: 'Y-m-d',
        maxDate: '+1970/01/02'
    }).datepicker("setDate", new Date());

    $('#txtFromDate').datetimepicker({
        timepicker: false,
        format: 'Y-m-d',
        maxDate: '+1970/01/02'
    }).datepicker("setDate", new Date());

    GetOfficerLogViewforHR();

    $('#btnClearOfficer').click(function () {
        $(':text').val('');
    });

    $('#ALogout').click(function () {
        $('#DivLogout').dialog({
            modal: true,
            resizable: false,
            width: "22%",
            "title": "Confirm",
            buttons: {
                "Yes": function () {
                    DeleteCookie('OfficerID');
                    DeleteCookie('OfficerName');
                    DeleteCookie('ManagerID');
                    DeleteCookie('pin');
                    DeleteCookie('lnkfrmgr');
                    DeleteCookie('tgVal');
                    DeleteCookie('aref');
                    DeleteCookie('bc');
                    DeleteCookie('RoleType');
                    DeleteCookie('CompanyID');
                    window.top.location.href = RedirectUrl;
                    $(this).dialog("close");
                },
                "No": function () {
                    $(this).dialog("close");
                }
            }
        });
    });
});

function GetOfficerLogViewforHR() {

    var Fdate = $.trim($('#txtFromDate').val()) == '' ? '1/1/1900' : $.trim($('#txtFromDate').val());
    var Tdate = $.trim($('#txtToDate').val()) == '' ? '1/1/1900' : $.trim($('#txtToDate').val());
    var oID = $.trim($('#txtOfficerNo').val()) == '' ? 0 : $.trim($('#txtOfficerNo').val());
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetOfficerLogViewforHR?OfficerID=" + oID + "&FromDate=" + Fdate + "&ToDate=" + Tdate, // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (msg) {//On Successfull service call  
            $('#tblOfficerLogRecords').dataTable().fnClearTable();
            var result = JSON.parse(msg);
            var cls;
            if (result != '') {
                $.each(result, function (key, value) {
                    $('#tbodyOfficerLogRecords').append($('<tr>').append($('<td>').html(value.OfficerID)).append($('<td>').html(value.OfficerName))
                                                                 .append($('<td>').html(value.ActionType)).append($('<td>').html(value.Comments))
                                                                 .append($('<td>').html(value.ManagerName)).append($('<td>').html(value.ActionDoneDate)))
                });

                $('#lblNoofrecords').html($('#tblOfficerLogRecords tbody tr').size() - 1);

                $('#tblOfficerLogRecords').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "bLengthChange": false,
                    "bFilter": false,
                    "sInfo": true,
                    "iDisplayLength": 10
                });
            }
            else {
                $('#tblOfficerLogRecords').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": false,
                    "bDestroy": true,
                    "bSort": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bInfo": false,
                    "iDisplayLength": 1
                });
            }
        },
        error: function () {

        }
    });
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}

function DeleteCookie(name) {
    document.cookie = name + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
}

