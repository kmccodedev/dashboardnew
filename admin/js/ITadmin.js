﻿
var selectedGroupID;
var selectedManagerID;
var selectedOfficerID;
var returntype;
var mID;
var OfficerType;
var OfficerRole;
var Certificate;
var Role;
var selectedADM = '';
var TreeLevel = 3;

var center;
var options;
var markers = [];
var map;
var latLng;
var marker;
var markerCluster;
var infowindow;
var mcOptions;
var Latitude = [];
var Longitude = [];
var Icon = [];
var Content = [];
var pLatitude = 0;
var pLongitude = 0;
var pContentkey;
var pLatMap = 0;
var pLongMap = 0;

//$(window).load(function () {
//    var phones = [{ "mask": "+44 (###) ###-####" }, { "mask": "+44 (###) ###-####" }];
//    $('#txtMobile').inputmask({
//        mask: phones,
//        greedy: false,
//        definitions: { '#': { validator: "[0-9]", cardinality: 1 } }
//    });
//});

$(document).ready(function () {
    if ($.trim($.session.get('OfficerID')).length > 0) {
    }
    else {
        window.location.href = HomeUrl;
    }
    if ($.session.get('RoleType') == 5)
        mID = 9999;
    else
        mID = $.session.get('OfficerID');
    $('#txtOfficerID,#txtOfficerName,#txtDateRegistered').val('');
    GetManagers();
    GetITAdminList();

    $('input[name=Type][value=' + TreeLevel + ']').prop('checked', true);

    $('input[type=radio][name=Type]').change(function () {
        TreeLevel = this.value;
        if (this.value == 2) {
            $('#btnCreatenewuser').val('Create new ADM');
            $('#lblOfficerID').html('ADM ID');
            $('#lblOfficerName').html('ADM Name');
        }
        else {
            $('#btnCreatenewuser').val('Create new officer');
            $('#lblOfficerID').html('Officer ID');
            $('#lblOfficerName').html('Officer Name');
        }
        GetOfficerListforGridView();
    });

    $('#btnCreatenewuser').click(function () {
        chkInputFields();
        $(':text,:password').val('');
        $('#btnPasswordReset').hide();
        $('#txtID').attr('disabled', false);
        $("#chkANPR").prop("checked", "");

        $('#txtName,#txtID,#txtPwd,#selCompany,#selOffType,#selRole,#SelTeamleader,#SelCertificated,#SelOfficerrole,#SelValidatedActions,#txtEmail,#selManager,#selADM,#txtMobile,#txtEmail').each(function () {
            $('#txtName,#txtID,#txtPwd,#selCompany,#selOffType,#selRole,#SelTeamleader,#SelCertificated,#SelOfficerrole,#SelValidatedActions,#txtEmail,#selManager,#selADM,#txtMobile,#txtEmail').css("border", "1px solid #D7D7D7");
        });
        $('#SelOfficerrole,#selOffType,#selCompany,#selRole,#SelTeamleader,#SelCertificated,#SelValidatedActions,#selADM,#selManager').val(0).attr('selected', 'selected');

        if (TreeLevel == 2) {
            DialogMessage("Add ADM");
            $('#lblFormOfficerID').html('ADM ID');
            $('#lblFormOfficerName').html('ADM Name');
            $('.trAdm').hide();
        }
        else {
            DialogMessage("Add Officer");
            $('#lblFormOfficerID').html('Officer ID');
            $('#lblFormOfficerName').html('Officer Name');
            $('.trAdm').show();
        }
        selectedADM = '';
        $('#txtPwd').val(createGuid()).attr('type', 'text');
    });

    $('#btnSearchOfficer').click(function () {
        GetOfficerListforGridView();
    });

    $('#btnClearOfficer').click(function () {
        $(':text').val('');
        //GetOfficerListforGridView();
        $('#lblNoofrecords').html('0');
        $('#tblGridView').dataTable().fnClearTable();
    });

    $('#DOfficersList').show();
    $('.TeamAdmin').hide();
    $('#lblOffList').html('Team Admin');

    $('#txtDateRegistered').datetimepicker({
        timepicker: false,
        format: 'd-m-Y',
        maxDate: '0'
    });

    $('#txtEmail').change(function () {
        if (!(filter.test($('#txtEmail').val()))) {
            $('#txtEmail').attr('style', 'border:2px solid #f19f79');
            return false;
        }
        else {
            $('#txtEmail').css("border", "1px solid #D7D7D7");
        }
        //return ReturnVal;
    });

    $('#btnPasswordReset').click(function () {
        ResetPassword(selectedOfficerID, 2);
    });

    GetOfficerListforGridView();
});

function GetManagers() {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/managers/' + mID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#selManager').empty();
        },
        success: function (data) {
            var ManagerList = '<option value="0">Select</option>';
            $.each(data, function (key, value) {
                if (key == 0)
                    GetADMList(0);

                ManagerList = ManagerList + '<option value="' + $.trim(value.OfficerID) + '">' + value.OfficerName + '</option>';
            });
            $('#selManager').html(ManagerList);
            $('#selADM').html('<option value="0">Select</option>');
            $('#selManager').change(function () {
                GetADMList($('#selManager').val());
            });
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function GetADMList(MgrID) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + MgrID + '/ADMList',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#selADM').empty();
        },
        success: function (data) {
            var ADMList = '<option value="0">Select</option>';
            $.each(data, function (key, value) {
                ADMList = ADMList + '<option value="' + value.ADMID + '">' + value.ADMName + '</option>';
            });
            $('#selADM').html(ADMList);
            if (selectedADM != '') {
                $('#selADM option:contains(' + (selectedADM == '' || selectedADM == 'null' ? 'Select' : selectedADM) + ')').prop("selected", "selected");
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function GetITAdminList() {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/companies',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#selCompany').empty();
        },
        success: function (data) {
            $.each(data, function (key, value) {
                if (key == 0)
                    $('#selCompany').append($('<option>').attr('value', 0).html('Select'));
                $('#selCompany').append($('<option>').attr('value', $.trim(value.CompanyID)).html(value.CompanyName));
            });
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });

    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/OfficerRoles',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#SelOfficerrole').empty();
        },
        success: function (data) {
            $.each(data, function (key, value) {
                if (key == 0)
                    $('#SelOfficerrole').append($('<option>').attr('value', 0).html('Select'));
                $('#SelOfficerrole').append($('<option>').attr('value', $.trim(value.OfficerRoleID)).html(value.OfficerRole));
            });
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });

    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/roles',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#selRole').empty();
        },
        success: function (data) {
            $.each(data, function (key, value) {
                if (key == 0)
                    $('#selRole').append($('<option>').attr('value', 0).html('Select'));

                if (value.RoleType == 'Officer')
                    $('#selRole').append($('<option>').attr('value', $.trim(value.RoleID)).html(value.RoleType));
            });
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function InsertManagerGroupOfficer() {
    chkInputFields();
    var pass = 0;
    if (TreeLevel == 3) {
        $('#txtName,#txtID,#txtPwd,#selCompany,#selOffType,#selRole,#SelTeamleader,#SelCertificated,#SelOfficerrole,#SelValidatedActions,#selManager,#selADM,#txtEmail,#txtMobile').each(function () {
            var $this = $(this);
            if ($this.attr('type') == 'text' || $this.attr('type') == 'password') {
                if ($this.val() == '') {
                    pass = 1;
                    $('#' + $this.attr('id')).attr('style', 'border:2px solid #f19f79');
                }
            }
            if ($this.is('select')) {
                if ($this.val() == '0') {
                    pass = 1;
                    $('#' + $this.attr('id')).attr('style', 'border:2px solid #f19f79;');
                }
            }
        });
    }
    else {
        $('#txtName,#txtID,#txtPwd,#selCompany,#selManager,#txtEmail,#txtMobile').each(function () {
            var $this = $(this);
            if ($this.attr('type') == 'text' || $this.attr('type') == 'password') {
                if ($this.val() == '') {
                    pass = 1;
                    $('#' + $this.attr('id')).attr('style', 'border:2px solid #f19f79');
                }
            }
            if ($this.is('select')) {
                if ($this.val() == '0') {
                    pass = 1;
                    $('#' + $this.attr('id')).attr('style', 'border:2px solid #f19f79;');
                }
            }
        });
    }

    if (!(Email.test($('#txtEmail').val()))) {
        $('#txtEmail').attr('style', 'border:2px solid #f19f79');
        return false;
    } else {
        $('#txtEmail').attr('style', 'border:1px solid #D7D7D7');
    }
    if (pass == 0) {
        $.ajax({
            type: 'POST',
            url: ServiceURL + 'api/v1/InsertManagerGroupOfficer',
            headers: {
                "Authorization": $.session.get('TokenAuthorization'),
                'Content-Type': 'application/json'
            },
            data: JSON.stringify({
                "Name": $.trim($('#txtName').val() + '|0'),
                "ManagerID": $('#selManager').val(),
                "OfficerID": $.trim($('#txtID').val()),
                "OfficerPwd": $.trim($('#txtPwd').val()),
                "GroupID": $.trim($('#selADM').val()),
                "Type": 3,
                "OfficerType": $('#selOffType').val(),
                "Company": $('#selCompany').val(),
                "Note": $.trim($('#txtNote').val()),
                "Email": $.trim($('#txtEmail').val()),
                "Mobile": $.trim($('#txtMobile').val()),
                "Role": (TreeLevel == 2 ? 8 : $('#selRole').val()),
                "Teamcode": $.trim($('#txtTeamcode').val() + '|' + mID),
                "Officerrole": $('#SelOfficerrole').val(),
                "Certificated": ($('#SelCertificated').val() == 1 ? true : false),
                "Postcode": $.trim($('#txtPostcode').val()),
                "Teamleader": ($('#SelTeamleader').val() == 1 ? true : false),
                "Validatedactions": ($('#SelValidatedActions').val() == 1 ? true : false),
                "TreeLevel": TreeLevel,
                "ANPR": ($("#chkANPR").is(':checked') == true ? true : false)
            }),
            dataType: 'json',
            beforeSend: function () { },
            success: function (data) {
                if (data == true) {
                    if (TreeLevel == '2')
                        AlertMessage("Info!", "ADM added successfully");
                    else
                        AlertMessage("Info!", "Officer added successfully");
                    $('#DAddDialog').dialog("close");
                    GetOfficerListforGridView();
                }
                else {
                    if (TreeLevel == '2')
                        AlertMessage("Info!", "ADMID already exists.");
                    else
                        AlertMessage("Info!", "OfficerID already exists.");
                    returntype = false;
                }
            },
            complete: function () { },
            error: function (xhr, textStatus, errorThrown) {
                //  alert('Error :' + errorThrown);
            }
        });
    }
}

function ResetPassword(OfficerID) {
    $('#DResetDialog').dialog({
        modal: true,
        resizeable: false,
        width: "25%",
        "title": "Confirm",
        buttons: {
            "Yes": function () {
                $('#txtPwd').val(createGuid()).attr('type', 'text');
                $(this).dialog("close");
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });
}

function DialogMessage(Title) {
    $('#DAddDialog').dialog({
        modal: true,
        resizable: false,
        width: "65%",
        "title": Title,
        buttons: {
            "Add": function () {
                InsertManagerGroupOfficer();
            }
        }
    })
}

function GetOfficerListforGridView() {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + ($.trim($('#txtOfficerID').val().replace(/[^a-zA-Z0-9]/g, '')) == '' ? 0 : $.trim($('#txtOfficerID').val().replace(/[^a-zA-Z0-9]/g, ''))) +
            '/' + ($.trim($('#txtOfficerName').val().replace(/[^a-zA-Z]/g, '')) == "" ? "Unknown" : $.trim($('#txtOfficerName').val().replace(/[^a-zA-Z]/g, ''))) +
            '/' + ($.trim($('#txtDateRegistered').val().replace(/[^0-9\-]/g, '')) == "" ? "1-1-1900" : $.trim($('#txtDateRegistered').val().replace(/[^0-9\-]/g, ''))) +
            '/' + $('input[name=AppUser]:checked').val() + '/' + mID + '/' + TreeLevel + '/OfficerListforGrid',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            if ($('input[name=AppUser]:checked').val() == 1)
                $('#btnCreatenewuser').show();
            else
                $('#btnCreatenewuser').hide();

            $.loader({
                className: "blue-with-image",
                content: ''
            });
        },
        success: function (data) {
            $('#tblGridView').dataTable().fnClearTable();
            var Role;
            if (!(data == '' || data == null || data == '[]')) {
                $.each(data, function (key, value) {

                    Role = (value.RoleType == null || value.RoleType == '' ? 'Select' : value.RoleType);
                    $('#tbodyGridView').append('<tr><td>' + $.trim(value.OfficerID) + '</td>' +
                               '<td>' + value.OfficerName +
                               '<br/> IMEI # : ' + value.IMEINumber + '<br/>' +
                               'Device : ' + value.DeviceName + '</td>' +
                               '<td class="tdAdmName">' + value.GroupName + '</td>' +
                               '<td>' + value.ManagerName + '</td>' +
                               '<td>' + ($('input[name=AppUser]:checked').val() == 1 ? value.RegisteredDate : value.DisabledDate) + '</td>' +
                               '<td>' + value.Email + '</td>' +
                               '<td>' + value.Mobile + '</td>' +
                               '<td class="tdLastLoginDate">' + value.LastloginDate + '</td>' +
                               '<td>' +
                               '<img class="imgCaseAction" src="images/hd-CaseActions-ico.png" alt="Case" title="Case Action" style="cursor:pointer; width:24px;padding-right:5px;" onclick="GetCaseAction(' + $.trim(value.OfficerID) + ',' + "'" + value.LastActionDate + "'" + ',' + "'" + value.OfficerName + "'" + ')" />' +
                               '<img class="imgAll" src="images/Audit_32_32.png" alt="Audit" title="Audit Trail" style="cursor:pointer;padding-right:5px;" onclick="GetAuditTrail(' + $.trim(value.OfficerID) + ')" />' +
                               '<img class="imgPrevious" src="images/hd-LastLocation-ico.png" alt="Last Location" title="Last Location" style="cursor:pointer;width:24px;padding-right:5px;" onclick="MapFillLastKnownLocation(' + $.trim(value.OfficerID) + ',' + "'" + value.LastActionDate + "'" + ')" />' +
                               '<img class="imgView" src="images/View_32_32.png" alt="Edit" title="View" style="cursor:pointer;padding-right:5px;display:none;" onclick="UpdateUser(' + $.trim(value.OfficerID) + ',2' + ')"/> ' +
                               '<img class="imgEdit" src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer;padding-right:5px;" onclick="UpdateUser(' + $.trim(value.OfficerID) + ',1' + ')"/> ' +
                               '<img class="imgUnRegister" src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;padding-right:5px;" onclick="UnRegister(' + $.trim(value.OfficerID) + ',' + "'" + value.OfficerName + "'" + ',' + "'" + Role + "'" + ',' + (value.OfficerCount == undefined ? 0 : value.OfficerCount) + ')" /></td>' +
                               '<td style="display:none">' + value.EmpType + '</td>' +
                               '<td style="display:none">' + value.Teamcode + '</td>' +
                               '<td style="display:none">' + value.OfficerRole + '</td>' +
                               '<td style="display:none">' + value.Certificated + '</td>' +
                               '<td style="display:none">' + value.Postcode + '</td>' +
                               '<td style="display:none">' + value.Teamleader + '</td>' +
                               '<td style="display:none">' + value.IsValidatedActions + '</td>' +
                               '<td style="display:none">' + value.RoleType + '</td>' +
                               '<td style="display:none">' + value.Password + '</td>' +
                               '<td style="display:none">' + value.CompanyID + '</td>' +
                               '<td style="display:none">' + value.OfficerName + '</td>' +
                               '<td style="display:none">' + value.Note + '</td>' +
                               '<td style="display:none">' + value.ANPR + '</td>' +
                               '</tr>');
                });
                if ($('input[name=AppUser]:checked').val() == 1) {
                    $('.tdDateType').html('Registered Date');
                    $('.tdLastLoginDate,.imgPrevious,.imgView').hide();
                    $('.imgUnRegister,.imgEdit').show();
                }
                else {
                    $('.tdDateType').html('Disabled Date');
                    $('.tdLastLoginDate,.imgPrevious,.imgView').show();
                    $('.imgUnRegister,.imgEdit').hide();
                }

                if (TreeLevel == 2) {
                    $('.tdAdmName,.tdnote,.imgPrevious,.imgAll,.imgCaseAction').hide();
                    $('.tdofficer').html('ADM ID');
                    $('.tdoffname').html('ADM  Name');
                }
                else {
                    $('.tdAdmName,.tdnote,.imgCaseAction').show();
                    $('.tdofficer').html('Officer ID');
                    $('.tdoffname').html('Officer Name');
                }

                $('#lblNoofrecords').html($('#tblGridView tbody tr').size() - 1);
                $('#tblGridView').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "bLengthChange": false,
                    "bFilter": false,
                    "sInfo": true,
                    "iDisplayLength": 5
                });

                $("#tblGridView").delegate("tbody tr", "click", function (key, event) {
                    selectedOfficerID = this.cells[0].innerHTML;
                    $('#txtID').val(this.cells[0].innerHTML).attr('disabled', true);
                    $('#txtName').val(this.cells[19].innerHTML);
                    $('#selManager option:contains(' + (this.cells[3].innerHTML == '' || this.cells[3].innerHTML == 'null' ? 'Select' : this.cells[3].innerHTML) + ')').prop("selected", "selected");
                    GetADMList($('#selManager').val());

                    $('#txtNote').val(this.cells[20].innerHTML);
                    $('#txtEmail').val(this.cells[5].innerHTML);
                    $('#txtMobile').val($.trim(this.cells[6].innerHTML));
                    // : 7 -image (Not a ID)
                    OfficerType = (this.cells[9].innerHTML == '' || this.cells[9].innerHTML == 'null' || this.cells[9].innerHTML == null ? 'Select' : this.cells[9].innerHTML);
                    OfficerRole = (this.cells[11].innerHTML == '' || this.cells[11].innerHTML == 'null' || this.cells[11].innerHTML == null ? 'Select' : this.cells[11].innerHTML);
                    Certificate = (this.cells[12].innerHTML == '' || this.cells[12].innerHTML == 'null' || this.cells[12].innerHTML == null ? 'Select' : this.cells[12].innerHTML);
                    Role = (this.cells[16].innerHTML == '' || this.cells[16].innerHTML == 'null' || this.cells[16].innerHTML == null ? 'Select' : this.cells[16].innerHTML);
                    $('#selOffType option:contains(' + OfficerType + ')').prop("selected", "selected");//Employee type : Type
                    $('#txtTeamcode').val(this.cells[10].innerHTML);// Team code
                    $('#SelOfficerrole option:contains(' + OfficerRole + ')').prop("selected", "selected");//Officer role
                    $('#SelCertificated option:contains(' + Certificate + ')').prop("selected", "selected");//Certificated
                    $('#txtPostcode').val(this.cells[13].innerHTML);//Postcode
                    $('#SelTeamleader option:contains(' + (this.cells[14].innerHTML == '' || this.cells[14].innerHTML == 'null' ? 'Select' : this.cells[14].innerHTML) + ')').prop("selected", "selected");
                    $('#SelValidatedActions option:contains(' + (this.cells[15].innerHTML == '' || this.cells[15].innerHTML == 'null' ? 'Select' : this.cells[15].innerHTML == 'true' ? 'Yes' : 'No') + ')').prop("selected", "selected");//Accepted actions
                    $('#selRole option:contains(' + Role + ')').prop("selected", "selected");
                    $('#txtPwd').val(this.cells[17].innerHTML).attr('type', 'password');//Password
                    $('#selCompany').val((this.cells[18].innerHTML == '' || this.cells[18].innerHTML == 'null' ? 0 : this.cells[18].innerHTML)).attr("selected", "selected");
                    selectedADM = this.cells[2].innerHTML;
                    if (this.cells[21].innerHTML == 'true')
                        $("#chkANPR").prop('checked', 'checked');//.is(':checked');
                    else
                        $("#chkANPR").prop('checked', '');//.is(':not(:checked)');
                    $('#txtName,#txtID,#txtPwd,#selCompany,#selOffType,#selRole,#SelTeamleader,#SelCertificated,#SelOfficerrole,#SelValidatedActions,#txtEmail').css("border", "1px solid #D7D7D7");
                });
            }
            else {

                if ($('input[name=AppUser]:checked').val() == 1) {
                    $('.tdDateType').html('Registered Date');
                }
                else {
                    $('.tdDateType').html('Disabled Date');
                }

                if (TreeLevel == 2) {
                    $('.tdAdmName,.tdnote').hide();
                    $('.tdofficer').html('ADM ID');
                    $('.tdoffname').html('ADM Name');
                }
                else {
                    $('.tdAdmName,.tdnote').show();
                    $('.tdofficer').html('Officer ID');
                    $('.tdoffname').html('Officer Name');
                }
                $('#tblGridView').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": false,
                    "bDestroy": true,
                    "bSort": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bInfo": false,
                    "iDisplayLength": 1
                });
                $('#lblNoofrecords').html('0');
            }
        },
        complete: function () { $.loader('close'); },
        error: function (xhr, textStatus, errorThrown) {
            // alert('Error :' + errorThrown);
        }
    });
}

function UpdateUser(id, IsView) {
    chkInputFields();
    $('#txtName,#txtID,#txtPwd,#selCompany,#selOffType,#selRole,#SelTeamleader,#SelCertificated,#SelOfficerrole,#SelValidatedActions,#txtEmail,#selManager,#selADM,#txtEmail,#txtMobile').each(function () {
        $('#txtName,#txtID,#txtPwd,#selCompany,#selOffType,#selRole,#SelTeamleader,#SelCertificated,#SelOfficerrole,#SelValidatedActions,#txtEmail,#selManager,#selADM,#txtEmail,#txtMobile').css("border", "1px solid #D7D7D7");
    });
    var Title;
    if (TreeLevel == 3) {
        $('.trAdm').show();
        if (IsView == 2)
            Title = "View Officer Details";
        else
            Title = "Update Officer Details";
    }
    else {
        $('.trAdm').hide();
        if (IsView == 2)
            Title = "View ADM Details";
        else
            Title = "Update ADM Details";

    }
    $('#btnPasswordReset').show();
    $('#DAddDialog').dialog({
        modal: true,
        resizable: false,
        width: "65%",
        "title": Title,
        buttons: {
            "Update": function () {
                if (UpdateOfficesForITAdmin(id))
                    $(this).dialog("close");
            }
        }
    });
    if ($('input[name=AppUser]:checked').val() == 1) {
    }
    else {
        $(".ui-dialog-buttonpane button:contains('Update')").button("disable");
    }

}

function UpdateOfficesForITAdmin(key) {
    chkInputFields();
    var LabelID, Attrib, pass = 0, RoleType;
    // $('#selRole option:contains(' + Type + ')').attr("selected", "selected");
    RoleType = $('#selRole').val();
    LabelID = 'POid';
    Attrib = 'Oid';
    if (TreeLevel == 3) {
        $('#txtName,#txtID,#txtPwd,#selCompany,#selOffType,#selRole,#SelTeamleader,#SelCertificated,#SelOfficerrole,#SelValidatedActions,#selManager,#selADM,#txtEmail,#txtMobile').each(function () {
            var $this = $(this);
            if ($this.attr('type') == 'text' || $this.attr('type') == 'password') {
                if ($this.val() == '') {
                    pass = 1;
                    $('#' + $this.attr('id')).attr('style', 'border:2px solid #f19f79');
                }
            }
            if ($this.is('select')) {
                if ($this.val() == '0') {
                    pass = 1;
                    $('#' + $this.attr('id')).attr('style', 'border:2px solid #f19f79;');
                }
            }
        });
    }
    else {
        $('#txtName,#txtID,#txtPwd,#selCompany,#selManager,#txtEmail,#txtMobile').each(function () {
            var $this = $(this);
            if ($this.attr('type') == 'text' || $this.attr('type') == 'password') {
                if ($this.val() == '') {
                    pass = 1;
                    $('#' + $this.attr('id')).attr('style', 'border:2px solid #f19f79');
                }
            }
            if ($this.is('select')) {
                if ($this.val() == '0') {
                    pass = 1;
                    $('#' + $this.attr('id')).attr('style', 'border:2px solid #f19f79;');
                }
            }
        });
    }
    if (!(Email.test($('#txtEmail').val()))) {
        $('#txtEmail').attr('style', 'border:2px solid #f19f79');
        return false;
    } else {
        $('#txtEmail').attr('style', 'border:1px solid #D7D7D7');
    }

    if (pass == 0) {
        $.ajax({
            type: 'POST',
            url: ServiceURL + 'api/v1/InsertManagerGroupOfficer',
            headers: {
                "Authorization": $.session.get('TokenAuthorization'),
                'Content-Type': 'application/json'
            },
            data: JSON.stringify({
                "Name": $.trim($('#txtName').val() + '|1'),
                "ManagerID": $('#selManager').val(),
                "OfficerID": $.trim($('#txtID').val()),
                "OfficerPwd": $.trim($('#txtPwd').val()),
                "GroupID": $.trim($('#selADM').val()),
                "Type": 3,
                "OfficerType": $('#selOffType').val(),
                "Company": $('#selCompany').val(),
                "Note": $.trim($('#txtNote').val()),
                "Email": $.trim($('#txtEmail').val()),
                "Mobile": $.trim($('#txtMobile').val()),
                "Role": (TreeLevel == 2 ? 8 : $('#selRole').val()),
                "Teamcode": $.trim($('#txtTeamcode').val() + '|' + mID),
                "Officerrole": $('#SelOfficerrole').val(),
                "Certificated": ($('#SelCertificated').val() == 1 ? 1 : 0),
                "Postcode": $.trim($('#txtPostcode').val()),
                "Teamleader": ($('#SelTeamleader').val() == 1 ? 1 : 0),
                "Validatedactions": ($('#SelValidatedActions').val() == 1 ? 1 : 0),
                "TreeLevel": TreeLevel,
                "ANPR": ($("#chkANPR").is(':checked') == true ? 1 : 0)
            }),
            dataType: 'json',
            beforeSend: function () { },
            success: function (data) {
                if (TreeLevel == '2')
                    AlertMessage('Info!', 'ADM updated successfully');
                else {
                    AlertMessage('Info!', 'Officer updated successfully');
                }
            },
            complete: function () {
                GetOfficerListforGridView();
                $('#DAddDialog').dialog("close");
            },
            error: function (xhr, textStatus, errorThrown) {
            }
        });
        return true;
    }
    if (pass == 1)
        return false;
}

function UnRegister(id, name, role, OfficerCount) {
    $(':text').val('');
    $('#bOfficerName').html(name);
    var Title;
    switch (role) {
        case 'DeptHead':
            Title = 'Confirmation of blocking Department Head';
            break;
        case 'Manager':
            Title = 'Confirmation of blocking Manager';
            break;
        case 'ADM':
            Title = 'Confirmation of blocking ADM';
            break;
        case 'Officer':
            Title = 'Confirmation of blocking officer';
            break;
        case 'ES':
            Title = 'Confirmation of blocking ES';
            break;
        case 'Auditor':
            Title = 'Confirmation of blocking auditor';
            break;
        case 'IT Support':
            Title = 'Confirmation of blocking IT support';
            break;
        case 'HR':
            Title = 'Confirmation of blocking HR';
            break;
        case 'SuperUser':
            Title = 'Confirmation of blocking super user';
            break;
    }

    if (TreeLevel == 2) {
        if (OfficerCount > 0) {
            AlertMessage("Info!", "Officers are available under this ADM, Please move to any ADM before unregister!");
        }
        else {
            // Type:  1 - Manager; 2 - Group; 3 - Officer; 4 - Block
            $('#DConfirmDialog').dialog({
                modal: true,
                resizable: false,
                width: "30%",
                "title": Title,
                buttons: {
                    "Yes": function () {
                        $.ajax({
                            type: 'PUT',
                            url: ServiceURL + 'api/v1/UpdateUserandGroupName',
                            headers: {
                                "Authorization": $.session.get('TokenAuthorization'),
                                'Content-Type': 'application/json'
                            },
                            data: JSON.stringify({
                                "Type": 4,
                                "ModifiedName": name,
                                "Id": id
                                //"OfficerType": 0, //optional
                                //"CompanyID": 0,
                                //"Note": "string",
                                //"Email": "string",
                                //"Mobile": "string",
                                //"Role": 0
                            }),
                            dataType: 'json',
                            beforeSend: function () { },
                            success: function (data) {
                                GetOfficerListforGridView();
                                GetManagers();
                            },
                            complete: function () { },
                            error: function (xhr, textStatus, errorThrown) {
                                // alert('Error :' + errorThrown);
                            }
                        });
                        $('#DConfirmDialog').dialog('close');
                    },
                    "No": function () {
                        $('#DConfirmDialog').dialog('close');
                    }
                }
            });
        }
    }
    else {
        // Type:  1 - Manager; 2 - Group; 3 - Officer; 4 - Block
        $('#DConfirmDialog').dialog({
            modal: true,
            resizable: false,
            width: "30%",
            "title": Title,
            buttons: {
                "Yes": function () {
                    $.ajax({
                        type: 'PUT',
                        url: ServiceURL + 'api/v1/UpdateUserandGroupName',
                        headers: {
                            "Authorization": $.session.get('TokenAuthorization'),
                            'Content-Type': 'application/json'
                        },
                        data: JSON.stringify({
                            "Type": 4,
                            "ModifiedName": name,
                            "Id": id
                            //"OfficerType": 0, //optional
                            //"CompanyID": 0,
                            //"Note": "string",
                            //"Email": "string",
                            //"Mobile": "string",
                            //"Role": 0
                        }),
                        dataType: 'json',
                        beforeSend: function () { },
                        success: function (data) {
                            GetOfficerListforGridView();
                            GetManagers();
                        },
                        complete: function () { },
                        error: function (xhr, textStatus, errorThrown) { }
                    });
                    $('#DConfirmDialog').dialog('close');
                },
                "No": function () {
                    $('#DConfirmDialog').dialog('close');
                }
            }
        });
    }
}

function createGuid() {
    //return 'xxxxxx'.replace(/[xy]/g, function (c) {
    //    var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
    //    return v.toString(16);
    //});

    var number = Math.floor(1000 + Math.random() * 9000);
    return number;
}

// TODO: Officer case action,warrant matching and last known location details of popup window
// TODO: Start=======================================================================================================

function GetCaseAction(OfficerID, ActionDate, OfficerName) {

    if (ActionDate == '' || ActionDate == null || ActionDate == 'null' || ActionDate == undefined) {
        AlertMessage("Info!", "No action found");
    }
    else {
        $.session.set("AdminOfficerID", OfficerID);
        $.session.set("AdminActionDate", ActionDate);
        $.session.set("AdminOfficerName", OfficerName);

        $('#iframeCase').attr('src', AdminCaseUrl);
        $('#DCaseAction').dialog({
            modal: true,
            resizable: false,
            width: "85%",
            //height: 500,
            "title": "Last Action",
            buttons: {
                "Close": function () {
                    $(this).dialog("close");
                }
            }
        });
    }
}

// TODO: End  =======================================================================================================

// TODO: Officer Audit trail
// TODO: Start=======================================================================================================

function GetAuditTrail(OfficerID) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + OfficerID + '/AuditTrail',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#DAudit').dialog({
                modal: true,
                resizable: false,
                width: "65%",
                //height: 500,
                "title": "Audit trail",
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
            $.loader({
                className: "blue-with-image",
                content: ''
            });
        },
        success: function (data) {
            $('#tblAudit').dataTable().fnClearTable();
            if (data != '') {
                $.each(data, function (key, value) {
                    $('#tbodyAudit').append('<tr><td>' + value.OfficerId + '</td>' +
                               '<td>' + value.Message + '</td>' +
                               '<td>' + value.DateActioned + '</td></tr>');
                });
                $('#tblAudit').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "bLengthChange": false,
                    "bFilter": false,
                    "sInfo": true,
                    "iDisplayLength": 5
                });
            }
            else {
                $('#tblAudit').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": false,
                    "bDestroy": true,
                    "bSort": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bInfo": false,
                    "iDisplayLength": 1
                });
            }
        },
        complete: function () {
            $.loader('close');
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

// TODO: End  =======================================================================================================

// TODO: Officer last known location details
// TODO: Start=======================================================================================================

function MapFillLastKnownLocation(mID, fDate) {
    if (fDate == '' || fDate == null || fDate == 'null' || fDate == undefined) {
        AlertMessage("Info!", "Last known location not found!");
    }
    else {
        var dateAr = fDate.split('/');
        var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
        $.ajax({
            type: 'GET',
            url: ServiceURL + 'api/v1/officers/' + mID + '/' + newDate + '/' + newDate + '/LastLocation',
            headers: {
                "Authorization": $.session.get('TokenAuthorization'),
                'Content-Type': 'application/json'
            },
            dataType: 'json',
            beforeSend: function () {
                $('#map_canvas').empty();
                $('#DLastLocation').dialog({
                    modal: true,
                    resizable: false,
                    width: "65%",
                    height: 450,
                    "title": "Last Location",
                    buttons: {
                        "Close": function () {
                            $(this).dialog("close");
                        }
                    }
                });
                markers = [];
                Latitude = [];
                Longitude = [];
                Icon = [];
                Content = [];
                pLatitude = 0;
                pLongitude = 0;
                pLatMap = 0;
                pLongMap = 0;
            },
            success: function (data) {
                if (data != undefined && data != '[]' && data != '[{"Result":"NoRecords"}]') {
                    $('#lblDisplay').html('').hide();
                    var iconImg1;
                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP
                    };

                    map = new google.maps.Map(document.getElementById("map_canvas"), options);
                    infowindow = new google.maps.InfoWindow();

                    $.each(data, function (key, value) {
                        switch (value.IconType) {
                            case 1: // UTTC
                                iconImg1 = "images/map-icon-yellow.png";
                                break;
                            case 2:  // Returned, DROPPED
                                iconImg1 = "images/map-icon-red.png";
                                break;
                            case 3:  // Revisit,TCG
                                iconImg1 = "images/map-icon-floresent.png";
                                break;
                            case 5:  //  Paid , Part paid , TCG PP,TCG Paid 
                                iconImg1 = "images/map-icon-orange.png";
                                break;
                            case 6:  // Login
                                iconImg1 = "images/map-icon-blue.png";
                                break;
                            case 7:  // ARR
                                iconImg1 = "images/map-icon-navyblue.png";
                                break;
                            case 8:  // DEP
                                iconImg1 = "images/map-icon-lightvio.png";
                                break;
                            case 9:  // Logout
                                iconImg1 = "images/map-icon-grey.png";
                                break;
                            default:
                                iconImg1 = "images/map-icon-white.png";
                        }
                        Latitude[key] = value.latitude;
                        Longitude[key] = value.longitude;
                        if (Latitude[key] != pLatMap && Longitude[key] != pLongMap) {
                            Icon[key] = iconImg1;
                            Content[key] = value.html;
                            pContentkey = key;
                        }
                        else {
                            Content[pContentkey] = Content[pContentkey] + '<br><hr>' + value.html;
                        }
                        pLatMap = value.latitude;
                        pLongMap = value.longitude;
                    });
                    for (var i = 0; i < data.length; i++) {
                        if (Latitude[i] != pLatitude && Longitude[i] != pLongitude) {
                            latLng = new google.maps.LatLng(Latitude[i], Longitude[i]);
                            marker = new google.maps.Marker({
                                'position': latLng,
                                'map': map,
                                'icon': Icon[i]
                            });
                            addInfoWindow(marker, Content[i]);
                            markers.push(marker);
                        }
                        pLatitude = Latitude[i];
                        pLongitude = Longitude[i];
                    }
                }
                else {
                    $('#lblDisplay').html('No activity recorded for today!').show();
                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP
                    };
                    map = new google.maps.Map(document.getElementById("map_canvas"), options);
                }
            },
            complete: function () {
                $.loader('close');
            },
            error: function (xhr, textStatus, errorThrown) {
            }
        });
    }
}

function addInfoWindow(marker, message) {
    var infoWindow = new google.maps.InfoWindow({
        content: message
    });
    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.open(map, marker);
    });
}

function chkInputFields() {
    $("#txtID").val($.trim($("#txtID").val().replace(/[^0-9]/g, '')));
    $("#txtName").val($.trim($("#txtName").val().replace(/[^a-zA-Z0-9]/g, '')));
    $("#txtPwd").val($.trim($("#txtPwd").val().replace(/[^0-9]/g, '')));
    $("#txtMobile").val($.trim($("#txtMobile").val().replace(/[^a-zA-Z0-9-+]/g, '')));
    $("#txtEmail").val($.trim($("#txtEmail").val().replace(/[^a-zA-Z0-9.-_@]/g, '')));
    $("#txtNote").val($.trim($("#txtNote").val().replace(/[^a-zA-Z0-9.-_]/g, '')));
    $("#txtPostcode").val($.trim($("#txtPostcode").val().replace(/[^a-zA-Z0-9.-_]/g, '')));
    $("#txtTeamcode").val($.trim($("#txtTeamcode").val().replace(/[^0-9]/g, '')));
}
// TODO: End  =======================================================================================================
