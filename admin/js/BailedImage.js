﻿//==========================================================================================================================================
/* File Name: BailedImage.js */
/* File Created: oct 5, 2016 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================
var mID;

$(document).ready(function () {
    if ($.trim($.session.get('OfficerID')).length > 0) {
    }
    else {
        window.location.href = HomeUrl;
    }
    mID = $.session.get("OfficerID");

    $('#txtFromDate,#txtToDate').attr('readonly', 'readonly');

    $('#txtFromDate').datetimepicker({
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('#txtToDate').val() ? $('#txtToDate').val() : false
            })
        },
        timepicker: false
    });
    $('#txtToDate').datetimepicker({
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('#txtFromDate').val() ? $('#txtFromDate').val() : false
            })
        },
        timepicker: false
    });

    $('#btnSearchBailed').click(function () {
        GetBailedImageSearchForAdmin();
    });

    $('#btnClearBailed').click(function () {
        $(':text').val('');
        $('#DBailedImage').hide();
    });
});

function GetBailedImageSearchForAdmin() {
    var Parameter = "";
    var FromDate = "", ToDate = "";

    FromDate = $.trim($('#txtFromDate').val().replace(/[^0-9\/]/g, ''));
    if (FromDate.length == 10) {
        var dateF = FromDate.split('/');
        FromDate = dateF[2] + '-' + dateF[1] + '-' + dateF[0];
    }
    else {
        FromDate = "";
    }

    ToDate = $.trim($('#txtToDate').val().replace(/[^0-9\/]/g, ''));
    if (ToDate.length == 10) {
        var dateT = ToDate.split('/');
        ToDate = dateT[2] + '-' + dateT[1] + '-' + dateT[0];
    }
    else {
        ToDate = "";
    }

    if ($.trim($('#txtCaseNumber').val().replace(/[^a-zA-Z0-9]/g, '')) != "") {
        Parameter = 'CaseNumber=' + $('#txtCaseNumber').val().replace(/[^a-zA-Z0-9]/g, '');
    }

    if ($.trim($('#txtOfficerID').val().replace(/[^a-zA-Z0-9]/g, '')) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'OfficerID=' + $('#txtOfficerID').val().replace(/[^a-zA-Z0-9]/g, '');
        }
        else
            Parameter = 'OfficerID=' + $('#txtOfficerID').val().replace(/[^a-zA-Z0-9]/g, '');
    }

    if (FromDate != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'FromDate=' + FromDate;
        }
        else
            Parameter = 'FromDate=' + FromDate;
    }

    if (ToDate != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'ToDate=' + ToDate;
        }
        else
            Parameter = 'ToDate=' + ToDate;
    }
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/BailedImageSearch?' + Parameter,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#tbodyBailed').empty();
            $('#DBailedImage').show();
            $.loader({
                className: "blue-with-image",
                content: ' '
            });
        },
        success: function (data) {
            $('#tblBailed').dataTable().fnClearTable();
            if (!(data == '' || data == null || data == '[]')) {
                $('#tbBailedImage').show();
                $.each(data, function (key, value) {
                    $('#tbodyBailed').append('<tr><td>' + value.CaseNumber + '</td><td>' + value.Officer + '</td>' +
                                   '<td>' + value.DateActioned + '</td>' +
                                   '<td>' +
                                   '<a href="' + OptimiseImageUrl + '" target="_blank" ><img style="padding-right:10px;" src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                   '<img id="imgBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="BailedUnselect" alt="View" title="View bailed image" style="cursor:pointer;padding-right:10px;" onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')" />' +
                                   '</td></tr>');
                    if (key == 0) {
                        ViewBailedImage(key, "'" + value.ImageURL + "'");
                        $('#imgBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'BailedSelect');
                    }

                });
                $('#tblBailed').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "aLengthMenu": [[5, 10, 50, 100, -1], [5, 10, 50, 100, "All"]],
                    "bLengthChange": false,
                    "bFilter": false,
                    "sInfo": true,
                    "iDisplayLength": 5
                });
            }
            else {
                $('#tbBailedImage').hide();
                $('#tblBailed').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": false,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "aLengthMenu": [[5, 10, 50, 100, -1], [5, 10, 50, 100, "All"]],
                    "bLengthChange": false,
                    "bFilter": false,
                    "sInfo": true,
                    "iDisplayLength": 5
                });
            }
        },
        complete: function () {
            $.loader('close');
        },
        error: function (xhr, textStatus, errorThrown) {
            //   alert('error');
        }
    });
}

function ViewBailedImage(key, BailedImageURL) {
    $.session.set('AdminImageType', 'B');
    $.session.set('AdminBailedImageURL', BailedImageURL);
    $('#iframeBailedImage').attr('src', OptimiseImageUrl);

    $('.BailedSelect').removeClass('BailedSelect').addClass('BailedUnselect').attr('src', 'images/ViewImage_24_24_2.png');

    if ($('#imgBailedImage' + key).attr('src') == 'images/ViewImage_24_24_2.png')
        $('#imgBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'BailedSelect');
    else
        $('#imgBailedImage' + key).attr('src', 'images/ViewImage_24_24_2.png').attr('class', 'BailedUnselect');
}

function DownloadBailedImage(ImageURL) {
    window.open(ImageURL, '_blank');
}
