﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OptimiseUpload
{
    public partial class SpecialCaseUpload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                try
                {
                    if (FileUploadSource.HasFile)
                    {
                        string CurrentDate = DateTime.Now.ToString("_dd-MMM-yyyy_hh-mm-ss_tt.");
                        string[] FileName = new string[2];
                        FileName[0] = FileUploadSource.FileName.Substring(0, FileUploadSource.FileName.LastIndexOf('.'));
                        FileName[1] = FileUploadSource.FileName.Substring(FileUploadSource.FileName.LastIndexOf('.') + 1);

                        try
                        {
                            FileUploadSource.SaveAs(Server.MapPath(".").Substring(0, Server.MapPath(".").LastIndexOf('\\') + 1) + "Uploads\\" + FileName[0] + CurrentDate + FileName[1]);
                        }
                        catch (Exception ex)
                        {
                            //lblInformation.Text = "ERROR: " + ex.Message.ToString();
                        }

                        lblInfo.Text = "File has been selected. Please upload now";

                        //string FileLocation = ConfigurationSettings.AppSettings["HomeURL"].ToString() + "Uploads/" + FileName[0] + CurrentDate + FileName[1];
                        string FileLocation = Server.MapPath(".").Substring(0, Server.MapPath(".").LastIndexOf('\\') + 1) + "Uploads\\" + FileName[0] + CurrentDate + FileName[1];
                        HttpContext.Current.Session["DocumentLocation"] = FileLocation;
                        //Session["DocumentLocation"] = FileLocation;
                        HttpCookie DocumentLocation = new HttpCookie("DocumentLocation", FileLocation);
                        Response.Cookies.Add(DocumentLocation);
                    }
                    else
                    {
                        //  lblInfo.Text = "File not found in specified folder.";
                    }
                }
                catch (Exception ex)
                {
                    lblInfo.Text = "Upload failed. Check source file and try again.";
                    //  btnUpload.Enabled = true;
                    FileUploadSource.Enabled = true;
                }
                finally
                {
                    //  btnUpload.Enabled = true;
                    FileUploadSource.Enabled = true;
                }
            }
        }
    }
}