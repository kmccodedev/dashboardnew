﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SpecialCaseUpload.aspx.cs" Inherits="OptimiseUpload.SpecialCaseUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#FileUploadSource').change(function () {
                this.form.submit();
            });
        });
    </script>
</head>

<body>
    <form id="form1" runat="server">
        <table cellpadding="0" cellspacing="10" width="100%" align="left" border="0">
            <tr>
                <td>Upload File
                </td>
                <td>
                    <asp:FileUpload ID="FileUploadSource" runat="server" />
                    <asp:RegularExpressionValidator ID="regexValidator" runat="server" ControlToValidate="FileUploadSource"
                        ErrorMessage="Only Excel files are allowed" ValidationExpression="(.*\.([Xx][Ll][Ss][Xx])|.*\.([Xx][Ll][Ss]))"
                        ForeColor="Red"></asp:RegularExpressionValidator>

                </td>
                <%--  <td style="width: 60%" colspan="2" align="left">
                                        <asp:Button ID="btnUpload" Text="Upload" runat="server" class="submitbtn"
                                            ToolTip="Click to uplad the excel file" OnClick="btnUpload_Click" />
                                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" class="cancelbtn"
                                            ToolTip="Click here to cancel upload" OnClick="btnCancel_Click" />
                                    </td>--%>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <strong style="color: Red">
                        <asp:Label runat="server" ID="lblInfo" /></strong>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
