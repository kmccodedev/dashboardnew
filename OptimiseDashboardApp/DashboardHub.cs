﻿using System;
//using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Timers;
using System.Web;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;

namespace OptimiseDashboardApp
{
    public class DashboardHub : Hub
    {
        private static int clientCounter = 0;
        private static Timer myTimerLL;
        private static Timer myTimerHB;
        private static Timer myTimer;
        private static Timer myTimerWMA;
        private static Timer myTimerESRequest;
        private static Timer myTimerESOfficer;

        private static DataTable dtCaseActions
        {
            get
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtCaseActions"] == null))
                {
                    HttpRuntime.Cache.Insert("dtCaseActions", new DataTable(), null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                    return HttpRuntime.Cache["dtCaseActions"] as DataTable;
                }
                else
                    return HttpRuntime.Cache["dtCaseActions"] as DataTable;
            }
            set
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtCaseActions"] == null))
                    HttpRuntime.Cache.Insert("dtCaseActions", value, null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                else
                    HttpRuntime.Cache["dtCaseActions"] = value;
            }
        }

        private static DataTable dtWMA
        {
            get
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtWMA"] == null))
                {
                    HttpRuntime.Cache.Insert("dtWMA", new DataTable(), null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                    return HttpRuntime.Cache["dtWMA"] as DataTable;
                }
                else
                    return HttpRuntime.Cache["dtWMA"] as DataTable;
            }
            set
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtWMA"] == null))
                    HttpRuntime.Cache.Insert("dtWMA", value, null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                else
                    HttpRuntime.Cache["dtWMA"] = value;
            }
        }

        private static DataTable dtOfficersTree
        {
            get
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtOfficersTree"] == null))
                {
                    HttpRuntime.Cache.Insert("dtOfficersTree", new DataTable(), null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                    return HttpRuntime.Cache["dtOfficersTree"] as DataTable;
                }
                else
                    return HttpRuntime.Cache["dtOfficersTree"] as DataTable;
            }
            set
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtOfficersTree"] == null))
                    HttpRuntime.Cache.Insert("dtOfficersTree", value, null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                else
                    HttpRuntime.Cache["dtOfficersTree"] = value;
            }
        }

        private static DataTable dtManagerTree
        {
            get
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtManagerTree"] == null))
                {
                    HttpRuntime.Cache.Insert("dtManagerTree", new DataTable(), null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                    return HttpRuntime.Cache["dtManagerTree"] as DataTable;
                }
                else
                    return HttpRuntime.Cache["dtManagerTree"] as DataTable;
            }
            set
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtManagerTree"] == null))
                    HttpRuntime.Cache.Insert("dtManagerTree", value, null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                else
                    HttpRuntime.Cache["dtManagerTree"] = value;
            }
        }
        /*
        private static DataTable dtManagerTreeDistress
        {
            get
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtManagerTreeDistress"] == null))
                {
                    HttpRuntime.Cache.Insert("dtManagerTreeDistress", new DataTable(), null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                    return HttpRuntime.Cache["dtManagerTreeDistress"] as DataTable;
                }
                else
                    return HttpRuntime.Cache["dtManagerTreeDistress"] as DataTable;
            }
            set
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtManagerTreeDistress"] == null))
                    HttpRuntime.Cache.Insert("dtManagerTreeDistress", value, null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                else
                    HttpRuntime.Cache["dtManagerTreeDistress"] = value;
            }
        }

        private static DataTable dtManagerTreeArrest
        {
            get
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtManagerTreeArrest"] == null))
                {
                    HttpRuntime.Cache.Insert("dtManagerTreeArrest", new DataTable(), null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                    return HttpRuntime.Cache["dtManagerTreeArrest"] as DataTable;
                }
                else
                    return HttpRuntime.Cache["dtManagerTreeArrest"] as DataTable;
            }
            set
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtManagerTreeArrest"] == null))
                    HttpRuntime.Cache.Insert("dtManagerTreeArrest", value, null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                else
                    HttpRuntime.Cache["dtManagerTreeArrest"] = value;
            }
        }

        private static DataTable dtManagerTreeHighCourt
        {
            get
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtManagerTreeHighCourt"] == null))
                {
                    HttpRuntime.Cache.Insert("dtManagerTreeHighCourt", new DataTable(), null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                    return HttpRuntime.Cache["dtManagerTreeHighCourt"] as DataTable;
                }
                else
                    return HttpRuntime.Cache["dtManagerTreeHighCourt"] as DataTable;
            }
            set
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtManagerTreeHighCourt"] == null))
                    HttpRuntime.Cache.Insert("dtManagerTreeHighCourt", value, null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                else
                    HttpRuntime.Cache["dtManagerTreeHighCourt"] = value;
            }
        }
        */
        private static DataTable dtStats
        {
            get
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtStats"] == null))
                {
                    HttpRuntime.Cache.Insert("dtStats", new DataTable(), null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                    return HttpRuntime.Cache["dtStats"] as DataTable;
                }
                else
                    return HttpRuntime.Cache["dtStats"] as DataTable;
            }
            set
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtStats"] == null))
                    HttpRuntime.Cache.Insert("dtStats", value, null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                else
                    HttpRuntime.Cache["dtStats"] = value;
            }
        }

        private static DataTable dtLastLocation
        {
            get
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtLastLocation"] == null))
                {
                    HttpRuntime.Cache.Insert("dtLastLocation", new DataTable(), null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                    return HttpRuntime.Cache["dtLastLocation"] as DataTable;
                }
                else
                    return HttpRuntime.Cache["dtLastLocation"] as DataTable;
            }
            set
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtLastLocation"] == null))
                    HttpRuntime.Cache.Insert("dtLastLocation", value, null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                else
                    HttpRuntime.Cache["dtLastLocation"] = value;
            }
        }

        private static DataTable dtHeartBeat
        {
            get
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtHeartBeat"] == null))
                {
                    HttpRuntime.Cache.Insert("dtHeartBeat", new DataTable(), null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                    return HttpRuntime.Cache["dtHeartBeat"] as DataTable;
                }
                else
                    return HttpRuntime.Cache["dtHeartBeat"] as DataTable;
            }
            set
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtHeartBeat"] == null))
                    HttpRuntime.Cache.Insert("dtHeartBeat", value, null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                else
                    HttpRuntime.Cache["dtHeartBeat"] = value;
            }
        }

        private static DataTable dtEnforcementService
        {
            get
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtEnforcementService"] == null))
                {
                    HttpRuntime.Cache.Insert("dtEnforcementService", new DataTable(), null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                    return HttpRuntime.Cache["dtEnforcementService"] as DataTable;
                }
                else
                    return HttpRuntime.Cache["dtEnforcementService"] as DataTable;
            }
            set
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtEnforcementService"] == null))
                    HttpRuntime.Cache.Insert("dtEnforcementService", value, null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                else
                    HttpRuntime.Cache["dtEnforcementService"] = value;
            }
        }

        private static DataTable dtESOfficer
        {
            get
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtESOfficer"] == null))
                {
                    HttpRuntime.Cache.Insert("dtESOfficer", new DataTable(), null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                    return HttpRuntime.Cache["dtESOfficer"] as DataTable;
                }
                else
                    return HttpRuntime.Cache["dtESOfficer"] as DataTable;
            }
            set
            {
                if ((HttpRuntime.Cache == null) || (HttpRuntime.Cache["dtESOfficer"] == null))
                    HttpRuntime.Cache.Insert("dtESOfficer", value, null, DateTime.Now.AddHours(12), System.Web.Caching.Cache.NoSlidingExpiration);
                else
                    HttpRuntime.Cache["dtESOfficer"] = value;
            }
        }

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static string Token = "";
        private static string ServerName = ConfigurationManager.AppSettings["ServerName"].ToString();

        public DashboardHub()
        {
            startCaseActionTimer();
            startHBTimer();
            startLLTimer();
            startWMATimer();
            startESRequestTimer();
            startESOfficerTimer();
        }

        private void startLLTimer()
        {
            if (myTimerLL == null)
            {
                getlastlocationfordate();
                myTimerLL = new Timer();
                myTimerLL.Elapsed += new ElapsedEventHandler(getlastlocationfordate_timer);
                myTimerLL.Interval = 30000;
                myTimerLL.Start();
            }
        }

        private void startHBTimer()
        {
            if (myTimerHB == null)
            {
                gethbfordate();
                myTimerHB = new Timer();
                myTimerHB.Elapsed += new ElapsedEventHandler(gethbfordate_timer);
                myTimerHB.Interval = 30000;
                myTimerHB.Start();
            }
        }

        private void startCaseActionTimer()
        {
            if (myTimer == null)
            {
                getcaseactionfordate();
                myTimer = new Timer();
                myTimer.Elapsed += new ElapsedEventHandler(getcaseactionfordate_timer);
                myTimer.Interval = 30000;
                myTimer.Start();
            }
        }

        private void startWMATimer()
        {
            if (myTimerWMA == null)
            {
                getwmafordate();
                myTimerWMA = new Timer();
                myTimerWMA.Elapsed += new ElapsedEventHandler(getwmafordate_timer);
                myTimerWMA.Interval = 30000;
                myTimerWMA.Start();
            }
        }

        private void startESRequestTimer()
        {
            if (myTimerESRequest == null)
            {
                getallrequestdashbord();
                myTimerESRequest = new Timer();
                myTimerESRequest.Elapsed += new ElapsedEventHandler(getallrequestdashboard_timer);
                myTimerESRequest.Interval = 30000;
                myTimerESRequest.Start();
            }
        }

        private void startESOfficerTimer()
        {
            if (myTimerESOfficer == null)
            {
                getesofficers();
                myTimerESOfficer = new Timer();
                myTimerESOfficer.Elapsed += new ElapsedEventHandler(getesofficers_timer);
                myTimerESOfficer.Interval = 30000;
                myTimerESOfficer.Start();
            }
        }

        #region FirstCall

        public void getmanagertree(int managerid)
        {
            DataTable dt = new DataTable();
            try
            {
                Token = Context.QueryString["Authorization"];
                Clients.Caller.updateYourself("getmanagertree entered in server");                
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/officers/" + managerid + "/OfficersManager";
                HttpClient client = new HttpClient();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);

                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!                
                var readAsStringAsync = response.Content.ReadAsStringAsync();

                string message = readAsStringAsync.Result;

                if (!message.Contains("Authorization has been denied for this request"))
                    dtManagerTree = ConvertJSONToDataTable(CleanJson(message));

                Clients.Caller.getmanagertree(JSONHelper.FromDataTable(dtManagerTree));
            }
            catch (AggregateException ex)
            {
                log.Info( ServerName + "get manager tree exception :-" + ex.Message);
                log.Info( ServerName + "get manager tree exception :-" + ex.StackTrace);
                log.Info( ServerName + "get manager tree exception :-" + ex.InnerException);
            }
            catch (System.Net.WebException ex)
            {
                log.Info( ServerName + "get manager tree exception :-" + ex.Message);
                log.Info( ServerName + "get manager tree exception :-" + ex.StackTrace);
                log.Info( ServerName + "get manager tree exception :-" + ex.InnerException);
            }
            catch (Exception ex)
            {
                log.Info( ServerName + "get manager tree exception :-" + ex.Message);
                log.Info( ServerName + "get manager tree exception :-" + ex.StackTrace);
                log.Info( ServerName + "get manager tree exception :-" + ex.InnerException);
            }
        }

        public void getofficerstree()
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/officers/OfficersTree";
                HttpClient client = new HttpClient();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Context.QueryString["Authorization"]);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;

                if (message.Length > 0)
                {
                    if (!message.Contains("Authorization has been denied for this request"))
                        dtOfficersTree = ConvertJSONToDataTable(CleanJson(message));

                    log.Info(ServerName + "Timer - getofficertree");
                }
            }
            catch (AggregateException ex)
            {
                log.Info( ServerName + "getofficerstree exception :-" + ex.Message);
                log.Info( ServerName + "getofficerstree exception :-" + ex.StackTrace);
                log.Info( ServerName + "getofficerstree exception :-" + ex.InnerException);
            }
            catch (Exception ex)
            {
                log.Info( ServerName + "getofficerstree exception :-" + ex.Message);
                log.Info( ServerName + "getofficerstree exception :-" + ex.StackTrace);
                log.Info( ServerName + "getofficerstree exception :-" + ex.InnerException);
            }
        }

        public void getcaseactionfordate_timer(object source, ElapsedEventArgs e)
        {
            getcaseactionfordate();
        }

        public void getesofficers_timer(object source, ElapsedEventArgs e)
        {
            getesofficers();
        }        

        public DataTable getmanagertreedt(int managerid)
        {
            DataTable dt = new DataTable();
            try
            {
                Token = Context.QueryString["Authorization"];
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/officers/" + managerid + "/OfficersManager";
                HttpClient client = new HttpClient();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);

                HttpResponseMessage response = client.GetAsync("").Result;
                var readAsStringAsync = response.Content.ReadAsStringAsync();

                string message = readAsStringAsync.Result;

                if (!message.Contains("Authorization has been denied for this request"))
                    dt = ConvertJSONToDataTable(CleanJson(message));

                log.Info(ServerName + "Timer - getmanagertreedt");

                return dt;
            }
            catch (AggregateException ex)
            {
                log.Info( ServerName + "get manager tree exception :-" + ex.Message);
                log.Info( ServerName + "get manager tree exception :-" + ex.StackTrace);
                log.Info( ServerName + "get manager tree exception :-" + ex.InnerException);
                return dt;
            }
            catch (System.Net.WebException ex)
            {
                log.Info( ServerName + "get manager tree exception :-" + ex.Message);
                log.Info( ServerName + "get manager tree exception :-" + ex.StackTrace);
                log.Info( ServerName + "get manager tree exception :-" + ex.InnerException);
                return dt;
            }
            catch (Exception ex)
            {
                log.Info( ServerName + "get manager tree exception :-" + ex.Message);
                log.Info( ServerName + "get manager tree exception :-" + ex.StackTrace);
                log.Info( ServerName + "get manager tree exception :-" + ex.InnerException);
                return dt;
            }
        }

        public void getcaseactionfordate()
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/cases/" + DateTime.Now.ToString("yyyy-MM-dd") + "/CaseActionForDate";// "GetCaseActionForDate_Dashboard";
                HttpClient client = new HttpClient();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                if (message.Length > 0)
                {
                    if (!message.Contains("Authorization has been denied for this request"))
                    {
                        dtCaseActions = ConvertJSONToDataTable(CleanJson(message));
                        broadcastclients(ServerName);
                        broadcastclients("CA");
                    }

                    log.Info(ServerName + "Timer - getcaseactionfordate");
                }
            }
            catch (AggregateException ex)
            {
                log.Info( ServerName + "getcaseactionfordate - exception :-" + ex.Message);
                log.Info( ServerName + "getcaseactionfordate - exception :-" + ex.StackTrace);
                log.Info( ServerName + "getcaseactionfordate - exception :-" + ex.InnerException);
            }
            catch (Exception ex)
            {
                log.Info( ServerName + "getcaseactionfordate - exception :-" + ex.Message);
                log.Info( ServerName + "getcaseactionfordate - exception :-" + ex.StackTrace);
                log.Info( ServerName + "getcaseactionfordate - exception :-" + ex.InnerException);
            }
        }

        public void getwmafordate_timer(object source, ElapsedEventArgs e)
        {
            getwmafordate();
        }

        public void getwmafordate()
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/officers/" + DateTime.Now.ToString("yyyy-MM-dd") + "/WMAForDate";//"GetWMAForDate_Dashboard";
                HttpClient client = new HttpClient();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                if (message.Length > 0)
                {
                    if (!message.Contains("Authorization has been denied for this request"))
                    {
                        dtWMA = ConvertJSONToDataTable(CleanJson(message));
                        broadcastclients(ServerName);
                        broadcastclients("WMA");
                        log.Info(ServerName + "Timer - getwmafordate");
                    }                        
                }
            }
            catch (AggregateException ex)
            {
                log.Info( ServerName + "getwmafordate - exception :-" + ex.Message);
                log.Info( ServerName + "getwmafordate - exception :-" + ex.StackTrace);
                log.Info( ServerName + "getwmafordate - exception :-" + ex.InnerException);
            }
            catch (Exception ex)
            {
                log.Info(ServerName + "getwmafordate - exception :-" + ex.Message);
                log.Info( ServerName + "getwmafordate - exception :-" + ex.StackTrace);
                log.Info( ServerName + "getwmafordate - exception :-" + ex.InnerException);
            }
        }

        public void getlastlocationfordate_timer(object source, ElapsedEventArgs e)
        {
            getlastlocationfordate();
        }

        public void getlastlocationfordate()
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/officers/" + DateTime.Now.ToString("yyyy-MM-dd") + "/GetLastLocationForDate";//"GetLastLocationForDate_Dashboard";
                HttpClient client = new HttpClient();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                if (message.Length > 0)
                {
                    if (!message.Contains("Authorization has been denied for this request"))
                    {
                        dtLastLocation = ConvertJSONToDataTable(CleanJson(message));
                        broadcastclients(ServerName);
                        broadcastclients("LL");
                        log.Info(ServerName + "Timer - getlastlocationfordate");
                    }
                        
                }

            }
            catch (AggregateException ex)
            {
                log.Info(ServerName + "getlastlocationfordate - exception :-" + ex.Message);
                log.Info( ServerName  + "getlastlocationfordate - exception :-" + ex.StackTrace);
                log.Info( ServerName  + "getlastlocationfordate - exception :-" + ex.InnerException);
            }
            catch (Exception ex)
            {
                log.Info(ServerName + "getlastlocationfordate - exception :-" + ex.Message);
                log.Info( ServerName  + "getlastlocationfordate - exception :-" + ex.StackTrace);
                log.Info( ServerName  + "getlastlocationfordate - exception :-" + ex.InnerException);
            }
        }

        public void gethbfordate_timer(object source, ElapsedEventArgs e)
        {
            gethbfordate();
        }

        public void gethbfordate()
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/officers/" + DateTime.Now.ToString("yyyy-MM-dd") + "/HBForDate";//yyyy/MM/dd "GetHBForDate_Dashboard";
                HttpClient client = new HttpClient();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                if (message.Length > 0)
                {
                    if (!message.Contains("Authorization has been denied for this request"))
                    {
                        dtHeartBeat = ConvertJSONToDataTable(CleanJson(message));
                        broadcastclients(ServerName);
                        broadcastclients("HB");
                        log.Info(ServerName + "Timer - gethbfordate");
                    }                        
                }
            }
            catch (AggregateException ex)
            {
                log.Info(ServerName + "gethbfordate - exception :-" + ex.Message);
                log.Info( ServerName + "gethbfordate - exception :-" + ex.StackTrace);
                log.Info( ServerName + "gethbfordate - exception :-" + ex.InnerException);
            }
            catch (System.Net.WebException ex)
            {
                log.Info(ServerName + "gethbfordate - exception :-" + ex.Message);
                log.Info( ServerName + "gethbfordate - exception :-" + ex.StackTrace);
                log.Info( ServerName + "gethbfordate - exception :-" + ex.InnerException);
            }
            catch (Exception ex)
            {
                log.Info(ServerName + "gethbfordate - exception :-" + ex.Message);
                log.Info( ServerName + "gethbfordate - exception :-" + ex.StackTrace);
                log.Info( ServerName + "gethbfordate - exception :-" + ex.InnerException);
            }
        }

        public void getstats()
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/officers/StatsData";// "GetStatsData";
                HttpClient client = new HttpClient();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                if (message.Length > 0)
                {
                    if (!message.Contains("Authorization has been denied for this request"))
                    {
                        dtStats = ConvertJSONToDataTable(CleanJson(message));
                        broadcastclients(ServerName);
                        broadcastclients("ST");
                        log.Info(ServerName + "Timer - getstats");
                    }                        
                }
            }
            catch (AggregateException ex)
            {
                log.Info(ServerName + "getstats - exception :-" + ex.Message);
                log.Info( ServerName + "getstats - exception :-" + ex.StackTrace);
                log.Info( ServerName + "getstats - exception :-" + ex.InnerException);
            }
            catch (Exception ex)
            {
                log.Info(ServerName + "getstats - exception :-" + ex.Message);
                log.Info( ServerName + "getstats - exception :-" + ex.StackTrace);
                log.Info( ServerName + "getstats - exception :-" + ex.InnerException);
            }
        }

        public void getofficersfores()
        {
            try
            {
                if(dtESOfficer.Rows.Count>0)
                {
                    Clients.Caller.getofficersfores(JSONHelper.FromDataTable(dtESOfficer));
                }
                else
                {
                    getesofficers();
                }
            }
            catch (AggregateException ex)
            {
                log.Info(ServerName + "getofficersfores - exception :-" + ex.Message);
                log.Info( ServerName + "getofficersfores - exception :-" + ex.StackTrace);
                log.Info( ServerName + "getofficersfores - exception :-" + ex.InnerException);
            }
            catch (Exception ex)
            {
                log.Info(ServerName + "getofficersfores - exception :-" + ex.Message);
                log.Info( ServerName + "getofficersfores - exception :-" + ex.StackTrace);
                log.Info( ServerName + "getofficersfores - exception :-" + ex.InnerException);
            }
        }

        public void getesofficers()
        {
            try
            {
                Token = Context.QueryString["Authorization"];
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/OfficersforES";
                HttpClient client = new HttpClient();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                if (message!=null && message.Length > 0)
                {
                    if (!message.Contains("Authorization has been denied for this request"))
                    {
                        dtESOfficer = ConvertJSONToDataTable(CleanJson(message));
                        broadcastclients(ServerName);
                        broadcastclients("ESOFFICER");
                        log.Info(ServerName + "Timer - getesofficers");
                    }
                }
            }
            catch (TimeoutException ex)
            {
                log.Info(ServerName + "getofficers - exception Timeout :-" + ex.Message);
                log.Info(ServerName + "getofficers - exception Timeout :-" + ex.StackTrace);
                log.Info(ServerName + "getofficers - exception Timeout :-" + ex.InnerException);
            }
            catch (AggregateException ex)
            {
                log.Info(ServerName + "getofficers - exception :-" + ex.Message);
                log.Info( ServerName + "getofficers - exception :-" + ex.StackTrace);
                log.Info( ServerName + "getofficers - exception :-" + ex.InnerException);
            }
            catch (Exception ex)
            {
                log.Info(ServerName + "getesofficers - exception :-" + ex.Message);
                log.Info( ServerName + "getesofficers - exception :-" + ex.StackTrace);
                log.Info( ServerName + "getesofficers - exception :-" + ex.InnerException);
            }
        }

        public void getallrequestdashboard_timer(object source, ElapsedEventArgs e)
        {
            getallrequestdashbord();
        }

        public void getallrequestdashbord()
        {
            try
            {
                Token = Context.QueryString["Authorization"];
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/AllRequest4Dashboard";// "GetAllRequest4Dashboard_ES";
                HttpClient client = new HttpClient();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                if (message != null && message.Length > 0)
                {
                    if (!message.Contains("Authorization has been denied for this request"))
                    {
                        dtEnforcementService = ConvertJSONToDataTable(CleanJson(message));
                        broadcastclients(ServerName);
                        broadcastclients("ESREQUEST");
                        log.Info(ServerName + "Timer - getallrequestdashboard");
                    }                        
                }
            }
            catch (TimeoutException ex)
            {
                log.Info(ServerName + "getallrequestdashbord - exception Timeout :-" + ex.Message);
                log.Info(ServerName + "getallrequestdashbord - exception Timeout :-" + ex.StackTrace);
                log.Info(ServerName + "getallrequestdashbord - exception Timeout :-" + ex.InnerException);
            }
            catch (AggregateException ex)
            {
                log.Info(ServerName + "getallrequestdashbord - exception :-" + ex.Message);
                log.Info( ServerName + "getallrequestdashbord - exception :-" + ex.StackTrace);
                log.Info( ServerName + "getallrequestdashbord - exception :-" + ex.InnerException);
            }
            catch (Exception ex)
            {
                log.Info(ServerName + "getallrequestdashbord - exception :-" + ex.Message);
                log.Info( ServerName + "getallrequestdashbord - exception :-" + ex.StackTrace);
                log.Info( ServerName + "getallrequestdashbord - exception :-" + ex.InnerException);
            }
        }

        #endregion

        #region CaseAction

        public void gethubcaseactions(string caParam)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add("CaseActionStats");
            ds.Tables["CaseActionStats"].Columns.Add("ActionText");
            ds.Tables["CaseActionStats"].Columns.Add("Case");
            ds.Tables["CaseActionStats"].Columns.Add("Ave");

            if (caParam != null)
            {
                var strParam = caParam.Split(new char[] { ',' });
                int ManagerID = int.Parse(strParam[0]);
                DateTime FromDate = DateTime.Parse(strParam[1]);
                int OfficerID = int.Parse(strParam[2]);
                int GroupID = int.Parse(strParam[3]);
                int OfficerRole = int.Parse(strParam[4]);
                int CompanyID = int.Parse(strParam[5]);
                int IsArrest = 0;
                if (OfficerRole == 9) IsArrest = 1;

                int Paid, PartPaid, Returned, LeftLetter, DefendentContact, Clamped, Bailed, Arrested, Surrendered;
                int TCG, TCGPAID, TCGPP, HPAID, HPARTPAID, UTTC, DROPPED, OTHER;

                Paid = PartPaid = Returned = LeftLetter = DefendentContact = Clamped = Bailed = Arrested = Surrendered = 0;
                TCG = TCGPAID = TCGPP = HPAID = HPARTPAID = UTTC = DROPPED = OTHER = 0;
                try
                {
                    if (ManagerID != 9999 && ManagerID != 6999)
                    {
                        DataTable dtFilteredCaseActions = new DataTable();
                        DataRow[] drFilteredCaseActions;
                        if ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0))
                        {
                            if (CompanyID == 1)
                                drFilteredCaseActions = dtCaseActions.Select("ManagerID = '" + ManagerID + "' AND IsArrest=" + IsArrest + " AND CompanyID=" + CompanyID);
                            else
                                drFilteredCaseActions = dtCaseActions.Select("ManagerID = '" + ManagerID + "' AND CompanyID=" + CompanyID);

                            if (drFilteredCaseActions.Length > 0)
                                dtFilteredCaseActions = drFilteredCaseActions.CopyToDataTable();
                            else
                            {
                                if (CompanyID == 1)
                                    drFilteredCaseActions = dtCaseActions.Select("ADMID = '" + ManagerID + "' AND IsArrest=" + IsArrest + " AND CompanyID=" + CompanyID);
                                else
                                    drFilteredCaseActions = dtCaseActions.Select("ADMID = '" + ManagerID + "' AND CompanyID=" + CompanyID);

                                if (drFilteredCaseActions.Length > 0)
                                    dtFilteredCaseActions = drFilteredCaseActions.CopyToDataTable();
                                else
                                {
                                    if (CompanyID == 1)
                                        drFilteredCaseActions = dtCaseActions.Select("Officer = '" + ((ManagerID == 0) ? OfficerID : ManagerID) + "' AND IsArrest=" + IsArrest + " AND CompanyID=" + CompanyID);
                                    else
                                        drFilteredCaseActions = dtCaseActions.Select("Officer = '" + ((ManagerID == 0) ? OfficerID : ManagerID) + "' AND CompanyID=" + CompanyID);

                                    if (drFilteredCaseActions.Length > 0)
                                        dtFilteredCaseActions = drFilteredCaseActions.CopyToDataTable();
                                }
                            }
                        }

                        if (CompanyID == 1)
                        {
                            Paid = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Paid' AND IsArrest=" + IsArrest).Length : 0;
                            PartPaid = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Part Paid' AND IsArrest=" + IsArrest).Length : 0;
                            Returned = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Returned' AND IsArrest=" + IsArrest).Length : 0;
                            LeftLetter = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Left Letter' AND IsArrest=" + IsArrest).Length : 0;
                            DefendentContact = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Defendant contact' AND IsArrest=" + IsArrest).Length : 0;
                            Clamped = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Clamped'").Length : 0;
                            Bailed = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Bailed'").Length : 0;
                            Arrested = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Arrested'").Length : 0;
                            Surrendered = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Surrender date agreed'").Length : 0;
                        }
                        else
                        {
                            TCG = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'TCG'").Length : 0;
                            TCGPAID = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'TCG PAID'").Length : 0;
                            TCGPP = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'TCG PP'").Length : 0;
                            HPAID = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'PAID'").Length : 0;
                            HPARTPAID = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'PART PAID'").Length : 0;
                            UTTC = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'UTTC'").Length : 0;
                            DROPPED = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'DROPPED'").Length : 0;
                            OTHER = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'OTHER'").Length : 0;
                        }
                    }
                    else
                    {
                        if (CompanyID == 1)
                        {
                            Paid = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'Paid' AND IsArrest=" + IsArrest).Length : 0;
                            PartPaid = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'Part Paid' AND IsArrest=" + IsArrest).Length : 0;
                            Returned = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'Returned' AND IsArrest=" + IsArrest).Length : 0;
                            LeftLetter = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'Left Letter' AND IsArrest=" + IsArrest).Length : 0;
                            DefendentContact = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'Defendant contact' AND IsArrest=" + IsArrest).Length : 0;
                            Clamped = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'Clamped'").Length : 0;
                            Bailed = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'Bailed'").Length : 0;
                            Arrested = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'Arrested'").Length : 0;
                            Surrendered = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'Surrender date agreed'").Length : 0;
                        }
                        else
                        {
                            TCG = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'TCG'").Length : 0;
                            TCGPAID = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'TCG PAID'").Length : 0;
                            TCGPP = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'TCG PP'").Length : 0;
                            HPAID = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'PAID'").Length : 0;
                            HPARTPAID = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'PART PAID'").Length : 0;
                            UTTC = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'UTTC'").Length : 0;
                            DROPPED = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'DROPPED'").Length : 0;
                            OTHER = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = 'OTHER'").Length : 0;
                        }
                    }
                    DataRow dr;
                    if (CompanyID == 1)
                    {
                        if (IsArrest == 0)
                        {
                            dr = ds.Tables["CaseActionStats"].NewRow();
                            dr["ActionText"] = "Paid";
                            dr["Case"] = Paid;
                            dr["Ave"] = "0";
                            ds.Tables["CaseActionStats"].Rows.Add(dr);

                            dr = ds.Tables["CaseActionStats"].NewRow();
                            dr["ActionText"] = "Part Paid";
                            dr["Case"] = PartPaid;
                            dr["Ave"] = "0";
                            ds.Tables["CaseActionStats"].Rows.Add(dr);

                            dr = ds.Tables["CaseActionStats"].NewRow();
                            dr["ActionText"] = "Returned";
                            dr["Case"] = Returned;
                            dr["Ave"] = "0";
                            ds.Tables["CaseActionStats"].Rows.Add(dr);

                            dr = ds.Tables["CaseActionStats"].NewRow();
                            dr["ActionText"] = "Left Letter";
                            dr["Case"] = LeftLetter;
                            dr["Ave"] = "0";
                            ds.Tables["CaseActionStats"].Rows.Add(dr);

                            dr = ds.Tables["CaseActionStats"].NewRow();
                            dr["ActionText"] = "Defendant contact";
                            dr["Case"] = DefendentContact;
                            dr["Ave"] = "0";
                            ds.Tables["CaseActionStats"].Rows.Add(dr);

                            dr = ds.Tables["CaseActionStats"].NewRow();
                            dr["ActionText"] = "Clamped";
                            dr["Case"] = Clamped;
                            dr["Ave"] = "0";
                            ds.Tables["CaseActionStats"].Rows.Add(dr);
                        }
                        else
                        {
                            dr = ds.Tables["CaseActionStats"].NewRow();
                            dr["ActionText"] = "Paid";
                            dr["Case"] = Paid;
                            dr["Ave"] = "0";
                            ds.Tables["CaseActionStats"].Rows.Add(dr);

                            dr = ds.Tables["CaseActionStats"].NewRow();
                            dr["ActionText"] = "Left Letter";
                            dr["Case"] = LeftLetter;
                            dr["Ave"] = "0";
                            ds.Tables["CaseActionStats"].Rows.Add(dr);

                            dr = ds.Tables["CaseActionStats"].NewRow();
                            dr["ActionText"] = "Bailed";
                            dr["Case"] = Bailed;
                            dr["Ave"] = "0";
                            ds.Tables["CaseActionStats"].Rows.Add(dr);

                            dr = ds.Tables["CaseActionStats"].NewRow();
                            dr["ActionText"] = "Arrested";
                            dr["Case"] = Arrested;
                            dr["Ave"] = "0";
                            ds.Tables["CaseActionStats"].Rows.Add(dr);

                            dr = ds.Tables["CaseActionStats"].NewRow();
                            dr["ActionText"] = "Surrender date agreed";
                            dr["Case"] = Surrendered;
                            dr["Ave"] = "0";
                            ds.Tables["CaseActionStats"].Rows.Add(dr);

                            dr = ds.Tables["CaseActionStats"].NewRow();
                            dr["ActionText"] = "Returned";
                            dr["Case"] = Returned;
                            dr["Ave"] = "0";
                            ds.Tables["CaseActionStats"].Rows.Add(dr);
                        }
                    }
                    else
                    {
                        dr = ds.Tables["CaseActionStats"].NewRow();
                        dr["ActionText"] = "TCG";
                        dr["Case"] = TCG;
                        dr["Ave"] = "0";
                        ds.Tables["CaseActionStats"].Rows.Add(dr);

                        dr = ds.Tables["CaseActionStats"].NewRow();
                        dr["ActionText"] = "TCG PAID";
                        dr["Case"] = TCGPAID;
                        dr["Ave"] = "0";
                        ds.Tables["CaseActionStats"].Rows.Add(dr);

                        dr = ds.Tables["CaseActionStats"].NewRow();
                        dr["ActionText"] = "TCG PP";
                        dr["Case"] = TCGPP;
                        dr["Ave"] = "0";
                        ds.Tables["CaseActionStats"].Rows.Add(dr);

                        dr = ds.Tables["CaseActionStats"].NewRow();
                        dr["ActionText"] = "PAID";
                        dr["Case"] = HPAID;
                        dr["Ave"] = "0";
                        ds.Tables["CaseActionStats"].Rows.Add(dr);

                        dr = ds.Tables["CaseActionStats"].NewRow();
                        dr["ActionText"] = "PART PAID";
                        dr["Case"] = HPARTPAID;
                        dr["Ave"] = "0";
                        ds.Tables["CaseActionStats"].Rows.Add(dr);

                        dr = ds.Tables["CaseActionStats"].NewRow();
                        dr["ActionText"] = "UTTC";
                        dr["Case"] = UTTC;
                        dr["Ave"] = "0";
                        ds.Tables["CaseActionStats"].Rows.Add(dr);

                        dr = ds.Tables["CaseActionStats"].NewRow();
                        dr["ActionText"] = "DROPPED";
                        dr["Case"] = DROPPED;
                        dr["Ave"] = "0";
                        ds.Tables["CaseActionStats"].Rows.Add(dr);

                        dr = ds.Tables["CaseActionStats"].NewRow();
                        dr["ActionText"] = "OTHER";
                        dr["Case"] = OTHER;
                        dr["Ave"] = "0";
                        ds.Tables["CaseActionStats"].Rows.Add(dr);
                    }
                }
                catch (AggregateException ex)
                {
                    log.Info(ServerName + "gethubcaseactions - exception :-" + ex.Message);
                    log.Info( ServerName + "gethubcaseactions - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "gethubcaseactions - exception :-" + ex.InnerException);
                }
                catch (Exception ex)
                {
                    log.Info(ServerName + "gethubcaseactions - exception :-" + ex.Message);
                    log.Info( ServerName + "gethubcaseactions - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "gethubcaseactions - exception :-" + ex.InnerException);
                }
            }
            Clients.Caller.gethubcaseactions(JSONHelper.FromDataTable(ds.Tables["CaseActionStats"]));
        }

        public void getcaseactions(string urlparam)
        {
            DataTable dtFilteredOfficer = new DataTable();
            if (urlparam != null)
            {
                var strParam = urlparam.Split(new char[] { ',' });
                int ManagerID = int.Parse(strParam[0]);
                DateTime FromDate = DateTime.Parse(strParam[1]);
                DateTime ToDate = DateTime.Parse(strParam[2]);
                int OfficerID = int.Parse(strParam[3]);
                int GroupID = int.Parse(strParam[4]);
                int CompanyID = int.Parse(strParam[5]);

                try
                {
                    if (OfficerID > 0)
                    {
                        if ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0))
                        {
                            if (dtCaseActions.Select("Officer='" + OfficerID + "' AND CompanyID=" + CompanyID).Length > 0)
                                dtFilteredOfficer = dtCaseActions.Select("Officer='" + OfficerID + "' AND CompanyID=" + CompanyID).CopyToDataTable();
                            ManagerID = 0;
                        }
                    }
                    else if (ManagerID > 0)
                    {
                        if ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0))
                            dtFilteredOfficer = dtCaseActions.Select("ManagerID='" + ManagerID + "' AND CompanyID=" + CompanyID).CopyToDataTable();
                    }
                }
                catch (Exception ex)
                {
                    log.Info(ServerName + "getcaseactions - exception :-" + ex.Message);
                    log.Info( ServerName + "getcaseactions - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getcaseactions - exception :-" + ex.InnerException);
                }
            }
            Clients.Caller.getcaseactions(JSONHelper.FromDataTable(dtFilteredOfficer));
        }

        public void getcaseactionmanager(string caParam, string flag, string type)
        {
            DataTable dt = new DataTable("CAManager");
            dt.Columns.Add("OfficerID");
            dt.Columns.Add("OfficerName");
            dt.Columns.Add("CaseCnt");
            if (caParam != null)
            {
                var strParam = caParam.Split(new char[] { ',' });
                string ActionText = strParam[0].ToString();
                DateTime FromDate = DateTime.Parse(strParam[1]);
                DateTime ToDate = DateTime.Parse(strParam[2]);
                int CompanyID = int.Parse(strParam[3]);
                int ManagerID = int.Parse(strParam[4]);
                int GroupID = int.Parse(strParam[4]);
                int OfficerRole = int.Parse(strParam[6]);

                int IsArrest = 0;
                if (OfficerRole == 9) IsArrest = 1;

                try
                {
                    if (ManagerID == 6999 || ManagerID == 9999)
                    {
                        if (CompanyID == 1)
                        {
                            if ((dtOfficersTree != null) && (dtOfficersTree.Rows.Count > 0))
                            {
                                var qOfficer = from r in dtOfficersTree.AsEnumerable()
                                               let otArray = new object[]
                                    {
                                    r.Field<string>("ManagerID"), r.Field<string>("ManagerName"), 0
                                    }
                                               select otArray;

                                foreach (var dr in qOfficer)
                                {
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                    }
                    if (type == "ADM")
                    {
                        if ((dtOfficersTree != null) && (dtOfficersTree.Rows.Count > 0))
                        {
                            DataRow[] drFilteredOfficer = dtOfficersTree.Select("ManagerID='" + ManagerID + "'");
                            DataTable dtFilteredOfficer = (drFilteredOfficer.Length > 0 ? drFilteredOfficer.CopyToDataTable() : new DataTable());
                            var qOfficer = (from r in dtFilteredOfficer.AsEnumerable()
                                            let otArray = new object[]
                                    {
                                    r.Field<string>("ADMID"), r.Field<string>("ADMName"), 0
                                    }
                                            select otArray);
                            foreach (var dr in qOfficer)
                            {
                                dt.Rows.Add(dr);
                            }
                        }
                    }
                    else if (type == "Officer")
                    {
                        if ((dtOfficersTree != null) && (dtOfficersTree.Rows.Count > 0))
                        {
                            DataRow[] drFilteredOfficer = dtOfficersTree.Select("ADMID='" + ManagerID + "'");
                            DataTable dtFilteredOfficer = (drFilteredOfficer.Length > 0 ? drFilteredOfficer.CopyToDataTable() : new DataTable());
                            var qOfficer = (from r in dtFilteredOfficer.AsEnumerable()
                                            let otArray = new object[]
                                    {
                                    r.Field<string>("OfficerID"), r.Field<string>("OfficerName"), 0
                                    }
                                            select otArray);
                            foreach (var dr in qOfficer)
                            {
                                dt.Rows.Add(dr);
                            }
                        }
                    }

                    DataView view = new DataView(dt);
                    dt = view.ToTable(true, "OfficerID", "OfficerName", "CaseCnt");
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (type == "Manager")
                        {
                            DataRow[] cRow = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = '" + ActionText + "' AND ManagerID='" + dr[0].ToString() + "' AND CompanyID=" + CompanyID + " AND IsArrest=" + IsArrest) : null;
                            DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                            HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                        }
                        else if (type == "ADM")
                        {
                            DataRow[] cRow = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = '" + ActionText + "' AND ADMID='" + dr[0].ToString() + "' AND CompanyID=" + CompanyID + " AND IsArrest=" + IsArrest) : null;
                            DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                            HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                        }
                        else if (type == "Officer")
                        {
                            DataRow[] cRow = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = '" + ActionText + "' AND Officer='" + dr[0].ToString() + "' AND CompanyID=" + CompanyID + " AND IsArrest=" + IsArrest) : null;
                            DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                            HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                        }
                    }
                    dt = dt.Select("", "CaseCnt DESC").CopyToDataTable();
                    Clients.Caller.getcaseactionmanager(JSONHelper.FromDataTable(dt), flag, type);
                }
                catch (AggregateException ex)
                {
                    Clients.Caller.getcaseactionmanager(JSONHelper.FromDataTable(dt), flag, type);
                }
                catch (Exception ex)
                {
                    Clients.Caller.getcaseactionmanager(JSONHelper.FromDataTable(dt), flag, type);
                }
            }
            else
            {
                Clients.Caller.getcaseactionmanager(JSONHelper.FromDataTable(dt), flag, type);
            }
        }

        #endregion

        #region WMA

        public void getwmadetail(string caParam, string type)
        {
            DataTable dt = new DataTable("WMADetail");
            dt.Columns.Add("OfficerID");
            dt.Columns.Add("OfficerName");
            dt.Columns.Add("CaseCnt");
            dt.Columns.Add("FromDate");

            if (caParam != null)
            {
                var strParam = caParam.Split(new char[] { ',' });
                int ManagerID = int.Parse(strParam[0]);
                string FromDate = strParam[1].ToString();
                int OfficerRole = int.Parse(strParam[2]);
                int CompanyID = int.Parse(strParam[3]);

                int IsArrest = 0;
                if (OfficerRole == 9) IsArrest = 1;
                try
                {
                    if (ManagerID == 6999 || ManagerID == 9999 || ManagerID == 8997)
                    {
                        if ((dtWMA != null) && (dtWMA.Rows.Count > 0))
                        {
                            DataRow[] drFilteredOfficer = dtWMA.Select("IsArrest=" + IsArrest);
                            DataTable dtFilteredOfficer = (drFilteredOfficer.Length > 0 ? drFilteredOfficer.CopyToDataTable() : new DataTable());
                            var qOfficer = (from r in dtFilteredOfficer.AsEnumerable()
                                            let otArray = new object[]
                                        {
                                        r.Field<string>("ManagerID"), r.Field<string>("ManagerName"), 0,FromDate//,r.Field<string>("SearchText"),r.Field<string>("TotalRecordCount"),r.Field<string>("LogType"),r.Field<string>("ActionedDate")
                                        }
                                            select otArray);

                            foreach (var dr in qOfficer)
                            {
                                dt.Rows.Add(dr);
                            }
                        }
                    }
                    if (type == "ADM")
                    {
                        if ((dtWMA != null) && (dtWMA.Rows.Count > 0))
                        {
                            DataRow[] drFilteredOfficer = dtWMA.Select("ManagerID='" + ManagerID + "' AND IsArrest=" + IsArrest);
                            DataTable dtFilteredOfficer = (drFilteredOfficer.Length > 0 ? drFilteredOfficer.CopyToDataTable() : new DataTable());

                            var qOfficer = (from r in dtFilteredOfficer.AsEnumerable()
                                            let otArray = new object[]
                                        {
                                        r.Field<string>("ADMID"), r.Field<string>("ADMName"), 0,FromDate//,r.Field<string>("SearchText"),r.Field<string>("TotalRecordCount"),r.Field<string>("LogType"),r.Field<string>("ActionedDate")
                                        }
                                            select otArray);

                            foreach (var dr in qOfficer)
                            {
                                dt.Rows.Add(dr);
                            }
                        }
                    }
                    else if (type == "Officer")
                    {
                        if ((dtWMA != null) && (dtWMA.Rows.Count > 0))
                        {
                            DataRow[] drFilteredOfficer = dtWMA.Select("ADMID='" + ManagerID + "' AND IsArrest=" + IsArrest);
                            DataTable dtFilteredOfficer = (drFilteredOfficer.Length > 0 ? drFilteredOfficer.CopyToDataTable() : new DataTable());

                            var qOfficer = (from r in dtFilteredOfficer.AsEnumerable()
                                            let otArray = new object[]
                                        {
                                        r.Field<string>("OfficerId"), r.Field<string>("OfficerName"), 0, FromDate//,r.Field<string>("SearchText"),r.Field<string>("TotalRecordCount"),r.Field<string>("LogType"),r.Field<string>("ActionedDate")
                                        }
                                            select otArray);

                            foreach (var dr in qOfficer)
                            {
                                dt.Rows.Add(dr);
                            }
                        }
                    }

                    DataView view = new DataView(dt);
                    dt = view.ToTable(true, "OfficerID", "OfficerName", "CaseCnt", "FromDate");//, "SearchText", "TotalRecordCount", "LogType", "ActionedDate"

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (type == "Manager")
                        {
                            DataRow[] cRow = ((dtWMA != null) && (dtWMA.Rows.Count > 0)) ? dtWMA.Select("ManagerID='" + dr[0].ToString() + "' AND IsArrest=" + IsArrest) : null;
                            DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                            HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                        }
                        else if (type == "ADM")
                        {
                            DataRow[] cRow = ((dtWMA != null) && (dtWMA.Rows.Count > 0)) ? dtWMA.Select("ADMID='" + dr[0].ToString() + "' AND IsArrest=" + IsArrest) : null;
                            DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                            HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                        }
                        else if (type == "Officer")
                        {
                            DataRow[] cRow = ((dtWMA != null) && (dtWMA.Rows.Count > 0)) ? dtWMA.Select("OfficerId='" + dr[0].ToString() + "' AND IsArrest=" + IsArrest) : null;
                            DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                            HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                        }
                    }
                    if (dt.Rows.Count > 0)
                        dt = dt.Select("", "CaseCnt DESC").CopyToDataTable();

                    Clients.Caller.getwmadetail(JSONHelper.FromDataTable(dt), type);
                }
                catch (AggregateException ex)
                {
                    //Clients.Caller.getwmadetail(JSONHelper.FromDataTable(dt), type);
                    log.Info(ServerName + "getwmadetail - exception :-" + ex.Message);
                    log.Info( ServerName + "getwmadetail - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getwmadetail - exception :-" + ex.InnerException);
                }
                catch (Exception ex)
                {
                    //Clients.Caller.getwmadetail(JSONHelper.FromDataTable(dt), type);
                    log.Info(ServerName + "getwmadetail - exception :-" + ex.Message);
                    log.Info( ServerName + "getwmadetail - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getwmadetail - exception :-" + ex.InnerException);
                }
            }
            else
                Clients.Caller.getwmadetail(JSONHelper.FromDataTable(dt), type);
        }

        public void getwmalogdetail(string caParam, string type)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add("WMALogType");
            ds.Tables["WMALogType"].Columns.Add("OfficerID");
            ds.Tables["WMALogType"].Columns.Add("FromDate");
            ds.Tables["WMALogType"].Columns.Add("LogType");
            ds.Tables["WMALogType"].Columns.Add("Case");
            if (caParam != null)
            {
                var strParam = caParam.Split(new char[] { ',' });
                int OfficerID = int.Parse(strParam[0]);
                string FromDate = strParam[1].ToString();

                int Postcode, Vrm, Caseno;
                Postcode = Vrm = Caseno = 0;
                try
                {
                    if (OfficerID != 9999 && OfficerID != 6999)
                    {
                        DataTable dtFilteredWMA = new DataTable();
                        DataRow[] drFilteredWMA;
                        if ((dtWMA != null) && (dtWMA.Rows.Count > 0))
                        {
                            drFilteredWMA = dtWMA.Select("ManagerID = '" + OfficerID + "'");
                            if (drFilteredWMA.Length > 0)
                                dtFilteredWMA = drFilteredWMA.CopyToDataTable();
                            else
                            {
                                drFilteredWMA = dtWMA.Select("ADMID = '" + OfficerID + "'");
                                if (drFilteredWMA.Length > 0)
                                    dtFilteredWMA = drFilteredWMA.CopyToDataTable();
                                else
                                {
                                    drFilteredWMA = dtWMA.Select("OfficerId = '" + ((OfficerID == 0) ? OfficerID : OfficerID) + "'");
                                    if (drFilteredWMA.Length > 0)
                                        dtFilteredWMA = drFilteredWMA.CopyToDataTable();
                                }
                            }
                        }
                        Postcode = (dtFilteredWMA != null) ? dtFilteredWMA.Select("LogType = 'Postcode'").Length : 0;
                        Vrm = (dtFilteredWMA != null) ? dtFilteredWMA.Select("LogType = 'VRM'").Length : 0;
                        Caseno = (dtFilteredWMA != null) ? dtFilteredWMA.Select("LogType = 'CaseNo'").Length : 0;
                    }
                    else
                    {
                        Postcode = ((dtWMA != null) && (dtWMA.Rows.Count > 0)) ? dtWMA.Select("LogType = 'Postcode'").Length : 0;
                        Vrm = ((dtWMA != null) && (dtWMA.Rows.Count > 0)) ? dtWMA.Select("LogType = 'VRM'").Length : 0;
                        Caseno = ((dtWMA != null) && (dtWMA.Rows.Count > 0)) ? dtWMA.Select("LogType = 'CaseNo'").Length : 0;
                    }

                    DataRow dr;
                    dr = ds.Tables["WMALogType"].NewRow();
                    dr["OfficerID"] = type;
                    dr["FromDate"] = FromDate;
                    dr["LogType"] = "Postcode";
                    dr["Case"] = Postcode;
                    ds.Tables["WMALogType"].Rows.Add(dr);

                    dr = ds.Tables["WMALogType"].NewRow();
                    dr["OfficerID"] = type;
                    dr["FromDate"] = FromDate;
                    dr["LogType"] = "VRM";
                    dr["Case"] = Vrm;
                    ds.Tables["WMALogType"].Rows.Add(dr);

                    dr = ds.Tables["WMALogType"].NewRow();
                    dr["OfficerID"] = type;
                    dr["FromDate"] = FromDate;
                    dr["LogType"] = "CaseNo";
                    dr["Case"] = Caseno;
                    ds.Tables["WMALogType"].Rows.Add(dr);

                    Clients.Caller.getwmalogdetail(JSONHelper.FromDataTable(ds.Tables["WMALogType"]));
                }
                catch (AggregateException ex)
                {
                    //Clients.Caller.getwmalogdetail(JSONHelper.FromDataTable(ds.Tables["WMALogType"]));
                    log.Info(ServerName + "getwmalogdetail - exception :-" + ex.Message);
                    log.Info( ServerName + "getwmalogdetail - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getwmalogdetail - exception :-" + ex.InnerException);
                }
                catch (Exception ex)
                {
                    //Clients.Caller.getwmalogdetail(JSONHelper.FromDataTable(ds.Tables["WMALogType"]));
                    log.Info(ServerName + "getwmalogdetail - exception :-" + ex.Message);
                    log.Info( ServerName + "getwmalogdetail - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getwmalogdetail - exception :-" + ex.InnerException);
                }
            }
            else
                Clients.Caller.getwmalogdetail(JSONHelper.FromDataTable(ds.Tables["WMALogType"]));
        }

        public void getwmacasedetail(string caParam)
        {
            DataTable dtFilteredWMACase = new DataTable();

            if (caParam != null)
            {
                var strParam = caParam.Split(new char[] { ',' });
                int OfficerID = int.Parse(strParam[0]);
                string FromDate = strParam[1].ToString();
                string LogType = strParam[2].ToString();
                try
                {
                    if ((dtWMA != null) && (dtWMA.Rows.Count > 0))
                    {
                        if (dtWMA.Select("LogType = '" + LogType + "' AND OfficerId='" + OfficerID + "'").Count() > 0)
                            dtFilteredWMACase = dtWMA.Select("LogType = '" + LogType + "' AND OfficerId='" + OfficerID + "'").CopyToDataTable();
                    }

                    Clients.Caller.getwmacasedetail(JSONHelper.FromDataTable(dtFilteredWMACase) + "||" + LogType);
                }
                catch (AggregateException ex)
                {
                    //Clients.Caller.getwmacasedetail(JSONHelper.FromDataTable(dtFilteredWMACase) + "||" + LogType);
                    log.Info(ServerName + "getwmacasedetail - exception :-" + ex.Message);
                    log.Info( ServerName + "getwmacasedetail - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getwmacasedetail - exception :-" + ex.InnerException);
                }
                catch (Exception ex)
                {
                    //Clients.Caller.getwmacasedetail(JSONHelper.FromDataTable(dtFilteredWMACase) + "||" + LogType);
                    log.Info(ServerName + "getwmacasedetail - exception :-" + ex.Message);
                    log.Info( ServerName + "getwmacasedetail - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getwmacasedetail - exception :-" + ex.InnerException);
                }
            }
            else
                Clients.Caller.getwmacasedetail(JSONHelper.FromDataTable(dtFilteredWMACase) + "||" + 0);
        }

        public void broadcastclients(string message)
        {
            Clients.All.broadcastMessage(message);
        }

        #endregion

        #region Lastlocation

        public void getlastlocation(string lcParam, string showAll, string Condition)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add("LastLocationStatus");
            ds.Tables["LastLocationStatus"].Columns.Add("latitude");
            ds.Tables["LastLocationStatus"].Columns.Add("longitude");
            ds.Tables["LastLocationStatus"].Columns.Add("ActionText");
            ds.Tables["LastLocationStatus"].Columns.Add("html");
            ds.Tables["LastLocationStatus"].Columns.Add("IconType");

            if (lcParam != null)
            {
                var strParam = lcParam.Split(new char[] { ',' });

                int ManagerID = int.Parse(strParam[0]);
                DateTime FromDate = DateTime.Parse(strParam[1]);
                int OfficerID = int.Parse(strParam[2]);
                int GroupID = int.Parse(strParam[3]);
                int CompanyID = int.Parse(strParam[4]);
                int OfficerRole = int.Parse(strParam[5]);

                int IsArrest = 0;
                if (OfficerRole == 9) IsArrest = 1;

                string GPSLatitude = string.Empty;
                string GPSLongitude = string.Empty;
                DataTable dtFilteredLastLocation = new DataTable();
                DataRow[] drFilteredLastLocation;
                try
                {
                    if (ManagerID != 9999 && ManagerID != 6999)
                    {
                        if ((dtLastLocation != null) && (dtLastLocation.Rows.Count > 0))
                        {
                            drFilteredLastLocation = dtLastLocation.Select("ManagerID = '" + ManagerID + "' AND IsArrest=" + IsArrest + " AND CompanyID=" + CompanyID);
                            if (drFilteredLastLocation.Length > 0)
                                dtFilteredLastLocation = drFilteredLastLocation.CopyToDataTable();
                            else
                            {
                                drFilteredLastLocation = dtLastLocation.Select("ADMID = '" + ManagerID + "' AND IsArrest=" + IsArrest + " AND CompanyID=" + CompanyID);
                                if (drFilteredLastLocation.Length > 0)
                                    dtFilteredLastLocation = drFilteredLastLocation.CopyToDataTable();
                                else
                                {
                                    drFilteredLastLocation = dtLastLocation.Select("OfficerID = '" + ((ManagerID == 0) ? OfficerID : ManagerID) + "' AND IsArrest=" + IsArrest + " AND CompanyID=" + CompanyID);
                                    if (drFilteredLastLocation.Length > 0)
                                        dtFilteredLastLocation = drFilteredLastLocation.CopyToDataTable();
                                }
                            }
                        }
                    }
                    else
                    {
                        if ((dtLastLocation != null) && (dtLastLocation.Rows.Count > 0))
                        {
                            drFilteredLastLocation = dtLastLocation.Select("IsArrest=" + IsArrest + " AND CompanyID=" + CompanyID);
                            if (drFilteredLastLocation.Length > 0)
                                dtFilteredLastLocation = drFilteredLastLocation.CopyToDataTable();
                        }
                    }

                    DataRowView drvOutput;
                    DataView dvOutput;
                    dvOutput = ds.Tables["LastLocationStatus"].DefaultView;

                    if (dtFilteredLastLocation != null)
                    {
                        foreach (DataRow dr in dtFilteredLastLocation.Rows)
                        {
                            drvOutput = dvOutput.AddNew();
                            drvOutput["latitude"] = dr["GPSLatitude"];
                            drvOutput["longitude"] = dr["GPSLongitude"];
                            drvOutput["ActionText"] = dr["ActionText"];
                            drvOutput["html"] = "<b>" + dr["OfficerName"] + "[" + dr["OfficerID"] + "]" + "</b><br>" + " " + dr["CaseNumber"] + " " + " - <b>" + dr["ActionText"] + "</b><br>" + " " + dr["DateActioned"] + "<br> Accuracy: " + dr["GPSHDOP"] + " metres <br>";
                            drvOutput["IconType"] = dr["IconType"];
                            drvOutput.EndEdit();
                        }
                    }
                    Clients.Caller.getlastlocation(JSONHelper.FromDataTable(ds.Tables["LastLocationStatus"]), showAll, Condition);
                }
                catch (AggregateException ex)
                {
                    //Clients.Caller.getlastlocation(JSONHelper.FromDataTable(ds.Tables["LastLocationStatus"]), showAll, Condition);
                    log.Info(ServerName + "getlastlocation - exception :-" + ex.Message);
                    log.Info( ServerName + "getlastlocation - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getlastlocation - exception :-" + ex.InnerException);
                }
                catch (Exception ex)
                {
                    //Clients.Caller.getlastlocation(JSONHelper.FromDataTable(ds.Tables["LastLocationStatus"]), showAll, Condition);
                    log.Info(ServerName + "getlastlocation - exception :-" + ex.Message);
                    log.Info( ServerName + "getlastlocation - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getlastlocation - exception :-" + ex.InnerException);
                }
            }
            else
                Clients.Caller.getlastlocation(JSONHelper.FromDataTable(ds.Tables["LastLocationStatus"]), showAll, Condition);
        }

        #endregion

        #region HeartBeat

        public void getheartbeat(string lcParam, string Condition)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add("HeartBeatStatus");
            ds.Tables["HeartBeatStatus"].Columns.Add("latitude");
            ds.Tables["HeartBeatStatus"].Columns.Add("longitude");
            ds.Tables["HeartBeatStatus"].Columns.Add("html");

            if (lcParam != null)
            {
                var strParam = lcParam.Split(new char[] { ',' });
                int OfficerID = int.Parse(strParam[0]);
                DateTime FromDate = DateTime.Parse(strParam[1]);
                int CompanyID = int.Parse(strParam[2]);
                int OfficerRole = int.Parse(strParam[3]);

                int IsArrest = 0;
                if (OfficerRole == 9) IsArrest = 1;
                string GPSLatitude = string.Empty;
                string GPSLongitude = string.Empty;
                DataTable dtFilteredHB = new DataTable();
                DataRow[] drFilteredHB;
                try
                {
                    if ((dtHeartBeat != null) && (dtHeartBeat.Rows.Count > 0))
                    {
                        drFilteredHB = dtHeartBeat.Select("OfficerID = '" + OfficerID + "' AND IsArrest=" + IsArrest + " AND CompanyID=" + CompanyID);
                        if (drFilteredHB.Length > 0)
                            dtFilteredHB = drFilteredHB.CopyToDataTable();
                    }

                    DataRowView drvOutput;
                    DataView dvOutput;
                    dvOutput = ds.Tables["HeartBeatStatus"].DefaultView;

                    if (dtFilteredHB != null)
                    {
                        foreach (DataRow dr in dtFilteredHB.Rows)
                        {
                            drvOutput = dvOutput.AddNew();
                            drvOutput["latitude"] = dr["latitude"];
                            drvOutput["longitude"] = dr["longitude"];
                            drvOutput["html"] = dr["html"];
                            drvOutput.EndEdit();
                        }
                    }
                    Clients.Caller.getheartbeat(JSONHelper.FromDataTable(ds.Tables["HeartBeatStatus"]), Condition);
                }
                catch (AggregateException ex)
                {
                    //Clients.Caller.getheartbeat(JSONHelper.FromDataTable(ds.Tables["HeartBeatStatus"]), Condition);
                    log.Info(ServerName + "getheartbeat - exception :-" + ex.Message);
                    log.Info( ServerName + "getheartbeat - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getheartbeat - exception :-" + ex.InnerException);
                }
                catch (Exception ex)
                {
                    //Clients.Caller.getheartbeat(JSONHelper.FromDataTable(ds.Tables["HeartBeatStatus"]), Condition);
                    log.Info(ServerName + "getheartbeat - exception :-" + ex.Message);
                    log.Info( ServerName + "getheartbeat - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getheartbeat - exception :-" + ex.InnerException);
                }
            }
            else
                Clients.Caller.getheartbeat(JSONHelper.FromDataTable(ds.Tables["HeartBeatStatus"]), Condition);
        }

        #endregion

        #region Ranking

        public void getrankingmanager(string caParam, string flag, string type)
        {
            DataTable dt = new DataTable("RankingManager");
            dt.Columns.Add("OfficerID");
            dt.Columns.Add("OfficerName");
            dt.Columns.Add("CaseCnt");
            dt.Columns.Add("ActionText");
            dt.Columns.Add("FromDate");
            dt.Columns.Add("ToDate");

            if (caParam != null)
            {
                var strParam = caParam.Split(new char[] { ',' });
                string ActionText = strParam[0].ToString();
                string FromDate = strParam[1].ToString();
                string ToDate = strParam[2].ToString();
                int CompanyID = int.Parse(strParam[3]);
                int ManagerID = int.Parse(strParam[4]);
                int OfficerRole = int.Parse(strParam[5]);

                int IsArrest = 0;
                if (OfficerRole == 9) IsArrest = 1;
                try
                {
                    if (ManagerID == 6999 || ManagerID == 9999)
                    {
                        if ((dtOfficersTree != null) && (dtOfficersTree.Rows.Count > 0))
                        {
                            var qOfficer = from r in dtOfficersTree.AsEnumerable()
                                           let otArray = new object[]
                                        {
                                    r.Field<string>("ManagerID"), r.Field<string>("ManagerName"), 0,ActionText,FromDate,ToDate
                                        }
                                           select otArray;

                            foreach (var dr in qOfficer)
                            {
                                dt.Rows.Add(dr);
                            }
                        }
                    }
                    if (type == "ADM")
                    {
                        if ((dtOfficersTree != null) && (dtOfficersTree.Rows.Count > 0))
                        {
                            DataRow[] drFilteredOfficer = dtOfficersTree.Select("ManagerID='" + ManagerID + "'");
                            DataTable dtFilteredOfficer = (drFilteredOfficer.Length > 0 ? drFilteredOfficer.CopyToDataTable() : new DataTable());

                            var qOfficer = (from r in dtFilteredOfficer.AsEnumerable()
                                            let otArray = new object[]
                                        {
                                    r.Field<string>("ADMID"), r.Field<string>("ADMName"), 0,ActionText,FromDate,ToDate
                                        }
                                            select otArray);

                            foreach (var dr in qOfficer)
                            {
                                dt.Rows.Add(dr);
                            }
                        }
                    }
                    else if (type == "Officer")
                    {
                        if ((dtOfficersTree != null) && (dtOfficersTree.Rows.Count > 0))
                        {
                            DataRow[] drFilteredOfficer = dtOfficersTree.Select("ADMID='" + ManagerID + "'");
                            DataTable dtFilteredOfficer = (drFilteredOfficer.Length > 0 ? drFilteredOfficer.CopyToDataTable() : new DataTable());

                            var qOfficer = (from r in dtFilteredOfficer.AsEnumerable()
                                            let otArray = new object[]
                                        {
                                    r.Field<string>("OfficerID"), r.Field<string>("OfficerName"), 0,ActionText,FromDate,ToDate//.ToString("yyyy/MM/dd")
                                        }
                                            select otArray);

                            foreach (var dr in qOfficer)
                            {
                                dt.Rows.Add(dr);
                            }
                        }
                    }

                    DataView view = new DataView(dt);
                    dt = view.ToTable(true, "OfficerID", "OfficerName", "CaseCnt", "ActionText", "FromDate", "ToDate");
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (type == "Manager")
                        {

                            DataRow[] cRow = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = '" + ActionText + "' AND ManagerID='" + dr[0].ToString() + "' AND CompanyID=" + CompanyID + " AND IsArrest=" + IsArrest) : null;
                            DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                            HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                        }
                        else if (type == "ADM")
                        {
                            DataRow[] cRow = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = '" + ActionText + "' AND ADMID='" + dr[0].ToString() + "' AND CompanyID=" + CompanyID + " AND IsArrest=" + IsArrest) : null;
                            DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                            HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                        }
                        else if (type == "Officer")
                        {
                            DataRow[] cRow = ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0)) ? dtCaseActions.Select("ActionText = '" + ActionText + "' AND Officer='" + dr[0].ToString() + "' AND CompanyID=" + CompanyID + " AND IsArrest=" + IsArrest) : null;
                            DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                            HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                        }
                    }
                    if (dt.Select("", "CaseCnt DESC").Count() > 0)
                        dt = dt.Select("", "CaseCnt DESC").CopyToDataTable();
                    Clients.Caller.getrankingmanager(JSONHelper.FromDataTable(dt), flag, type);
                }
                catch (AggregateException ex)
                {
                    Clients.Caller.getrankingmanager(JSONHelper.FromDataTable(dt), flag, type);
                }
                catch (Exception ex)
                {
                    Clients.Caller.getrankingmanager(JSONHelper.FromDataTable(dt), flag, type);
                }
            }
            else
                Clients.Caller.getrankingmanager(JSONHelper.FromDataTable(dt), flag, type);
        }

        #endregion

        #region Stats

        public void gethubstats(string caParam, int link)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add("StatsAction");
            ds.Tables["StatsAction"].Columns.Add("Description");
            ds.Tables["StatsAction"].Columns.Add("DisplayValue");
            ds.Tables["StatsAction"].Columns.Add("Formula");
            ds.Tables["StatsAction"].Columns.Add("Calculation");

            if (caParam != null)
            {
                var strParam = caParam.Split(new char[] { ',' });
                int ManagerID = int.Parse(strParam[0]);
                int CompanyID = int.Parse(strParam[1]);

                double Productivity, Efficiency, Conversion, TotalPaidCaseCnt, Target, TotalVisitedCnt, TotalCaseCnt, Availability, Caseholdings;
                int PaidsVsTarget, PredictedEfficiency;
                Productivity = Efficiency = Conversion = Caseholdings = PaidsVsTarget = PredictedEfficiency = 0;
                TotalPaidCaseCnt = Target = TotalVisitedCnt = TotalCaseCnt = Availability = 0;

                double TakenControlofGoods, FullPaid, PartPaid, Dropped, AverageDaily;
                TakenControlofGoods = FullPaid = PartPaid = Dropped = AverageDaily = 0;

                double TotalVisitCntSum, TotalCaseCntSum, TotalPaidCaseCntSum;
                TotalVisitCntSum = TotalCaseCntSum = TotalPaidCaseCntSum = 0;
                DataTable dtFilteredStats = new DataTable();
                DataRow[] drFilteredStats;

                if (!(ManagerID == 9999 || ManagerID == 6999))
                {
                    if ((dtStats != null) && (dtStats.Rows.Count > 0))
                    {
                        drFilteredStats = dtStats.Select("ManagerID='" + ManagerID + "'");
                        if (drFilteredStats.Length > 0)
                            dtFilteredStats = drFilteredStats.CopyToDataTable();
                        else
                        {
                            drFilteredStats = dtStats.Select("ADMID = '" + ManagerID + "'");
                            if (drFilteredStats.Length > 0)
                                dtFilteredStats = drFilteredStats.CopyToDataTable();
                            else
                            {
                                drFilteredStats = dtStats.Select("OfficerID = '" + ((ManagerID == 0) ? ManagerID : ManagerID) + "'");
                                if (drFilteredStats.Length > 0)
                                    dtFilteredStats = drFilteredStats.CopyToDataTable();
                            }
                        }
                    }

                    DataTable dtClonedFilteredStats = dtFilteredStats.Clone();
                    if (dtClonedFilteredStats.Rows.Count > 0)
                    {
                        dtClonedFilteredStats.Columns[0].DataType = typeof(double);
                        dtClonedFilteredStats.Columns[1].DataType = typeof(double);
                        dtClonedFilteredStats.Columns[2].DataType = typeof(double);
                        dtClonedFilteredStats.Columns[3].DataType = typeof(double);
                        dtClonedFilteredStats.Columns[4].DataType = typeof(double);//totalcasecnt
                        dtClonedFilteredStats.Columns[5].DataType = typeof(double);//totalvisitcnt
                        dtClonedFilteredStats.Columns[6].DataType = typeof(double);//totalpaidcnt
                        dtClonedFilteredStats.Columns[7].DataType = typeof(double);//target
                        dtClonedFilteredStats.Columns[8].DataType = typeof(double);//availability
                        foreach (DataRow row in dtFilteredStats.Rows)
                        {
                            dtClonedFilteredStats.ImportRow(row);
                        }

                        Productivity = double.Parse(dtClonedFilteredStats.Compute("Avg(Productivity)", "").ToString());
                        Efficiency = double.Parse(dtClonedFilteredStats.Compute("Avg(Efficiency)", "").ToString());
                        Conversion = double.Parse(dtClonedFilteredStats.Compute("Avg(Conversion)", "").ToString());
                        Caseholdings = double.Parse(dtClonedFilteredStats.Compute("Sum(Holdings)", "").ToString());
                        TotalPaidCaseCnt = double.Parse(dtClonedFilteredStats.Compute("Avg(TotalPaidsCnt)", "").ToString());
                        Target = double.Parse(dtClonedFilteredStats.Compute("Avg(Target)", "").ToString());
                        PaidsVsTarget = Convert.ToInt32(TotalPaidCaseCnt) / Convert.ToInt32(Target);
                        Availability = double.Parse(dtClonedFilteredStats.Compute("Avg(Availability)", "").ToString());
                        TotalVisitedCnt = double.Parse(dtClonedFilteredStats.Compute("Avg(TotalVisitCnt)", "").ToString());
                        TotalCaseCnt = double.Parse(dtClonedFilteredStats.Compute("Avg(TotalCaseCnt)", "").ToString());
                        PredictedEfficiency = Convert.ToInt32(Availability) * Convert.ToInt32(TotalPaidCaseCnt) / (Convert.ToInt32(TotalVisitedCnt) == 0 ? 1 : Convert.ToInt32(TotalVisitedCnt));

                        TotalVisitCntSum = double.Parse(dtClonedFilteredStats.Compute("Sum(TotalVisitCnt)", "").ToString());
                        TotalCaseCntSum = double.Parse(dtClonedFilteredStats.Compute("Sum(TotalCaseCnt)", "").ToString());
                        TotalPaidCaseCntSum = double.Parse(dtClonedFilteredStats.Compute("Sum(TotalPaidsCnt)", "").ToString());
                    }
                }
                else
                {
                    if ((dtStats != null) && (dtStats.Rows.Count > 0))
                    {
                        try
                        {
                            drFilteredStats = dtStats.Select();
                            dtFilteredStats = drFilteredStats.CopyToDataTable();

                            DataTable dtClonedFilteredStats = dtFilteredStats.Clone();
                            dtClonedFilteredStats.Columns[0].DataType = typeof(double);
                            dtClonedFilteredStats.Columns[1].DataType = typeof(double);
                            dtClonedFilteredStats.Columns[2].DataType = typeof(double);
                            dtClonedFilteredStats.Columns[3].DataType = typeof(double);
                            dtClonedFilteredStats.Columns[4].DataType = typeof(double);//totalcasecnt
                            dtClonedFilteredStats.Columns[5].DataType = typeof(double);//totalvisitcnt
                            dtClonedFilteredStats.Columns[6].DataType = typeof(double);//totalpaidcnt
                            dtClonedFilteredStats.Columns[7].DataType = typeof(double);//target
                            dtClonedFilteredStats.Columns[8].DataType = typeof(double);//availability
                            foreach (DataRow row in dtFilteredStats.Rows)
                            {
                                dtClonedFilteredStats.ImportRow(row);
                            }

                            Productivity = double.Parse(dtClonedFilteredStats.Compute("Avg(Productivity)", "").ToString());
                            Efficiency = double.Parse(dtClonedFilteredStats.Compute("Avg(Efficiency)", "").ToString());
                            Conversion = double.Parse(dtClonedFilteredStats.Compute("Avg(Conversion)", "").ToString());
                            Caseholdings = double.Parse(dtClonedFilteredStats.Compute("Sum(Holdings)", "").ToString());
                            TotalPaidCaseCnt = double.Parse(dtClonedFilteredStats.Compute("Avg(TotalPaidsCnt)", "").ToString());
                            Target = double.Parse(dtClonedFilteredStats.Compute("Avg(Target)", "").ToString());
                            PaidsVsTarget = Convert.ToInt32(TotalPaidCaseCnt) / Convert.ToInt32(Target);
                            Availability = double.Parse(dtClonedFilteredStats.Compute("Avg(Availability)", "").ToString());
                            TotalVisitedCnt = double.Parse(dtClonedFilteredStats.Compute("Avg(TotalVisitCnt)", "").ToString());
                            TotalCaseCnt = double.Parse(dtClonedFilteredStats.Compute("Avg(TotalCaseCnt)", "").ToString());
                            PredictedEfficiency = Convert.ToInt32(Availability) * Convert.ToInt32(TotalPaidCaseCnt) / (Convert.ToInt32(TotalVisitedCnt) == 0 ? 1 : Convert.ToInt32(TotalVisitedCnt));

                            TotalVisitCntSum = double.Parse(dtClonedFilteredStats.Compute("Sum(TotalVisitCnt)", "").ToString());
                            TotalCaseCntSum = double.Parse(dtClonedFilteredStats.Compute("Sum(TotalCaseCnt)", "").ToString());
                            TotalPaidCaseCntSum = double.Parse(dtClonedFilteredStats.Compute("Sum(TotalPaidsCnt)", "").ToString());
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                    }
                }

                DataRow dr;
                dr = ds.Tables["StatsAction"].NewRow();
                dr["Description"] = "Productivity %";
                dr["DisplayValue"] = Math.Round(Productivity);
                dr["Formula"] = "( Total visit / Total case ) * 100";
                dr["Calculation"] = "( " + TotalVisitCntSum + " / " + TotalCaseCntSum + " ) " + " * 100";
                ds.Tables["StatsAction"].Rows.Add(dr);

                dr = ds.Tables["StatsAction"].NewRow();
                dr["Description"] = "Efficiency %";
                dr["DisplayValue"] = Math.Round(Efficiency);
                dr["Formula"] = "( Total Paid Case / Total Visited ) * 100";
                dr["Calculation"] = "( " + TotalPaidCaseCntSum + " / " + TotalVisitCntSum + " ) " + " * 100";
                ds.Tables["StatsAction"].Rows.Add(dr);

                dr = ds.Tables["StatsAction"].NewRow();
                dr["Description"] = "Conversion %";
                dr["DisplayValue"] = Math.Round(Conversion);
                dr["Formula"] = "( Total Paid Case / Total Case ) * 100";
                dr["Calculation"] = "( " + TotalPaidCaseCntSum + " / " + TotalCaseCntSum + " ) " + " * 100";
                ds.Tables["StatsAction"].Rows.Add(dr);

                dr = ds.Tables["StatsAction"].NewRow();
                dr["Description"] = "Case holdings";
                dr["DisplayValue"] = Caseholdings;
                dr["Formula"] = "Case holdings";
                dr["Calculation"] = "( " + Caseholdings + " ) ";
                ds.Tables["StatsAction"].Rows.Add(dr);

                dr = ds.Tables["StatsAction"].NewRow();
                dr["Description"] = "Paids Vs Target %";
                dr["DisplayValue"] = PaidsVsTarget;
                dr["Formula"] = "( Total Paid Case / Target )";
                dr["Calculation"] = "( " + PaidsVsTarget + " ) ";
                ds.Tables["StatsAction"].Rows.Add(dr);

                dr = ds.Tables["StatsAction"].NewRow();
                dr["Description"] = "Predicted Efficiency %";
                dr["DisplayValue"] = PredictedEfficiency;
                dr["Formula"] = "( Total Paid Case / Total Visited ) * 100";
                dr["Calculation"] = "( " + TotalPaidCaseCntSum + " / " + TotalVisitCntSum + " ) " + " * 100";
                ds.Tables["StatsAction"].Rows.Add(dr);
            }
            Clients.Caller.gethubstats(JSONHelper.FromDataTable(ds.Tables["StatsAction"]), link);
        }

        public void getstatscaseactions(string urlparam)
        {
            DataTable dtFilteredStatsOfficer = new DataTable();
            if (urlparam != null)
            {
                var strParam = urlparam.Split(new char[] { ',' });
                int OfficerID = int.Parse(strParam[3]);
                try
                {
                    if ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0))
                    {
                        if (OfficerID > 0)
                        {
                            dtFilteredStatsOfficer = dtCaseActions.Select("Officer='" + OfficerID + "'").CopyToDataTable();
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Info(ServerName + "getstatscaseactions - exception :-" + ex.Message);
                    log.Info( ServerName + "getstatscaseactions - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getstatscaseactions - exception :-" + ex.InnerException);
                }

                Clients.Caller.getstatscaseactions(JSONHelper.FromDataTable(dtFilteredStatsOfficer));
            }
            else
                Clients.Caller.getstatscaseactions(JSONHelper.FromDataTable(dtFilteredStatsOfficer));
        }

        #endregion

        #region Location_with_HeartBeat

        public void getlocationheartbeat(string lcParam, string Condition)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add("LocationHeartBeatStatus");
            ds.Tables["LocationHeartBeatStatus"].Columns.Add("latitude");
            ds.Tables["LocationHeartBeatStatus"].Columns.Add("longitude");
            ds.Tables["LocationHeartBeatStatus"].Columns.Add("ActionText");
            ds.Tables["LocationHeartBeatStatus"].Columns.Add("html");
            ds.Tables["LocationHeartBeatStatus"].Columns.Add("IconType");

            if (lcParam != null)
            {

                var strParam = lcParam.Split(new char[] { ',' });
                int OfficerID = int.Parse(strParam[0]);
                DateTime FromDate = DateTime.Parse(strParam[1]);
                int CompanyID = int.Parse(strParam[2]);
                int OfficerRole = int.Parse(strParam[3]);

                int IsArrest = 0;
                if (OfficerRole == 9) IsArrest = 1;

                string GPSLatitude = string.Empty;
                string GPSLongitude = string.Empty;
                DataTable dtFilteredLocHB = new DataTable();
                DataRow[] drFilteredLocHB;

                DataTable dtFilteredCases = new DataTable();

                try
                {
                    if ((dtCaseActions != null) && (dtCaseActions.Rows.Count > 0))
                    {
                        if (dtCaseActions.Select("Officer='" + OfficerID + "' AND CompanyID=" + CompanyID).Length > 0)
                            dtFilteredCases = dtCaseActions.Select("Officer='" + OfficerID + "' AND IsArrest=" + IsArrest + " AND CompanyID=" + CompanyID).CopyToDataTable();
                    }

                    if ((dtHeartBeat != null) && (dtHeartBeat.Rows.Count > 0))
                    {
                        drFilteredLocHB = dtHeartBeat.Select("OfficerID = '" + OfficerID + "' AND IsArrest=" + IsArrest + " AND CompanyID=" + CompanyID);
                        if (drFilteredLocHB.Length > 0)
                            dtFilteredLocHB = drFilteredLocHB.CopyToDataTable();
                    }

                    DataRowView drvOutput;
                    DataView dvOutput;
                    dvOutput = ds.Tables["LocationHeartBeatStatus"].DefaultView;

                    if (dtFilteredCases != null)
                    {
                        foreach (DataRow dr in dtFilteredCases.Rows)
                        {
                            string l_actiontxt = dr["ActionText"].ToString();
                            var l_latitude = dr["GPSLatitude"].ToString().Trim();
                            var l_logitude = dr["GPSLongitude"].ToString().Trim();
                            if ((l_actiontxt != "Defendant contact") && (l_latitude != "0") && (l_logitude != "0"))
                            {
                                drvOutput = dvOutput.AddNew();
                                string l_actiontext = dr["ActionText"].ToString();
                                string l_actiontimestr = dr["GPSSatDateTime"].ToString();
                                DateTime l_actiontime = DateTime.Parse(l_actiontimestr);
                                l_actiontimestr = l_actiontime.ToString("dd/MM/yyyy hh:mm:ss");
                                drvOutput["latitude"] = dr["GPSLatitude"];
                                drvOutput["longitude"] = dr["GPSLongitude"];
                                drvOutput["ActionText"] = dr["ActionText"];

                                drvOutput["html"] = "<b>" + dr["OfficerName"] + "[" + dr["Officer"] + "]</b><br>" + l_actiontimestr + "<br>" + dr["CaseNumber"] + "<br>" + dr["ActionText"] + "<br> Accuracy: " + dr["GPSHDOP"] + " metres";
                                if (l_actiontext == "Paid" || l_actiontext == "Part Paid")
                                    drvOutput["IconType"] = 1;
                                else if (l_actiontext == "Returned")
                                    drvOutput["IconType"] = 2;
                                else if (l_actiontext == "Revisit")
                                    drvOutput["IconType"] = 3;
                                else
                                    drvOutput["IconType"] = 0;
                                drvOutput.EndEdit();
                            }

                        }
                    }

                    if (dtFilteredLocHB != null)
                    {
                        foreach (DataRow dr in dtFilteredLocHB.Rows)
                        {
                            var l_latitude = dr["latitude"].ToString().Trim();
                            var l_logitude = dr["longitude"].ToString().Trim();
                            if ((l_latitude != "0") && (l_logitude != "0"))
                            {
                                drvOutput = dvOutput.AddNew();
                                drvOutput["latitude"] = dr["latitude"];
                                drvOutput["longitude"] = dr["longitude"];
                                drvOutput["ActionText"] = "";
                                drvOutput["html"] = dr["html"];
                                drvOutput["IconType"] = 5;
                                drvOutput.EndEdit();
                            }
                        }
                    }

                    Clients.Caller.getlocationheartbeat(JSONHelper.FromDataTable(ds.Tables["LocationHeartBeatStatus"]), Condition);
                }
                catch (AggregateException ex)
                {
                    //Clients.Caller.getlocationheartbeat(JSONHelper.FromDataTable(ds.Tables["LocationHeartBeatStatus"]), Condition);
                    log.Info(ServerName + "getlocationheartbeat - exception :-" + ex.Message);
                    log.Info( ServerName + "getlocationheartbeat - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getlocationheartbeat - exception :-" + ex.InnerException);
                }
                catch (Exception ex)
                {
                    //Clients.Caller.getlocationheartbeat(JSONHelper.FromDataTable(ds.Tables["LocationHeartBeatStatus"]), Condition);
                    log.Info(ServerName + "getlocationheartbeat - exception :-" + ex.Message);
                    log.Info( ServerName + "getlocationheartbeat - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getlocationheartbeat - exception :-" + ex.InnerException);
                }
            }
            else
                Clients.Caller.getlocationheartbeat(JSONHelper.FromDataTable(ds.Tables["LocationHeartBeatStatus"]), Condition);
        }
        #endregion

        #region ES_Dashboard

        public void getallrequest(string caParam)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add("AssignRequest");
            ds.Tables["AssignRequest"].Columns.Add("RequestType");
            ds.Tables["AssignRequest"].Columns.Add("Count");
            ds.Tables["AssignRequest"].Columns.Add("Ave");
            ds.Tables["AssignRequest"].Columns.Add("TAT");

            ds.Tables.Add("HPICheck");
            ds.Tables["HPICheck"].Columns.Add("RequestType");
            ds.Tables["HPICheck"].Columns.Add("Count");
            ds.Tables["HPICheck"].Columns.Add("Ave");
            ds.Tables["HPICheck"].Columns.Add("TAT");

            ds.Tables.Add("PermissionToRemove");
            ds.Tables["PermissionToRemove"].Columns.Add("RequestType");
            ds.Tables["PermissionToRemove"].Columns.Add("Count");
            ds.Tables["PermissionToRemove"].Columns.Add("Ave");
            ds.Tables["PermissionToRemove"].Columns.Add("TAT");

            ds.Tables.Add("CaseReturn");
            ds.Tables["CaseReturn"].Columns.Add("RequestType");
            ds.Tables["CaseReturn"].Columns.Add("Count");
            ds.Tables["CaseReturn"].Columns.Add("Ave");
            ds.Tables["CaseReturn"].Columns.Add("TAT");

            if (caParam != null)
            {
                var strParam = caParam.Split(new char[] { ',' });
                int TreeLevel = int.Parse(strParam[0]);
                int OfficerID = int.Parse(strParam[1]);
                int ReqDateType = int.Parse(strParam[2]);

                DataTable dtFilteredRequest = new DataTable();
                DataRow[] drFilteredRequest;
                try
                {
                    if ((dtEnforcementService != null) && (dtEnforcementService.Rows.Count > 0))
                    {
                        if (TreeLevel == 1)
                        {
                            //Manager
                            switch (ReqDateType)
                            {
                                case 1: // Today
                                    drFilteredRequest = dtEnforcementService.Select("TodayFilter = 1");
                                    if (drFilteredRequest.Length > 0)
                                        dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                                    break;
                                case 2://Last 24 Hours
                                    drFilteredRequest = dtEnforcementService.Select("Last24hrs = 1");
                                    if (drFilteredRequest.Length > 0)
                                        dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                                    break;
                                case 3:// This Week
                                    drFilteredRequest = dtEnforcementService.Select("Thisweek = 1");
                                    if (drFilteredRequest.Length > 0)
                                        dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                                    break;
                                case 4:// This Month
                                    drFilteredRequest = dtEnforcementService.Select("ThisMonth = 1");
                                    if (drFilteredRequest.Length > 0)
                                        dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                                    break;
                            }
                        }
                        else
                        {
                            //Agent 
                            switch (ReqDateType)
                            {
                                case 1: // Today
                                    drFilteredRequest = dtEnforcementService.Select("ActionBy = '" + OfficerID + "' AND TodayFilter = 1");
                                    if (drFilteredRequest.Length > 0)
                                        dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                                    break;
                                case 2://Last 24 Hours
                                    drFilteredRequest = dtEnforcementService.Select("ActionBy = '" + OfficerID + "' AND Last24hrs = 1");
                                    if (drFilteredRequest.Length > 0)
                                        dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                                    break;
                                case 3:// This Week
                                    drFilteredRequest = dtEnforcementService.Select("ActionBy = '" + OfficerID + "' AND Thisweek = 1");
                                    if (drFilteredRequest.Length > 0)
                                        dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                                    break;
                                case 4:// This Month
                                    drFilteredRequest = dtEnforcementService.Select("ActionBy = '" + OfficerID + "' AND ThisMonth = 1");
                                    if (drFilteredRequest.Length > 0)
                                        dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                                    break;
                            }
                        }
                        DataRow dr;
                        // On all tables' rows
                        //AssignRequest
                        dr = ds.Tables["AssignRequest"].NewRow();
                        dr["RequestType"] = "Open";
                        DataRow[] aRow = ((dtEnforcementService != null) && (dtEnforcementService.Rows.Count > 0)) ? dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='AssignRequest'") : null;
                        dr["Count"] = (aRow == null) ? 0 : aRow.Length;
                        if ((dtEnforcementService != null) && (dtEnforcementService.Rows.Count > 0))
                        {
                            if (dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='AssignRequest'").Length > 0)
                            {
                                dr["Ave"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='AssignRequest'")[0]["Lastweekavg"].ToString();
                                dr["TAT"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='AssignRequest'")[0]["TAT"].ToString();
                            }
                            else
                            {
                                dr["Ave"] = "0";
                                dr["TAT"] = "0";
                            }
                        }
                        else
                        {
                            dr["Ave"] = "0";
                            dr["TAT"] = "0";
                        }
                        ds.Tables["AssignRequest"].Rows.Add(dr);

                        dr = ds.Tables["AssignRequest"].NewRow();
                        dr["RequestType"] = "Approved";
                        DataRow[] aRow1 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='AssignRequest'") : null;
                        dr["Count"] = (aRow1 == null) ? 0 : aRow1.Length;
                        if (dtFilteredRequest.Rows.Count > 0)
                        {
                            if (dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='AssignRequest'").Length > 0)
                            {
                                dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='AssignRequest'")[0]["Lastweekavg"].ToString();
                                dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='AssignRequest'")[0]["TAT"].ToString();
                            }
                            else
                            {
                                dr["Ave"] = "0";
                                dr["TAT"] = "0";
                            }
                        }
                        else
                        {
                            dr["Ave"] = "0";
                            dr["TAT"] = "0";
                        }

                        ds.Tables["AssignRequest"].Rows.Add(dr);

                        dr = ds.Tables["AssignRequest"].NewRow();
                        dr["RequestType"] = "Declined";
                        DataRow[] aRow2 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='AssignRequest'") : null;
                        dr["Count"] = (aRow2 == null) ? 0 : aRow2.Length;
                        if (dtFilteredRequest.Rows.Count > 0)
                        {
                            if (dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='AssignRequest'").Length > 0)
                            {
                                dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='AssignRequest'")[0]["Lastweekavg"].ToString();
                                dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='AssignRequest'")[0]["TAT"].ToString();
                            }
                            else
                            {
                                dr["Ave"] = "0";
                                dr["TAT"] = "0";
                            }
                        }
                        else
                        {
                            dr["Ave"] = "0";
                            dr["TAT"] = "0";
                        }

                        ds.Tables["AssignRequest"].Rows.Add(dr);

                        ////HPI Check
                        dr = ds.Tables["HPICheck"].NewRow();
                        dr["RequestType"] = "Open";
                        DataRow[] hRow = ((dtEnforcementService != null) && (dtEnforcementService.Rows.Count > 0)) ? dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='HPI Check'") : null;
                        dr["Count"] = (hRow == null) ? 0 : hRow.Length;
                        if (dtEnforcementService.Rows.Count > 0)
                        {
                            if (dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='HPI Check'").Length > 0)
                            {
                                dr["Ave"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='HPI Check'")[0]["Lastweekavg"].ToString();
                                dr["TAT"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='HPI Check'")[0]["TAT"].ToString();
                            }
                            else
                            {
                                dr["Ave"] = "0";
                                dr["TAT"] = "0";
                            }
                        }
                        else
                        {
                            dr["Ave"] = "0";
                            dr["TAT"] = "0";
                        }
                        ds.Tables["HPICheck"].Rows.Add(dr);

                        dr = ds.Tables["HPICheck"].NewRow();
                        dr["RequestType"] = "Approved";
                        DataRow[] hRow1 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='HPI Check'") : null;
                        dr["Count"] = (hRow1 == null) ? 0 : hRow1.Length;
                        if (dtFilteredRequest.Rows.Count > 0)
                        {
                            if (dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='HPI Check'").Length > 0)
                            {
                                dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='HPI Check'")[0]["Lastweekavg"].ToString();
                                dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='HPI Check'")[0]["TAT"].ToString();
                            }
                            else
                            {
                                dr["Ave"] = "0";
                                dr["TAT"] = "0";
                            }
                        }
                        else
                        {
                            dr["Ave"] = "0";
                            dr["TAT"] = "0";
                        }
                        ds.Tables["HPICheck"].Rows.Add(dr);

                        dr = ds.Tables["HPICheck"].NewRow();
                        dr["RequestType"] = "Declined";
                        DataRow[] hRow2 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='HPI Check'") : null;
                        dr["Count"] = (hRow2 == null) ? 0 : hRow2.Length;
                        if (dtFilteredRequest.Rows.Count > 0)
                        {
                            if (dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='HPI Check'").Length > 0)
                            {
                                dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='HPI Check'")[0]["Lastweekavg"].ToString();
                                dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='HPI Check'")[0]["TAT"].ToString();
                            }
                            else
                            {
                                dr["Ave"] = "0";
                                dr["TAT"] = "0";
                            }
                        }
                        else
                        {
                            dr["Ave"] = "0";
                            dr["TAT"] = "0";
                        }
                        ds.Tables["HPICheck"].Rows.Add(dr);

                        ////Permission to remove
                        dr = ds.Tables["PermissionToRemove"].NewRow();
                        dr["RequestType"] = "Open";
                        DataRow[] pRow = (dtEnforcementService.Rows.Count > 0) ? dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='Permission to remove'") : null;
                        dr["Count"] = (pRow == null) ? 0 : pRow.Length;
                        if (dtEnforcementService.Rows.Count > 0)
                        {
                            if (dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='Permission to remove'").Length > 0)
                            {
                                dr["Ave"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='Permission to remove'")[0]["Lastweekavg"].ToString();
                                dr["TAT"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='Permission to remove'")[0]["TAT"].ToString();
                            }
                            else
                            {
                                dr["Ave"] = "0";
                                dr["TAT"] = "0";
                            }
                        }
                        else
                        {
                            dr["Ave"] = "0";
                            dr["TAT"] = "0";
                        }
                        ds.Tables["PermissionToRemove"].Rows.Add(dr);

                        dr = ds.Tables["PermissionToRemove"].NewRow();
                        dr["RequestType"] = "Approved";
                        DataRow[] pRow1 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='Permission to remove'") : null;
                        dr["Count"] = (pRow1 == null) ? 0 : pRow1.Length;
                        if (dtFilteredRequest.Rows.Count > 0)
                        {
                            if (dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='Permission to remove'").Length > 0)
                            {
                                dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='Permission to remove'")[0]["Lastweekavg"].ToString();
                                dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='Permission to remove'")[0]["TAT"].ToString();
                            }
                            else
                            {
                                dr["Ave"] = "0";
                                dr["TAT"] = "0";
                            }
                        }
                        else
                        {
                            dr["Ave"] = "0";
                            dr["TAT"] = "0";
                        }
                        ds.Tables["PermissionToRemove"].Rows.Add(dr);

                        dr = ds.Tables["PermissionToRemove"].NewRow();
                        dr["RequestType"] = "Declined";
                        DataRow[] pRow2 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='Permission to remove'") : null;
                        dr["Count"] = (pRow2 == null) ? 0 : pRow2.Length;
                        if (dtFilteredRequest.Rows.Count > 0)
                        {
                            if (dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='Permission to remove'").Length > 0)
                            {
                                dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='Permission to remove'")[0]["Lastweekavg"].ToString();
                                dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='Permission to remove'")[0]["TAT"].ToString();
                            }
                            else
                            {
                                dr["Ave"] = "0";
                                dr["TAT"] = "0";
                            }
                        }
                        else
                        {
                            dr["Ave"] = "0";
                            dr["TAT"] = "0";
                        }
                        ds.Tables["PermissionToRemove"].Rows.Add(dr);

                        ////CaseReturn
                        dr = ds.Tables["CaseReturn"].NewRow();
                        dr["RequestType"] = "Open";
                        DataRow[] cRow = ((dtEnforcementService != null) && (dtEnforcementService.Rows.Count > 0)) ? dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='PendingReturn'") : null;
                        dr["Count"] = (cRow == null) ? 0 : cRow.Length;
                        if (dtEnforcementService.Rows.Count > 0)
                        {
                            if (dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='PendingReturn'").Length > 0)
                            {
                                dr["Ave"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='PendingReturn'")[0]["Lastweekavg"].ToString();
                                dr["TAT"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='PendingReturn'")[0]["TAT"].ToString();
                            }
                            else
                            {
                                dr["Ave"] = "0";
                                dr["TAT"] = "0";
                            }
                        }
                        else
                        {
                            dr["Ave"] = "0";
                            dr["TAT"] = "0";
                        }
                        ds.Tables["CaseReturn"].Rows.Add(dr);

                        dr = ds.Tables["CaseReturn"].NewRow();
                        dr["RequestType"] = "Approved";
                        DataRow[] cRow1 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='PendingReturn'") : null;
                        dr["Count"] = (cRow1 == null) ? 0 : cRow1.Length;
                        if (dtFilteredRequest.Rows.Count > 0)
                        {
                            if (dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='PendingReturn'").Length > 0)
                            {
                                dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='PendingReturn'")[0]["Lastweekavg"].ToString();
                                dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='PendingReturn'")[0]["TAT"].ToString();
                            }
                            else
                            {
                                dr["Ave"] = "0";
                                dr["TAT"] = "0";
                            }
                        }
                        else
                        {
                            dr["Ave"] = "0";
                            dr["TAT"] = "0";
                        }
                        ds.Tables["CaseReturn"].Rows.Add(dr);

                        dr = ds.Tables["CaseReturn"].NewRow();
                        dr["RequestType"] = "Declined";
                        DataRow[] cRow2 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='PendingReturn'") : null;
                        dr["Count"] = (cRow2 == null) ? 0 : cRow2.Length;
                        if (dtFilteredRequest.Rows.Count > 0)
                        {
                            if (dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='PendingReturn'").Length > 0)
                            {
                                dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='PendingReturn'")[0]["Lastweekavg"].ToString();
                                dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='PendingReturn'")[0]["TAT"].ToString();
                            }
                            else
                            {
                                dr["Ave"] = "0";
                                dr["TAT"] = "0";
                            }
                        }
                        else
                        {
                            dr["Ave"] = "0";
                            dr["TAT"] = "0";
                        }
                        ds.Tables["CaseReturn"].Rows.Add(dr);
                    }
                    Clients.Caller.getallrequest(JSONHelper.FromDataTable(ds.Tables["AssignRequest"]) + "|" + JSONHelper.FromDataTable(ds.Tables["HPICheck"]) + "|" + JSONHelper.FromDataTable(ds.Tables["PermissionToRemove"]) + "|" + JSONHelper.FromDataTable(ds.Tables["CaseReturn"]));
                }
                catch (AggregateException ex)
                {
                    //Clients.Caller.getallrequest(JSONHelper.FromDataTable(ds.Tables["AssignRequest"]) + "|" + JSONHelper.FromDataTable(ds.Tables["HPICheck"]) + "|" + JSONHelper.FromDataTable(ds.Tables["PermissionToRemove"]) + "|" + JSONHelper.FromDataTable(ds.Tables["CaseReturn"]));
                    log.Info(ServerName + "getallrequest - exception :-" + ex.Message);
                    log.Info( ServerName + "getallrequest - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getallrequest - exception :-" + ex.InnerException);
                }
                catch (Exception ex)
                {
                    //Clients.Caller.getallrequest(JSONHelper.FromDataTable(ds.Tables["AssignRequest"]) + "|" + JSONHelper.FromDataTable(ds.Tables["HPICheck"]) + "|" + JSONHelper.FromDataTable(ds.Tables["PermissionToRemove"]) + "|" + JSONHelper.FromDataTable(ds.Tables["CaseReturn"]));
                    log.Info(ServerName + "getallrequest - exception :-" + ex.Message);
                    log.Info( ServerName + "getallrequest - exception :-" + ex.StackTrace);
                    log.Info( ServerName + "getallrequest - exception :-" + ex.InnerException);
                }
            }
            else
                Clients.Caller.getallrequest(JSONHelper.FromDataTable(ds.Tables["AssignRequest"]) + "|" + JSONHelper.FromDataTable(ds.Tables["HPICheck"]) + "|" + JSONHelper.FromDataTable(ds.Tables["PermissionToRemove"]) + "|" + JSONHelper.FromDataTable(ds.Tables["CaseReturn"]));
        }

        public void getesofficerloggedon()
        {
            Clients.Caller.getesofficerloggedon(JSONHelper.FromDataTable(dtESOfficer));
        }

        #endregion

        #region HubConnection

        public void tokenupdated()
        {
            Token = Context.QueryString["Authorization"];
            Clients.Caller.tokenupdated("Token updated in the Hub");
        }

        protected string CleanJson(string jsonString)
        {
            return jsonString.Replace("{\"d\":\"", "").Replace("]\"}", "]");
        }

        protected DataTable ConvertJSONToDataTable(string jsonString)
        {
            DataTable dt = new DataTable();
            dt = JsonConvert.DeserializeObject<DataTable>(jsonString);
            return dt;
        }

        /// <summary>
        /// register online user
        /// </summary>
        /// <returns></returns>
        public async override System.Threading.Tasks.Task OnConnected()
        {            
            if (dtStats.Rows.Count <= 0)
                getstats();

            Token = Context.QueryString["Authorization"];
            clientCounter++;            

            log.Info( ServerName + Context.ConnectionId + " New Client Connected");

            Clients.Client(Context.ConnectionId).ClientOnConnected(ServerName + Context.ConnectionId + " New Client Connected");
            await base.OnConnected();
        }

        public async override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            // do the logging here
            log.Info( ServerName + Context.ConnectionId + " - disconnected");
            await base.OnDisconnected(stopCalled);
        }

        public async override System.Threading.Tasks.Task OnReconnected()
        {
            Clients.Caller.updateYourself(Context.ConnectionId + " - reconnected");
            Clients.Caller.updateYourself("ALL");
            log.Info( ServerName + Context.ConnectionId + " - reconnected");
            await base.OnReconnected();
        }

        ///// <summary>
        ///// unregister disconected user
        ///// </summary>
        ///// <returns></returns>
        //public override System.Threading.Tasks.Task OnDisconnected()
        //{
        //    //broadcastclients("-Server- A client exited");
        //    clientCounter--;
        //    log.Info( ServerName + "New Client Connected");
        //    return base.OnDisconnected();
        //}

        #endregion
    }
}