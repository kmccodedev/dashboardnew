﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;

[assembly: OwinStartup(typeof(OptimiseDashboardApp.Startup))]

namespace OptimiseDashboardApp
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            //var hubConfiguration = new HubConfiguration();

            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888

            //string sqlConnectionString = "Server = ; Database = ; User ID = ; Password = ;Trusted_Connection = False;";
            //string sqlConnectionString = "Data Source = 10.20.48.26;Initial Catalog = OptimiseSR; User ID = marstondba; Password = Monday12$;Integrated Security=SSPI;";
            //GlobalHost.DependencyResolver.UseSqlServer(sqlConnectionString);
            //string sqlConnectionString = "Data Source=.\\SQLEXPRESS;Initial Catalog=OptimiseSignalR;Connect Timeout=200; pooling=true; Max Pool Size=200; Persist Security Info=False;User ID=sa;Password=1234";
            //string sqlConnectionString = "Server=192.168.170.150; Database=OptimiseSignalR;User Id=GatewayUser; Password=G@t3w@y#;";

            GlobalHost.Configuration.ConnectionTimeout = TimeSpan.FromSeconds(20);
            // Wait a maximum of 30 seconds after a transport connection is lost
            // before raising the Disconnected event to terminate the SignalR connection.

            GlobalHost.Configuration.DisconnectTimeout = TimeSpan.FromSeconds(15);
            // For transports other than long polling, send a keepalive packet every
            // 10 seconds. 
            // This value must be no more than 1/3 of the DisconnectTimeout value.
            GlobalHost.Configuration.KeepAlive = TimeSpan.FromSeconds(5);

            //GlobalHost.DependencyResolver.UseSqlServer(sqlConnectionString);

            app.UseCors(CorsOptions.AllowAll);

            //hubConfiguration.EnableDetailedErrors = true;

            app.MapSignalR();

            //app.MapSignalR();

            /*
            app.MapSignalR();
            <add name="MarstonDB"  connectionString="Server = 10.20.48.26; Database = GatewayUAT; User Id = marstondba; Password = Monday12$;"/>
             <add name="SignalR" providerName="System.Data.SqlClient" connectionString="Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=SignalR;Data Source=machine1" />
             */
            //string sqlConnectionString = "Server=10.20.48.26; Database=OptimiseSR; User Id = marstondba; Password = Monday12$;";
            //GlobalHost.DependencyResolver.UseSqlServer(sqlConnectionString);

            //var hubConfiguration = new HubConfiguration();
            //hubConfiguration.EnableDetailedErrors = true;

            /*
            app.Map("/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration
                {
                };
                map.RunSignalR(hubConfiguration);
            });*/
        }
    }
}
