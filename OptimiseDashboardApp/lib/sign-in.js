﻿/*
==========================================================================================================================================
 File Name     : sign-in.js 
 File Created  : Feb 5, 2017 
 Created By    : Joy
==========================================================================================================================================
 Modified By   : Joy 
 Modified Date : Mar 24, 2017 
==========================================================================================================================================
*/
$(document).ready(function () {
    checksession();
});

//==========================================================================================================================================
// TODO : Check Session
//==========================================================================================================================================
function checksession() {
    var l_companyid = $.session.get('CompanyID');
    var l_roletype = $.session.get('RoleType');
    l_companyid = $.trim(l_companyid);
    l_roletype = $.trim(l_roletype);
    if ((l_companyid.length > 0) && (l_roletype.length > 0)) {
        if (l_roletype == 6) {
            $.session.set('bc', 1);
            $.session.set('IsVedio', 1);
            window.top.location.href = HrUrl;
        }
        else if (l_roletype == 5) {
            $.session.set('bc', 1);
            $.session.set('IsVedio', 1);
            window.top.location.href = TeamAdminUrl;
        }
        else if (l_roletype == 1) {
            $.session.set('IsVedio', 1);
            if (l_companyid == 1)
                window.top.location = "Dashboard_MS.html";
            else
                window.top.location = "Dashboard_HC.html";
        }
        else if (l_roletype == 7) {
            if (l_companyid == 1)
                window.top.location = "Dashboard_ES.html";
            else
                window.top.location = "Dashboard_HC.html";
        }
        else {
            $.session.set('bc', 1);
            $.session.set('IsVedio', 1);
            if (l_companyid == 1)
                window.top.location = "Dashboard_MS.html";
            else
                window.top.location = "Dashboard_HC.html";
        }
    }
    else {
        $.session.clear();
    }
}
//==========================================================================================================================================
