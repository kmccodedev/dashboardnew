﻿/// <reference path="../../admin/js/jquery.session.js" />
//==========================================================================================================================================
/* File Name: Marston_dataservice.js */
/* File Created: January 21, 2014 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================
//TODO : Tree view list and availability show

//==========================================================================================================================================
//TODO : Map


//==========================================================================================================================================
//TODO : Notification

function UpdateAutoRefresh(OfficerID, AutoRefresh) {
    if (OfficerID == undefined) OfficerID = 0;
    $.ajax({
        url: ServiceURL + 'api/v1/officers/' + OfficerID + '/' + AutoRefresh + '/UpdateAutoRefresh',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'PUT',
        dataType: 'json',
        beforeSend: function () { },
        success: function (data) {
            AlertMessage("Info", "Auto refresh value has been updated successfully.");
            //$('#lblTargetInfo').html('Auto refresh value has been updated successfully.');
            $('#ATargetInfo').click();
            $.session.set('aref', AutoRefresh, 1);
            $('#txtRefreshTime').val($.session.get('aref'));
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function showhidetabs(tabname) {
    $('#show-search-details,#tblbodySearchType').hide();//,#tblWMAADM
    $('#' + tabname).show();
}

function caseactionshowhidetabs() {
    $('#leftletterformanager,#revisitformanager,#partpaidformanager,#returnedformanager,#paidformanager,#deviatedformanager,#DeviatedGPS').hide();
    $('#bailedformanager,#arrestedformanager,#Surrenderdateagreedformanager').hide();
    $('#tcgpaidformanager,#tcgppformanager,#tcgformanager,#uttcformanager,#droppedformanager,#otherformanager,#Clampedformanager').hide();
}
//==========================================================================================================================================
//Unwanted functions
//Old - Not Modified
function FillWMADetailL3(OfficerID, FromDate, ToDate) {
    return;
    Type = "GET";
    var inputParams = "/GetWMADetailL1_Dashboard?OfficerID=" + OfficerID + "&FromDate=" + FromDate + "&ToDate=" +
        FromDate + "&" + $.session.get('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    //result = JSON.parse(result);

                    $('#tblbodySearchType').empty();
                    showhidetabs('tblbodySearchType');
                    $('#lblSearchType').html('');
                    $('#btnBack').hide();
                    $('#tblbodyWMASearchType').empty();
                    $('#tblWMASearchType').show();
                    $('#tblWMA').hide();

                    $.each(result, function (key, value) {
                        $('#tblbodySearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', value.LogType).html(value.LogType)))
                            .append($('<td>').html(value.Activities)))

                        $('#tblbodyWMASearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', 'A' + value.LogType)
                            .attr('data-toggle', 'modal').attr('href', '#windowTitleDialog').html(value.LogType))).append($('<td>').html(value.Activities)))

                        $('#A' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        });

                        $('#' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        })
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

//TODO : Statistics function
//Old - Not Modified
function GetStatistics() {//OfficerID, link
    return;
    $('.popover').remove();
    Type = "GET";
    var inputParams = "/GetStatsDashboard?OfficerID=" + OfficerID + "&" + $.session.get('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null) {
                    //result = JSON.parse(result);
                    $('#tbodyStats').empty();
                    $.each(result, function (key, value) {
                        var StatFormula = '<table width="100%" class="display-status-table"><tr><td>' +
                                            '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                            '<a class="close" data-dismiss="modal" href="#">X</a>' +
                                            '<p class="block-heading">Percentage calculation</p><table class="">' +
                                            '<tbody>' +
                                            '<tr><td>Formula for ' + value.Description + ' : ' + value.Formula + '</td></tr>' +
                                            '<tr><td>' + '</td></tr>' +
                                            '<tr><td>Calculation for ' + value.Description + ' : ' + value.Calculation + '</td></tr>' +
                                           '</tbody></table></div></td>' +
                                           '</td></tr></table>';

                        if ($.session.get('CompanyID') == 1) {
                            $('#tbodyStats').append($('<tr>')
                                .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                .append($('<td>').append($('<a>').attr('id', 'AStat' + key).attr('style', 'cursor:pointer;')
                                                    .attr('onclick', 'GetStatsOnClick(' + key + ')').html(value.Description)
                                                    .attr('data-original-title', '').attr('rel', 'popover').attr('class', 'a1').attr('data-html', 'true')))
                                .append($('<td id="tdbodyStats' + key + '">').html(value.DisplayValue)))
                        }
                        else {
                            $('#tbodyStats').append($('<tr>')
                              .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                              .append($('<td>').append($('<a>').attr('id', 'AStat' + key).attr('style', 'cursor:pointer;')
                                                 .html(value.Description)
                                                  .attr('data-original-title', '').attr('rel', 'popover').attr('class', 'a1').attr('data-html', 'true')))
                              .append($('<td id="tdbodyStats' + key + '">').html(value.DisplayValue)))
                        }


                        $('#AStat' + key).mouseover(function () {
                            $('#AStat' + key).attr('data-content', StatFormula);
                            $('#AStat' + key).attr('data-placement', 'left');
                            $('#AStat' + key).popover('show');
                        });
                        $('#AStat' + key).mouseout(function () {
                            $('#AStat' + key).popover('hide');
                        });

                        if (link == 1) {
                            if ($.trim(value.Description) == "Case holdings") {
                                $('#tdbodyStats' + key).empty();//data-toggle="modal" href="#DivLogout"
                                $('#tdbodyStats' + key).append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseSearchByOfficerID').attr('onclick', 'CaseSearchByOfficerID(' + OfficerID + ')').html(value.DisplayValue));
                            }
                        }
                    });
                    $('.close').click(function () { $('.popover').remove(); });
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

//Old - Not Modified
function CaseSearchByOfficerID(OfficerID) {
    var Type = "GET";
    var DataType = "jsonp"; var ProcessData = false;
    var Url = serviceUrl + "/GetCaseSearchByOfficerID?OfficerID=" + OfficerID + "&" + $.session.get('BrowserId');

    var ContentType = "application/javascript";
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call                      
            if (!(result == null || result == '[]')) {

                //result = JSON.parse(result);
                $('#tbodyCaseSearchByOfficerID').empty();
                var HTMLString = '';
                $.each(result, function (key, value) {
                    HTMLString += '<tr><td>' + value.casenumber + '</td><td>' + value.DebtName + '</td><td>' + value.ClientName + '</td><td>' + value.ContactFirstLine + ', ' + value.ContactAddress + ', ' + value.PostCode + '</td>' +
                            '<td>' + value.CaseStatus + '</td><td>' + value.OfficerFirstName + ' ' + value.OfficerLastName + '</td><td>' + value.Ageing + '</td><td>' + value.IssueDate + '</td><td>' + value.DateActioned + '</td></tr>';
                });
                $('#tbodyCaseSearchByOfficerID').html(HTMLString);

                $('#tblCaseSearchByOfficerID').dataTable({
                    "sScrollY": "370px",
                    "bAutoWidth": false,
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "bLengthChange": false,
                    "bScrollCollapse": true,
                    "sInfo": true,
                    "iDisplayLength": 5

                });
            }
            else {
                $('#tbodyCaseSearchByOfficerID').empty();
                $('#tbodyCaseSearchByOfficerID').append($('<tr><td colspan="9"> No records found </td></tr>'));
            }

        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

//Old - Modified
function FillOfficerRankOnClick123(Action, key, Condition, MgrName, ActionText) {
    var FnName = FillRankingsForADM_Normal;
    switch (Condition) {
        case 1: //Ranking ADM list
            //Tree level click function
            switch ($.session.get('TreeLevel')) {
                case '0':// Manager click: Shown ADM list
                    $('#DRankingOfficer,#DRankingCase,#btnRankADMBack,#btnRankOfficerBack').hide();
                    $('#DRankingADM').show();
                    $('#lblRankOfficerName,#lblRankMgrName,#lblRankADMName').html('');
                    $('#lblRankMgrName').html(' - ' + MgrName);
                    FnName = FillRankingsForADM_Normal;
                    break;
                case '1'://ADM click: Shown Officer list
                    $('#DRankingADM,#DRankingCase,#btnRankADMBack,#btnRankOfficerBack,#btnRankADMBack').hide();
                    $('#DRankingOfficer').show();
                    $('#lblRankOfficerName').html('');
                    $('#lblRankADMName').html(' - ' + MgrName);
                    FnName = FillRankingsForOfficer_Normal;
                    break;
                case '2'://Officer click: Shown case list
                    $('#DRankingADM,#DRankingOfficer,#DRankingCase,#btnRankADMBack,#btnRankOfficerBack').hide();
                    FillCaseActionDetailsOnClick(Action, key, MgrName, 0);
                    break;
            }
            break;
        case 2: //Ranking More ADM list
            switch (Action) {
                case 1://Paid
                    $('#DRankMorePaid,#DRankMorePaidCaseList').hide();
                    $('#DRankingADM,#btnPaidADMBack').show();
                    $('#lblRankPaidManager').html(' - ' + MgrName);
                    break;
                case 2://Returned
                    $('#DRankMoreReturned,#DRankMoreReturnedCaseList').hide();
                    $('#DRankMoreReturnedADM,#btnReturnedADMBack').show();
                    $('#lblRankReturnedManager').html(' - ' + MgrName);
                    break;
                case 3://Part Paid
                    $('#DRankMorePP,#DRankMorePPCaseList').hide();
                    $('#DRankMorePPADM,#btnPPADMBack').show();
                    $('#lblRankPPManager').html(' - ' + MgrName);
                    break;
            }
            FnName = FillRankingsForADM_Normal;
            break;
        case 3: // Ranking Officer list
            $('#DRankingADM,#DRankingCase,#btnRankOfficerBack').hide();
            if ($.session.get('TreeLevel') == '1')
                $('#btnRankADMBack').hide();
            else
                $('#btnRankADMBack').show();

            $('#DRankingOfficer').show();
            $('#lblRankOfficerName').html('');
            $('#lblRankADMName').html(' - ' + MgrName);
            FnName = FillRankingsForOfficer_Normal;
            break;
        case 4://Ranking More Officer list
            switch (Action) {
                case 1://Paid
                    $('#DRankMorePaid,#DRankMorePaidCaseList,#DRankMorePaidADM,#btnPaidADMBack').hide();
                    $('#DRankMorePaidOfficer,#btnPaidOfficerBack').show();
                    $('#lblRankPaidADM').html(' - ' + MgrName);
                    break;
                case 2://Returned
                    $('#DRankMoreReturned,#DRankMoreReturnedCaseList,#DRankMoreReturnedADM,#btnReturnedADMBack').hide();
                    $('#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').show();
                    $('#lblRankReturnedADM').html(' - ' + MgrName);
                    break;
                case 3://Part Paid
                    $('#DRankMorePP,#DRankMorePPCaseList,#DRankMorePPADM,#btnPPADMBack').hide();
                    $('#DRankMorePPOfficer,#btnPPOfficerBack').show();
                    $('#lblRankPPADM').html(' - ' + MgrName);
                    break;
            }
            FnName = FillRankingsForOfficer_Normal;
            break;
    }
    FnName(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), $.session.get('RankingType'), 0);
}

function FillRankingMoreOnClick123(Action, key, Condition, MgrName) {
    // alert('Action :' + Action + ' key :' + key + ' Condition :' + Condition + ' Mgrname :' + MgrName);
    //FillRankingMoreOnClick(1, 779, 2, "Manager 15")
    var FnName = FillRankingsForADM_Normal;
    //Ranking More ADM list
    switch (Action) {
        case 1://Paid
            $('#DRankMorePaid,#DRankMorePaidCaseList').hide();
            $('#DRankMorePaidADM,#btnPaidADMBack').show();
            $('#lblRankPaidManager').html(' - ' + MgrName);
            break;
        case 2://Returned
            $('#DRankMoreReturned,#DRankMoreReturnedCaseList').hide();
            $('#DRankMoreReturnedADM,#btnReturnedADMBack').show();
            $('#lblRankReturnedManager').html(' - ' + MgrName);
            break;
        case 3://Part Paid
            $('#DRankMorePP,#DRankMorePPCaseList').hide();
            $('#DRankMorePPADM,#btnPPADMBack').show();
            $('#lblRankPPManager').html(' - ' + MgrName);
            break;
    }
    FnName = FillRankingsForADM_Normal;

    switch (Action) {
        case 1:
            FnName(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', 0, true);//$('#lblPaidOfficerID' + key).html()
            break;
        case 2:
            FnName(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', 0, true);//$('#lblReturnOfficerID' + key).html()
            break;
        case 3:
            FnName(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', 0, true);//$('#lblPartPaidOfficerID' + key).html()
            break;
    }
}

function FillRankingsForADM_Normal123(ManagerID, FromDate, ToDate, ActionText, GroupID, ShowAll) {
    Type = "GET";
    serviceTypeLogin = "getrankings";
    if (GroupID == undefined) GroupID = 0;
    if (ShowAll == undefined) ShowAll = false;
    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '-' + dateTr[1] + '-' + dateTr[0];
    ToDate = newToDate;
    
    var Flag;
    switch (ActionText) {
        case 'Paid':
            $('#lblRankStatus').html('Ranking - Paid');
            $('#HCaseActionTitle').html('Paid');
            Flag = 1;
            break;
        case 'Returned':
            $('#lblRankStatus').html('Ranking - Returned');
            $('#HCaseActionTitle').html('Returned');
            Flag = 4;
            break;
        case 'Part Paid':
            $('#lblRankStatus').html('Ranking - Part Paid');
            $('#HCaseActionTitle').html('Part Paid');
            Flag = 2;
            break;
    }
    
    $.ajax({
        type: 'GET',
        //url: ServiceURL + 'api/v1/officers/' + ManagerID + '/' + FromDate + '/' + ToDate + '/' + ActionText + '/Top3RankDashboard',
        url: ServiceURL + 'api/v1/cases/' + ActionText + '/' + FromDate + '/' + ToDate + '/' + 'CaseActionssDetailForManager?ManagerID=' + ManagerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader8').show();
            $('#changeclass1 div').css('background', '#F9F9F9');
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                $('#tbodyADMRank').empty();                
                //TODO: Rank more
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    //result = JSON.parse(result);                    
                    $('#tbodyRMPaidADM').empty();
                    $('#tbodyRMReturnADM').empty();
                    $('#tbodyRMPPADM').empty();
                    $.each(result, function (key, value) {
                        //if (value.ActionText == ActionText) {
                        //switch (value.ActionText) {//
                        switch (ActionText) {
                                case 'Paid':
                                    //$('#tbodyRankMorePaidADM').append($('<tr>').append($('<td>')
                                    $('#tbodyRMPaidADM').append($('<tr>').append($('<td>')
                                                 .append($('<a>').attr('onclick', 'FillOfficerRankOnClick(1,' + value.OfficerID + ',4,"' + value.OfficerName + '","' + ActionText + '")')
                                                    .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                                 .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                                 .append($('<td>').html(value.CaseCnt)))

                                    $('#tbodyADMRank').append($('<tr>').append($('<td>')
                                           .append($('<a>').attr('onclick', 'FillOfficerRankOnClick(1,' + value.OfficerID + ',3,"' + value.OfficerName + '","' + value.ActionText + '")').attr('href', '#paid').html(value.OfficerName))
                                           .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide()))
                                           .append($('<td>').html(value.CaseCnt)));
                                    break;
                                case 'Returned':

                                    $('#tbodyRMReturnADM').append($('<tr>').append($('<td>')
                                                 .append($('<a>').attr('onclick', 'FillOfficerRankOnClick(2,' + value.OfficerID + ',4,"' + value.OfficerName + '","' + ActionText + '")')
                                                    .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                                 .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                                 .append($('<td>').html(value.Case)));

                                    $('#tbodyADMRank').append($('<tr>').append($('<td>')
                                          .append($('<a>').attr('onclick', 'FillOfficerRankOnClick(2,' + value.OfficerID + ',3,"' + value.OfficerName + '","' + ActionText + '")').attr('href', '#paid').html(value.OfficerName))
                                          .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                          .append($('<td>').html(value.CaseCnt)));
                                    break;
                                case 'Part Paid':
                                    $('#tbodyRMPPADM').append($('<tr>').append($('<td>')
                                                .append($('<a>').attr('onclick', 'FillOfficerRankOnClick(3,' + value.OfficerID + ',4,"' + value.OfficerName + '","' + ActionText + '")')
                                                   .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                                .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                                .append($('<td>').html(value.Case)))

                                    $('#tbodyADMRank').append($('<tr>').append($('<td>')
                                          .append($('<a>').attr('onclick', 'FillOfficerRankOnClick(3,' + value.Officer + ',3,"' + value.OfficerName + '","' + ActionText + '")').attr('href', '#paid').html(value.OfficerName))
                                          .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                          .append($('<td>').html(value.CaseCnt)));
                                    break;
                            }
                        //}
                    });
                }
                else {
                    $('#tbodyADMRank').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                    //TODO: Rank more
                    switch (ActionText) {
                        case 'Paid':
                            $('#tbodyRankMorePaidOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                        case 'Returned':
                            $('#tbodyRankMoreReturnedOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                        case 'Part Paid':
                            $('#tbodyRankMorePPOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                    }
                }
            }
        },
        complete: function () {
            $('#imgLoader8').fadeOut(1000);
            $('#changeclass1 div').css('background', '');
        },
        error: function (xhr, textStatus, errorThrown) {
            //alert('error');
        } // When Service call fails
    });
}

function FillRankingsForOfficer_Normal123(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    alert('FillRankingsForOfficer');
    Type = "GET";
    serviceTypeLogin = "getrankings";
    if (GroupID == undefined) GroupID = 0;

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '-' + dateTr[1] + '-' + dateTr[0];
    ToDate = newToDate;

    var Flag;
    switch (ActionText) {
        case 'Paid':
            $('#lblRankStatus').html('Ranking - Paid');
            $('#HCaseActionTitle').html('Paid');
            Flag = 1;
            break;
        case 'Returned':
            $('#lblRankStatus').html('Ranking - Returned');
            $('#HCaseActionTitle').html('Returned');
            Flag = 4;
            break;
        case 'Part Paid':
            $('#lblRankStatus').html('Ranking - Part Paid');
            $('#HCaseActionTitle').html('Part Paid');
            Flag = 2;
            break;
    }
    alert(ServiceURL + 'api/v1/cases/' + ActionText + '/' + FromDate + '/' + ToDate + '/' + 'CaseActionssDetailForManager?ManagerID=' + ManagerID);
    $.ajax({
        type: 'GET',
        //url: ServiceURL + 'api/v1/officers/' + ManagerID + '/' + FromDate + '/' + ToDate + '/' + ActionText + '/Top3RankDashboard',
        url: ServiceURL + 'api/v1/cases/' + ActionText + '/' + FromDate + '/' + ToDate + '/' + 'CaseActionssDetailForManager?ManagerID=' + ManagerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader8').show();
            $('#changeclass1 div').css('background', '#F9F9F9');
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                $('#tbodyOfficerRank').empty();
                //TODO: Rank more
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    //result = JSON.parse(result);
                    $('#tbodyRankMorePaidOfficer').empty();
                    $('#tbodyRankMoreReturnedOfficer').empty();
                    $('#tbodyRankMorePPOfficer').empty();
                    $.each(result, function (key, value) {
                        //if (value.ActionText == ActionText) {
                            switch (ActionText) {
                                case 'Paid':
                                    $('#tbodyRankMorePaidOfficer').append($('<tr>').append($('<td>')
                                            .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                            .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                            .append($('<td>').html(value.CaseCnt)));

                                    $('#tbodyOfficerRank').append($('<tr>').append($('<td>')
                                           .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick(1,' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                           .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                           .append($('<td>').html(value.CaseCnt)));
                                    break;
                                case 'Returned':
                                    $('#tbodyRankMoreReturnedOfficer').append($('<tr>').append($('<td>')
                                                 .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                                    .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                                 .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                                 .append($('<td>').html(value.CaseCnt)));

                                    $('#tbodyOfficerRank').append($('<tr>').append($('<td>')
                                          .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick(2,' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                          .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                          .append($('<td>').html(value.CaseCnt)));
                                    break;
                                case 'Part Paid':
                                    $('#tbodyRankMorePPOfficer').append($('<tr>').append($('<td>')
                                                .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                                   .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                                .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                                .append($('<td>').html(value.CaseCnt)));

                                    $('#tbodyOfficerRank').append($('<tr>').append($('<td>')
                                          .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick(3,' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                          .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                          .append($('<td>').html(value.CaseCnt)));
                                    break;
                            }
                        //}
                    });
                }
                else {
                    $('#tbodyOfficerRank').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                    //TODO: Rank more
                    switch (ActionText) {
                        case 'Paid':
                            $('#tbodyRankMorePaidOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                        case 'Returned':
                            $('#tbodyRankMoreReturnedOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                        case 'Part Paid':
                            $('#tbodyRankMorePPOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                    }
                }
            }
        },
        complete: function () {
            $('#imgLoader8').fadeOut(1000);
            $('#changeclass1 div').css('background', '');
        },
        error: function (xhr, textStatus, errorThrown) {
            //alert('error');
        } // When Service call fails
    });
}

function FillCaseActionDetailsOnClick123(Action, OfficerID, OfficerName, Check) {
    $('#HCaseActionNavigation').show();
    $('#btnCaseActionBack').show();
    $('#divCaseActionsManagerCount,#divCaseActionsADMCount,#divCaseActionsOfficerCount,#btnCaseActionManager').hide();
    $('#DRankingOfficer,#DRankMorePaidOfficer,#btnPaidOfficerBack,#DRankMoreReturnedOfficer,#btnReturnedOfficerBack,#DRankMorePPOfficer,#btnPPOfficerBack,#btnRankADMBack,#btnRankOfficerBack').hide();
    $('#tblRankOfficerReturn,#tblRankOfficerPartPaid,#tblRankOfficerPaid').hide();
    $('#lblCAOfficerName,#lblRankOfficerName,#lblRankPaidOfficer,#lblRankReturnedOfficer,#lblRankPPOfficer').html(' - ' + OfficerName);

    if (Check == 0) {
        switch (Action) {
            case 1:
                $('#lblRankStatus').html('Ranking - Paid');
                $('#DRankingCase,#tblRankOfficerPaid').show();//,#btnRankOfficerBack
                if ($.session.get('TreeLevel') == 2)
                    $('#btnRankOfficerBack').hide();
                else
                    $('#btnRankOfficerBack').show();

                $('#HCaseActionTitle').html('Paid');
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 2:
                $('#lblRankStatus').html('Ranking - Returned');
                $('#DRankingCase,#tblRankOfficerReturn').show();
                if ($.session.get('TreeLevel') == 2)
                    $('#btnRankOfficerBack').hide();
                else
                    $('#btnRankOfficerBack').show();
                $('#HCaseActionTitle').html('Returned');
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 3:
                $('#lblRankStatus').html('Ranking - Part Paid');
                $('#DRankingCase,#tblRankOfficerPartPaid').show();
                if ($.session.get('TreeLevel') == 2)
                    $('#btnRankOfficerBack').hide();
                else
                    $('#btnRankOfficerBack').show();
                $('#HCaseActionTitle').html('Part Paid');
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 4:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
        }
    }
    else {
        switch (Action) {
            case 1:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 2:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 3:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 4:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
        }
    }
}

function FillCaseDetailsAction123(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Clamp = 0;
    Bailed = 0;
    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;


    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '-' + dateTr[1] + '-' + dateTr[0];
    ToDate = newToDate;
    
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + ManagerID + '/' + FromDate + '/' + ToDate + '/CaseActionssDetail?OfficerID=' + OfficerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader8').show();
            $('#changeclass1 div').css('background', '#F9F9F9');
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    //result = JSON.parse(result);
                    $('#tblbodyActionPaid').empty();
                    $('#tbodyActionPP').empty();
                    $('#tbodyActionReturned').empty();
                    $('#tbodyActionLeftLetter').empty();
                    $('#tbodyActionEnforcementStart').empty();
                    $('#tbodyActionRevisit').empty();
                    $('#tbodyActionEnforcementEnd').empty();

                    $('#tbodyActionClamped').empty();
                    $('#tbodyActionBailed').empty();
                    $('#tbodyActionArrested').empty();
                    $('#tbodyActionSurrenderDateAgreed').empty();


                    $('#tbodyActionTCG').empty();
                    $('#tbodyActionTCGPAID').empty();
                    $('#tbodyActionTCGPP').empty();
                    //$('#tbodyAction_PAID').empty();
                    //$('#tbodyAction_PARTPAID').empty();
                    $('#tbodyActionUTTC').empty();
                    $('#tbodyActionDROPPED').empty();
                    $('#tbodyActionOTHER').empty();

                    $('#tbodyRankOfficerPaid').empty();
                    $('#tbodyRankOfficerPartPaid').empty();
                    $('#tbodyRankOfficerReturn').empty();

                    $.each(result, function (key, value) {
                        switch (value.ActionText) {
                            case 'Paid':
                                if (value.ResponseType != '0') {
                                    alert(JSON.stringify(value));
                                    if (value.ResponseType == 'NoError') {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerPaid').append($('<tr style = "background:#66FF99">')
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerPaid').append($('<tr style = "background:#FFCCFF">')
                                     .append($('<td>').html(''))
                                     .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                     .append($('<td>').html(value.ActionText))
                                     .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                     .append($('<td>').html(value.DateActioned))
                                     .append($('<td>').html("" + value.Fees))
                                     .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                     .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tblbodyActionPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                    $('#tbodyRankOfficerPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Part Paid':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerPartPaid').append($('<tr style = "background:#66FF99">')
                                    .append($('<td>').html(''))
                                    .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                    .append($('<td>').html(value.ActionText))
                                    .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                    .append($('<td>').html(value.DateActioned))
                                    .append($('<td>').html("" + value.Fees))
                                    .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                    .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerPartPaid').append($('<tr style = "background:#FFCCFF">')
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                    $('#tbodyRankOfficerPartPaid').append($('<tr>')
                                      .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td>').html(value.DoorColour))
                                      .append($('<td>').html(value.HouseType))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Returned':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionReturned').append($('<tr style = "background:#66FF99">')//,#tblbodyReturned
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerReturn').append($('<tr style = "background:#66FF99">')//,#tblbodyReturned
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionReturned').append($('<tr style = "background:#FFCCFF">')//,#tblbodyReturned
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerReturn').append($('<tr style = "background:#FFCCFF">')//,#tblbodyReturned
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionReturned').append($('<tr>')//,#tblbodyReturned
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                    $('#tbodyRankOfficerReturn').append($('<tr>')//,#tblbodyReturned
                                    .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                    .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                    .append($('<td>').html(value.ActionText))
                                    .append($('<td>').html(value.DoorColour))
                                    .append($('<td>').html(value.HouseType))
                                    .append($('<td>').html(value.DateActioned))
                                    .append($('<td>').html("" + value.Fees))
                                    .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                    .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Left Letter':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionLeftLetter').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionLeftLetter').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionLeftLetter').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Clamped':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionClamped')
                                         .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                   '<img id="imgActionClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionclampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                        if (Clamp == 0) {
                                            ClampKey = key;
                                            ViewActionClampedImage(key, value.Officer, value.CaseNumber);
                                            $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionclampSelect');
                                            Clamp += 1;
                                        }
                                    }
                                    else {
                                        $('#tbodyActionClamped')
                                        .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                   '<img id="imgActionClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionclampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                        if (Clamp == 0) {
                                            ClampKey = key;
                                            ViewActionClampedImage(key, value.Officer, value.CaseNumber);
                                            $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionclampSelect');
                                            Clamp += 1;
                                        }
                                    }
                                }
                                else {
                                    $('#tbodyActionClamped')
                                       .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                   '<img id="imgActionClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionclampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                    if (Clamp == 0) {
                                        ClampKey = key;
                                        ViewActionClampedImage(key, value.Officer, value.CaseNumber);
                                        $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionclampSelect');
                                        Clamp += 1;
                                    }
                                }
                                break;
                            case 'Bailed':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionBailed')
                                     .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                    '<td>' + value.Officer + '</td>' +
                                                    '<td>' + value.DateActioned + '</td>' +
                                                    '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                     '<img id="imgActionBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionBailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                                        if (Bailed == 0) {
                                            BailedKey = key;
                                            $('#imgActionBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionBailedSelect');
                                            ViewActionBailedImage(key, "'" + value.ImageURL + "'");
                                            Bailed += 1;
                                        }
                                    }
                                    else {
                                        $('#tbodyActionBailed')
                                     .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                    '<td>' + value.Officer + '</td>' +
                                                    '<td>' + value.DateActioned + '</td>' +
                                                    '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                     '<img id="imgActionBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionBailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                                        if (Bailed == 0) {
                                            BailedKey = key;
                                            $('#imgActionBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionBailedSelect');
                                            ViewActionBailedImage(key, "'" + value.ImageURL + "'");
                                            Bailed += 1;
                                        }
                                    }
                                }
                                else {
                                    $('#tbodyActionBailed')
                                    .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                    '<img id="imgActionBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionBailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                                    if (Bailed == 0) {
                                        BailedKey = key;
                                        if ($.session.get('TreeLevel') == '3') {
                                            $('#imgActionBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionBailedSelect');
                                            ViewActionBailedImage(key, "'" + value.ImageURL + "'");
                                        }
                                        Bailed += 1;
                                    }
                                }
                                break;
                            case 'Arrested':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionArrested').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionArrested').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionArrested').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Surrender date agreed':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionSurrenderDateAgreed').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionSurrenderDateAgreed').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionSurrenderDateAgreed').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'EnforcementStart':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionEnforcementStart').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionEnforcementStart').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionEnforcementStart').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case '£ Collected':
                            case 'Revisit':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionRevisit').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionRevisit').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionRevisit').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'EnforcementEnd':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionEnforcementEnd').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionEnforcementEnd').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionEnforcementEnd').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;

                            case 'TCG':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionTCG').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'TCG PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPAID').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'TCG PP':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tblbodyActionPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'PART PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'UTTC':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionUTTC').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'DROPPED':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionDROPPED').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'OTHER':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionOTHER').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                        }
                    });
                }
                else {
                    $('#tblbodyActionPaid').empty().html('No records found');
                    $('#tbodyActionPP').empty().html('No records found');
                    $('#tbodyActionReturned').empty().html('No records found');
                    $('#tbodyActionLeftLetter').empty().html('No records found');
                    $('#tbodyActionEnforcementStart').empty().html('No records found');
                    $('#tbodyActionRevisit').empty().html('No records found');
                    $('#tbodyActionEnforcementEnd').empty().html('No records found');

                    $('#tbodyActionClamped').empty().html('No records found');
                    $('#tbodyActionBailed').empty().html('No records found');
                    $('#tbodyActionArrested').empty().html('No records found');
                    $('#tbodyActionSurrenderDateAgreed').empty().html('No records found');
                    //$('#tbodyleftletterformanager').empty().html('No records found');
                    //$('#tbodyrevisitformanager').empty().html('No records found');

                    $('#tbodyActionTCG').empty().html('No records found');
                    $('#tbodyActionTCGPAID').empty().html('No records found');
                    $('#tbodyActionTCGPP').empty().html('No records found');
                    $('#tbodyActionUTTC').empty().html('No records found');
                    $('#tbodyActionDROPPED').empty().html('No records found');
                    $('#tbodyActionOTHER').empty().html('No records found');
                }
            }
        },
        complete: function () {
            $('#imgLoader8').fadeOut(1000);
            $('#changeclass1 div').css('background', '');
        },
        error: function (xhr, textStatus, errorThrown) {
            //alert('error');
        } // When Service call fails
    });
}
