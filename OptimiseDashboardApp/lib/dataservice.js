
var serviceUrl = "http://www.testoptimise.org:8196/OAService/MarstonOfficeToolService.svc"; // FOR ATOM8 USE; IGNORE IT
//var serviceUrl = "http://www.testoptimise.org:8065/MobinocisService/MarstonOfficeToolService.svc"; // FOR ATOM8 USE; IGNORE IT
//var serviceUrl = "http://localhost:8065/MobinocisService/MarstonOfficeToolService.svc";
//var RedirectUrl = "http://www.testoptimise.org:8196/testoam/Admin/admin-page.html";
var RedirectUrl = "http://localhost:8065/Admin/admin-page.html";

/*****  START SERVICE CALLS *************************/

var Type;
var Url;
var Data;
var ContentType = "application/javascript";
var DataType;
var ProcessData;
var response;

var MTV = '';

var serviceTypeLogin;
var GlobalUserName;
var GlobalPassword;

var serviceTypeCaseSearch;
var serviceTypeCaseSearchDetail;
var serviceTypeSearchVRM;
var serviceTypeSearchVRMDetail;

var CaseNumber;
var LinkForManager = 0;
var CompanyID = 1;
var OffID;
var tableIndex = 1; // for officer status tables
var IntervalvalueOfficerStatus;
//Generic function to call WCF Service for GET
function CallService() {

    var isGridProcessing = (serviceTypeCaseSearchDetail == 'getcasedetails') ? true : false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (msg) {//On Successfull service call 

            DataServiceSucceeded(msg);
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

//Result handler.
function DataServiceSucceeded(result) {

    if (DataType == "jsonp") {

        if (result != undefined) {

            if (serviceTypeLogin != undefined && serviceTypeLogin == "login") {

                serviceTypeLogin = undefined;
                if (result != undefined) {
                    if (result != "[]" && result != null) {

                        result = JSON.parse(result);

                        $.each(result, function (key, value) {
                            //window.name = value.OfficerID + "|" + value.OfficerName;
                            setCookie('OfficerID', $.trim(value.OfficerID.toString()), 1);
                            setCookie('OfficerName', $.trim(value.OfficerName), 1);
                            setCookie('CompanyID', $.trim(value.CompanyID), 1);

                            setCookie('tgVal', $.trim(value.TargetNo), 1);

                            setCookie('aref', $.trim(value.AutoRefresh), 1);

                            if (value.BlockChat == 1) {
                                setCookie('bc', 1, 1);
                                window.top.location = "index.html";
                            }
                            else if (value.BlockChat == 2) {
                                setCookie('bc', 2, 1);
                                window.top.location = "index.html";
                            }
                            else {
                                setCookie('bc', 0, 1);
                                window.top.location = "index.html";
                            }

                        });
                    }
                    else {
                        $('#lblUNPWDWarningInfo').html("Officer ID and Password didnot match.");
                    }
                }


            }
        }
    }

}

function Login(officerId, password) {

    Type = "GET";
    serviceTypeLogin = "login";

    if (officerId != "" && password != "") {

        var inputParams = "/LoginOfficer?officerID=" + officerId + "&password=" + password;

        Url = serviceUrl + inputParams;
        DataType = "jsonp"; ProcessData = false;

        CallService();
    }
}


function GetOfficerManager(ManagerID, SelectedOfficerID, multiselect) {

    $('.popover').remove();

    Type = "GET";
    serviceTypeLogin = "getofficermanager";
    var inputParams = "/GetOfficersForManager?ManagerID=" + ManagerID;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $('#selCurrentMonth').val((new Date).getMonth() + 1);

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null) {

                    result = JSON.parse(result);

                    $("#ulOfficers").empty();
                    if (multiselect) {
                        $('#selOfficer').empty();
                        $('#tbodyOfficerList').empty();


                    }

                    $.each(result, function (key, value) {

                        if (value.COO == undefined) {
                            if ($('#Group' + value.GroupID).length == 0) {

                                $("#ulOfficers").append($('<li style="padding:10px 0px !important;">')
                                        .append($('<a style="padding-left:0px !important;cursor:pointer;">')
                                        .attr('id', 'Group' + value.GroupID).html(value.GroupName)
                                    .prepend($('<img src="images/a-minus.png" hspace="10">')))
                                    .append($('<ul style="list-style-type:none;padding-top:10px;">').attr('id', 'UL' + value.GroupID))
                                    );

                                $('#Group' + value.GroupID).click(function () {
                                    $('.activeListview').removeClass('activeListview');
                                    $('#Group' + value.GroupID).addClass('activeListview');

                                    if ($('#Group' + value.GroupID).find('img').attr('src') == 'images/a-minus.png')
                                        $('#Group' + value.GroupID).find('img').attr('src', 'images/a-plus.png');
                                    else
                                        $('#Group' + value.GroupID).find('img').attr('src', 'images/a-minus.png');

                                    $('#UL' + value.GroupID).toggle();

                                    FillWMA('0', $('#txtFromDate').val(), $('#txtToDate').val(), '0', value.GroupID);

                                    FillWMAMore('0', $('#txtFromDate').val(), $('#txtToDate').val(), '0', value.GroupID);

                                    FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtToDateCA').val(), '0', value.GroupID);

                                    FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtToDateCA').val(), '0', value.GroupID);

                                    FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', value.GroupID);

                                    SetActionGraph('0', $('#txtFromDateCA').val(), $('#txtToDateCA').val(), '0', value.GroupID);
                                    SetRankGraph($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtToDateCA').val(), 'Paid', '');
                                    SetRankGraph($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtToDateCA').val(), 'Returned', '');
                                    SetRankGraph($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtToDateCA').val(), 'Part Paid', '');

                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Paid', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Returned', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Part Paid', value.GroupID);

                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Paid', value.GroupID);
                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Returned', value.GroupID);
                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Part Paid', value.GroupID);
                                    $('#AShowAll').hide();
                                });

                                if (multiselect) {
                                    $('#selOfficer').append($('<optgroup>').attr('label', value.Manager).attr('id', 'og' + value.ManagerID)
                                        //.append($('<option>').html($.trim(value.OfficerID)))
                                        )
                                }

                            }



                            //.append($('<ul style="list-style-type:none;>').attr('id', 'UL' + value.GroupID))
                            if (multiselect) {
                                // $('#og' + value.ManagerID).append('<li>' + value.OfficerName + ' - ' + value.OfficerID + '</li>');

                                $('#og' + value.ManagerID)
                                .append($('<option>').attr('value', $.trim(value.OfficerID)).html(value.OfficerName + ' - ' + value.OfficerID))

                                $('#lblCurrentMonth').html($('#selCurrentMonth option:selected').text());

                                $('#lblCurrentMonth').show();
                                $('#selCurrentMonth').hide();

                                $('#lblMonthTotalTarget').show();
                                $('#txtMonthTotalTarget').hide();

                                $('#tbodyOfficerList').append($('<tr>')
                                    .append($('<td>').append($('<a>').attr('id', 'AOfficerTarget')
                                        .attr('data-toggle', 'modal').html(value.OfficerName + ' ' + value.OfficerID))
                                        .append($('<label>').attr('id', 'lblOfficerID' + key).html(value.OfficerID).hide())
                                        )
                                    .append($('<td>').append($('<input>').attr('type', 'text').attr('id', 'txtOfficerTarget' + key)))
                                    )

                                $('#txtOfficerTarget' + key).keydown(function (event) {

                                    if (event.shiftKey == true) {
                                        $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                        return false;
                                    }
                                    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                                        return true;

                                    }
                                    else {

                                        $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                        return false;
                                    }



                                });

                                $('#txtOfficerTarget' + key).keyup(function (event) {

                                    var TTarget = 0;
                                    $('#tbodyOfficerList tr').each(function (key, value) {
                                        if ($(this).find('input').val() > 0) {
                                            //alert($(this).find('input').val());
                                            TTarget += parseInt($(this).find('input').val());
                                        }
                                    });
                                    $('#lblTotalTarget').html(TTarget);

                                    if ($('#lblTotalTarget').html() != $('#lblMonthTotalTarget').html()) {
                                        $('#btnSubmitOfficerTotal').attr('disabled', 'disabled');

                                        $('#lblInfo').html('Total Target should match.');
                                        $("#lblInfo").show().delay(3000).fadeOut();
                                    }
                                    else {
                                        $('#btnSubmitOfficerTotal').removeAttr('disabled');
                                    }

                                });
                            }

                            $('#UL' + value.GroupID)
                                .append($('<li>').attr('id', 'LI' + $.trim(value.OfficerID))
                                .append($('<a>').attr('id', 'A' + $.trim(value.OfficerID))
                                  .attr('data-original-title', '')
                                 .attr('rel', 'popover').attr('class', 'a1')
                                .attr('href', '#').html(value.OfficerName + ' ' + value.OfficerID))
                                //.append($('<img>').attr('src', 'images/chat.png').attr('id', 'img' + $.trim(value.OfficerID)))
                                );

                            //$("#ulOfficers").append($('<li>').attr('id', 'LI' + $.trim(value.OfficerID))
                            //    .append($('<a>').attr('id', 'A' + $.trim(value.OfficerID)).attr('href', '#').html(value.OfficerName + ' ' + value.OfficerID))
                            // );

                            $(function () {


                                //****************Setting up data content for the popover *******************************
                                Type = "GET";

                                var inputParams = "/GetOfficerAvailability?OfficerID=" + value.OfficerID;

                                Url = serviceUrl + inputParams;
                                DataType = "jsonp"; ProcessData = false;

                                //CallService();
                                $.ajax({
                                    type: Type,
                                    url: Url, // Location of the service
                                    contentType: ContentType, // content type sent to server
                                    dataType: DataType, //Expected data format from server       
                                    processdata: ProcessData, //True or False      
                                    async: true,
                                    timeout: 20000,
                                    beforeSend: function () {
                                    },
                                    complete: function () {

                                    },
                                    success: function (result) {//On Successfull service call  
                                        if (result != undefined) {
                                            if (result != "[]" && result != null) {

                                                result = JSON.parse(result);
                                                $('#DivAvailability').empty();
                                                $.each(result, function (key, value1) {


                                                    var datacontent = '<table width="100%" class="display-status-table"><tr><td>' +
                                                  '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                                  '<p class="block-heading">Availability</p><table class="table">' +
                                                  '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                                  '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                  '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                  '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                  '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                 '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                 '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                 '</tbody></table></div>' +
                                                  '</td></tr></table>';




                                                    var datacontentforDisplay = '<table width="100%"><tr><td>' +
                                    '<div class="block display-status-charttb">' +
                                    '<table class="table ">' +
                                    '<tbody><tr><td>Week</td><td>Current Week</td><td>Next Week</td></tr>' +
                                              '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                              '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                              '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                              '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                   '</tbody></table></div></td>' +
                                           //'<td><div class="block">' +
                                           //'<table class="table">' +
                                           //'<tbody><tr><td>' +
                                           //'<div id="chart3" class="plot" style="width: 260px; height: 180px; border:0px;"></div>' +
                                           //'</td></tr>' +
                                           //'<tr><td>Productivity : ' + value1.Productivity + '</td></tr>' +
                                           //         '<tr><td>Efficiency : ' + value1.Efficiency + '</td></tr>' +
                                           //         '<tr><td>Target : ' + value1.Target + '</td></tr></tbody></table></div>' +
                                                    //'</td>
                                                    '</tr></table>';

                                                    //$('.speedometerHolder').speedometer();

                                                    //$('.speedometerHolder').speedometer({
                                                    //    percentage: value1.CurrentStatus
                                                    //});

                                                    //$('.changeSpeedometer').trigger("click");

                                                    $('#A' + $.trim(value.OfficerID)).attr('data-content', datacontent);

                                                    $('#A' + $.trim(value.OfficerID)).attr('Targetvalue', value1.CurrentStatus);

                                                    $('#A' + $.trim(value.OfficerID)).popover();

                                                    $('#A' + $.trim(value.OfficerID)).attr('panelcontent', datacontentforDisplay);



                                                    // $('#DivAvailability').append(datacontentforDisplay);

                                                });

                                                if (SelectedOfficerID != '' && SelectedOfficerID != '0') {
                                                    $('#DivAvailability').empty().append($('#A' + SelectedOfficerID).attr('panelcontent'));




                                                    //var s1 = [$('#A' + SelectedOfficerID).attr('Targetvalue')];

                                                    //plot3 = $.jqplot('chart3', [s1], {
                                                    //    seriesDefaults: {
                                                    //        renderer: $.jqplot.MeterGaugeRenderer,
                                                    //        rendererOptions: {
                                                    //            min: 0,
                                                    //            max: 100,
                                                    //            intervals: [25, 50, 75, 100],
                                                    //            intervalColors: ['#cc6666', '#E7E658', '#93b75f', '#66cc66']
                                                    //        }
                                                    //    }
                                                    //});

                                                    $('#A' + SelectedOfficerID).popover();

                                                }

                                            }

                                        }
                                    },
                                    error: function () {
                                        //alert('error');
                                    }
                                });

                                //****************Setting up data content for the popover *******************************

                            });


                            if (SelectedOfficerID > 0) {
                                $('.activeListview').removeClass('activeListview');
                                $('#A' + SelectedOfficerID).addClass('activeListview');
                            }


                            if (value.IsLogged) {
                                $('#LI' + $.trim(value.OfficerID)).addClass('sidebar-nav-active');
                            }
                            else {
                                $('#LI' + $.trim(value.OfficerID)).removeClass('sidebar-nav-active');
                            }


                            $('#A' + $.trim(value.OfficerID)).click(function () {

                                $('.activeListview').removeClass('activeListview');
                                $(this).addClass('activeListview');

                                $('#lblSelectedOfficerID').html(value.OfficerID);
                                //$('#tblWMAForOfficer').show();
                                $('#tblWMASearchType').show();


                                $('#DivAvailability').empty().append($('#A' + $.trim(value.OfficerID)).attr('panelcontent'));



                                if ($(this).attr('data-content') != undefined)
                                    $('#DAvailability').show();
                                else
                                    $('#DAvailability').hide();



                                $('#tblWMA').hide();

                                FillWMA(ManagerID, $('#txtFromDate').val(), $('#txtToDate').val(), $(this).attr('id').substr(1));

                                FillWMAMore(ManagerID, $('#txtFromDate').val(), $('#txtToDate').val(), $(this).attr('id').substr(1));

                                FillCaseActions(ManagerID, $('#txtFromDateCA').val(), $('#txtToDateCA').val(), $(this).attr('id').substr(1));

                                FillCaseDetailsAction(ManagerID, $('#txtFromDateCA').val(), $('#txtToDateCA').val(), $(this).attr('id').substr(1));

                                SetActionGraph(ManagerID, $('#txtFromDateCA').val(), $('#txtToDateCA').val(), $(this).attr('id').substr(1));

                                FillLastKnownLocation(ManagerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), $(this).attr('id').substr(1), 0, 0);

                                GetStatistics($(this).attr('id').substr(1), 1);


                                $('#DivRanking').hide();
                                $('#AShowAll').show();

                                OffID = $(this).attr('id').substr(1);
                                $('#AHeartBeat').show();
                                $('#ALocationwithHB').show();
                            });
                        }
                        else {

                            // COO tree section *********************************************
                            if ($('#Manager' + value.ManagerID).length == 0) {

                                ///////////////////////************************************************************************
                                if (multiselect) {
                                    $('#pListName').html('Manager list');

                                    $('#tbodyOfficerList').append($('<tr>')
                                       .append($('<td>').append($('<a>').attr('id', 'AOfficerTarget')
                                           .attr('data-toggle', 'modal').html(value.Manager + ' ' + value.ManagerID))
                                           .append($('<label>').attr('id', 'lblOfficerID' + key).html(value.ManagerID).hide())
                                           )
                                       .append($('<td>').append($('<input>').attr('type', 'text').attr('id', 'txtOfficerTarget' + key)))
                                       )


                                    //$('#txtOfficerTarget' + key).keyup(function () {
                                    //    var TTarget = 0;
                                    //    $('#tbodyOfficerList tr').each(function (key, value) {
                                    //        if ($(this).find('input').val() > 0) {
                                    //            TTarget += parseInt($(this).find('input').val());
                                    //        }
                                    //    });
                                    //    $('#lblTotalTarget').html(TTarget);
                                    //});

                                    $('#txtOfficerTarget' + key).keydown(function (event) {

                                        if (event.shiftKey == true) {
                                            $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                            return false;
                                        }
                                        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                                            return true;

                                        }
                                        else {

                                            $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                            return false;
                                        }

                                    });

                                    $('#txtOfficerTarget' + key).keyup(function (event) {

                                        var TTarget = 0;
                                        $('#tbodyOfficerList tr').each(function (key, value) {
                                            if ($(this).find('input').val() > 0) {
                                                //alert($(this).find('input').val());
                                                TTarget += parseInt($(this).find('input').val());
                                            }
                                        });
                                        $('#lblTotalTarget').html(TTarget);

                                        if ($('#lblTotalTarget').html() != $('#txtMonthTotalTarget').val()) {
                                            $('#btnSubmitOfficerTotal').attr('disabled', 'disabled');
                                            $('#lblInfo').html('Total Target should match.');
                                            $("#lblInfo").show().delay(3000).fadeOut();
                                        }
                                        else {
                                            $('#btnSubmitOfficerTotal').removeAttr('disabled');
                                        }

                                    });

                                }

                                ///////////////////////************************************************************************

                                $("#ulOfficers").append($('<li style="padding:10px 0px !important;">')
                                        .append($('<a style="padding-left:0px !important;cursor:pointer;">')
                                        .attr('id', 'Manager' + value.ManagerID).html(value.Manager)
                                    .prepend($('<img src="images/a-plus.png" hspace="10">')))
                                    .append($('<ul style="list-style-type:none;padding-top:10px;">').attr('id', 'ULMan' + value.ManagerID))
                                    );


                                $('#ULMan' + value.ManagerID).toggle();


                                //if (getCookie('MTV') != undefined) {
                                //    var MTVtoEllapse = getCookie('MTV').toString().split(',');

                                //    $.each(MTVtoEllapse, function (key, value) {
                                //        //alert(MTVtoEllapse);

                                //    });
                                //}

                                $('#Manager' + value.ManagerID).click(function () {
                                    LinkForManager = 1;

                                    setCookie('ManagerID', value.ManagerID, 1);
                                    // if ($('#ULMan' + value.ManagerID).css('display') == "none") {
                                    //     if (MTV != '') MTV += ',';
                                    //     MTV += value.ManagerID;
                                    // }
                                    // else {
                                    //     MTV = MTV.replace(',' + value.ManagerID, '');
                                    //     MTV = MTV.replace(value.ManagerID, '');
                                    //     MTV = MTV.replace(value.ManagerID + ',', '');
                                    // }

                                    //// alert(MTV);

                                    // setCookie('MTV', MTV);

                                    $('.activeListview').removeClass('activeListview');
                                    $('#Manager' + value.ManagerID).addClass('activeListview');

                                    if ($('#Manager' + value.ManagerID).find('img').attr('src') == 'images/a-minus.png')
                                        $('#Manager' + value.ManagerID).find('img').attr('src', 'images/a-plus.png');
                                    else
                                        $('#Manager' + value.ManagerID).find('img').attr('src', 'images/a-minus.png');

                                    $('#lblSelectedOfficerID').html('0');
                                    $('#lblSelectedGroupID').html('0');
                                    $('#lblSelectedManagerID').html(value.ManagerID);

                                    $('#ULMan' + value.ManagerID).toggle();

                                    FillWMA(value.ManagerID, $('#txtFromDate').val(), $('#txtToDate').val(), '0', '0');

                                    FillWMAMore(value.ManagerID, $('#txtFromDate').val(), $('#txtToDate').val(), '0', '0');

                                    FillCaseActions(value.ManagerID, $('#txtFromDateCA').val(), $('#txtToDateCA').val(), '0', '0');

                                    FillCaseDetailsAction(value.ManagerID, $('#txtFromDateCA').val(), $('#txtToDateCA').val(), '0', '0');

                                    FillLastKnownLocation(value.ManagerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', '0');


                                    GetStatistics(value.ManagerID, 1);

                                    SetActionGraph(value.ManagerID, $('#txtFromDateCA').val(), $('#txtToDateCA').val(), '0', '0');

                                    SetRankGraph(value.ManagerID, $('#txtFromDateCA').val(), $('#txtToDateCA').val(), 'Paid', '0');
                                    SetRankGraph(value.ManagerID, $('#txtFromDateCA').val(), $('#txtToDateCA').val(), 'Returned', '0');
                                    SetRankGraph(value.ManagerID, $('#txtFromDateCA').val(), $('#txtToDateCA').val(), 'Part Paid', '0');

                                    FillRankings(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Paid', '0');
                                    FillRankings(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Returned', '0');
                                    FillRankings(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Part Paid', '0');

                                    FillRankingsMore(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Paid', '0');
                                    FillRankingsMore(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Returned', '0');
                                    FillRankingsMore(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Part Paid', '0');
                                    $('#AShowAll').hide();

                                    $('#DivRanking').show();
                                    $('#DAvailability').hide();

                                    $('#AHeartBeat').hide();
                                    $('#ALocationwithHB').hide();
                                    $('#imgRefreshLocation').show();
                                    $('#pMap').html('Last known location');
                                });

                            }
                            if ($('#Group' + value.GroupID).length == 0) {

                                $("#ULMan" + value.ManagerID).append($('<li style="padding:10px 0px !important;">')
                                        .append($('<a style="padding-left:0px !important;cursor:pointer;">')
                                        .attr('id', 'Group' + value.GroupID).html(value.GroupName)
                                    .prepend($('<img src="images/a-plus.png" hspace="10">')))
                                    .append($('<ul style="list-style-type:none;padding-top:10px;">').attr('id', 'UL' + value.GroupID))
                                    );

                                $('#UL' + value.GroupID).toggle();

                                $('#Group' + value.GroupID).click(function () {
                                    LinkForManager = 1;
                                    $('.activeListview').removeClass('activeListview');
                                    $('#Group' + value.GroupID).addClass('activeListview');

                                    if ($('#Group' + value.GroupID).find('img').attr('src') == 'images/a-minus.png')
                                        $('#Group' + value.GroupID).find('img').attr('src', 'images/a-plus.png');
                                    else
                                        $('#Group' + value.GroupID).find('img').attr('src', 'images/a-minus.png');

                                    $('#lblSelectedOfficerID').html('0');
                                    $('#lblSelectedGroupID').html(value.GroupID);
                                    $('#lblSelectedManagerID').html('0');


                                    $('#UL' + value.GroupID).toggle();

                                    FillWMA('0', $('#txtFromDate').val(), $('#txtToDate').val(), '0', value.GroupID);

                                    FillWMAMore('0', $('#txtFromDate').val(), $('#txtToDate').val(), '0', value.GroupID);

                                    FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtToDateCA').val(), '0', value.GroupID);

                                    FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtToDateCA').val(), '0', value.GroupID);

                                    FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', value.GroupID);

                                    SetActionGraph('0', $('#txtFromDateCA').val(), $('#txtToDateCA').val(), '0', value.GroupID);

                                    SetRankGraph('0', $('#txtFromDateCA').val(), $('#txtToDateCA').val(), 'Paid', value.GroupID);
                                    SetRankGraph('0', $('#txtFromDateCA').val(), $('#txtToDateCA').val(), 'Returned', value.GroupID);
                                    SetRankGraph('0', $('#txtFromDateCA').val(), $('#txtToDateCA').val(), 'Part Paid', value.GroupID);

                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Paid', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Returned', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Part Paid', value.GroupID);

                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Paid', value.GroupID);
                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Returned', value.GroupID);
                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Part Paid', value.GroupID);
                                    $('#AShowAll').hide();

                                    $('#DivRanking').show();
                                    $('#DAvailability').hide();
                                    $('#AHeartBeat').hide();
                                    $('#ALocationwithHB').hide();
                                    $('#imgRefreshLocation').show();
                                    $('#pMap').html('Last known location');
                                });
                                if (multiselect)
                                    $('#selOfficer').append($('<optgroup>').attr('label', value.Manager).attr('id', 'og' + value.ManagerID))
                            }
                            if (multiselect) {
                                $('#og' + value.ManagerID)
                                .append($('<option>').attr('value', $.trim(value.OfficerID)).html(value.OfficerName + ' - ' + value.OfficerID))

                                $('#lblCurrentMonth').hide();
                                $('#selCurrentMonth').show();

                                $('#lblMonthTotalTarget').hide();
                                $('#txtMonthTotalTarget').show();


                            }

                            $('#UL' + value.GroupID)
                                .append($('<li>').attr('id', 'LI' + $.trim(value.OfficerID))
                                .append($('<a>').attr('id', 'A' + $.trim(value.OfficerID))
                                .attr('data-original-title', '')
                                 .attr('rel', 'popover').attr('class', 'a1')
                                .attr('href', '#').html(value.OfficerName + ' ' + value.OfficerID)

                                )
                                //.append($('<img>').attr('src', 'images/chat.png').attr('id', 'img' + $.trim(value.OfficerID)))
                                );
                            $(function () {


                                //****************Setting up data content for the popover *******************************
                                Type = "GET";

                                var inputParams = "/GetOfficerAvailability?OfficerID=" + value.OfficerID;

                                Url = serviceUrl + inputParams;
                                DataType = "jsonp"; ProcessData = false;

                                //CallService();
                                $.ajax({
                                    type: Type,
                                    url: Url, // Location of the service
                                    contentType: ContentType, // content type sent to server
                                    dataType: DataType, //Expected data format from server       
                                    processdata: ProcessData, //True or False      
                                    async: true,
                                    timeout: 20000,
                                    beforeSend: function () {
                                    },
                                    complete: function () {

                                    },
                                    success: function (result) {//On Successfull service call  
                                        if (result != undefined) {
                                            if (result != "[]" && result != null) {

                                                result = JSON.parse(result);
                                                $('#DivAvailability').empty();
                                                $.each(result, function (key, value1) {



                                                    var datacontent = '<table width="100%" class="display-status-table"><tr><td>' +
                                              '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                              '<p class="block-heading">Availability</p><table class="table">' +
                                              '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                              '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                              '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                              '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                              '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '</tbody></table></div>' +
                                              '</td></tr></table>';


                                                    var datacontentforDisplay = '<table class="table">' +
                                              '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                              '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                              '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                              '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                              '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '</tbody></table></div>' +
                                              '</td></tr></table>';

                                                    $('#A' + $.trim(value.OfficerID)).attr('data-content', datacontent);

                                                    $('#A' + $.trim(value.OfficerID)).attr('panelcontent', datacontentforDisplay);

                                                    $('#A' + $.trim(value.OfficerID)).popover();


                                                    //$('#DivAvailability').append(datacontentforDisplay);

                                                    //var s1 = [$('#A' + $.trim(value.OfficerID)).attr('Targetvalue')];

                                                    //plot3 = $.jqplot('chart3', [s1], {
                                                    //    seriesDefaults: {
                                                    //        renderer: $.jqplot.MeterGaugeRenderer,
                                                    //        rendererOptions: {
                                                    //            min: 0,
                                                    //            max: 100,
                                                    //            intervals: [25, 50, 75, 100],
                                                    //            intervalColors: ['#cc6666', '#E7E658', '#93b75f', '#66cc66']
                                                    //        }
                                                    //    }
                                                    //});

                                                });

                                            }
                                        }
                                    },
                                    error: function () {
                                        //alert('error');
                                    }
                                });

                                //****************Setting up data content for the popover *******************************




                            });

                            if (SelectedOfficerID > 0) {
                                $('.activeListview').removeClass('activeListview');
                                $('#A' + SelectedOfficerID).addClass('activeListview');
                            }

                            if (value.IsLogged) {
                                $('#LI' + $.trim(value.OfficerID)).addClass('sidebar-nav-active');
                            }
                            else {
                                $('#LI' + $.trim(value.OfficerID)).removeClass('sidebar-nav-active');
                            }

                            $('#A' + $.trim(value.OfficerID)).click(function () {

                                $('.activeListview').removeClass('activeListview');
                                $(this).addClass('activeListview');

                                $('#lblSelectedOfficerID').html(value.OfficerID);
                                $('#lblSelectedGroupID').html('0');
                                $('#lblSelectedManagerID').html('0');

                                $('#DivAvailability').empty().append($('#A' + $.trim(value.OfficerID)).attr('panelcontent'));

                                $('#DAvailability').show();

                                //$('#tblWMASearchType').show();

                                //$('#tblWMA').hide();

                                FillWMA('0', $('#txtFromDate').val(), $('#txtToDate').val(), $(this).attr('id').substr(1));

                                FillWMAMore('0', $('#txtFromDate').val(), $('#txtToDate').val(), $(this).attr('id').substr(1));

                                FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtToDateCA').val(), $(this).attr('id').substr(1));

                                FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtToDateCA').val(), $(this).attr('id').substr(1));

                                SetActionGraph('0', $('#txtFromDateCA').val(), $('#txtToDateCA').val(), $(this).attr('id').substr(1));

                                FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), $(this).attr('id').substr(1));

                                $('#DivRanking').hide();

                                GetStatistics($(this).attr('id').substr(1), 1);
                                $('#AShowAll').show();
                                //var s1 = [$('#A' + $.trim(value.OfficerID)).attr('Targetvalue')];

                                //plot3 = $.jqplot('chart3', [s1], {
                                //    seriesDefaults: {
                                //        renderer: $.jqplot.MeterGaugeRenderer,
                                //        rendererOptions: {
                                //            min: 0,
                                //            max: 100,
                                //            intervals: [25, 50, 75, 100],
                                //            intervalColors: ['#cc6666', '#E7E658', '#93b75f', '#66cc66']
                                //        }
                                //    }
                                //});

                                OffID = $(this).attr('id').substr(1);
                                $('#AHeartBeat').show();
                                $('#ALocationwithHB').show();
                                $('#imgRefreshLocation').hide();
                            });


                            //COO tree section **********************************************
                        }

                    }
);
                    $('.close').click(function () { $('.popover').remove(); });
                    if (multiselect) {



                        var divTarget = parseInt($('#lblMonthTotalTarget').html() / ($('#tbodyOfficerList tr').length));
                        var remTarget = parseInt($('#lblMonthTotalTarget').html() % ($('#tbodyOfficerList tr').length));

                        $('#tbodyOfficerList tr').each(function (key, value) {
                            if (key == ($('#tbodyOfficerList tr').length - 1))
                                $(this).find('input').val(divTarget + remTarget);
                            else
                                $(this).find('input').val(divTarget);
                        });


                        $('#lblTotalTarget').html(getCookie('tgVal'));

                        $('#tbodyOfficerList').append($('<tr>')
                            .append($('<td>').append($('<span>').attr('class', 'pull-right').html('Total')))
                            .append($('<td>').append($('<label>').attr('id', 'lblTotalTarget')))
                            )
                        .append($('<tr>').append($('<td>'))
                            .append($('<td>').append($('<button>').attr('id', 'btnSubmitOfficerTotal').attr('class', 'btn-primary').html('Submit'))))

                        $('#btnSubmitOfficerTotal').click(function () {
                            var TTargetInfo = '';
                            $('#tbodyOfficerList tr').each(function (key, value) {
                                if ($(this).find('label').html() != '' && $(this).find('label').html() != undefined && parseInt($(this).find('input').val()) > 0) {
                                    if (TTargetInfo != '') TTargetInfo += ',';
                                    TTargetInfo += $(this).find('label').html() + '|' + parseInt($(this).find('input').val());
                                }
                            });

                            // ****************************** submit functionality

                            Type = "GET";

                            var inputParams = "/UpdateOfficerTarget?TMonth=" + $('#selCurrentMonth').val() + "&TargetNo=" +
                                $('#txtMonthTotalTarget').val() + "&ManagerID=" + ManagerID + "&OfficerIDs=" + TTargetInfo;

                            Url = serviceUrl + inputParams;
                            DataType = "jsonp"; ProcessData = false;


                            $.ajax({
                                type: Type,
                                url: Url, // Location of the service
                                contentType: ContentType, // content type sent to server
                                dataType: DataType, //Expected data format from server       
                                processdata: ProcessData, //True or False      
                                async: true,
                                timeout: 20000,
                                beforeSend: function () {

                                },
                                complete: function () {

                                },
                                success: function (result) {//On Successfull service call  
                                    // $('#lblInfo').html('Target updated successfully.');

                                    $('#lblTargetInfo').html('Target has been updated successfully.');
                                    $('#ATargetInfo').click();

                                    //$("#lblInfo").show().delay(3000).fadeOut();
                                },
                                error: function () {
                                    //alert('error');
                                } // When Service call fails
                            });

                            // *********************************


                            //alert(TTargetInfo);
                        });

                    }
                    $('#AShowAll').click(function () {
                        FillAllLocation();
                    });
                    $('#selOfficer').multiselect({
                        noneSelectedText: 'Select Officers',
                        selectedList: 5,
                        multiple: true
                    }).multiselectfilter();

                    $('#selOfficer').multiselect("uncheckAll");
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

function FillCaseActions(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;

    if (GroupID == undefined) GroupID = 0;

    var inputParams = "/GetCaseActions_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" +
        ToDate + "&OfficerID=" + OfficerID + "&GroupID=" + GroupID;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);

                    $('#tblbodyCA').empty();
                    if ((LinkForManager == 0) && ($.trim($('#lblLoginOfficer').html()) == 'David Burton')) {
                        $.each(result, function (key, value) {

                            //$("#ulOfficers").append($('<li>').append($('<a>').attr('href', '#').html(value.OfficerName)));
                            if (value.ActionText != 'Defendant contact') {
                                $('#tblbodyCA').append($('<tr>').append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                    .append($('<td>').html(value.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(value.Ave))
                                    )
                            } else {
                                $('#tblbodyCA').append($('<tr>').append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#').html(value.ActionText))) //.attr('href', '#' + value.ActionText.toString().toLowerCase().replace(" ", ""))
                                    .append($('<td>').html(value.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(value.Ave))
                                    )
                            }


                        });
                    }
                    else {

                        $.each(result, function (key, value) {

                            //$("#ulOfficers").append($('<li>').append($('<a>').attr('href', '#').html(value.OfficerName)));

                            $('#tblbodyCA').append($('<tr>').append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#' + value.ActionText.toString().toLowerCase().replace(" ", "")).attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                .append($('<td>').html(value.Case))
                                .append($('<td>').attr('title', 'Last four week average').html(value.Ave))
                                )
                        });
                    }
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

function FillCaseDetailsActionForManager(ManagerID, FromDate, ToDate, OfficerID, GroupID) {

    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;

    var inputParams = "/GetCaseActionsDetail_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&OfficerID=" + OfficerID
        + "&GroupID=" + GroupID;//+ "&ActionText=" + ActionText;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {

                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);
                    $('#tbodypaidformanager').empty();
                    $('#tbodypartpaidformanager').empty();
                    $('#tbodyreturnedformanager').empty();
                    $('#tbodyleftletterformanager').empty();
                    $('#tbodyrevisitformanager').empty();

                    $('#tbodyActionTCG').empty();
                    $('#tbodyActionTCGPAID').empty();
                    $('#tbodyActionTCGPP').empty();
                    //$('#tbodyAction_PAID').empty();
                    //$('#tbodyAction_PARTPAID').empty();
                    $('#tbodyActionUTTC').empty();
                    $('#tbodyActionDROPPED').empty();
                    $('#tbodyActionOTHER').empty();

                    $.each(result, function (key, value) {
                        alert(value.ActionText);
                        switch (value.ActionText) {
                            case 'Paid':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodypaidformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
									   .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodypaidformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodypaidformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Part Paid':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodypartpaidformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodypartpaidformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodypartpaidformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Returned':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyreturnedformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyreturnedformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyreturnedformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Left Letter':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyleftletterformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyleftletterformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyleftletterformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Revisit':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyrevisitformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyrevisitformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyrevisitformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;

                            case 'TCG':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCG').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'TCG PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPAID').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'TCG PP':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'UTTC':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionUTTC').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'DROPPED':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionDROPPED').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'OTHER':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionOTHER').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                        }
                    });
                }
                else {
                    $('#tbodypaidformanager').empty().html('No records found');
                    $('#tbodypartpaidformanager').empty().html('No records found');
                    $('#tbodyreturnedformanager').empty().html('No records found');
                    $('#tbodyleftletterformanager').empty().html('No records found');
                    $('#tbodyrevisitformanager').empty().html('No records found');

                    $('#tbodyActionTCG').empty().html('No records found');
                    $('#tbodyActionTCGPAID').empty().html('No records found');
                    $('#tbodyActionTCGPP').empty().html('No records found');
                    //$('#tbodyAction_PAID').empty().html('No records found');
                    //$('#tbodyAction_PARTPAID').empty().html('No records found');
                    $('#tbodyActionUTTC').empty().html('No records found');
                    $('#tbodyActionDROPPED').empty().html('No records found');
                    $('#tbodyActionOTHER').empty().html('No records found');
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}


function FullDescription(key, val) {

    $('#btnCaseActionBack,#divCaseActionsManagerCount').hide();
    //$('#btnCaseActionBack,#Notesformanager,#btnNotesBack').hide();
    switch (val) {
        case 1:
            $('#paidformanager,#paid').hide();
            break;
        case 2:
            $('#partpaidformanager,#partpaid').hide();
            break;
        case 4:
            $('#returnedformanager,#returned').hide();
            break;
        case 5:
            $('#leftletterformanager,#leftletter').hide();
            break;
        case 3:
            //     $('#divCaseActionsManagerCount').attr('style', 'display:none');
            $('#revisitformanager,#revisit').hide();
            break;
    }
    $('#divCaseActionModal').attr('class', 'modal hide fade in').show();
    $('#Notesformanager').empty().html($('#tdFullDesc' + key).html()).show();
    $('#HCaseNumber').empty().html($.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html() + " - " + $('#tdCaseOfficer' + key).html()).show();
    //$('#Notesformanager').html($('#tdFullDesc' + key).html()).show();
    //$('#HCaseNumber').empty().html($.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html()).show();
    $('#btnNotesBack').show().click(function () {
        switch (val) {
            case 1:
                // $('#paidformanager').show();
                $('#paid').show();
                break;
            case 2:
                //   $('#partpaidformanager').show();
                $('#partpaid').show();
                break;
            case 4:
                //$('#returnedformanager').show();
                $('#returned').show();
                break;
            case 5:
                //$('#leftletterformanager').show();
                $('#leftletter').show();
                break;
            case 3:
                //$('#revisitformanager').show();
                $('#revisit').show();

                break;
        }
        $('#divCaseActionModal').attr('class', 'modal hide fade').show();
        $('#Notesformanager,#HCaseNumber,#btnNotesBack').hide();
        $('#btnCaseActionBack').hide();
    });
}


function FillRankings(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getrankings";


    if (GroupID == undefined) GroupID = 0;
    var inputParams = "/GetTop3Rank_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate
        + "&ActionText=" + ActionText + "&GroupID=" + GroupID;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {

                switch (ActionText) {
                    case 'Paid':
                        $('#tblbodyPaid').empty();
                        $('#tblbodyPaid').append($('<tr>').append($('<td colspan="2">').html('No Rankings for Paid actions.')));
                        break;

                    case 'Returned':
                        $('#tblbodyReturned').empty();
                        $('#tblbodyReturned').append($('<tr>').append($('<td colspan="2">').html('No Rankings for returned actions.')));
                        break;

                    case 'Part Paid':
                        $('#tblbodyPP').empty();
                        $('#tblbodyPP').append($('<tr>').append($('<td colspan="2">').html('No Rankings for part Paid actions.')));
                        break;

                }

                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);

                    switch (ActionText) {
                        case 'Paid':
                            $('#tblbodyPaid').empty();
                            if (($.trim($('#lblLoginOfficer').html()) == "David Burton") && (LinkForManager == 0)) {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPaid').append($('<tr>').append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPaidOfficerID' + key).html(value.Officer)).append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillOfficerRankOnClick(1,"' + key + '")').html(value.OfficerName)))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'FillCaseActionDetailsOnClick(1,' + value.Officer + ',0)').attr('href', '#paid').html(value.Case)))
                                        )
                                });
                            }
                            else {

                                $.each(result, function (key, value) {
                                    $('#tblbodyPaid').append($('<tr>').append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPaidOfficerID' + key).html(value.Officer)).html(value.OfficerName))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'FillCaseActionDetailsOnClick(1,' + value.Officer + ',1)').attr('href', '#paid').html(value.Case)))
                                        )
                                });
                            }

                            break;
                        case 'Returned':

                            $('#tblbodyReturned').empty();
                            if (($.trim($('#lblLoginOfficer').html()) == "David Burton") && (LinkForManager == 0)) {
                                $.each(result, function (key, value) {
                                    $('#tblbodyReturned').append($('<tr>').append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblReturnOfficerID' + key).html(value.Officer)).append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillOfficerRankOnClick(2,"' + key + '")').html(value.OfficerName)))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'FillCaseActionDetailsOnClick(2,' + value.Officer + ',0)').attr('href', '#returned').html(value.Case))))

                                });
                            }
                            else {
                                $.each(result, function (key, value) {

                                    $('#tblbodyReturned').append($('<tr>').append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblReturnOfficerID' + key).html(value.Officer)).html(value.OfficerName))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'FillCaseActionDetailsOnClick(2,' + value.Officer + ',1)').attr('href', '#returned').html(value.Case)))
                                        )

                                });
                            }
                            break;
                        case 'Part Paid':
                            $('#tblbodyPP').empty();
                            if (($.trim($('#lblLoginOfficer').html()) == "David Burton") && (LinkForManager == 0)) {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPP').append($('<tr>').append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPartPaidOfficerID' + key).html(value.Officer)).append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillOfficerRankOnClick(3,"' + key + '")').html(value.OfficerName)))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'FillCaseActionDetailsOnClick(3,' + value.Officer + ',0)').attr('href', '#partpaid').html(value.Case)))
                                        )
                                });
                            }
                            else {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPP').append($('<tr>').append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPartPaidOfficerID' + key).html(value.Officer)).html(value.OfficerName))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'FillCaseActionDetailsOnClick(3,' + value.Officer + ',1)').attr('href', '#partpaid').html(value.Case)))
                                        )
                                });
                            }
                            break;
                        default:
                            $('#tblbodyPaid').empty();
                            if (($.trim($('#lblLoginOfficer').html()) == "David Burton") && (LinkForManager == 0)) {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPaid').append($('<tr>').append($('<td>').append($('<label>').attr('id', 'lblDefaultPaidOfficerID' + key).html(value.Officer)).append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillOfficerRankOnClick(4,"' + key + '")').html(value.OfficerName)))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'FillCaseActionDetailsOnClick(4,' + value.Officer + ',0)').attr('href', '#paid').html(value.Case)))
                                        )
                                });
                            }
                            else {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPaid').append($('<tr>').append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblDefaultPaidOfficerID' + key).html(value.Officer)).html(value.OfficerName))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'FillCaseActionDetailsOnClick(4,' + value.Officer + ',1)').attr('href', '#paid').html(value.Case)))
                                        )
                                });
                            }
                            break;

                    }


                }

            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });


}

function FillOfficerRankOnClick(Action, key) {
    //Action 1-Paid , 2-Returned, 3-Part Paid, 4-Paid(default)
    // alert($('#lblReturnOfficerID' + key).html() + ',' + $('#txtFromDateRanking').val() + ',' + $('#txtToDateRanking').val() + ',' + 'Returned' + ',' + 0);

    switch (Action) {
        //$('#txtFromDateRanking').val(), $('#txtToDateRanking')
        case 1:
            FillRankingsForManagers($('#lblPaidOfficerID' + key).html(), $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Paid', 0);
            break;
        case 2:
            FillRankingsForManagers($('#lblReturnOfficerID' + key).html(), $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Returned', 0);
            break;
        case 3:
            FillRankingsForManagers($('#lblPartPaidOfficerID' + key).html(), $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Part Paid', 0);
            break;
        case 4:
            FillRankingsForManagers($('#lblDefaultPaidOfficerID' + key).html(), $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), 'Paid', 0);
            break;
    }

}

function FillCaseActionDetailsOnClick(Action, OfficerID, Check) {
    if (Check == 0) {
        switch (Action) {
            case 1:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), OfficerID, 0);
                break;
            case 2:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), OfficerID, 0);
                break;
            case 3:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), OfficerID, 0);
                break;
            case 4:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), OfficerID, 0);
                break;
        }
    }
    else {
        switch (Action) {
            case 1:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), OfficerID, 0);
                break;
            case 2:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), OfficerID, 0);
                break;
            case 3:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), OfficerID, 0);
                break;
            case 4:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtToDateRanking').val(), OfficerID, 0);
                break;
        }
    }
}

function FillRankingsForManagers(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getrankings";


    if (GroupID == undefined) GroupID = 0;
    var inputParams = "/GetTop3Rank_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate
        + "&ActionText=" + ActionText + "&GroupID=" + GroupID;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  

            if (result != undefined) {


                $('#tbodyOfficerRank').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));

                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);
                    $('#tbodyOfficerRank').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyOfficerRank').append($('<tr>').append($('<td>').html(value.OfficerName))
                                        .append($('<td>').html(value.Case))
                                        .append($('<td>').html(value.ActionText))
                                        )
                    });

                }

            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });


}

function FillRankingsMore(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getrankings";

    var ShowAll = true;

    if (GroupID == undefined) GroupID = 0;
    var inputParams = "/GetTop3Rank_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate
        + "&ActionText=" + ActionText + "&GroupID=" + GroupID + "&ShowAll=" + ShowAll;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  

            if (result != undefined) {

                switch (ActionText) {
                    case 'Paid':
                        $('#tblbodyRAMore').empty();
                        $('#tblbodyRAMore').append($('<tr>').append($('<td colspan="2">').html('No Rankings for Paid actions.')));
                        break;

                    case 'Returned':
                        $('#tblbodyReturnedMore').empty();
                        $('#tblbodyReturnedMore').append($('<tr>').append($('<td colspan="2">').html('No Rankings for returned actions.')));
                        break;

                    case 'Part Paid':
                        $('#tblbodyPPRanking').empty();
                        $('#tblbodyPPRanking').append($('<tr>').append($('<td colspan="2">').html('No Rankings for part Paid actions.')));
                        break;

                }

                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);

                    switch (ActionText) {
                        case 'Paid':

                            $('#tblbodyRAMore').empty();
                            $.each(result, function (key, value) {
                                $('#tblbodyRAMore').append($('<tr>').append($('<td>').html(value.OfficerName))
                                    .append($('<td>').html(value.Case))
                                    )
                            });

                            break;
                        case 'Returned':

                            $('#tblbodyReturnedMore').empty();
                            $.each(result, function (key, value) {

                                $('#tblbodyReturnedMore').append($('<tr>').append($('<td>').html(value.OfficerName))
                                    .append($('<td>').html(value.Case))
                                    )

                            });
                            break;
                        case 'Part Paid':
                            $('#tblbodyPPRanking').empty();

                            $.each(result, function (key, value) {
                                $('#tblbodyPPRanking').append($('<tr>').append($('<td>').html(value.OfficerName))
                                    .append($('<td>').html(value.Case))
                                    )
                            });
                            break;
                        default:
                            $('#tblbodyRAMore').empty();

                            $.each(result, function (key, value) {
                                $('#tblbodyRAMore').append($('<tr>').append($('<td>').html(value.OfficerName))
                                    .append($('<td>').html(value.Case))
                                    )
                            });
                            break;

                    }


                }

            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });


}
var MID, FD, TD, OID, GID;
function FillLastKnownLocation(ManagerID, FromDate, ToDate, OfficerID, GroupID, ShowAll) {
    //Type = "GET";
    //serviceTypeLogin = "getlastknownlocation";
    if (ManagerID == getCookie('OfficerID') && OfficerID == undefined) ManagerID = getCookie('ManagerID');

    MID = ManagerID;

    if (OfficerID == undefined) {
        if ($('#lblSelectedOfficerID').html() == undefined || $.trim($('#lblSelectedOfficerID').html()) == '') {
            OfficerID = 0;
        }
        else {
            OfficerID = $('#lblSelectedOfficerID').html();
        }
    }
    //else
    //{
    //OfficerID = OID;
    //}
    OID = OfficerID;
    GID = GroupID;
    //alert(OID);
    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;
    if (ShowAll == undefined) ShowAll = 0;


    FD = FromDate;
    TD = FromDate;

    //var inputParams = "/GetLastLocation_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate;
    //Url = serviceUrl + inputParams;

    $('#iframeGMAP').attr('src', 'GMaps.html?mID=' + ManagerID + "&fDate=" + FromDate + "&tDate=" + FromDate +
        "&oID=" + OfficerID + "&Map=Medium" + "&gID=" + GroupID + "&ShowAll=" + ShowAll);

    $('#iframePopupMap').attr('src', 'GMaps.html?mID=' + ManagerID + "&fDate=" + FromDate + "&tDate="
        + FromDate + "&oID=" + OfficerID + "&Map=Large" + "&gID=" + GroupID + "&ShowAll=" + ShowAll);
}

function FillAllLocation() {

    if (OID == undefined) OID = 0;
    if (GID == undefined) GID = 0;

    $('#iframeGMAP').attr('src', 'GMaps.html?mID=' + MID + "&fDate=" + FD + "&tDate=" + TD +
        "&oID=" + OID + "&Map=Medium" + "&gID=" + GID + "&ShowAll=1");

    $('#iframePopupMap').attr('src', 'GMaps.html?mID=' + MID + "&fDate=" + FD + "&tDate="
        + TD + "&oID=" + OID + "&Map=Large" + "&gID=" + GID + "&ShowAll=1");
}

function FillCaseDetailsAction(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    // FillCaseDetailsActionForManager(ManagerID, FromDate, ToDate, OfficerID, GroupID);
    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;


    var inputParams = "/GetCaseActionsDetail_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&OfficerID=" + OfficerID
        + "&GroupID=" + GroupID;//+ "&ActionText=" + ActionText;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);
                    $('#tblbodyActionPaid').empty();
                    $('#tbodyActionPP').empty();
                    $('#tbodyActionReturned').empty();
                    $('#tbodyActionLeftLetter').empty();
                    $('#tbodyActionEnforcementStart').empty();
                    $('#tbodyActionRevisit').empty();
                    $('#tbodyActionEnforcementEnd').empty();

                    $('#tbodyActionTCG').empty();
                    $('#tbodyActionTCGPAID').empty();
                    $('#tbodyActionTCGPP').empty();
                    //$('#tbodyAction_PAID').empty();
                    //$('#tbodyAction_PARTPAID').empty();
                    $('#tbodyActionUTTC').empty();
                    $('#tbodyActionDROPPED').empty();
                    $('#tbodyActionOTHER').empty();

                    $.each(result, function (key, value) {
                        switch (value.ActionText) {
                            case 'Paid':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tblbodyActionPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Part Paid':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Returned':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionReturned').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionReturned').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionReturned').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Left Letter':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionLeftLetter').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionLeftLetter').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionLeftLetter').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'EnforcementStart':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionEnforcementStart').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionEnforcementStart').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionEnforcementStart').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Revisit':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionRevisit').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
									   .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionRevisit').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionRevisit').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'EnforcementEnd':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionEnforcementEnd').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionEnforcementEnd').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionEnforcementEnd').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;

                            case 'TCG':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCG').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'TCG PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPAID').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'TCG PP':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tblbodyActionPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'PART PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'UTTC':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionUTTC').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'DROPPED':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionDROPPED').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'OTHER':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionOTHER').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                        }
                    });
                }

            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function SetActionGraph(ManagerID, FromDate, ToDate, OfficerID, GroupID) {

    Type = "GET";

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;

    var inputParams = "/GetCaseActions_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate +
        "&ToDate=" + ToDate + "&OfficerID=" + OfficerID + "&GroupID=" + GroupID;


    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {

                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);

                    $('#targetchart').empty();

                    $('#targetchart').append('<div id="CaseActionChart" style="width: 100%; height: 100%; display: inline-block"></div>');


                    //<div id="CaseActionChart" style="width: 100%; height: 100%; display: inline-block">
                    $("div", ".ChartDiv1").chart({
                        gallery: cfx.Gallery.Bar,
                        titles: [{ text: "Case Actions", richText: true }],
                        dataValues: result,
                        allSeries: {
                            pointLabels: {
                                visible: true
                            }
                        },
                        axisY: {
                            grids: {
                                major: {
                                    visible: false,
                                    tickMark: cfx.TickMark.None
                                },
                                minor: {
                                    visible: false,
                                    tickMark: cfx.TickMark.None
                                }
                            }
                            //                            ,
                            //                            customGridLines: [{value:2533, text:"Avg"}],
                            //                            sections: [{from:0, to:900}]
                        },
                    });

                    $('#CaseActionChart').find('svg svg:last-child').find('g').remove();
                }

            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

function SetRankGraph(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    Type = "GET";

    if (GroupID == undefined) GroupID = 0;
    var inputParams = "/GetTop3Rank_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate +
        "&ActionText=" + ActionText + "&GroupID=" + GroupID;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);

                    switch (ActionText) {
                        case 'Paid':
                            $('#RAPaidChart').empty();
                            $('#RAPaidChart').append('<div id="RankingPaidChart" style="width: 100%; height: 100%; display: inline-block"></div>');

                            $("#RankingPaidChart").chart({
                                gallery: cfx.Gallery.Bar,
                                titles: [{ text: "Ranking", richText: true }],
                                dataValues: result,
                                allSeries: {
                                    pointLabels: {
                                        visible: true
                                    }
                                },
                                axisY: {
                                    grids: {
                                        major: {
                                            visible: false,
                                            tickMark: cfx.TickMark.None
                                        },
                                        minor: {
                                            visible: false,
                                            tickMark: cfx.TickMark.None
                                        }
                                    }

                                    //                            ,
                                    //                            customGridLines: [{value:2533, text:"Avg"}],
                                    //                            sections: [{from:0, to:900}]
                                },
                            });
                            $('#RankingPaidChart').find('svg svg:last-child').find('g').remove();
                            break;
                        case 'Returned':
                            $('#RAReturnedChart').empty();
                            $('#RAReturnedChart').append('<div id="RankingReturnedChart" style="width: 100%; height: 100%; display: inline-block"></div>');

                            $("#RankingReturnedChart").chart({
                                gallery: cfx.Gallery.Bar,
                                titles: [{ text: "Ranking", richText: true }],
                                dataValues: result,
                                allSeries: {
                                    pointLabels: {
                                        visible: true
                                    }
                                },
                                axisY: {
                                    grids: {
                                        major: {
                                            visible: false,
                                            tickMark: cfx.TickMark.None
                                        },
                                        minor: {
                                            visible: false,
                                            tickMark: cfx.TickMark.None
                                        }
                                    }
                                    //                            ,
                                    //                            customGridLines: [{value:2533, text:"Avg"}],
                                    //                            sections: [{from:0, to:900}]
                                },
                            });
                            $('#RankingReturnedChart').find('svg svg:last-child').find('g').remove();
                            break;
                        case 'Part Paid':
                            $('#RAPartPaidChart').empty();
                            $('#RAPartPaidChart').append('<div id="RankingPartPaidChart" style="width: 100%; height: 100%; display: inline-block"></div>');

                            $("#RankingPartPaidChart").chart({
                                gallery: cfx.Gallery.Bar,
                                titles: [{ text: "Ranking", richText: true }],
                                dataValues: result,
                                allSeries: {
                                    pointLabels: {
                                        visible: true
                                    }
                                },
                                axisY: {
                                    grids: {
                                        major: {
                                            visible: false,
                                            tickMark: cfx.TickMark.None
                                        },
                                        minor: {
                                            visible: false,
                                            tickMark: cfx.TickMark.None
                                        }
                                    }
                                    //                            ,
                                    //                            customGridLines: [{value:2533, text:"Avg"}],
                                    //                            sections: [{from:0, to:900}]
                                },
                            });
                            $('#RankingPartPaidChart').find('svg svg:last-child').find('g').remove();
                            break;
                        default:

                            break;

                    }


                }

            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FillWMA(ManagerID, FromDate, ToDate, OfficerID, GroupID) {

    if (OfficerID == undefined) OfficerID = 0;

    if (GroupID == undefined) GroupID = 0;

    Type = "GET";

    var inputParams = "/GetWMA_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate
        + "&ToDate=" + ToDate + "&OfficerID=" + OfficerID + "&GroupID=" + GroupID;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);

                    $('#tbodyWMA').empty();
                    $('#tbodyWMAForOfficer').empty();

                    $.each(result, function (key, value) {

                        //$("#ulOfficers").append($('<li>').append($('<a>').attr('href', '#').html(value.OfficerName)));

                        $('#tbodyWMA').append($('<tr>').append($('<td>').append($('<a>').attr('id', value.OfficerId).attr('data-toggle', 'modal').attr('href', '#windowTitleDialog').html(value.OfficerName)))
                            .append($('<td>').html(value.Activities))
                            )

                        $('#tbodyWMAForOfficer').append($('<tr>').append($('<td>').append($('<label>').html(value.OfficerName)))
                            .append($('<td>').html(value.Activities))
                            )

                        if (OfficerID > 0) {
                            FillWMADetailL1(OfficerID, FromDate, ToDate);
                        }
                        else {
                            FillWMADetailL1(value.OfficerId, FromDate, ToDate);
                        }

                        $('#' + value.OfficerId).click(function () {

                            //alert($(this).attr('id'));
                            $('#lblOfficerDisplay').html($(this).html())
                            FillWMADetailL1($(this).attr('id'), FromDate, ToDate);


                        });


                    });


                    //WMAChart
                    $('#WMAChart').empty();

                    $('#WMAChart').append('<div id="DivWMAChart" style="width: 100%; height: 100%; display: inline-block"></div>');


                    //<div id="CaseActionChart" style="width: 100%; height: 100%; display: inline-block">
                    $("#DivWMAChart").chart({
                        gallery: cfx.Gallery.Bar,
                        titles: [{ text: "Warrant Matching Activity", richText: true }],
                        dataValues: result,
                        allSeries: {
                            pointLabels: {
                                visible: true
                            }
                        },
                        axisY: {
                            grids: {
                                major: {
                                    visible: false,
                                    tickMark: cfx.TickMark.None
                                },
                                minor: {
                                    visible: false,
                                    tickMark: cfx.TickMark.None
                                }
                            }
                            //                            ,
                            //                            customGridLines: [{value:2533, text:"Avg"}],
                            //                            sections: [{from:0, to:900}]
                        },
                    });

                    $('#AMoreWMA').show();
                    //************
                }
                else {
                    $('#tbodyWMA').empty();
                    $('#tbodyWMAForOfficer').empty();
                    $('#AMoreWMA').hide();

                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });


}

function FillWMAMore(ManagerID, FromDate, ToDate, OfficerID, GroupID) {

    if (OfficerID == undefined) OfficerID = 0;

    if (GroupID == undefined) GroupID = 0;

    var ShowAll = true;

    Type = "GET";

    var inputParams = "/GetWMA_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate
        + "&ToDate=" + ToDate + "&OfficerID=" + OfficerID + "&GroupID=" + GroupID + "&ShowAll=" + ShowAll;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);

                    $('#tbodyWMAMore').empty();


                    $.each(result, function (key, value) {

                        //$("#ulOfficers").append($('<li>').append($('<a>').attr('href', '#').html(value.OfficerName)));

                        $('#tbodyWMAMore').append($('<tr>').append($('<td>').append($('<a>').attr('id', 'AMoreWMA' + value.OfficerId).html(value.OfficerName)))
                            .append($('<td>').html(value.Activities))
                            )



                    });

                    //************
                }

            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });


}

function SetWMAGraph(ManagerID, FromDate, ToDate, OfficerID, GroupID) {

    Type = "GET";

    if (OfficerID == undefined) OfficerID = 0;

    if (GroupID == undefined) GroupID = 0;

    var inputParams = "/GetWMA_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate
                        + "&OfficerID=" + OfficerID + "&GroupID=" + GroupID;;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;


    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null) {

                    result = JSON.parse(result);

                    //WMAChart
                    $('#WMAChart').empty();

                    $('#WMAChart').append('<div id="DivWMAChart" style="width: 100%; height: 100%; display: inline-block"></div>');


                    $("#DivWMAChart").chart({
                        gallery: cfx.Gallery.Bar,
                        titles: [{ text: "Warrant Matching Activity", richText: true }],
                        dataValues: result,
                        allSeries: {
                            pointLabels: {
                                visible: true
                            }
                        },
                        axisY: {
                            grids: {
                                major: {
                                    visible: false,
                                    tickMark: cfx.TickMark.None
                                },
                                minor: {
                                    visible: false,
                                    tickMark: cfx.TickMark.None
                                }
                            }
                        },
                    });

                    $('#DivWMAChart').find('svg svg:last-child').find('g').remove();


                    //************
                }

            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });


}

function FillWMADetailL1(OfficerID, FromDate, ToDate) {

    Type = "GET";



    var inputParams = "/GetWMADetailL1_Dashboard?OfficerID=" + OfficerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);

                    $('#tblbodySearchType').empty();
                    showhidetabs('tblbodySearchType');
                    $('#lblSearchType').html('');

                    $('#btnBack').hide();

                    $('#tblbodyWMASearchType').empty();

                    $.each(result, function (key, value) {

                        //$("#ulOfficers").append($('<li>').append($('<a>').attr('href', '#').html(value.OfficerName)));

                        $('#tblbodySearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', value.LogType).html(value.LogType)))
                            .append($('<td>').html(value.Activities))
                            )

                        $('#tblbodyWMASearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', 'A' + value.LogType).attr('data-toggle', 'modal').attr('href', '#windowTitleDialog').html(value.LogType)))
                           .append($('<td>').html(value.Activities))
                           )

                        $('#A' + value.LogType).click(function () {

                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        });


                        $('#' + value.LogType).click(function () {

                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        })

                        //$('#vrm').click(function () {
                        //    $('#lblSearchType').html(' - VRM');
                        //    showhidetabs('show-vrm-details');
                        //})
                        //$('#caseno').click(function () {
                        //    $('#lblSearchType').html(' - Case No Search');
                        //    showhidetabs('show-case-details');
                        //})


                    });

                }

            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

function caseaction(Flag) {
    $('#btnCaseActionBack,#Notesformanager,#btnNotesBack').hide();
    caseactionshowhidetabs();
    $('#divCaseActionsManagerCount').show();
    var ID = getCookie('OfficerID');
    CompanyID = getCookie('CompanyID');
    Flag += 1;
    if (CompanyID == 1) {
        switch (Flag) {
            case 1:
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#HCaseActionTitle').html('Part Paid');
                break;
            case 3:
                $('#HCaseActionTitle').html('Revisit');
                break;
            case 4:
                $('#HCaseActionTitle').html('Returned');
                break;
            case 5:
                $('#HCaseActionTitle').html('Left Letter');
                break;
            case 6:
                $('#HCaseActionTitle').html('Defendant Contact');
                break;
        }
    }
    else {
        switch (Flag) {
            case 1:
                $('#HCaseActionTitle').html('TCG');
                break;
            case 2:
                $('#HCaseActionTitle').html('TCG PAID');
                break;
            case 3:
                $('#HCaseActionTitle').html('TCG PP');
                break;
            case 4:
                $('#HCaseActionTitle').html('PAID');
                break;
            case 5:
                $('#HCaseActionTitle').html('PART PAID');
                break;
            case 6:
                $('#HCaseActionTitle').html('UTTC');
                break;
            case 7:
                $('#HCaseActionTitle').html('DROPPED');
                break;
            case 8:
                $('#HCaseActionTitle').html('OTHER');
                break;
        }
    }
    //
    Type = "GET";

    var inputParams = "/GetCaseActionsDashboardForManager?ActionText=" + $('#HCaseActionTitle').html() + "&FromDate=" + $('#txtFromDateCA').val() + "&ToDate=" + $('#txtToDateCA').val();

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);

                    $('#tbodyCaseActionsManagerCount').empty();

                    $.each(result, function (key, value) {
                        $('#tbodyCaseActionsManagerCount').append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.ManagerId).hide()).append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ')').attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.ManagerName))).append($('<td>').html(value.CaseCnt)))
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });

}

function clickoncaseactionmanager(caseid, key) {
    caseactionshowhidetabs();
    FillCaseDetailsActionForManager($('#HCaseActionManagerID' + key).html(), $('#txtFromDateCA').val(), $('#txtToDateCA').val(), '0', '0');
    $('#HCaseActionNavigation').show();
    $('#btnCaseActionBack').show();
    $('#divCaseActionsManagerCount').hide();
    switch (caseid) {
        case 4:
            $('#returnedformanager').show();
            //$('#leftletterformanager').show();
            $('#HCaseActionTitle').html('Returned');
            break;
        case 2:
            $('#partpaidformanager').show();
            //$('#returnedformanager').show();
            $('#HCaseActionTitle').html('Part Paid');
            break;
        case 1:
            $('#paidformanager').show();
            $('#HCaseActionTitle').html('Paid');
            break;
        case 3:
            $('#revisitformanager').show();
            $('#HCaseActionTitle').html('Revisit');
            break;
        case 5:
            $('#leftletterformanager').show();
            $('#HCaseActionTitle').html('Left Letter');
            break;

    }
}

function GetStatistics(OfficerID, link) {

    $('.popover').remove();

    Type = "GET";

    var inputParams = "/GetStatsDashboard?OfficerID=" + OfficerID;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null) {

                    result = JSON.parse(result);

                    $('#tbodyStats').empty();

                    $.each(result, function (key, value) {
                        var StatFormula = '<table width="100%" class="display-status-table"><tr><td>' +
                                            '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                            '<a class="close" data-dismiss="modal" href="#">X</a>' +
											'<p class="block-heading">Percentage calculation</p><table class="table">' +
                                            '<tbody>' +
                                            '<tr><td>Formula for ' + value.Description + ' : ' + value.Formula + '</td></tr>' +
                                            '<tr><td>' + '</td></tr>' +
                                            '<tr><td>Calculation for ' + value.Description + ' : ' + value.Calculation + '</td></tr>' +
                                           '</tbody></table></div></td>' +
                                           '</td></tr></table>';

                        $('#tbodyStats').append($('<tr>').append($('<td>').append($('<a>').attr('id', 'AStat' + key).attr('style', 'cursor:pointer;').attr('onclick', 'GetStatsOnClick(' + key + ')').html(value.Description)
                            .attr('data-original-title', '')
                            .attr('rel', 'popover').attr('class', 'a1')
                            )
                            )
                            .append($('<td id="tdbodyStats' + key + '">').html(value.DisplayValue))
                            )
                        if (link == 1) {
                            if ($.trim(value.Description) == "Case holdings") {
                                $('#tdbodyStats' + key).empty();//data-toggle="modal" href="#DivLogout"
                                $('#tdbodyStats' + key).append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseSearchByOfficerID').attr('onclick', 'CaseSearchByOfficerID(' + OfficerID + ')').html(value.DisplayValue));
                            }
                        }

                        $(function () {

                            $('#AStat' + key).attr('data-content', StatFormula);
                            $('#AStat' + key).attr('data-placement', 'left');
                            $('#AStat' + key).popover();
                        });
                    });
                    $('.close').click(function () { $('.popover').remove(); });
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function GetStatsOnClick(key) {
    var ID;
    var Flag = key + 1;
    if (LinkForManager == 0) {
        ID = getCookie('OfficerID');
        $('#AStat' + key).attr('data-toggle', 'modal').attr('href', '#divStats');
        stats(ID, Flag);
    }
    else if (($('#lblSelectedOfficerID').html() != "0") || ($('#lblSelectedGroupID').html() != "0")) {
        $('#AStat' + key).removeAttr('data-toggle').removeAttr('href');
    }
    else {
        ID = getCookie('ManagerID');
        $('#AStat' + key).attr('data-toggle', 'modal').attr('href', '#divStats');
        stats(ID, Flag);
    }
}

function stats(ID, Flag) {
    switch (Flag) {
        case 1:
            $('#HStatsTitle').html('Productivity');
            break;
        case 2:
            $('#HStatsTitle').html('Efficiency');
            break;
        case 3:
            $('#HStatsTitle').html('Conversion');
            break;
        case 4:
            $('#HStatsTitle').html('Case Holdings');
            break;
        case 5:
            $('#HStatsTitle').html('Paids Vs Targets');
            break;
        case 6:
            $('#HStatsTitle').html('Predicted Efficiency');
            break;


    }
    Type = "GET";

    var inputParams = "/GetStatsOfficerCount?OfficerID=" + ID + "&Flag=" + Flag;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null) {

                    result = JSON.parse(result);

                    $('#tbodyStatsOfficerCount').empty();

                    $.each(result, function (key, value) {

                        $('#tbodyStatsOfficerCount').append($('<tr>').append($('<td>').html(value.OfficerName)).append($('<td>').html(value.Cnt)))

                    });
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });

}

function FillWMADetailL2(OfficerID, FromDate, ToDate, LogType) {
    Type = "GET";
    var inputParams = "/GetWMADetailL2_Dashboard?OfficerID=" + OfficerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&LogType=" + LogType;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            $('#tbodysearchdetails').empty();

            $('#tbodysearchdetails').append($('<tr>').append($('<td colspan="3">').html('No warrant matching activity found for ' + LogType)));

            //$('#tbodysearchdetails').prepend($('<tr>')
            //            .append('<td colspan="3" align="right" style="padding: 15px;"><button class="Back btn btn-primary">Back</button></td>')
            //            )




            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);

                    $('#tbodysearchdetails').empty();

                    $.each(result, function (key, value) {

                        //$("#ulOfficers").append($('<li>').append($('<a>').attr('href', '#').html(value.OfficerName)));

                        $('#tbodysearchdetails').append($('<tr>')
                            .append($('<td>').html(value.SearchKey))
                            .append($('<td>').html(value.ResultReturned))
                            .append($('<td>').html(value.ActionedDate))
                            )

                    });


                    //$('#tbodysearchdetails').prepend($('<tr>')
                    //    .append('<td colspan="3" align="right" style="padding: 15px;"><button class="Back btn btn-primary">Back</button></td>')
                    //    )


                }

            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

function UpdateNotificationAlert(ManagerID, MessageText, OfficerID, IsPriority) {

    if (OfficerID == undefined) OfficerID = 0;
    Type = "GET";

    var inputParams = "/UpdateNotificationMessage?ManagerID=" + ManagerID + "&MessageText=" + MessageText
        + "&Officer=" + $.trim(OfficerID) + "&IsPriority=" + IsPriority;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('.word_count').hide();
            $('#imgLoading').show();
            $('#btnSendNotification').hide();
        },
        complete: function () {
            $('#selOfficer').multiselect("uncheckAll");
            if ($("#chkIsPriority").is(':checked')) {
                $('.word_count span').text('200 characters left');
            }
            else {
                $('.word_count span').text('900 characters left');
            }

            $('#imgLoading').hide();
            $('#btnSendNotification').show();

            $('#lblMsgReport').html('Message sent successfully').show().fadeOut(3000);
            // $('#lblMsgReport').css('color', 'green');
            $('.word_count').fadeIn(2000);


            //$('#selOfficer').multiselect({
            //    noneSelectedText: 'Select Officers',
            //    selectedList: 5,
            //    multiple: true
            //}).multiselectfilter();

            //$('#selOfficer').multiselect("uncheckAll");
        },
        success: function (result) {//On Successfull service call  

        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });


}

function UpdateAutoRefresh(OfficerID, AutoRefresh) {

    if (OfficerID == undefined) OfficerID = 0;
    Type = "GET";

    var inputParams = "/UpdateAutoRefresh?OfficerID=" + OfficerID + "&AutoRefresh=" + AutoRefresh;


    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call

            $('#lblTargetInfo').html('Auto refresh value has been updated successfully.');
            $('#ATargetInfo').click();
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

function showhidetabs(tabname) {
    $('#show-search-details,#tblbodySearchType').hide();
    $('#' + tabname).show();
}

function caseactionshowhidetabs() {
    $('#leftletterformanager,#revisitformanager,#partpaidformanager,#returnedformanager,#paidformanager').hide();
    //   FillCaseActions(value.ManagerID, $('#txtFromDateCA').val(), $('#txtToDateCA').val(), '0', '0');

}

function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}

function DeleteCookie(name) {
    document.cookie = name + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
}

/************************************copied from officer web for showing case holdings**************************************/

function CaseSearchByOfficerID(OfficerID) {
    var Type = "GET";
    var DataType = "jsonp"; var ProcessData = false;
    var Url = serviceUrl + "/GetCaseSearchByOfficerID?OfficerID=" + OfficerID;

    var ContentType = "application/javascript";
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call                      
            if (!(result == null || result == '[]')) {

                result = JSON.parse(result);
                $('#tbodyCaseSearchByOfficerID').empty();
                var HTMLString = '';
                $.each(result, function (key, value) {
                    HTMLString += '<tr><td>' + value.casenumber + '</td><td>' + value.DebtName + '</td><td>' + value.ClientName + '</td><td>' + value.ContactFirstLine + ', ' + value.ContactAddress + ', ' + value.PostCode + '</td>' +
                            '<td>' + value.CaseStatus + '</td><td>' + value.OfficerFirstName + ' ' + value.OfficerLastName + '</td><td>' + value.Ageing + '</td><td>' + value.IssueDate + '</td><td>' + value.DateActioned + '</td></tr>';
                });
                $('#tbodyCaseSearchByOfficerID').html(HTMLString);
            }
            else {
                $('#tbodyCaseSearchByOfficerID').empty();
                $('#tbodyCaseSearchByOfficerID').append($('<tr><td colspan="9"> No records found </td></tr>'));
            }

        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}


/***********************************************************HeartBeat********************************************************/

function HeartBeat() {
    //alert('HB' + FD);    
    // $('#iframeGMAP').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Medium&fDate=" + $('#txtFromDateLocation').val() + "&tDate=" + $('#txtFromDateLocation').val());// + "&gID=" + GroupID + "&ShowAll=" + ShowAll);
    $('#iframeGMAP').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Medium&fDate=" + FD + "&tDate=" + FD);// + "&gID=" + GroupID + "&ShowAll=" + ShowAll);

    $('#iframePopupMap').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Large&fDate=" + FD + "&tDate=" + FD);// + "&gID=" + GroupID + "&ShowAll=" + ShowAll);
}

/***********************************************************HeartBeat********************************************************/

function GetOfficerActivityStatus() {
    var id = getCookie('OfficerID');
    Type = "GET";


    var inputParams = "/GetOfficerActivityStatus?IndexValue=" + tableIndex + "&OfficerID=" + id;

    Url = serviceUrl + inputParams;

    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            $('#DivOfficerStatusContent').empty();

            $('#DivOfficerStatusContent').append($('<table style="width:100%">')
                .append($('<tr >')
                .append($('<td >').attr('id', 'tdCont'))
               )
                )

            $('#DivOfficerStatusContent').append($('<div style="border: 1px solid #ededed; margin: -15px; margin-left:12px; width:1150px; height: 300px;display:none;" id="divMapOAS"><iframe  width="100%" height="100%" frameborder="0"' +
                                    'scrolling="yes" marginheight="0"' +
                                    'marginwidth="0" id="iframeGMAPOAS" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=London,+United+Kingdom&amp;aq=0&amp;oq=lond&amp;sll=55.378051,-3.435973&amp;sspn=20.606847,39.506836&amp;ie=UTF8&amp;hq=&amp;hnear=London,+United+Kingdom&amp;t=m&amp;z=11&amp;iwloc=A&amp;ll=51.511214,-0.119824&amp;output=embed"></iframe></div>')

               )

            if (result != undefined) {
                if (result != "[]" && result != null) {

                    result = JSON.parse(result);

                    $.each(result, function (key, value) {

                        tableIndex = value.IndexNo;
                        $('#tdSpanGray').html(value.Darkgray);
                        $('#tdGray').attr('style', 'width:' + value.Darkgray);
                        $('#lblManagerNameActivityStatus').html('Officer`s Activity Status - ' + value.ManagerName);
                        $('#tdSpanGreen').html(value.Green);
                        $('#tdGreen').attr('style', 'width:' + value.Green);
                        $('#tdSpanRed').html(value.Red);
                        $('#tdRed').attr('style', 'width:' + value.Red);
                        $('#tdSpanAmber').html(value.Amber);
                        $('#tdAmber').attr('style', 'width:' + value.Amber);
                        if ($('#tbl' + $.trim(value.ManagerId)).length == 0) {

                            $('#tdCont').append($('<table cellspacing="0" cellpadding="10" border="0" style="border-spacing: 0px;">')
                                .attr('mname', value.ManagerName)
                                .attr('id', 'tbl' + $.trim(value.ManagerId)).attr('class', 'table'));
                        }

                        if ($('#tbl' + $.trim(value.ManagerId) + ' tr').length == 0) {
                            $('#tbl' + $.trim(value.ManagerId)).append($('<tr>')
                            .append($('<td style="padding:10px;">').append($('<span style="cursor:pointer;">').attr('class', value.Status).html(value.OfficerName).attr('id', 'OAS' + value.OfficerID)))
                            )
                        }
                        else {
                            if ($('#tbl' + $.trim(value.ManagerId) + ' tr:last' + ' td').length >= 7) {
                                $('#tbl' + $.trim(value.ManagerId)).append($('<tr>')
                                 )
                            }
                            $('#tbl' + $.trim(value.ManagerId) + ' tr:last').append($('<td style="padding:10px !important;">')
                                .append($('<span style="cursor:pointer;">').attr('class', value.Status).html(value.OfficerName).attr('id', 'OAS' + value.OfficerID)));
                        }

                        $('#OAS' + value.OfficerID).click(function () {


                            $('#btnBacktoStatus').show();
                            $('#lblOfficerNameMapview').html(value.OfficerName);
                            $('#lblOfficerNameMapview').show();

                            clearInterval(IntervalvalueOfficerStatus);

                            $('#iframeGMAPOAS').attr('src', 'GMaps.html?mID=0' + "&fDate=" + $.datepicker.formatDate('yy/mm/dd', new Date()) + "&tDate="
                                + $.datepicker.formatDate('yy/mm/dd', new Date())
                                + "&oID=" + $(this).attr('id').split('OAS')[1] + "&Map=Large&gID=0&ShowAll=1");

                            $('#tdPrev').hide();
                            $('#tdNext').hide();



                            $('#divMapOAS').show();
                            $('#tdCont').hide();
                            $('#divOfficerActivityStatus').css('width', '80%').css('height', '70%');

                            $('#btnBacktoStatus').unbind().click(function () {
                                $('#tdCont').show();
                                $('#divMapOAS').hide();
                                $('#tdPrev').show();
                                $('#tdNext').show();

                                $('#lblOfficerNameMapview').hide();

                                clearInterval(IntervalvalueOfficerStatus);

                                $('#divOfficerActivityStatus').css('width', '').css('height', '');

                                $(this).hide();

                                tableIndex = tableIndex - 1;
                                GetOfficerActivityStatus();

                                IntervalvalueOfficerStatus = setInterval(function () {
                                    GetOfficerActivityStatus();
                                }, 30000);

                            });

                        });

                    });

                    //$('#DivOfficerStatusContent table:first').show();
                    //$('#lblManagerNameActivityStatus').html($('#DivOfficerStatusContent table:first').attr('mname'));

                    //todo: to set interval for displaying each manager's 

                    //alert($('#DivOfficerStatusContent table:eq(1)').attr('mname'));
                    //IntervalvalueOfficerStatus = setInterval(function () {



                    $('#lblManagerNameActivityStatus').html($('#tdCont table:eq(' + tableIndex + ')').attr('mname'));

                    //if (tableIndex != 0)
                    //    $('#tdCont table:eq(' + (tableIndex - 1) + ')').hide();
                    //else
                    //    $('#tdCont table:last').hide();

                    // $('#tdCont table:eq(' + tableIndex + ')').show();



                    //if ($('#tdCont table:last').index() == tableIndex) {
                    //    tableIndex = -1;
                    //}



                    tableIndex = tableIndex + 1;


                    //}, 10000);


                }

            }

            $('#DivOfficerStatusContent').append($('<table style="width:100%">')
           .append($('<tr >').append($('<td style="width:50%">').attr('id', 'tdPrev'))
           .append($('<td style="width:50%;text-align:right;">').attr('id', 'tdNext'))
           )
           )
            $('#tdPrev').append('<img src="images/Previous.png" style="cursor:pointer;" />');
            $('#tdNext').append('<img src="images/Next.png" style="cursor:pointer;" />');

            $('#tdPrev').unbind().click(function () {

                tableIndex = tableIndex - 2;

                if (tableIndex <= -1) tableIndex = 0;
                GetOfficerActivityStatus();
            });

            $('#tdNext').unbind().click(function () {

                GetOfficerActivityStatus();

                clearInterval(IntervalvalueOfficerStatus);
                IntervalvalueOfficerStatus = setInterval(function () {
                    GetOfficerActivityStatus();
                }, 30000);
            });
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}


function OfficerActivityStatusInit() {
    //clearInterval(IntervalvalueOfficerStatus);
    GetOfficerActivityStatus();

    // IntervalvalueOfficerStatus = setInterval(function () {
    //    GetOfficerActivityStatus();
    //}, 30000);

}

function LocationwithHB() {
    $('#iframeGMAP').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Medium&fDate=" + $('#txtFromDateLocation').val() + "&tDate=" + $('#txtFromDateLocation').val() + "&LocationwithHB=1");

    $('#iframePopupMap').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Large&fDate=" + $('#txtFromDateLocation').val() + "&tDate=" + $('#txtFromDateLocation').val() + "&LocationwithHB=1");
}
