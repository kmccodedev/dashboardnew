﻿//==========================================================================================================================================
/* File Name: Admin_Custom.js */
/* File Created: Jan 18, 2017 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================
//TODO : Sevice URL
var BaseURLService = "https://optimise.marstongroup.co.uk";
var BaseURL = BaseURLService;
var ServiceURL = BaseURLService + "/OptimiseAPI/";
//Test
//==========================================================================================================================================
// TODO: Home page link
var HomeUrl = BaseURL + "/sign-in.html";
var VideoUrl = BaseURL + "/Intro.html";
var SCUrl = BaseURL + "/Dashboard_SC.html";
var OptimiseHomeUrl = BaseURL + "/Dashboard_MS.html";
//==========================================================================================================================================
// TODO: Menu link
//==========================================================================================================================================

var TeamAdminUrl = BaseURL + "/Admin/ITadmin.html";
var HrUrl = BaseURL + "/Admin/HRView.html";
var ReturnActionUrl = BaseURL + "/Admin/PendingAction.html";
var CaseSearchUrl = BaseURL + "/Admin/CaseSearch.html";
var ApproveRequestUrl = BaseURL + "/Admin/AssignRequest.html";
var HPICheckRequestUrl = BaseURL + "/Admin/HPICheckRequest.html";
var PaymentReportUrl = BaseURL + "/Admin/Reports.html";
var CaseuploadUrl = BaseURL + "/Admin/CaseUpload.html";
var CGAdminUrl = BaseURL + "/Admin/CGadmin.html";
var ClampedImageUrl = BaseURL + "/Admin/ClampedImage.html";
var BailedImageSearchUrl = BaseURL + "/Admin/BailedImage.html";
var CaseReturnUrl = BaseURL + "/Admin/CaseReturn.html";
var SCAdminUrl = BaseURL + "/Admin/SCadmin.html";
//==========================================================================================================================================
// TODO: Default parameters
//==========================================================================================================================================

var DataType = 'json';
var max;// Notification message character
var OfficerID;// = window.name.toString().split('|');
var Tab, CntTab1, CntTab2, CntTab3;
var date;
var TodayDate;
var TodayDateOnly;
var RefreshIntervalValue;
var counter;
var CaseActionURL = "";
var WMAURL = "", WMAType = "";
var StatsURL = "";
var LocationURL = "";
var RankURL = "", RankActionText = "", RankViewType = "";
var MTV = '';

var serviceTypeLogin;
var GlobalUserName;
var GlobalPassword;

var serviceTypeCaseSearch;
var serviceTypeCaseSearchDetail;
var serviceTypeSearchVRM;
var serviceTypeSearchVRMDetail;

var LinkForManager = 0;
var CompanyID = 1;
var OffID;
var tableIndex = 1; // for officer status tables
var IntervalvalueOfficerStatus;
var MapIntervalStatus;
var MID, FD, TD, OID, GID;
var datacontent, datacontentforDisplay;

var center;
var options;
var markers = [];
var map;
var DMap;
var latLng;
var marker;
var markerCluster;
var infowindow;
var mcOptions;
var Latitude = [];
var Longitude = [];
var Icon = [];
var Content = [];
var pLatitude = 0;
var pLongitude = 0;
var pContentkey;
var pLatMap = 0;
var pLongMap = 0;
var Clamp = 0;
var ClampMgr = 0;
var ClampImg = '';
var ViewZoneMgr;
var Bailed = 0;
var BailedMgr = 0;
var BailedKey, ClampKey;
var LinkForManager = 0;
var Type;
var RequestURL = "";
var counter;
var geocoder;

var entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
};

//==========================================================================================================================================

$(document).ready(function () {

    history.pushState(null, document.title, location.href);
    window.addEventListener('popstate', function (event) {
        history.pushState(null, document.title, location.href);
    });

    $('#officerId,#password').keyup(function (e) {
        if (e.keyCode == 13)
            $('#btnLogin').trigger('click');
    });

    $('#btnLogin').click(function () {
        var l_officername = $.trim($.session.get('OfficerName'));
        l_officername = $.trim(l_officername);
        console.log('Logged Officername ' + l_officername);

        if ($.trim($('#officerId').val()).length > 0) {
            if ($.trim($('#password').val()).length > 0) {
                $.session.clear();
                $('#imgLoginProcessing').show();
                GetToken();
            }
            else {
                $('#lblUNPWDWarningInfo').html('Whoops! Password is empty.');
                $(".errormsg").show();
                return;
            }
        }
        else {
            $('#lblUNPWDWarningInfo').html('Whoops! OfficerId is empty.');
            $(".errormsg").show();
            return;
        }
    });

    if ($.session.get("timervalue") >= 0) {
        Timer();
    }

    $(".Numeric").keydown(function (e) {
        $(this).attr('style', 'border-color: none');
        if (e.shiftKey == true) {
            if (e.shiftKey == true && e.keyCode != 16) {
                $(this).attr('style', 'border:2px solid #7A4073');
                $(this).attr('placeholder', 'Enter numeric values');
                return false;
            }
            return false;
        }
        if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 35 || e.keyCode == 36 ||
                     e.keyCode == 37 || e.keyCode == 39 || e.keyCode == 45 || e.keyCode == 46 || e.keyCode == 144 || e.keyCode == 59 || e.ctrlKey == true) {
            $(this).attr('style', 'border-color: none');
            $(this).removeAttr('placeholder');
            return true;
        }
        else {
            $(this).attr('style', 'border:2px solid #7A4073');
            $(this).attr('placeholder', 'Enter numeric values');
            return false;
        }
    });
});

//==========================================================================================================================================
// TODO : Token check
//==========================================================================================================================================
function GetToken() {
    $.ajax({
        type: 'POST',
        url: ServiceURL + 'token',
        //beforeSend: function (jqXHR, settings) {
        //    jqXHR.setRequestHeader('Access-Control-Allow-Origin', null);
        //},
        data: {
            "grant_type": "password",
            "username": $('#officerId').val(),
            "password": $('#password').val()
        },
        dataType: DataType,
        success: function (data, textStatus, xhr) {
            $.session.set('Token', data.access_token);
            $.session.set('ExpiresIn', data.expires_in);
            $.session.set('TokenAuthorization', data.token_type + ' ' + data.access_token);
            $.session.set('OfficerID', $.trim($('#officerId').val()));
            $.session.set('Password', $.trim($('#password').val()));
            Loginnew();
        },
        error: function (xhr, textStatus, errorThrown) {
            $('#lblUNPWDWarningInfo').html("Officer ID and Password didnot match.");
        }
    });
}

function CheckToken() {
    $.ajax({
        type: 'POST',
        url: ServiceURL + 'token',
        data: {
            "grant_type": "password",
            "username": $.session.get('OfficerID'),
            "password": $.session.get('Password')
        },
        dataType: DataType,
        success: function (data, textStatus, xhr) {
            $.session.set('Token', data.access_token);
            $.session.set('TokenAuthorization', data.token_type + ' ' + data.access_token);
            $.session.set("timervalue", 0);
            updateConnection();
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function showconsole(a_message) {
    var d = new Date();
    console.log(d.toLocaleTimeString() + " : " + a_message);
}

//==========================================================================================================================================
// TODO : Login function
//==========================================================================================================================================

function Loginnew() {
    $.ajax({
        type: 'POST',
        url: ServiceURL + 'api/v1/officers/' + $.session.get('OfficerID') + '/Login',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: DataType,
        success: function (data) {
            if (!(data == '' || data == null || data == '[]')) {
                $.session.set("timervalue", 0);
                $.session.set('OfficerName', data.OfficerName);
                $.session.set('CompanyID', data.CompanyID);
                $.session.set('tgVal', data.TargetNo);
                $.session.set('aref', data.AutoRefresh);
                $.session.set('RoleType', data.RoleType);
                $.session.set('OfficerRole', data.OfficerRole);
                $.session.set("signalrconnected", 0);
                checksession();
            }
            else {
                $('#lblUNPWDWarningInfo').html("Officer ID and Password did not match.");
            }
        },
        complete: function () {
        },
        error: function (xhr, textStatus, errorThrown) {
            // alert('Error');
        }
    });
}

//==========================================================================================================================================
// TODO : Logout function
//==========================================================================================================================================

function Logout() {
    $.session.clear();
    window.location.href = HomeUrl;
    /*
    $.ajax({
        type: 'PUT',
        url: ServiceURL + 'api/v1/officers/' + $.session.get('OfficerID') + '/Logout',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: DataType,
        beforeSend: function () {
        },
        success: function (data) {
            if (data == true) {
                $.session.clear();
                window.location.href = HomeUrl;
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });*/
}

function LogoutCancel() {
    $('#' + $('#popup3Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
}

//==========================================================================================================================================
// TODO : Alert message
//==========================================================================================================================================

function AlertMessage(title, message) {
    $('#DAlert').attr("title", title.toString());
    $('#DAlert').html(message.toString());
    $('#DAlert').dialog({
        modal: true,
        resizable: false,
        width: "25%",
        buttons: {
            "Close": function () {
                $(this).dialog("close");
            }
        }
    });
}
//==========================================================================================================================================

function Timer() {
    setInterval(function () {
        var timervalue = parseInt($.session.get("timervalue"));
        timervalue = timervalue + 1;
        $.session.set("timervalue", timervalue);
        if (timervalue >= 20) {
            CheckToken();
        }
    }, 60000);
}

function parameterPush(paramData, paramKey, paramValue) {
    if ($.trim(paramValue) != '') {
        var myObj = jQuery.parseJSON('{ "' + paramKey + '": ' + paramValue + '}');
        $.extend(true, paramData, myObj);
    }
    return paramData;
}

function escapeHtml(string) {
    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
        return entityMap[s];
    });
}

function disableBack() { window.history.forward(); }

function showOverlay(overlaydiv) {
    var overlayobj = $("#" + overlaydiv);
    overlayobj.css("width", overlayobj.parent().width() + "px");
    overlayobj.css("height", overlayobj.parent().height() + "px");
}

function hideOverlay(overlaydiv) {
    var overlayobj = $("#" + overlaydiv);
    overlayobj.css("width", "1px");
    overlayobj.css("height", "1px");
    overlayobj.delay(2000).hide();
}

//==========================================================================================================================================

//==========================================================================================================================================
// TODO : Check Session
//==========================================================================================================================================
function checksession() {
    var l_companyid = $.session.get('CompanyID');
    var l_roletype = $.session.get('RoleType');
    l_companyid = $.trim(l_companyid);
    l_roletype = $.trim(l_roletype);
    if ((l_companyid.length > 0) && (l_roletype.length > 0)) {
        if (l_roletype == 6) {
            $.session.set('bc', 1);
            $.session.set('IsVedio', 1);
            window.top.location.href = HrUrl;
        }
        else if (l_roletype == 5) {
            $.session.set('bc', 1);
            $.session.set('IsVedio', 1);
            window.top.location.href = TeamAdminUrl;
        }
        else if (l_roletype == 1) {
            $.session.set('IsVedio', 1);
            if (l_companyid == 1)
                window.top.location = "Dashboard_MS.html";
            else
                window.top.location = "Dashboard_HC.html";
        }
        else if (l_roletype == 7) {
            if (l_companyid == 1)
                window.top.location = "Dashboard_ES.html";
            else
                window.top.location = "Dashboard_HC.html";
        }
        else {
            $.session.set('bc', 1);
            $.session.set('IsVedio', 1);
            if (l_companyid == 1)
                window.top.location = "Dashboard_MS.html";
            else
                window.top.location = "Dashboard_HC.html";
        }
    }
    else {
        $.session.clear();
    }
}
//==========================================================================================================================================
