﻿
//var serviceUrl = "https://test-optimise.marstongroup.co.uk:26443/OAService/MarstonOfficeToolService.svc"; // UAT
//var serviceUrl = "http://intranet.atom8itsolutions.com:8195/OAService/MarstonOfficeToolService.svc"; // FOR ATOM8 USE; IGNORE IT
var serviceUrl = "https://test-optimise.marstongroup.co.uk:26443/OAServiceV2/MarstonOfficeToolService.svc"; // FOR ATOM8 USE; IGNORE IT
//var serviceUrl = "https://secure.marstongroup.co.uk/OptimiseSvc5/OAService/MarstonOfficeToolService.svc";
//var serviceUrl = "http://localhost:8065/MobinocisService/MarstonOfficeToolService.svc";
var RedirectUrl = "http://intranet.atom8itsolutions.com:8195/testoam/Admin/admin-page.html";
var HrUrl = "http://intranet.atom8itsolutions.com:8195/testoam/Admin/HRView.html";
var VideoUrl = "http://intranet.atom8itsolutions.com:8195/testoam/Intro.html";


/*****  START SERVICE CALLS *************************/

var Type;
var Url;
var Data;
var ContentType = "application/javascript";
var DataType;
var ProcessData;
var response;

var MTV = '';

var serviceTypeLogin;
var GlobalUserName;
var GlobalPassword;

var serviceTypeCaseSearch;
var serviceTypeCaseSearchDetail;
var serviceTypeSearchVRM;
var serviceTypeSearchVRMDetail;

var CaseNumber;
var IsManager = 0;
var LinkForManager = 0;
var CompanyID = 1;
var OffID;
var tableIndex = 1; // for officer status tables
var IntervalvalueOfficerStatus;


//function Login(officerId, password) {
//    Type = "GET";
//    serviceTypeLogin = "login";
//    if (officerId != "" && password != "") {
//        var inputParams = "/LoginOfficer?officerID=" + officerId + "&password=" + password + "&" + getCookie('BrowserId');
//        Url = serviceUrl + inputParams;
//        DataType = "jsonp"; ProcessData = false;
//        CallService();
//    }
//}
////Generic function to call WCF Service for GET
//function CallService() {
//    var isGridProcessing = (serviceTypeCaseSearchDetail == 'getcasedetails') ? true : false;
//    $.ajax({
//        type: Type,
//        url: Url, // Location of the service
//        contentType: ContentType, // content type sent to server
//        dataType: DataType, //Expected data format from server       
//        processdata: ProcessData, //True or False      
//        async: true,
//        timeout: 20000,
//        beforeSend: function () {
//        },
//        complete: function () {

//        },
//        success: function (msg) {//On Successfull service call 
//            DataServiceSucceeded(msg);
//        },
//        error: function () {
//            //alert('error');
//        } // When Service call fails
//    });
//}

////Result handler.
//function DataServiceSucceeded(result) {
//    if (DataType == "jsonp") {
//        if (result != undefined) {
//            if (serviceTypeLogin != undefined && serviceTypeLogin == "login") {
//                serviceTypeLogin = undefined;
//                if (result != undefined) {
//                    if (result != "[]" && result != null) {
//                        result = JSON.parse(result);
//                        $.each(result, function (key, value) {
//                            //$.session.set('OfficerID', value.OfficerID);
//                            setCookie('OfficerID', $.trim(value.OfficerID.toString()), 1);
//                            setCookie('OfficerName', $.trim(value.OfficerName), 1);
//                            setCookie('CompanyID', $.trim(value.CompanyID), 1);
//                            setCookie('tgVal', $.trim(value.TargetNo), 1);
//                            setCookie('aref', $.trim(value.AutoRefresh), 1);
//                            setCookie('RoleType', $.trim(value.RoleType), 1);
//                            setCookie('OfficerRole', $.trim(value.Officerrole), 1);

//                            if (value.RoleType == 6) {
//                                setCookie('bc', 1, 1);
//                                setCookie('IsVedio', 1, 1);
//                                window.top.location.href = HrUrl;// "HRView.html";
//                            }
//                            else if (value.RoleType == 5) {
//                                setCookie('bc', 1, 1);
//                                setCookie('IsVedio', 1, 1);
//                                window.top.location.href = RedirectUrl;
//                            }
//                            else if (value.RoleType == 1) {
//                                setCookie('IsVedio', 1, 1);
//                                if (value.CompanyID == 1)
//                                    window.top.location = "Dashboard_MS.html";
//                                else
//                                    window.top.location = "Dashboard_HC.html";
//                            }
//                            else if (value.RoleType == 7) {
//                                if (value.CompanyID == 1)
//                                    window.top.location = "Dashboard_ES.html";
//                                else
//                                    window.top.location = "Dashboard_HC.html";
//                            }
//                            else {
//                                setCookie('bc', 1, 1);
//                                setCookie('IsVedio', 1, 1);
//                                if (value.CompanyID == 1)
//                                    window.top.location = "Dashboard_ES.html";// "Dashboard_MS.html";
//                                else
//                                    window.top.location = "Dashboard_HC.html";
//                            }
//                        });
//                    }
//                    else {
//                        $('#lblUNPWDWarningInfo').html("Officer ID and Password didnot match.");
//                    }
//                }
//            }
//        }
//    }
//}

function LoginDetails4SuperUser(companyid, superuser) {
    if (companyid == 2) {
        $('#DClient').hide();
        setCookie('ClientId', 0, 1);
        setCookie('ClientName', '', 1);
        $('#pClientName').html('');
    }

    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    var inputParams = "/LoginDetails4SuperUser?CompanyID=" + companyid + "&SuperUserName=" + superuser;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {
            if (companyid == 1)
                $('#lblClientSearch').show();
            else
                $('#lblClientSearch').hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        setCookie('OfficerID', $.trim(value.OfficerID.toString()), 1);
                        setCookie('OfficerName', $.trim(value.OfficerName), 1);
                        setCookie('CompanyID', $.trim(value.CompanyID), 1);
                        setCookie('tgVal', $.trim(value.TargetNo), 1);
                        setCookie('aref', $.trim(value.AutoRefresh), 1);
                        setCookie('RoleType', $.trim(value.RoleType), 1);

                        if (value.RoleType == 6) {
                            setCookie('bc', 1, 1);
                            setCookie('IsVedio', 1, 1);
                            window.top.location.href = HrUrl;// "HRView.html";
                        }
                        else if (value.RoleType == 5) {
                            setCookie('bc', 1, 1);
                            setCookie('IsVedio', 1, 1);
                            window.top.location.href = RedirectUrl;
                        }
                        else if (value.RoleType == 7) {
                            //  setCookie('IsVedio', 1, 1);
                            if (value.CompanyID == 1)
                                window.top.location = "Dashboard_MS.html";
                            else
                                window.top.location = "Dashboard_HC.html";
                        }
                        else {
                            setCookie('bc', 1, 1);
                            setCookie('IsVedio', 1, 1);
                            if (value.CompanyID == 1)
                                window.top.location = "Dashboard_MS.html";
                            else
                                window.top.location = "Dashboard_HC.html";
                        }
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

function introvideo() {
    var params = {};
    params.type = 'iframe';
    params.src = VideoUrl;
    params.height = 550;
    params.width = 800;
    params.overflow_hidden = false;
    // Opens the lightbox
    ws_lightbox.open(params);
    setCookie('IsVedio', 0, 1);
}

function GetCompany() {
    $('#selCompanyList').empty();
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetCompany", // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (msg) {//On Successfull service call  
            var result = JSON.parse(msg);
            $.each(result, function (key, value) {
                $('#selCompanyList').append($('<option>').attr('value', $.trim(value.CompanyID)).html(value.CompanyName));
            });
            $('#selCompanyList').val(getCookie('CompanyID')).attr("selected", "selected");
        },
        error: function () {
            //  alert('error');
        }
    });
}

function GetOfficerManager(ManagerID, SelectedOfficerID, multiselect) {
    Type = "GET";
    serviceTypeLogin = "getofficermanager";
    var inputParams = "/GetOfficersForManager?ManagerID=" + ManagerID;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $.loader({
                className: "blue-with-image",
                content: ' '
            });
        },
        complete: function () {
            setTimeout(function () {
                $.loader('close');
            }, 1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $("#ulOfficers").empty();
                    if (multiselect) {
                        $('#selOfficer').empty();
                        $('#tbodyOfficerList').empty();
                    }
                    $.each(result, function (key, value) {
                        if (value.COO == undefined) {
                            if ($('#Group' + value.GroupID).length == 0) {
                                $("#ulOfficers").append('<li><a href="#" id="Group' + value.GroupID + '">' +
                                               '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.GroupName + '</a>' +
                                               '<ul id="UL' + value.GroupID + '"></ul></li>');
                                GetCaseActionsOfficerCount(1, value.ManagerID, 0);
                                GetWorkingHours(value.ManagerID);
                                $('#Group' + value.GroupID).click(function () {
                                    $('.lastLeaf').removeClass('lastLeaf');
                                    $(this).addClass('lastLeaf');
                                    LinkForManager = 1;
                                    GetCaseActionsOfficerCount(1, value.ManagerID, value.GroupID);

                                    if (getCookie('CompanyID') == 1) {
                                        $('#tblWMASearchType').hide();
                                        $('#tblWMA').show();
                                        FillCaseActions(getCookie('OfficerID'), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                        FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                        FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', value.GroupID);
                                        FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', value.GroupID);
                                    }
                                    else {

                                        FillCaseActions(getCookie('OfficerID'), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                        FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                    }

                                    FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', value.GroupID);

                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', value.GroupID);

                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', value.GroupID);
                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', value.GroupID);
                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', value.GroupID);
                                    $('#AShowAll,#AShowEnlarge').hide();
                                });

                                if (multiselect) {
                                    $('#selOfficer').append($('<optgroup>').attr('label', value.Manager).attr('id', 'og' + value.ManagerID))
                                }
                            }

                            if (multiselect) {
                                $('#og' + value.ManagerID)
                                    .append($('<option>').attr('value', $.trim(value.OfficerID)).html(value.OfficerName))
                                $('#lblCurrentMonth').html($('#selCurrentMonth option:selected').text());

                                $('#lblCurrentMonth').show();
                                $('#selCurrentMonth').hide();

                                $('#lblMonthTotalTarget').show();
                                $('#txtMonthTotalTarget').hide();

                                $('#tbodyOfficerList').append($('<tr>')
                                    .append($('<td>').append($('<a class="targetNew">').attr('id', 'AOfficerTarget').html(value.OfficerName + ' ' + value.OfficerID))
                                    .append($('<label>').attr('id', 'lblOfficerID' + key).html(value.OfficerID).hide()))
                                    .append($('<td>').append($('<input>').attr('type', 'text').attr('id', 'txtOfficerTarget' + key).attr('style', 'color:black'))))

                                $('#txtOfficerTarget' + key).keydown(function (event) {
                                    if (event.shiftKey == true) {
                                        $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                        return false;
                                    }
                                    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                                        return true;
                                    }
                                    else {

                                        $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                        return false;
                                    }
                                });

                                $('#txtOfficerTarget' + key).keyup(function (event) {
                                    var TTarget = 0;
                                    $('#tbodyOfficerList tr').each(function (key, value) {
                                        if ($(this).find('input').val() > 0) {
                                            TTarget += parseInt($(this).find('input').val());
                                        }
                                    });
                                    $('#lblTotalTarget').html(TTarget);

                                    if ($('#lblTotalTarget').html() != $('#lblMonthTotalTarget').html()) {
                                        $('#btnSubmitOfficerTotal').attr('disabled', 'disabled');
                                        $('#lblInfo').html('Total Target should match.');
                                        $("#lblInfo").show().delay(3000).fadeOut();
                                    }
                                    else {
                                        $('#btnSubmitOfficerTotal').removeAttr('disabled');
                                    }
                                });
                            }

                            $('#UL' + value.GroupID)
                              .append('<li id="LI' + $.trim(value.OfficerID) + '">' +//style="display: list-block;" 
                                      '<a style="cursor:pointer;"  id="A' + $.trim(value.OfficerID) + '" rel="popover" data-original-title=""  class="a1" data-html="true" data-popover="true">' +//class="a1"
                                      '<i class="glyphicon glyphicon-circle-arrow-right"></i> ' + value.OfficerName + '<span class="pull-right">' + value.OfficerID + '</span>' +
                                      '<span style="padding-left:20px"><img src="images/circle-active.png" id="img' + $.trim(value.OfficerID) + '"></span></a> </li>');

                            $(function () {
                                //****************Setting up data content for the popover *******************************
                                Type = "GET";
                                var inputParams = "/GetOfficerAvailability?OfficerID=" + value.OfficerID;
                                Url = serviceUrl + inputParams;
                                DataType = "jsonp"; ProcessData = false;

                                $.ajax({
                                    type: Type,
                                    url: Url, // Location of the service
                                    contentType: ContentType, // content type sent to server
                                    dataType: DataType, //Expected data format from server       
                                    processdata: ProcessData, //True or False      
                                    async: true,
                                    timeout: 20000,
                                    beforeSend: function () { },
                                    complete: function () { },
                                    success: function (result) {//On Successfull service call  
                                        if (result != undefined) {
                                            if (result != "[]" && result != null) {
                                                result = JSON.parse(result);
                                                $('#DivAvailability').empty();
                                                $.each(result, function (key, value1) {

                                                    var datacontent = '<table width="100%" class="display-status-table"><tr><td>' +
                                                       '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                                       '<p class="block-heading">Availability</p><table class="table">' +
                                                       '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                                       '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                       '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                       '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                       '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                      '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                      '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                      '</tbody></table></div></td></tr></table>';

                                                    var datacontentforDisplay = '<table class="table table-striped table-condensed ">' +
                                                      '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                                      '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                      '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                      '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                      '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                     '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                     '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                     '</tbody></table>';

                                                    $('#A' + $.trim(value.OfficerID)).mouseover(function () {
                                                        $('#A' + $.trim(value.OfficerID)).attr('class', 'lastLeafMouse');
                                                        $('#A' + $.trim(value.OfficerID)).attr('data-content', datacontent);
                                                        $('#A' + $.trim(value.OfficerID)).attr('Targetvalue', value1.CurrentStatus);
                                                        $('#A' + $.trim(value.OfficerID)).attr('panelcontent', datacontentforDisplay);
                                                        $('#A' + $.trim(value.OfficerID)).attr('data-placement', 'right');
                                                        $('#A' + $.trim(value.OfficerID)).popover('show');
                                                    });
                                                    $('#A' + $.trim(value.OfficerID)).mouseout(function () {
                                                        $('#A' + $.trim(value.OfficerID)).removeClass('lastLeafMouse');
                                                        $('#A' + $.trim(value.OfficerID)).popover('hide');
                                                    });
                                                });
                                            }
                                        }
                                    },
                                    error: function () {
                                        //alert('error');
                                    }
                                });
                                //****************Setting up data content for the popover *******************************
                            });

                            if (SelectedOfficerID > 0) {
                                $('.activeListview').removeClass('activeListview');
                                $('#A' + SelectedOfficerID).addClass('activeListview');
                            }

                            if (value.IsLogged) {
                                $('#LI' + $.trim(value.OfficerID)).addClass('sidebar-nav-active');
                                $('#img' + $.trim(value.OfficerID)).show();
                            }
                            else {
                                $('#LI' + $.trim(value.OfficerID)).removeClass('sidebar-nav-active');
                                $('#img' + $.trim(value.OfficerID)).hide();
                            }

                            $('#A' + $.trim(value.OfficerID)).click(function () {
                                LinkForManager = 2;
                                $('.lastLeaf').removeClass('lastLeaf');
                                $(this).addClass('lastLeaf');

                                $('#A' + $.trim(value.OfficerID)).mouseout(function () {
                                    $(this).addClass('lastLeaf');
                                });
                                $('#A' + $.trim(value.OfficerID)).mouseover(function () {
                                    $('.lastLeaf').removeClass('lastLeaf');
                                });

                                $('#lblSelectedOfficerID').html(value.OfficerID);

                                $('#DivAvailability').empty().append($('#A' + $.trim(value.OfficerID)).attr('panelcontent'));

                                if ($(this).attr('data-content') != undefined) {
                                    $('#DTimeline').show();
                                }
                                else {
                                    $('#DTimeline').hide();
                                }

                                $('#showPanel2').hide();
                                $('#DAvailability').show();


                                if (getCookie('CompanyID') == 1) {
                                    FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $(this).attr('id').substr(1));
                                    FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $(this).attr('id').substr(1));
                                }
                                else {
                                    $('#tblWMASearchType').hide();
                                    FillCaseActions(ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillCaseDetailsAction(ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                }

                                FillLastKnownLocation(ManagerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), $(this).attr('id').substr(1), 0, 0);
                                GetStatistics($(this).attr('id').substr(1), 1);

                                $('#DivRanking').hide();
                                $('#AShowAll,#AShowEnlarge').show();

                                OffID = $(this).attr('id').substr(1);
                                $('#AHeartBeat,#AHeartBeatEnlarge').show();
                                $('#ALocationwithHB,#ALocationwithHBEnlarge').show();
                            });
                        }
                        else {
                            // COO tree section *********************************************
                            if ($('#Manager' + value.ManagerID).length == 0) {
                                ///////////////////////************************************************************************
                                if (multiselect) {
                                    $('#pListName').html('Manager list');
                                    $('#tbodyOfficerList').append($('<tr>')
                                       .append($('<td>').append($('<a class="targetNew">').attr('id', 'AOfficerTarget')
                                           .attr('data-toggle', 'modal').html(value.Manager + ' ' + value.ManagerID))
                                           .append($('<label>').attr('id', 'lblOfficerID' + key).html(value.ManagerID).hide()))
                                       .append($('<td>').append($('<input>').attr('type', 'text').attr('id', 'txtOfficerTarget' + key).attr('style', 'color:black'))))

                                    $('#txtOfficerTarget' + key).keydown(function (event) {
                                        if (event.shiftKey == true) {
                                            $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                            return false;
                                        }
                                        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                                            return true;
                                        }
                                        else {
                                            $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                            return false;
                                        }
                                    });

                                    $('#txtOfficerTarget' + key).keyup(function (event) {
                                        var TTarget = 0;
                                        $('#tbodyOfficerList tr').each(function (key, value) {
                                            if ($(this).find('input').val() > 0) {
                                                //alert($(this).find('input').val());
                                                TTarget += parseInt($(this).find('input').val());
                                            }
                                        });
                                        $('#lblTotalTarget').html(TTarget);

                                        if ($('#lblTotalTarget').html() != $('#txtMonthTotalTarget').val()) {
                                            $('#btnSubmitOfficerTotal').attr('disabled', 'disabled');
                                            $('#lblInfo').html('Total Target should match.');
                                            $("#lblInfo").show().delay(3000).fadeOut();
                                        }
                                        else {
                                            $('#btnSubmitOfficerTotal').removeAttr('disabled');
                                        }
                                    });
                                }
                                ///////////////////////************************************************************************

                                $("#ulOfficers").append('<li><a href="#" id="Manager' + value.ManagerID + '">' +   // class="parent_li"
                                                '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.Manager + '</a>' +
                                                '<ul id="ULMan' + value.ManagerID + '"></ul></li>');

                                //TODO: Get Working hours for DH
                                if (key == 0) {
                                    GetWorkingHours(getCookie('OfficerID'));
                                    setCookie('Availability', 0, 1);
                                }
                                $('#Manager' + value.ManagerID).click(function () {
                                    $('.lastLeaf').removeClass('lastLeaf');
                                    $(this).addClass('lastLeaf');
                                    LinkForManager = 1;
                                    setCookie('ManagerID', value.ManagerID, 1);
                                    setCookie('Availability', 0, 1);
                                    setCookie('IsManagerSelect', value.ManagerID, 1);
                                    $('#lblSelectedOfficerID').html('0');
                                    $('#lblSelectedGroupID').html('0');
                                    $('#lblSelectedManagerID').html(value.ManagerID);
                                    IsManager = 1;
                                    GetCaseActionsOfficerCount(1, value.ManagerID, 0);
                                    GetWorkingHours(value.ManagerID);
                                    if (getCookie('CompanyID') == 1) {
                                        $('#tblWMASearchType').hide();
                                        $('#tblWMA').show();
                                        FillCaseActions(value.ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                                        FillCaseDetailsAction(value.ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                                        FillWMA(value.ManagerID, $('#txtFromDate').val(), $('#txtFromDate').val(), '0', '0');
                                        FillWMAMore(value.ManagerID, $('#txtFromDate').val(), $('#txtFromDate').val(), '0', '0');
                                    }
                                    else {
                                        FillCaseActions(value.ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                                        FillCaseDetailsAction(value.ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                                    }
                                    FillLastKnownLocation(value.ManagerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', '0');
                                    GetStatistics(value.ManagerID, 1);

                                    FillRankings(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', '0');
                                    FillRankings(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', '0');
                                    FillRankings(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', '0');

                                    FillRankingsMore(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', '0');
                                    FillRankingsMore(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', '0');
                                    FillRankingsMore(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', '0');
                                    $('#AShowAll,#AShowEnlarge').hide();

                                    $('#DivRanking').show();
                                    $('#DTimeline').hide();

                                    $('#showPanel2').show();
                                    $('#DAvailability').hide();

                                    $('#AHeartBeat,#AHeartBeatEnlarge').hide();
                                    $('#ALocationwithHB,#ALocationwithHBEnlarge').hide();
                                    $('#imgRefreshLocation').show();
                                    $('#pMap').html('Last known location');
                                });

                            }
                            if ($('#Group' + value.GroupID).length == 0) {
                                $("#ULMan" + value.ManagerID).append('<li><a href="#" id="Group' + value.GroupID + '">' +// class="parent_li" style="display: list-block;"
                                               '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.GroupName + '</a>' +
                                               '<ul id="UL' + value.GroupID + '"></ul></li>');

                                $('#Group' + value.GroupID).click(function () {
                                    LinkForManager = 1;
                                    $('#lblSelectedOfficerID').html('0');
                                    $('#lblSelectedGroupID').html(value.GroupID);
                                    $('#lblSelectedManagerID').html('0');
                                    $('.lastLeaf').removeClass('lastLeaf');
                                    $(this).addClass('lastLeaf');
                                    GetCaseActionsOfficerCount(1, value.ManagerID, value.GroupID);
                                    setCookie('Availability', 0, 1);
                                    if (getCookie('CompanyID') == 1) {
                                        $('#tblWMASearchType').hide();
                                        $('#tblWMA').show();
                                        FillCaseActions(getCookie('OfficerID'), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                        FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                        FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', value.GroupID);
                                        FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', value.GroupID);
                                    }
                                    else {
                                        FillCaseActions(getCookie('OfficerID'), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                        FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                    }

                                    FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', value.GroupID);

                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', value.GroupID);

                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', value.GroupID);
                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', value.GroupID);
                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', value.GroupID);
                                    $('#AShowAll,#AShowEnlarge').hide();

                                    $('#DivRanking').show();
                                    $('#DTimeline').hide();
                                    $('#showPanel2').show();
                                    $('#DAvailability').hide();

                                    $('#AHeartBeat,#AHeartBeatEnlarge').hide();
                                    $('#ALocationwithHB,#ALocationwithHBEnlarge').hide();
                                    $('#imgRefreshLocation').show();
                                    $('#pMap').html('Last known location');
                                });

                                if (multiselect)
                                    $('#selOfficer').append($('<optgroup>').attr('label', value.Manager).attr('id', 'og' + value.ManagerID))
                            }
                            if (multiselect) {
                                $('#og' + value.ManagerID)
                                .append($('<option>').attr('value', $.trim(value.OfficerID)).html(value.OfficerName + ' - ' + value.OfficerID))

                                $('#lblCurrentMonth').hide();
                                $('#selCurrentMonth').show();
                                $('#lblMonthTotalTarget').hide();
                                $('#txtMonthTotalTarget').show();
                            }

                            $('#UL' + value.GroupID)
                                .append('<li id="LI' + $.trim(value.OfficerID) + '">' +//style="display: list-block;" 
                                        '<a style="cursor:pointer;"  id="A' + $.trim(value.OfficerID) + '" rel="popover" data-original-title=""  class="a1" data-html="true" data-popover="true">' +//class="a1"
                                        '<i class="glyphicon glyphicon-circle-arrow-right"></i> ' + value.OfficerName + '<span class="pull-right">' + value.OfficerID + '</span>' +
                                        '<span style="padding-left:20px"><img src="images/circle-active.png" id="img' + $.trim(value.OfficerID) + '"></span></a> </li>');


                            $(function () {
                                //****************Setting up data content for the popover *******************************
                                Type = "GET";
                                var inputParams = "/GetOfficerAvailability?OfficerID=" + value.OfficerID;
                                Url = serviceUrl + inputParams;
                                DataType = "jsonp"; ProcessData = false;

                                $.ajax({
                                    type: Type,
                                    url: Url, // Location of the service
                                    contentType: ContentType, // content type sent to server
                                    dataType: DataType, //Expected data format from server       
                                    processdata: ProcessData, //True or False      
                                    async: true,
                                    timeout: 20000,
                                    beforeSend: function () { },
                                    complete: function () { },
                                    success: function (result) {//On Successfull service call  
                                        if (result != undefined) {
                                            if (result != "[]" && result != null) {
                                                result = JSON.parse(result);
                                                $('#DivAvailability').empty();
                                                var datacontent, datacontentforDisplay;
                                                $.each(result, function (key, value1) {

                                                    datacontent = '<table width="100%" class="display-status-table"><tr><td>' +
                                             '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                             '<p class="block-heading">Availability</p><table class="table">' +
                                             '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                             '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                            '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                            '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                            '</tbody></table></div></td></tr></table>';

                                                    datacontentforDisplay = '<table class="table table-striped table-condensed ">' +
                                             '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                             '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                             '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                            '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                            '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                            '</tbody></table>';
                                                    //if ($('#txtSearchOfficer').val() == '') {
                                                    //$('#A' + $.trim(value.OfficerID)).attr('data-content', datacontent);
                                                    //$('#A' + $.trim(value.OfficerID)).attr('panelcontent', datacontentforDisplay);
                                                    //$('#A' + $.trim(value.OfficerID)).popover('show');


                                                    $('#A' + $.trim(value.OfficerID)).mouseover(function () {
                                                        $('#A' + $.trim(value.OfficerID)).attr('class', 'lastLeafMouse');
                                                        $('#A' + $.trim(value.OfficerID)).attr('data-content', datacontent);
                                                        $('#A' + $.trim(value.OfficerID)).attr('panelcontent', datacontentforDisplay);
                                                        $('#A' + $.trim(value.OfficerID)).attr('data-placement', 'right');
                                                        $('#A' + $.trim(value.OfficerID)).popover('show');
                                                    });
                                                    $('#A' + $.trim(value.OfficerID)).mouseout(function () {
                                                        $('#A' + $.trim(value.OfficerID)).removeClass('lastLeafMouse');
                                                        $('#A' + $.trim(value.OfficerID)).popover('hide');
                                                    });
                                                    //}
                                                });
                                            }
                                        }
                                    },
                                    error: function () {
                                        //alert('error');
                                    }
                                });
                                //****************Setting up data content for the popover *******************************
                            });


                            if (SelectedOfficerID > 0) {
                                $('.activeListview').removeClass('activeListview');
                                $('#A' + SelectedOfficerID).addClass('activeListview');
                            }

                            if (value.IsLogged) {
                                $('#LI' + $.trim(value.OfficerID)).addClass('sidebar-nav-active');
                                $('#img' + $.trim(value.OfficerID)).show();
                            }
                            else {
                                $('#LI' + $.trim(value.OfficerID)).removeClass('sidebar-nav-active');
                                $('#img' + $.trim(value.OfficerID)).hide();
                            }

                            $('#A' + $.trim(value.OfficerID)).click(function () {
                                LinkForManager = 2;
                                $('.lastLeaf').removeClass('lastLeaf');
                                $(this).addClass('lastLeaf');
                                $('#A' + $.trim(value.OfficerID)).mouseout(function () {
                                    $(this).addClass('lastLeaf');
                                });
                                $('#A' + $.trim(value.OfficerID)).mouseover(function () {
                                    $('.lastLeaf').removeClass('lastLeaf');
                                });
                                GetTimelineResult($.trim(value.OfficerID));

                                $('#lblSelectedOfficerID').html(value.OfficerID);
                                $('#lblSelectedGroupID').html('0');
                                $('#lblSelectedManagerID').html('0');
                                $('#DivAvailability').empty().append($('#A' + $.trim(value.OfficerID)).attr('panelcontent'));
                                $('#DTimeline').show();
                                $('#DAvailability').show();

                                setCookie('Availability', 1, 1);
                                setCookie('TimelineOfficer', value.OfficerID, 1);

                                if (getCookie('CompanyID') == 1) {
                                    FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $(this).attr('id').substr(1));
                                    FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $(this).attr('id').substr(1));
                                }
                                else {
                                    FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                }

                                FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), $(this).attr('id').substr(1));
                                $('#DivRanking').hide();
                                $('#showPanel2').hide();

                                GetStatistics($(this).attr('id').substr(1), 1);
                                $('#AShowAll,#AShowEnlarge').show();

                                OffID = $(this).attr('id').substr(1);
                                $('#AHeartBeat,#AHeartBeatEnlarge').show();
                                $('#ALocationwithHB,#ALocationwithHBEnlarge').show();
                                $('#imgRefreshLocation').hide();
                            });

                            //COO tree section **********************************************
                        }
                    });

                    //   $('.close').click(function () { $('.popover').remove(); });

                    if (multiselect) {
                        var divTarget = parseInt($('#lblMonthTotalTarget').html() / ($('#tbodyOfficerList tr').length));
                        var remTarget = parseInt($('#lblMonthTotalTarget').html() % ($('#tbodyOfficerList tr').length));

                        $('#tbodyOfficerList tr').each(function (key, value) {
                            if (key == ($('#tbodyOfficerList tr').length - 1))
                                $(this).find('input').val(divTarget + remTarget);
                            else
                                $(this).find('input').val(divTarget);
                        });

                        $('#lblTotalTarget').html(getCookie('tgVal'));

                        $('#tbodyOfficerList').append($('<tr>')
                            .append($('<td>').append($('<span>').attr('class', 'pull-right').html('Total')))
                            .append($('<td>').append($('<label>').attr('id', 'lblTotalTarget'))))
                        .append($('<tr>').append($('<td>'))
                            .append($('<td>').append($('<button>').attr('id', 'btnSubmitOfficerTotal').attr('class', 'btn-primary')
                                                                  .attr('onclick', 'SubmitOfficerTotal()').html('Submit'))))

                        $('#btnSubmitOfficerTotal').click(function () {
                            var TTargetInfo = '';
                            $('#tbodyOfficerList tr').each(function (key, value) {
                                if ($(this).find('label').html() != '' && $(this).find('label').html() != undefined && parseInt($(this).find('input').val()) > 0) {
                                    if (TTargetInfo != '') TTargetInfo += ',';
                                    TTargetInfo += $(this).find('label').html() + '|' + parseInt($(this).find('input').val());
                                }
                            });

                            // ****************************** submit functionality
                            Type = "GET";
                            var inputParams = "/UpdateOfficerTarget?TMonth=" + $('#selCurrentMonth').val() + "&TargetNo=" +
                                $('#txtMonthTotalTarget').val() + "&ManagerID=" + ManagerID + "&OfficerIDs=" + TTargetInfo;

                            Url = serviceUrl + inputParams;
                            DataType = "jsonp"; ProcessData = false;

                            $.ajax({
                                type: Type,
                                url: Url, // Location of the service
                                contentType: ContentType, // content type sent to server
                                dataType: DataType, //Expected data format from server       
                                processdata: ProcessData, //True or False      
                                async: true,
                                timeout: 20000,
                                beforeSend: function () { },
                                complete: function () { },
                                success: function (result) {//On Successfull service call  
                                    $('#lblTargetInfo').html('Target has been updated successfully.');
                                    $('#ATargetInfo').click();
                                },
                                error: function () {
                                } // When Service call fails
                            });

                        });

                    }
                    $('#AShowAll,#AShowEnlarge').click(function () {
                        FillAllLocation();
                    });
                    $('#selOfficer').multiselect({
                        noneSelectedText: 'Select Officers',
                        selectedList: 5,
                        multiple: true
                    }).multiselectfilter();

                    $('#selOfficer').multiselect("uncheckAll");
                }
            }

            // Code moved from Common.js - onload fn
            $('.tree li:has(ul)').addClass('parent_li');
            $('.tree li.parent_li > a').on('click', function (e) {
                var children = $(this).parent('li.parent_li').find(' > ul > li');
                if (children.is(":visible")) {
                    children.hide('fast');
                    $(this).find(' > i').addClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign');
                } else {
                    children.show('fast');
                    $(this).find(' > i').addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');
                }
                e.stopPropagation();
            });
            //

        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });



    $('#popup1Trigger').click(function () {
        $('#selCurrentMonth').val((new Date).getMonth() + 1);

        $('#txtMonthTotalTarget').keydown(function (event) {
            if (event.shiftKey == true) {
                $('#txtMonthTotalTarget' + key).attr('title', 'Enter numeric values');
                return false;
            }
            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                return true;
            }
            else {
                $('#txtMonthTotalTarget' + key).attr('title', 'Enter numeric values');
                return false;
            }
        });

        $('#txtMonthTotalTarget').keyup(function () {
            var divTarget = parseInt($('#txtMonthTotalTarget').val() / (($('#tbodyOfficerList tr').length / 2) - 2));
            var remTarget = parseInt($('#txtMonthTotalTarget').val() % (($('#tbodyOfficerList tr').length / 2) - 2));
            $('#tbodyOfficerList tr').each(function (key, value) {
                if (key == (($('#tbodyOfficerList tr').length / 2) - 3))
                    $(this).find('input').val(divTarget + remTarget);
                else
                    $(this).find('input').val(divTarget);
            });

            $('#lblTotalTarget').html($('#txtMonthTotalTarget').val());
        });
    });
}

function FillCaseActions(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;

    if (GroupID == undefined) GroupID = 0;

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetCaseActions_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" +
        ToDate + "&OfficerID=" + OfficerID + "&GroupID=" + GroupID;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tblbodyCA').empty();//,
                    if (getCookie('CompanyID') == 2) {
                        $('#tbodyWMA').empty();
                    }

                    if ((LinkForManager == 0 || LinkForManager == 1) && (getCookie('OfficerID') == '9999' || getCookie('OfficerID') == '99991')) {//($.trim($('#lblLoginOfficer').html()) == 'David Burton')
                        $.each(result, function (key, value) {
                            if (getCookie('CompanyID') == 1) {
                                $('#SpanAction1').html('Case Actions');
                                $('#tblbodyCA').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                    .append($('<td>').html(value.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                            }
                            else// Company-2
                            {
                                if (!(value.ActionText == 'TCG' || value.ActionText == 'UTTC' || value.ActionText == 'DROPPED' || value.ActionText == 'OTHER')) {
                                    $('#SpanAction1').html('Paid Actions');
                                    $('#tblbodyCA').append($('<tr>')
                                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                        .append($('<td>').html(value.Case))
                                        .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                }
                                else {
                                    $('#SpanAction2').html('Other Actions');
                                    $('#tbodyWMA').append($('<tr>')
                                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                        .append($('<td>').html(value.Case))
                                        .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                }
                            }
                        });
                    }
                    else if ((LinkForManager == 0 || LinkForManager == 1) && (getCookie('OfficerID') != '9999' || getCookie('OfficerID') != '99991')) {
                        IsManager = 2;
                        $.each(result, function (key, value) {
                            if (getCookie('CompanyID') == 1) {
                                $('#SpanAction1').html('Case Actions');
                                $('#tblbodyCA').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                    .append($('<td>').html(value.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                            }
                            else// Company-2
                            {
                                if (!(value.ActionText == 'TCG' || value.ActionText == 'UTTC' || value.ActionText == 'DROPPED' || value.ActionText == 'OTHER')) {
                                    $('#SpanAction1').html('Paid Actions');
                                    $('#tblbodyCA').append($('<tr>')
                                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                        .append($('<td>').html(value.Case))
                                        .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                }
                                else {
                                    $('#SpanAction2').html('Other Actions');
                                    $('#tbodyWMA').append($('<tr>')
                                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                        .append($('<td>').html(value.Case))
                                        .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                }
                            }
                        });
                    }
                    else {
                        if (getCookie('CompanyID') != 1) {
                            $('#tbodyWMA').empty();

                            $.each(result, function (key, value) {
                                if (!(value.ActionText == 'TCG' || value.ActionText == 'UTTC' || value.ActionText == 'DROPPED' || value.ActionText == 'OTHER')) {
                                    $('#SpanAction1').html('Paid Actions');
                                    $('#tblbodyCA').append($('<tr>')
                                       .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                       .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#' + value.ActionText.toString().toLowerCase().replace(" ", "")).attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                       .append($('<td>').html(value.Case))
                                       .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                }
                                else {
                                    $('#SpanAction2').html('Other Actions');
                                    $('#tbodyWMA').append($('<tr>')
                                       .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                       .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#' + value.ActionText.toString().toLowerCase().replace(" ", "")).attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                       .append($('<td>').html(value.Case))
                                       .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                }
                            });
                        }
                        else {
                            $('#tblbodyCA').empty();
                            $('#SpanAction1').html('Case Actions');
                            $('#SpanAction2').html('Warrant Matching');
                            $.each(result, function (key, value) {
                                $('#tblbodyCA').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#' + value.ActionText.toString().toLowerCase().replace(" ", "")).attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                    .append($('<td>').html(value.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                            });
                        }
                    }
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

function FillCaseDetailsActionForManager(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;


    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetCaseActionsDetail_Dashboard?ManagerID=" + $.trim(ManagerID) + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&OfficerID=" + OfficerID
        + "&GroupID=" + GroupID;//+ "&ActionText=" + ActionText;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader1').show();
        },
        complete: function () {
            $('#imgLoader1').hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodypaidformanager').empty();
                    $('#tbodypartpaidformanager').empty();
                    $('#tbodyreturnedformanager').empty();
                    $('#tbodyleftletterformanager').empty();
                    $('#tbodyrevisitformanager').empty();

                    $('#tbodyActionTCG').empty();
                    $('#tbodyActionTCGPAID').empty();
                    $('#tbodyActionTCGPP').empty();
                    $('#tbodyActionUTTC').empty();
                    $('#tbodyActionDROPPED').empty();
                    $('#tbodyActionOTHER').empty();

                    $('#tbodyotherformanager').empty();
                    $('#tbodydroppedformanager').empty();
                    $('#tbodyuttcformanager').empty();
                    $('#tbodytcgformanager').empty();
                    $('#tbodytcgppformanager').empty();
                    $('#tbodytcgpaidformanager').empty();
                    $.each(result, function (key, value) {
                        switch (value.ActionText) {
                            case 'Paid':
                            case 'PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodypaidformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
									   .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodypaidformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodypaidformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Part Paid':
                            case 'PART PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodypartpaidformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodypartpaidformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodypartpaidformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Returned':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyreturnedformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyreturnedformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyreturnedformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Left Letter':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyleftletterformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyleftletterformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyleftletterformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case '£ Collected':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyrevisitformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyrevisitformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyrevisitformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;

                            case 'TCG':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCG,#tbodytcgformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCG,#tbodytcgformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCG,#tbodytcgformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'TCG PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPAID,#tbodytcgpaidformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCGPAID,#tbodytcgpaidformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPAID,#tbodytcgpaidformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'TCG PP':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPP,#tbodytcgppformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCGPP,#tbodytcgppformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPP,#tbodytcgppformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'UTTC':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionUTTC,#tbodyuttcformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionUTTC,#tbodyuttcformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionUTTC,#tbodyuttcformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'DROPPED':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionDROPPED,#tbodydroppedformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionDROPPED,#tbodydroppedformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionDROPPED,#tbodydroppedformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'OTHER':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionOTHER,#tbodyotherformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionOTHER,#tbodyotherformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionOTHER,#tbodyotherformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                        }
                    });
                }
                else {
                    $('#tbodypaidformanager').empty().html('No records found');
                    $('#tbodypartpaidformanager').empty().html('No records found');
                    $('#tbodyreturnedformanager').empty().html('No records found');
                    $('#tbodyleftletterformanager').empty().html('No records found');
                    $('#tbodyrevisitformanager').empty().html('No records found');

                    $('#tbodyActionTCG').empty().html('No records found');
                    $('#tbodyActionTCGPAID').empty().html('No records found');
                    $('#tbodyActionTCGPP').empty().html('No records found');
                    $('#tbodyActionUTTC').empty().html('No records found');
                    $('#tbodyActionDROPPED').empty().html('No records found');
                    $('#tbodyActionOTHER').empty().html('No records found');

                    $('#tbodyotherformanager').empty().html('No records found');
                    $('#tbodydroppedformanager').empty().html('No records found');
                    $('#tbodyuttcformanager').empty().html('No records found');
                    $('#tbodytcgformanager').empty().html('No records found');
                    $('#tbodytcgppformanager').empty().html('No records found');
                    $('#tbodytcgpaidformanager').empty().html('No records found');
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FillDeviationDetailForManager(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;


    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetDeviationDetail_Dashboard?ManagerID=" + $.trim(ManagerID) + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&OfficerID=" + OfficerID
        + "&GroupID=" + GroupID;//+ "&ActionText=" + ActionText;

    Url = serviceUrl + inputParams;

    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader1').show();
        },
        complete: function () {
            $('#imgLoader1').hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodydeviatedformanager').empty();
                    $.each(result, function (key, value) {
                        $('#tbodydeviatedformanager').append($('<tr style = "background:#66FF99">')
                           .append($('<td>').html(value.CaseNumber))
                           .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                          // .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                           .append($('<td>').html(value.DateActioned))
                          // .append($('<td>').html("" + value.Fees))
                           .append($('<td>').html(value.DefenderGPSLatitude))
                           .append($('<td>').html(value.DefenderGPSLongitude))
                           .append($('<td>').html(value.DeviatedDistance))
                           .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                           .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                           )

                    });
                }
                else {
                    $('#tbodydeviatedformanager').empty().html('No records found');
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FullDescription(key, val) {
    $('#btnCaseActionBack,#divCaseActionsManagerCount,#divCaseActionsOfficerCount').hide();
    if (getCookie('CompanyID') == 1) {
        switch (val) {
            case 1:
                $('#paidformanager,#paid').hide();
                break;
            case 2:
                $('#partpaidformanager,#partpaid').hide();
                break;
            case 4:
                $('#returnedformanager,#returned').hide();
                break;
            case 5:
                $('#leftletterformanager,#leftletter').hide();
                break;
            case 3:
                $('#revisitformanager,#revisit').hide();
                break;
        }
    }
    else {
        switch (val) {
            case 1:
                $('#paidformanager,#paid').hide();
                break;
            case 2:
                $('#partpaidformanager,#partpaid').hide();
                break;
            case 3:
                $('#tcgformanager,#tcg').hide();
                break;
            case 4:
                $('#tcgpaidformanager,#tcgpaid').hide();
                break;
            case 5:
                $('#tcgppformanager,#tcgpp').hide();
                break;
            case 6:
                $('#uttcformanager,#uttc').hide();
                break;
            case 7:
                $('#droppedformanager,#dropped').hide();
                break;
            case 8:
                $('#otherformanager,#other').hide();
                break;
        }
    }
    $('#divCaseActionModal').attr('class', 'modal fade in').show();
    $('#Notesformanager').empty().html($('#tdFullDesc' + key).html()).show();
    $('#HCaseNumber').empty().html(' :  ' + $.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html() + " - " + $('#tdCaseOfficer' + key).html()).show();
    $('#btnNotesBack').show().click(function () {
        if (getCookie('CompanyID') == 1) {
            switch (val) {
                case 1:
                    $('#paid').show();
                    break;
                case 2:
                    $('#partpaid').show();
                    break;
                case 4:
                    $('#returned').show();
                    break;
                case 5:
                    $('#leftletter').show();
                    break;
                case 3:
                    $('#revisit').show();
                    break;
            }
        }
        else {
            switch (val) {
                case 1:
                    $('#paidformanager').show();
                    break;
                case 2:
                    $('#partpaidformanager').show();
                    break;
                case 3:
                    $('#tcgformanager').show();
                    break;
                case 4:
                    $('#tcgpaidformanager').show();
                    break;
                case 5:
                    $('#tcgppformanager').show();
                    break;
                case 6:
                    $('#uttcformanager').show();
                    break;
                case 7:
                    $('#droppedformanager').show();
                    break;
                case 8:
                    $('#otherformanager').show();
                    break;
            }
        }
        $('#divCaseActionModal').attr('class', 'modal fade in').show();
        $('#Notesformanager,#HCaseNumber,#btnNotesBack').hide();
        $('#btnCaseActionBack').show();
    });
}

function FillRankings(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getrankings";

    if (GroupID == undefined) GroupID = 0;
    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetTop3Rank_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate
        + "&ActionText=" + ActionText + "&GroupID=" + GroupID + "&ShowAll=" + 0;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                switch (ActionText) {
                    case 'Paid':
                        $('#ARAPaidMore').hide();
                        $('#tblbodyPaid').empty();
                        $('#tblbodyPaid').append($('<tr>').append($('<td colspan="2">').html('No Rankings for Paid actions.')));
                        break;

                    case 'Returned':
                        $('#ARankingPaidMore').hide();
                        $('#tblbodyReturned').empty();
                        $('#tblbodyReturned').append($('<tr>').append($('<td colspan="2">').html('No Rankings for returned actions.')));
                        break;

                    case 'Part Paid':
                        $('#ARankingPartPaidMore').hide();
                        $('#tblbodyPP').empty();
                        $('#tblbodyPP').append($('<tr>').append($('<td colspan="2">').html('No Rankings for part Paid actions.')));
                        break;

                }

                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    switch (ActionText) {
                        case 'Paid':
                            $('#ARAPaidMore').show();
                            $('#tblbodyPaid').empty();
                            if (($.trim($('#lblLoginOfficer').html()) == "David Burton") && (LinkForManager == 0)) {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPaid').append($('<tr>')
                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                            .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPaidOfficerID' + key).html(value.Officer))
                                            .append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                .attr('onclick', 'FillOfficerRankOnClick(1,"' + key + '")').html(value.OfficerName)))
                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal')
                                                .attr('onclick', 'FillCaseActionDetailsOnClick(1,' + value.Officer + ',0)').attr('href', '#paid').html(value.Case))))
                                });
                            }
                            else {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPaid').append($('<tr>')
                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                            .append($('<td>').append($('<label>').attr('style', 'display:none;')
                                                .attr('id', 'lblPaidOfficerID' + key).html(value.Officer)).html(value.OfficerName))
                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal')
                                                .attr('onclick', 'FillCaseActionDetailsOnClick(1,' + value.Officer + ',1)').attr('href', '#paid').html(value.Case))))
                                });
                            }
                            break;
                        case 'Returned':
                            $('#ARankingPaidMore').show();
                            $('#tblbodyReturned').empty();
                            if (($.trim($('#lblLoginOfficer').html()) == "David Burton") && (LinkForManager == 0)) {
                                $.each(result, function (key, value) {
                                    $('#tblbodyReturned').append($('<tr>')
                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                            .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblReturnOfficerID' + key).html(value.Officer))
                                            .append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                .attr('onclick', 'FillOfficerRankOnClick(2,"' + key + '")').html(value.OfficerName)))
                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal')
                                                .attr('onclick', 'FillCaseActionDetailsOnClick(2,' + value.Officer + ',0)').attr('href', '#returned').html(value.Case))))
                                });
                            }
                            else {
                                $.each(result, function (key, value) {
                                    $('#tblbodyReturned').append($('<tr>')
                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                            .append($('<td>').append($('<label>').attr('style', 'display:none;')
                                                .attr('id', 'lblReturnOfficerID' + key).html(value.Officer)).html(value.OfficerName))
                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal')
                                                .attr('onclick', 'FillCaseActionDetailsOnClick(2,' + value.Officer + ',1)').attr('href', '#returned').html(value.Case))))
                                });
                            }
                            break;
                        case 'Part Paid':
                            $('#ARankingPartPaidMore').show();
                            $('#tblbodyPP').empty();
                            if (($.trim($('#lblLoginOfficer').html()) == "David Burton") && (LinkForManager == 0)) {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPP').append($('<tr>')
                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                            .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPartPaidOfficerID' + key).html(value.Officer))
                                            .append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                .attr('onclick', 'FillOfficerRankOnClick(3,"' + key + '")').html(value.OfficerName)))
                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal')
                                                .attr('onclick', 'FillCaseActionDetailsOnClick(3,' + value.Officer + ',0)').attr('href', '#partpaid').html(value.Case))))
                                });
                            }
                            else {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPP').append($('<tr>')
                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                            .append($('<td>').append($('<label>').attr('style', 'display:none;')
                                                .attr('id', 'lblPartPaidOfficerID' + key).html(value.Officer)).html(value.OfficerName))
                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal')
                                                .attr('onclick', 'FillCaseActionDetailsOnClick(3,' + value.Officer + ',1)').attr('href', '#partpaid').html(value.Case))))
                                });
                            }
                            break;
                        default:
                            $('#ARAPaidMore').show();
                            $('#tblbodyPaid').empty();
                            if (($.trim($('#lblLoginOfficer').html()) == "David Burton") && (LinkForManager == 0)) {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPaid').append($('<tr>')
                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                            .append($('<td>').append($('<label>').attr('id', 'lblDefaultPaidOfficerID' + key).html(value.Officer))
                                            .append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                .attr('onclick', 'FillOfficerRankOnClick(4,"' + key + '")').html(value.OfficerName)))
                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal')
                                                .attr('onclick', 'FillCaseActionDetailsOnClick(4,' + value.Officer + ',0)').attr('href', '#paid').html(value.Case))))
                                });
                            }
                            else {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPaid').append($('<tr>')
                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                            .append($('<td>').append($('<label>').attr('style', 'display:none;')
                                                .attr('id', 'lblDefaultPaidOfficerID' + key).html(value.Officer)).html(value.OfficerName))
                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal')
                                                .attr('onclick', 'FillCaseActionDetailsOnClick(4,' + value.Officer + ',1)').attr('href', '#paid').html(value.Case))))
                                });
                            }
                            break;
                    }
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });


}

function FillOfficerRankOnClick(Action, key) {
    //Action 1-Paid , 2-Returned, 3-Part Paid, 4-Paid(default)
    switch (Action) {
        case 1:
            FillRankingsForManagers($('#lblPaidOfficerID' + key).html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', 0);
            break;
        case 2:
            FillRankingsForManagers($('#lblReturnOfficerID' + key).html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', 0);
            break;
        case 3:
            FillRankingsForManagers($('#lblPartPaidOfficerID' + key).html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', 0);
            break;
        case 4:
            FillRankingsForManagers($('#lblDefaultPaidOfficerID' + key).html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', 0);
            break;
    }

}

function FillCaseActionDetailsOnClick(Action, OfficerID, Check) {
    if (Check == 0) {
        switch (Action) {
            case 1:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 2:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 3:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 4:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
        }
    }
    else {
        switch (Action) {
            case 1:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 2:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 3:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 4:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
        }
    }
}

function FillRankingsForManagers(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getrankings";
    if (GroupID == undefined) GroupID = 0;

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetTop3Rank_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate
        + "&ActionText=" + ActionText + "&GroupID=" + GroupID + "&ShowAll=" + 0;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                $('#tbodyOfficerRank').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodyOfficerRank').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyOfficerRank').append($('<tr>').append($('<td>').html(value.OfficerName))
                                        .append($('<td>').html(value.Case))
                                        .append($('<td>').html(value.ActionText)))
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FillRankingsMore(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getrankings";
    var ShowAll = true;
    if (GroupID == undefined) GroupID = 0;

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetTop3Rank_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate
        + "&ActionText=" + ActionText + "&GroupID=" + GroupID + "&ShowAll=" + ShowAll;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                switch (ActionText) {
                    case 'Paid':
                        $('#tblbodyRAMore').empty();
                        $('#tblbodyRAMore').append($('<tr>').append($('<td colspan="2">').html('No Rankings for Paid actions.')));
                        break;
                    case 'Returned':
                        $('#tblbodyReturnedMore').empty();
                        $('#tblbodyReturnedMore').append($('<tr>').append($('<td colspan="2">').html('No Rankings for returned actions.')));
                        break;
                    case 'Part Paid':
                        $('#tblbodyPPRanking').empty();
                        $('#tblbodyPPRanking').append($('<tr>').append($('<td colspan="2">').html('No Rankings for part Paid actions.')));
                        break;
                }

                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    switch (ActionText) {
                        case 'Paid':
                            $('#tblbodyRAMore').empty();
                            $.each(result, function (key, value) {
                                $('#tblbodyRAMore')
                                    .append($('<tr>').append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                     .append($('<td>').html(value.OfficerName))
                                                     .append($('<td>').html(value.Case)))
                            });
                            break;
                        case 'Returned':
                            $('#tblbodyReturnedMore').empty();
                            $.each(result, function (key, value) {
                                $('#tblbodyReturnedMore')
                                    .append($('<tr>').append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                     .append($('<td>').html(value.OfficerName))
                                                     .append($('<td>').html(value.Case)))
                            });
                            break;
                        case 'Part Paid':
                            $('#tblbodyPPRanking').empty();
                            $.each(result, function (key, value) {
                                $('#tblbodyPPRanking')
                                    .append($('<tr>').append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                     .append($('<td>').html(value.OfficerName))
                                                     .append($('<td>').html(value.Case)))
                            });
                            break;
                        default:
                            $('#tblbodyRAMore').empty();
                            $.each(result, function (key, value) {
                                $('#tblbodyRAMore')
                                    .append($('<tr>').append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                     .append($('<td>').html(value.OfficerName))
                                                     .append($('<td>').html(value.Case)))
                            });
                            break;
                    }
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });


}
var MID, FD, TD, OID, GID;
function FillLastKnownLocation(ManagerID, FromDate, ToDate, OfficerID, GroupID, ShowAll) {
    if (ManagerID == getCookie('OfficerID') && OfficerID == undefined) ManagerID = getCookie('ManagerID');
    MID = ManagerID;
    if (OfficerID == undefined) {
        if ($('#lblSelectedOfficerID').html() == undefined || $.trim($('#lblSelectedOfficerID').html()) == '') {
            OfficerID = 0;
        }
        else {
            OfficerID = $('#lblSelectedOfficerID').html();
        }
    }
    OID = OfficerID;
    GID = GroupID;
    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;
    if (ShowAll == undefined) ShowAll = 0;

    FD = FromDate;
    TD = FromDate;

    $('#iframeGMAP').attr('src', 'GMaps.html?mID=' + ManagerID + "&fDate=" + FromDate + "&tDate=" + FromDate +
        "&oID=" + OfficerID + "&Map=Medium" + "&gID=" + GroupID + "&ShowAll=" + ShowAll);

    $('#iframePopupMap').attr('src', 'GMaps.html?mID=' + ManagerID + "&fDate=" + FromDate + "&tDate="
        + FromDate + "&oID=" + OfficerID + "&Map=Large" + "&gID=" + GroupID + "&ShowAll=" + ShowAll);
}

function FillAllLocation() {

    if (OID == undefined) OID = 0;
    if (GID == undefined) GID = 0;
    $('#iframeGMAP').attr('src', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate=" + TD +
        "&oID=" + OID + "&Map=Medium" + "&gID=" + GID + "&ShowAll=1");

    $('#iframePopupMap').attr('src', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate="
        + TD + "&oID=" + OID + "&Map=Large" + "&gID=" + GID + "&ShowAll=1");
}

function FillCaseDetailsAction(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;


    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetCaseActionsDetail_Dashboard?ManagerID=" + $.trim(ManagerID) + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&OfficerID=" + OfficerID
        + "&GroupID=" + GroupID;//+ "&ActionText=" + ActionText;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);
                    $('#tblbodyActionPaid').empty();
                    $('#tbodyActionPP').empty();
                    $('#tbodyActionReturned').empty();
                    $('#tbodyActionLeftLetter').empty();
                    $('#tbodyActionEnforcementStart').empty();
                    $('#tbodyActionRevisit').empty();
                    $('#tbodyActionEnforcementEnd').empty();

                    $('#tbodyActionTCG').empty();
                    $('#tbodyActionTCGPAID').empty();
                    $('#tbodyActionTCGPP').empty();
                    $('#tbodyActionUTTC').empty();
                    $('#tbodyActionDROPPED').empty();
                    $('#tbodyActionOTHER').empty();

                    $.each(result, function (key, value) {
                        switch (value.ActionText) {
                            case 'Paid':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tblbodyActionPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Part Paid':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Returned':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionReturned').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionReturned').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionReturned').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Left Letter':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionLeftLetter').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionLeftLetter').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionLeftLetter').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'EnforcementStart':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionEnforcementStart').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionEnforcementStart').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionEnforcementStart').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case '£ Collected':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionRevisit').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
									   .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionRevisit').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionRevisit').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'EnforcementEnd':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionEnforcementEnd').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionEnforcementEnd').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionEnforcementEnd').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;

                            case 'TCG':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCG').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'TCG PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPAID').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'TCG PP':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tblbodyActionPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'PART PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'UTTC':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionUTTC').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'DROPPED':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionDROPPED').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'OTHER':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionOTHER').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                        }
                    });
                }
                else {
                    $('#tblbodyActionPaid').empty().html('No records found');
                    $('#tbodyActionPP').empty().html('No records found');
                    $('#tbodyActionReturned').empty().html('No records found');
                    $('#tbodyActionLeftLetter').empty().html('No records found');
                    $('#tbodyActionEnforcementStart').empty().html('No records found');
                    $('#tbodyActionRevisit').empty().html('No records found');
                    $('#tbodyActionEnforcementEnd').empty().html('No records found');

                    $('#tbodyActionTCG').empty().html('No records found');
                    $('#tbodyActionTCGPAID').empty().html('No records found');
                    $('#tbodyActionTCGPP').empty().html('No records found');
                    $('#tbodyActionUTTC').empty().html('No records found');
                    $('#tbodyActionDROPPED').empty().html('No records found');
                    $('#tbodyActionOTHER').empty().html('No records found');
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FillWMA(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;
    Type = "GET";

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var inputParams = "/GetWMA_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate
        + "&ToDate=" + FromDate + "&OfficerID=" + OfficerID + "&GroupID=" + GroupID;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);

                    $('#tbodyWMA').empty();
                    $('#tbodyWMAForOfficer').empty();

                    $.each(result, function (key, value) {

                        $('#tbodyWMA').append($('<tr>').append($('<td>').append($('<a>').attr('id', value.OfficerId).attr('data-toggle', 'modal')
                            .attr('href', '#windowTitleDialog').html(value.OfficerName))).append($('<td>').html(value.Activities)))

                        $('#tbodyWMAForOfficer').append($('<tr>').append($('<td>').append($('<label>').html(value.OfficerName)))
                            .append($('<td>').html(value.Activities)))
                        if (OfficerID > 0) {
                            FillWMADetailL3(OfficerID, FromDate, ToDate);
                        }
                        else {
                            FillWMADetailL1(value.OfficerId, FromDate, ToDate);
                        }

                        $('#' + value.OfficerId).click(function () {
                            $('#lblOfficerDisplay').html($(this).html())
                            FillWMADetailL1($(this).attr('id'), FromDate, ToDate);
                        });
                    });

                    $('#WMAChart').empty();
                    $('#WMAChart').append('<div id="DivWMAChart" style="width: 100%; height: 100%; display: inline-block"></div>');

                    $("#DivWMAChart").chart({
                        gallery: cfx.Gallery.Bar,
                        titles: [{ text: "Warrant Matching Activity", richText: true }],
                        dataValues: result,
                        allSeries: {
                            pointLabels: {
                                visible: true
                            }
                        },
                        axisY: {
                            grids: {
                                major: {
                                    visible: false,
                                    tickMark: cfx.TickMark.None
                                },
                                minor: {
                                    visible: false,
                                    tickMark: cfx.TickMark.None
                                }
                            }
                        },
                    });
                    $('#AMoreWMA').show();
                    //************
                }
                else {
                    if (getCookie('CompanyID') == 1) {
                        $('#tbodyWMA').empty();
                        $('#tblbodyWMASearchType').empty();//-- not comment
                    }
                    $('#tbodyWMAForOfficer').empty();
                    $('#AMoreWMA').hide();
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FillWMAMore(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;
    var ShowAll = true;
    Type = "GET";

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var inputParams = "/GetWMA_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate
        + "&ToDate=" + FromDate + "&OfficerID=" + OfficerID + "&GroupID=" + GroupID + "&ShowAll=" + ShowAll;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodyWMAMore').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyWMAMore').append($('<tr>').append($('<td>').append($('<a>').attr('id', 'AMoreWMA' + value.OfficerId).html(value.OfficerName)))
                            .append($('<td>').html(value.Activities)))
                    });
                }
            }
            else {
                $('#tblbodyWMASearchType').empty();
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FillWMADetailL1(OfficerID, FromDate, ToDate) {
    Type = "GET";
    var inputParams = "/GetWMADetailL1_Dashboard?OfficerID=" + OfficerID + "&FromDate=" + FromDate + "&ToDate=" + FromDate;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);

                    $('#tblbodySearchType').empty();
                    showhidetabs('tblbodySearchType');
                    $('#lblSearchType').html('');
                    $('#btnBack').hide();
                    $('#tblbodyWMASearchType').empty();

                    $.each(result, function (key, value) {
                        $('#tblbodySearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', value.LogType).html(value.LogType)))
                            .append($('<td>').html(value.Activities)))

                        $('#tblbodyWMASearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', 'A' + value.LogType)
                            .attr('data-toggle', 'modal').attr('href', '#windowTitleDialog').html(value.LogType))).append($('<td>').html(value.Activities)))

                        $('#A' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        });

                        $('#' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        })
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FillWMADetailL3(OfficerID, FromDate, ToDate) {
    Type = "GET";
    var inputParams = "/GetWMADetailL1_Dashboard?OfficerID=" + OfficerID + "&FromDate=" + FromDate + "&ToDate=" + FromDate;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);

                    $('#tblbodySearchType').empty();
                    showhidetabs('tblbodySearchType');
                    $('#lblSearchType').html('');
                    $('#btnBack').hide();
                    $('#tblbodyWMASearchType').empty();
                    $('#tblWMASearchType').show();
                    $('#tblWMA').hide();

                    $.each(result, function (key, value) {
                        $('#tblbodySearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', value.LogType).html(value.LogType)))
                            .append($('<td>').html(value.Activities)))

                        $('#tblbodyWMASearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', 'A' + value.LogType)
                            .attr('data-toggle', 'modal').attr('href', '#windowTitleDialog').html(value.LogType))).append($('<td>').html(value.Activities)))

                        $('#A' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        });

                        $('#' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        })
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function caseaction(Flag) {
    $('#btnCaseActionBack,#btnCaseActionManager,#Notesformanager,#btnNotesBack,#divCaseActionsOfficerCount').hide();
    caseactionshowhidetabs();
    $('#divCaseActionsManagerCount').show();
    var ID = getCookie('OfficerID');
    CompanyID = getCookie('CompanyID');
    Flag += 1;
    if (CompanyID == 1) {
        switch (Flag) {
            case 1:
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#HCaseActionTitle').html('Part Paid');
                break;
            case 3:
                $('#HCaseActionTitle').html('£ Collected');
                break;
            case 4:
                $('#HCaseActionTitle').html('Returned');
                break;
            case 5:
                $('#HCaseActionTitle').html('Left Letter');
                break;
            case 6:
                $('#HCaseActionTitle').html('Defendant Contact');
                break;
        }
    }
    else {
        switch (Flag) {
            case 1:
                $('#HCaseActionTitle').html('TCG');
                break;
            case 2:
                $('#HCaseActionTitle').html('TCG PAID');
                break;
            case 3:
                $('#HCaseActionTitle').html('TCG PP');
                break;
            case 4:
                $('#HCaseActionTitle').html('PAID');
                break;
            case 5:
                $('#HCaseActionTitle').html('PART PAID');
                break;
            case 6:
                $('#HCaseActionTitle').html('UTTC');
                break;
            case 7:
                $('#HCaseActionTitle').html('DROPPED');
                break;
            case 8:
                $('#HCaseActionTitle').html('OTHER');
                break;
        }
    }
    Type = "GET";

    if ($('#txtFromDateCA').val() == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        $('#txtFromDateCA').val(TDate);
    }

    var dateAr = $('#txtFromDateCA').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

    var inputParams = "/GetCaseActionsDashboardForManager?ActionText=" + $('#HCaseActionTitle').html() + "&FromDate="
            + newDate + "&ToDate=" + newDate + "&CompanyID=" + getCookie('CompanyID') + "&ManagerID=" + 0 + "&GroupID=0";
    Url = serviceUrl + inputParams;

    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader1').show();
        },
        complete: function () {
            $('#imgLoader1').hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodyCaseActionsManagerCount').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyCaseActionsManagerCount')
                            .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.ManagerId).hide())
                            .append($('<a>').attr('onclick', 'GetCaseActionsOfficerCount(' + Flag + ',' + $.trim(value.ManagerId) + ',' + 0 + ')')
                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.ManagerName)))
                            .append($('<td>').html(value.CaseCnt)))
                    });
                }
                else {
                    $('#tbodyCaseActionsManagerCount').empty().html('No records found');
                }
            }
            else {
                $('#tbodyCaseActionsManagerCount').empty().html('No records found');
            }
        },
        error: function () {
            //alert('error');
        }
    });

    if (IsManager == 0) {
        $('#divCaseActionsOfficerCount').hide();
        $('#divCaseActionsManagerCount').show();
    }
    else {
        // fill offier calle
        GetCaseActionsOfficerCount(Flag, getCookie('ManagerID'), 0);
        $('#divCaseActionsOfficerCount').show();
        $('#divCaseActionsManagerCount').hide();
    }
}

function GetCaseActionsOfficerCount(Flag, MgrID, GrpID) {
    $('#btnCaseActionBack,#Notesformanager,#btnNotesBack,#divCaseActionsManagerCount').hide();
    caseactionshowhidetabs();
    $('#divCaseActionsOfficerCount').show();
    if (!(getCookie('IsManagerSelect') == '' || getCookie('IsManagerSelect') == undefined))
        $('#btnCaseActionManager').hide();
    else
        $('#btnCaseActionManager').show();
    var ID = getCookie('OfficerID');
    CompanyID = getCookie('CompanyID');
    if (CompanyID == 1) {
        switch (Flag) {
            case 1:
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#HCaseActionTitle').html('Part Paid');
                break;
            case 3:
                $('#HCaseActionTitle').html('£ Collected');
                break;
            case 4:
                $('#HCaseActionTitle').html('Returned');
                break;
            case 5:
                $('#HCaseActionTitle').html('Left Letter');
                break;
            case 6:
                $('#HCaseActionTitle').html('Defendant Contact');
                break;
        }
    }
    else {
        switch (Flag) {
            case 1:
                $('#HCaseActionTitle').html('TCG');
                break;
            case 2:
                $('#HCaseActionTitle').html('TCG PAID');
                break;
            case 3:
                $('#HCaseActionTitle').html('TCG PP');
                break;
            case 4:
                $('#HCaseActionTitle').html('PAID');
                break;
            case 5:
                $('#HCaseActionTitle').html('PART PAID');
                break;
            case 6:
                $('#HCaseActionTitle').html('UTTC');
                break;
            case 7:
                $('#HCaseActionTitle').html('DROPPED');
                break;
            case 8:
                $('#HCaseActionTitle').html('OTHER');
                break;
        }
    }
    Type = "GET";

    if ($('#txtFromDateCA').val() == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        $('#txtFromDateCA').val(TDate);
    }


    var dateAr = $('#txtFromDateCA').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

    var inputParams = "/GetCaseActionsDashboardForManager?ActionText=" + $('#HCaseActionTitle').html() + "&FromDate="
            + newDate + "&ToDate=" + newDate + "&CompanyID=" + getCookie('CompanyID') + "&ManagerID=" + MgrID + "&GroupID=" + GrpID;
    Url = serviceUrl + inputParams;

    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader1').show();
        },
        complete: function () {
            $('#imgLoader1').hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodyCaseActionsOfficerCount').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyCaseActionsOfficerCount')
                            .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())
                            .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ',' + $.trim(value.OfficerID) + ')')
                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                            .append($('<td>').html(value.CaseCnt)))
                    });
                }
                else {
                    $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
                }
            }
            else {
                $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
            }
        },
        error: function () {
            //alert('error');
        }
    });

}

function clickoncaseactionmanager(caseid, key, OfficerID) {
    caseactionshowhidetabs();
    if (getCookie('CompanyID') == 1 && caseid == 7) {
        FillDeviationDetailForManager($('#HCaseActionManagerID' + key).html(), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), OfficerID, '0');
    }
    else {
        FillCaseDetailsActionForManager($('#HCaseActionManagerID' + key).html(), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), OfficerID, '0');
    }
    $('#HCaseActionNavigation').show();
    $('#btnCaseActionBack').show();
    $('#divCaseActionsManagerCount,#divCaseActionsOfficerCount,#btnCaseActionManager').hide();

    if (getCookie('CompanyID') == 1) {
        switch (caseid) {
            case 4:
                $('#returnedformanager').show();
                $('#HCaseActionTitle').html('Returned');
                break;
            case 2:
                $('#partpaidformanager').show();
                $('#HCaseActionTitle').html('Part Paid');
                break;
            case 1:
                $('#paidformanager').show();
                $('#HCaseActionTitle').html('Paid');
                break;
            case 3:
                $('#revisitformanager').show();
                $('#HCaseActionTitle').html('£ Collected');
                break;
            case 5:
                $('#leftletterformanager').show();
                $('#HCaseActionTitle').html('Left Letter');
                break;
        }
    }
    else {
        switch (caseid) {
            case 1:
                $('#tcgformanager').show();
                $('#HCaseActionTitle').html('TCG');
                break;
            case 2:
                $('#tcgpaidformanager').show();
                $('#HCaseActionTitle').html('TCG PAID');
                break;
            case 3:
                $('#tcgppformanager').show();
                $('#HCaseActionTitle').html('TCG PP');
                break;
            case 4:
                $('#paidformanager').show();
                $('#HCaseActionTitle').html('PAID');
                break;
            case 5:
                $('#partpaidformanager').show();
                $('#HCaseActionTitle').html('PART PAID');
                break;
            case 6:
                $('#uttcformanager').show();
                $('#HCaseActionTitle').html('UTTU');
                break;
            case 7:
                $('#droppedformanager').show();
                $('#HCaseActionTitle').html('DROPPED');
                break;
            case 8:
                $('#otherformanager').show();
                $('#HCaseActionTitle').html('OTHER');
                break;
        }
    }
}

//Working Hours - TAB

function GetWorkingHours(OfficerID) {
    Type = "GET";
    var inputParams = "/GetOfficersWorkingHours?OfficerID=" + OfficerID;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call
            if (result != undefined) {
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $('#tbodyWorkingHours').empty();
                    $.each(result, function (key, value) {
                        if (value.CallId == '9999' || value.CallId == '99991') {
                            $('#thWorkingName').html('Manager Name');
                            $('#tbodyWorkingHours').append($('<tr>').append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#DivWorkingHoursMore')
                                                                        .attr('onclick', 'GetWorkingHoursMore(' + value.OfficerID + ',0' + ')').html(value.OfficerName)))
                                                                    .append($('<td id="tdbodyWorkingHours' + key + '">').html(value.AvgHours))
                                                                    .append($('<td>').html(value.PredictedHours)))
                        }
                        else {
                            $('#thWorkingName').html('Officer Name');
                            $('#tbodyWorkingHours').append($('<tr>').append($('<td>').html(value.OfficerName))
                                                               .append($('<td id="tdbodyWorkingHours' + key + '">').html(value.AvgHours))
                                                               .append($('<td>').html(value.PredictedHours)))
                        }
                    });
                }
                else {
                    $('#tbodyWorkingHours').append('<tr><td colspan="3">No data found</td></tr>');
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function GetWorkingHoursMore(OfficerID, Flag) {
    $('#DivWorkingHoursOfficersList').hide();
    $('#DivWorkingHoursManagersList').show();
    $('#btnWorkingHoursBack').hide();

    Type = "GET";
    var inputParams = "/GetOfficersWorkingHours?OfficerID=" + OfficerID + "&ShowAll=" + Flag;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call 
            if (result != undefined) {
                $('#tbodyWorkingHoursManagersList').empty();
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        if (value.CallId == '9999' || value.CallId == '99991') {
                            $('#thWorkingName').html('Manager Name');
                            $('#tbodyWorkingHoursManagersList').append($('<tr>').append($('<td>').append($('<a>').attr('style', 'cursor:pointer;')
                                                                    .attr('onclick', 'GetWorkingHoursForOfficers(' + value.OfficerID + ')').html(value.OfficerName)))
                                                               .append($('<td id="tdbodyWorkingHoursManagerList' + key + '">').html(value.AvgHours))
                                                               .append($('<td>').html(value.PredictedHours)))
                        }
                        else {
                            $('#thWorkingName').html('Officer Name');
                            $('#tbodyWorkingHoursManagersList').append($('<tr>').append($('<td>').html(value.OfficerName))
                                                           .append($('<td id="tdbodyWorkingHoursManagerList' + key + '">').html(value.AvgHours))
                                                           .append($('<td>').html(value.PredictedHours)))
                        }
                    });
                }
                else {
                    $('#tbodyWorkingHoursManagersList').append('<tr><td colspan="3">No data found </td></tr>');
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });

}

function GetWorkingHoursForOfficers(OfficerID) {
    $('#DivWorkingHoursOfficersList').show();
    $('#DivWorkingHoursManagersList').hide();
    $('#btnWorkingHoursBack').show();

    Type = "GET";
    var inputParams = "/GetOfficersWorkingHours?OfficerID=" + OfficerID + "&ShowAll=0";
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $('#tbodyWorkingHoursOfficersList').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyWorkingHoursOfficersList').append($('<tr>').append($('<td>').html(value.OfficerName))
                                                                .append($('<td id="tdbodyWorkingHoursOfficerList' + key + '">').html(value.AvgHours))
                                                                .append($('<td>').html(value.PredictedHours)))
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

// End Working hours tab

// Flash News

function GetFlashNews() {
    Type = "GET";
    var inputParams = "/GetFlashNews?CompanyID=" + getCookie('CompanyID');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#DMarquee').empty();
                    $.each(result, function (key, value) {
                        $('#DMarquee').append('<marquee behavior="scroll" direction="left" scrollamount="6">' + value.MessageText + '</marquee>')
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}
// End Flash news

function GetStatistics(OfficerID, link) {
    $('.popover').remove();
    Type = "GET";
    var inputParams = "/GetStatsDashboard?OfficerID=" + OfficerID;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $('#tbodyStats').empty();
                    $.each(result, function (key, value) {
                        var StatFormula = '<table width="100%" class="display-status-table"><tr><td>' +
                                            '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                            '<a class="close" data-dismiss="modal" href="#">X</a>' +
                                            '<p class="block-heading">Percentage calculation</p><table class="">' +
                                            '<tbody>' +
                                            '<tr><td>Formula for ' + value.Description + ' : ' + value.Formula + '</td></tr>' +
                                            '<tr><td>' + '</td></tr>' +
                                            '<tr><td>Calculation for ' + value.Description + ' : ' + value.Calculation + '</td></tr>' +
                                           '</tbody></table></div></td>' +
                                           '</td></tr></table>';

                        $('#tbodyStats').append($('<tr>')
                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                            .append($('<td>').append($('<a>').attr('id', 'AStat' + key).attr('style', 'cursor:pointer;')
                                                .attr('onclick', 'GetStatsOnClick(' + key + ')').html(value.Description)
                                                .attr('data-original-title', '').attr('rel', 'popover').attr('class', 'a1').attr('data-html', 'true')))
                            .append($('<td id="tdbodyStats' + key + '">').html(value.DisplayValue)))


                        $('#AStat' + key).mouseover(function () {
                            $('#AStat' + key).attr('data-content', StatFormula);
                            $('#AStat' + key).attr('data-placement', 'left');
                            $('#AStat' + key).popover('show');
                        });
                        $('#AStat' + key).mouseout(function () {
                            $('#AStat' + key).popover('hide');
                        });

                        if (link == 1) {
                            if ($.trim(value.Description) == "Case holdings") {
                                $('#tdbodyStats' + key).empty();//data-toggle="modal" href="#DivLogout"
                                $('#tdbodyStats' + key).append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseSearchByOfficerID').attr('onclick', 'CaseSearchByOfficerID(' + OfficerID + ')').html(value.DisplayValue));
                            }
                        }
                    });
                    $('.close').click(function () { $('.popover').remove(); });
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function GetStatsOnClick(key) {
    var ID;
    var Flag = key + 1;
    if (LinkForManager == 0) {
        ID = getCookie('OfficerID');
        $('#AStat' + key).attr('data-toggle', 'modal').attr('href', '#divStats');
        stats(ID, Flag);
    }
    else if (($('#lblSelectedOfficerID').html() != "0") || ($('#lblSelectedGroupID').html() != "0")) {
        $('#AStat' + key).removeAttr('data-toggle').removeAttr('href');
    }
    else {
        ID = getCookie('ManagerID');
        $('#AStat' + key).attr('data-toggle', 'modal').attr('href', '#divStats');
        stats(ID, Flag);
    }
}

function stats(ID, Flag) {
    switch (Flag) {
        case 1:
            $('#HStatsTitle').html('Productivity');
            break;
        case 2:
            $('#HStatsTitle').html('Efficiency');
            break;
        case 3:
            $('#HStatsTitle').html('Conversion');
            break;
        case 4:
            $('#HStatsTitle').html('Case Holdings');
            break;
        case 5:
            $('#HStatsTitle').html('Paids Vs Targets');
            break;
        case 6:
            $('#HStatsTitle').html('Predicted Efficiency');
            break;
    }
    Type = "GET";
    var inputParams = "/GetStatsOfficerCount?OfficerID=" + ID + "&Flag=" + Flag;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $('#tbodyStatsOfficerCount').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyStatsOfficerCount').append($('<tr>').append($('<td>').html(value.OfficerName)).append($('<td>').html(value.Cnt)))
                    });
                }
                else {
                    $('#tbodyStatsOfficerCount').empty().html('No records found');
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });

}

function FillWMADetailL2(OfficerID, FromDate, ToDate, LogType) {
    Type = "GET";
    var inputParams = "/GetWMADetailL2_Dashboard?OfficerID=" + OfficerID + "&FromDate=" + FromDate + "&ToDate=" + FromDate + "&LogType=" + LogType;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            $('#tbodysearchdetails').empty();
            $('#tbodysearchdetails').append($('<tr>').append($('<td colspan="3">').html('No warrant matching activity found for ' + LogType)));
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodysearchdetails').empty();
                    $.each(result, function (key, value) {
                        $('#tbodysearchdetails').append($('<tr>')
                            .append($('<td>').html(value.SearchKey))
                            .append($('<td>').html(value.ResultReturned))
                            .append($('<td>').html(value.ActionedDate)))
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

function UpdateNotificationAlert(ManagerID, MessageText, OfficerID, IsPriority) {
    if (OfficerID == undefined) OfficerID = 0;
    Type = "GET";
    var inputParams = "/UpdateNotificationMessage?ManagerID=" + ManagerID + "&MessageText=" + MessageText.replace('&', 'ybcc')
        + "&Officer=" + $.trim(OfficerID) + "&IsPriority=" + IsPriority;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('.word_count').hide();
            $('#imgLoading').show();
            $('#btnSendNotification').hide();
        },
        complete: function () {
            //  $('#selOfficer').multiselect("uncheckAll");
            if ($("#chkIsPriority").is(':checked')) {
                $('.word_count span').text('200 characters left');
            }
            else {
                $('.word_count span').text('900 characters left');
            }
            $('#lblMsgReport').html('Message sent successfully').show().fadeOut(3000);
            $('#lblMsgReport').css('color', 'green');
            $('.word_count').fadeIn(4000);
            $('#imgLoading').hide();
            $('#btnSendNotification').show();

            $('#selOfficer').multiselect({
                noneSelectedText: 'Select Officers',
                selectedList: 5,
                multiple: true
            }).multiselectfilter();

            $('#selOfficer').multiselect("uncheckAll");
        },
        success: function (result) {//On Successfull service call  
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });


}

function UpdateAutoRefresh(OfficerID, AutoRefresh) {
    if (OfficerID == undefined) OfficerID = 0;
    Type = "GET";
    var inputParams = "/UpdateAutoRefresh?OfficerID=" + OfficerID + "&AutoRefresh=" + AutoRefresh;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call
            $('#lblTargetInfo').html('Auto refresh value has been updated successfully.');
            $('#ATargetInfo').click();
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

function showhidetabs(tabname) {
    $('#show-search-details,#tblbodySearchType').hide();
    $('#' + tabname).show();
}

function caseactionshowhidetabs() {
    $('#leftletterformanager,#revisitformanager,#partpaidformanager,#returnedformanager,#paidformanager').hide();
    $('#tcgpaidformanager,#tcgppformanager,#tcgformanager,#uttcformanager,#droppedformanager,#otherformanager').hide();
}

function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}

function DeleteCookie(name) {
    document.cookie = name + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
}

/************************************copied from officer web for showing case holdings**************************************/

function CaseSearchByOfficerID(OfficerID) {
    var Type = "GET";
    var DataType = "jsonp"; var ProcessData = false;
    var Url = serviceUrl + "/GetCaseSearchByOfficerID?OfficerID=" + OfficerID;

    var ContentType = "application/javascript";
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call                      
            if (!(result == null || result == '[]')) {

                result = JSON.parse(result);
                $('#tbodyCaseSearchByOfficerID').empty();
                var HTMLString = '';
                $.each(result, function (key, value) {
                    HTMLString += '<tr><td>' + value.casenumber + '</td><td>' + value.DebtName + '</td><td>' + value.ClientName + '</td><td>' + value.ContactFirstLine + ', ' + value.ContactAddress + ', ' + value.PostCode + '</td>' +
                            '<td>' + value.CaseStatus + '</td><td>' + value.OfficerFirstName + ' ' + value.OfficerLastName + '</td><td>' + value.Ageing + '</td><td>' + value.IssueDate + '</td><td>' + value.DateActioned + '</td></tr>';
                });
                $('#tbodyCaseSearchByOfficerID').html(HTMLString);
            }
            else {
                $('#tbodyCaseSearchByOfficerID').empty();
                $('#tbodyCaseSearchByOfficerID').append($('<tr><td colspan="9"> No records found </td></tr>'));
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}


/***********************************************************HeartBeat********************************************************/

function HeartBeat() {
    $('#iframeGMAP').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Medium&fDate=" + FD + "&tDate=" + FD);// + "&gID=" + GroupID + "&ShowAll=" + ShowAll);

    $('#iframePopupMap').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Large&fDate=" + FD + "&tDate=" + FD);// + "&gID=" + GroupID + "&ShowAll=" + ShowAll);
}

/***********************************************************HeartBeat********************************************************/

function GetOfficerActivityStatus() {
    var id = getCookie('OfficerID');
    Type = "GET";
    var inputParams = "/GetOfficerActivityStatus?IndexValue=" + tableIndex + "&OfficerID=" + id;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
            $('#DivOfficerStatusContent').empty();
            $('#DivOfficerStatusContent').append('<div id="tdCont"></div>');
            $('#DivOfficerStatusContent').append($('<div style="border: 1px solid #ededed; margin: -15px; margin-left:12px; width:1150px; height: 370px;display:none;" id="divMapOAS"><iframe  width="100%" height="100%" frameborder="0"' +
                                    'scrolling="yes" marginheight="0"' +
                                    'marginwidth="0" id="iframeGMAPOAS" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=London,+United+Kingdom&amp;aq=0&amp;oq=lond&amp;sll=55.378051,-3.435973&amp;sspn=20.606847,39.506836&amp;ie=UTF8&amp;hq=&amp;hnear=London,+United+Kingdom&amp;t=m&amp;z=11&amp;iwloc=A&amp;ll=51.511214,-0.119824&amp;output=embed"></iframe></div>')

               )

            if (result != undefined) {
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        tableIndex = value.IndexNo;
                        $('#lblManagerNameActivityStatus').html(value.ManagerName);//.html('Officer`s Activity Status - ' + value.ManagerName);
                        $('#tdGray').html(value.Darkgray);
                        $('#tdGreen').html(value.Green);
                        $('#tdRed').html(value.Red);
                        $('#tdAmber').html(value.Amber);

                        if ($('#tbl' + $.trim(value.ManagerId)).length == 0) {
                            $('#tdCont').append('<div id="tbl' + $.trim(value.ManagerId) + '"></div>')
                        }

                        if ($('#tbl' + $.trim(value.ManagerId) + ' tr').length == 0) {
                            $('#tbl' + $.trim(value.ManagerId)).append($('<div class="col-md-4 col-sm-4">').append($('<div class="bubble ' + value.Status + 'Bub">')
                                    .html(value.OfficerName).attr('id', 'OAS' + value.OfficerID)))
                        }
                        else {
                            if ($('#tbl' + $.trim(value.ManagerId) + ' tr:last' + ' td').length >= 3) {
                                $('#tbl' + $.trim(value.ManagerId)).append($('<tr>'))
                            }
                            $('#tbl' + $.trim(value.ManagerId) + ' tr:last').append($('<td style="padding:10px !important;">')
                                .append($('<div class="bubble ' + value.Status + 'Bub">').html(value.OfficerName).attr('id', 'OAS' + value.OfficerID)));
                        }

                        $('#OAS' + value.OfficerID).click(function () {
                            $('#btnBacktoStatus').show();
                            $('#lblOfficerNameMapview').html(value.OfficerName);
                            $('#lblOfficerNameMapview').show();

                            clearInterval(IntervalvalueOfficerStatus);

                            $('#iframeGMAPOAS').attr('src', 'GMaps.html?mID=0' + "&fDate=" + $.datepicker.formatDate('yy/mm/dd', new Date()) + "&tDate="
                                + $.datepicker.formatDate('yy/mm/dd', new Date())
                                + "&oID=" + $(this).attr('id').split('OAS')[1] + "&Map=Large&gID=0&ShowAll=1");

                            $('#tdPrev').hide();
                            $('#tdNext').hide();

                            $('#divMapOAS').show();
                            $('#tdCont').hide();
                            $('#divOfficerActivityStatus').css('width', '87%').css('height', '90%');

                            $('#btnBacktoStatus').unbind().click(function () {
                                $('#tdCont').show();
                                $('#divMapOAS').hide();
                                $('#tdPrev').show();
                                $('#tdNext').show();

                                $('#lblOfficerNameMapview').hide();
                                clearInterval(IntervalvalueOfficerStatus);
                                $('#divOfficerActivityStatus').css('width', '').css('height', '');
                                $(this).hide();

                                tableIndex = tableIndex - 1;
                                GetOfficerActivityStatus();

                                IntervalvalueOfficerStatus = setInterval(function () {
                                    GetOfficerActivityStatus();
                                }, 30000);
                            });
                        });

                    });

                    $('#lblManagerNameActivityStatus').html($('#tdCont table:eq(' + tableIndex + ')').attr('mname'));
                    tableIndex = tableIndex + 1;
                }
            }

            $('#DivOfficerStatusContent').append($('<table style="width:100%">')
           .append($('<tr >').append($('<td style="width:50%">').attr('id', 'tdPrev'))
           .append($('<td style="width:50%;text-align:right;">').attr('id', 'tdNext'))))

            $('#tdPrev').append('<img src="images/Previous_W.png" style="cursor:pointer;" /><br><span style="cursor:pointer;">Previous</span>');
            $('#tdNext').append('<img src="images/Next_W.png" style="cursor:pointer;" /><br><span style="cursor:pointer;">Next</span>');

            $('#tdPrev').unbind().click(function () {

                tableIndex = tableIndex - 2;

                if (tableIndex <= -1) tableIndex = 0;
                GetOfficerActivityStatus();
            });

            $('#tdNext').unbind().click(function () {

                GetOfficerActivityStatus();

                clearInterval(IntervalvalueOfficerStatus);
                IntervalvalueOfficerStatus = setInterval(function () {
                    GetOfficerActivityStatus();
                }, 30000);
            });
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}


function OfficerActivityStatusInit() {
    GetOfficerActivityStatus();
}

function LocationwithHB() {
    $('#iframeGMAP').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Medium&fDate=" + $('#txtFromDateLocation').val() + "&tDate=" + $('#txtFromDateLocation').val() + "&LocationwithHB=1");

    $('#iframePopupMap').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Large&fDate=" + $('#txtFromDateLocation').val() + "&tDate=" + $('#txtFromDateLocation').val() + "&LocationwithHB=1");
}

function SubmitOfficerTotal() {
    var TTargetInfo = '';
    $('#tbodyOfficerList tr').each(function (key, value) {
        if ($(this).find('label').html() != '' && $(this).find('label').html() != undefined && parseInt($(this).find('input').val()) > 0) {
            if (TTargetInfo != '') TTargetInfo += ',';
            TTargetInfo += $(this).find('label').html() + '|' + parseInt($(this).find('input').val());
        }
    });

    // ****************************** submit functionality
    Type = "GET";
    var inputParams = "/UpdateOfficerTarget?TMonth=" + $('#selCurrentMonth').val() + "&TargetNo=" +
        $('#txtMonthTotalTarget').val() + "&ManagerID=" + ManagerID + "&OfficerIDs=" + TTargetInfo;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            $('#lblTargetInfo').html('Target has been updated successfully.');
            $('#ATargetInfo').click();
        },
        error: function () {
        } // When Service call fails
    });
}


// Timeline
function GetTimelineResult(OfficerID) {
    var Fdate = $.trim($('#txtTLineFromDate').val()) == '' ? '1/1/1900' : $.trim($('#txtTLineFromDate').val());
    var Tdate = $.trim($('#txtTLineToDate').val()) == '' ? '1/1/1900' : $.trim($('#txtTLineToDate').val());

    var dateT1 = Fdate.split('/');
    var newDateFrom = dateT1[2] + '/' + dateT1[1] + '/' + dateT1[0];

    var dateT2 = Tdate.split('/');
    var newDateTo = dateT2[2] + '/' + dateT2[1] + '/' + dateT2[0];

    Type = "GET";
    var inputParams = "/GetTimelineDetailsforOfficer?OfficerID=" + OfficerID + "&FromDate=" + newDateFrom + "&ToDate=" + newDateTo;// + "&OfficerID=" + id;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (result) {//On Successfull service call  
            $('#cd-timeline').empty();
            if (result != undefined) {
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        $('#cd-timeline').append('<div class="cd-timeline-block">' +
                                                 '<div id="DTimeLinePicture' + key + '" ></div>' +
                                                 '<div class="cd-timeline-content"><h2>' + value.TTitle + '</h2> <p>' + value.TDescription + '</p>' +
                                                 '<span class="cd-date">' + value.TDate + '</span></div></div>');

                        switch (value.TTitle) {
                            case 'LOGIN':
                            case 'ARR':
                            case 'DEP':
                                $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-picture');
                                break;
                            case 'DROPPED':
                            case 'Returned':
                            case 'UTTC':
                                $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-movie');
                                break;
                            case 'OTHER':
                            case 'TCG':
                            case 'Left Letter':
                                $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-location');
                                break;
                            case 'PAID':
                            case 'PART PAID':
                            case 'TCG PAID':
                            case 'TCG PP':
                            case 'Paid':
                            case 'Part Paid':
                                $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-location');
                                break;
                        }
                    });
                }
                else {
                    $('#cd-timeline').append('<div class="cd-timeline-block">' +
                                               '<div class="cd-timeline-img cd-location" ></div>' +
                                               '<div class="cd-timeline-content"><h2>No data found...</h2></div></div>');
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}