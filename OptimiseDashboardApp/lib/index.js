﻿//==========================================================================================================================================
/* File Name: index.js */
/* File Created: January 21, 2014 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================
var OfficerOnFocus;
var l_connectionid;
var RankURL = [];
var RankActionText = [];
var RankViewType = [];

var RankADMURL = [];
var RankADMActionText = [];
var RankADMViewType = [];

var RankOFFURL = [];
var RankOFFActionText = [];
var RankOFFViewType = [];

var WMAMGR_URL = "";
var WMAADM_URL = "";
var WMAOFF_URL = "";
var CAMGR_URL = "";
var CAADM_URL = "";
var CAOFF_URL = "";
var CA_FLAG;
var CA_TYPE = "";
var RA_TYPE = "Manager";

var tryingToReconnect = false;
var signalrconnected = false;

var hbtimer;
var lochbtimer;

var officerHBMap;
var officerHBArray = [];
var hbmarkers = [];
var HB_bounds;
var HB_polyline;

var officerHBMapExpand;
var officerHBExpandArray = [];
var hbexpandmarkers = [];
var HBExpand_bounds;
var HB_Expand_polyline;

var officerLLMap;
var officerLLArray = [];
var llmarkers = [];
var LL_Bounds;

var officerLLMap_Expand;
var officerLLArray_Expand = [];
var llmarkers_expand = [];
var LL_Bounds_Expand;

var officerLocHBMap;
var officerLocHBArray = [];
var LocHB_markers = [];
var LocHB_bounds;
var LocHB_polyline;

var officerLocHBMap_Expand;
var officerLocHBArray_Expand = [];
var LocHB_markers_Expand = [];
var LocHB_bounds_Expand;
var LocHB_polyline_Expand;

$(document).ready(function () {

    clearmaparrays();

    $("body").click(function (e) {
        $('.menupopover').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.menupopover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

    window.onerror = function (message, url, lineNumber) {
        showconsole(message);
    };

    window.onbeforeunload = function (e) {
        $.connection.hub.stop();
    };

    $.loader({
        className: "blue-with-image",
        content: ' '
    });

    if ($.trim($.session.get('RoleType')) == 1) {
        if ($.trim($.session.get('CompanyID')) != 1)
            window.location.href = HomeUrl;
    }
    else if (($.trim($.session.get('RoleType')) != 5) && ($.trim($.session.get('RoleType')) != 7)) {
        if ($.trim($.session.get('CompanyID')) != 1)
            window.location.href = HomeUrl;
    }
    else {
        window.location.href = HomeUrl;
    }

    $.connection.hub.qs = { "Authorization": $.session.get('Token') };
   // $.connection.hub.qs = {  };
    var l_signalrconnected = $.session.get("signalrconnected");
    counter = $.connection.dashboardHub;
    OfficerID = $.session.get('OfficerID');
    showconsole(OfficerID);
    $.session.set('CaseActionMode', 'S');
    $.session.set('WMAMode', 'S');
    $.session.set('LocationMode', 'S');
    $.session.set('RankMode', 'S');
    $.session.set('Map', 'LL');

    $.connection.hub.reconnecting(function () {
        var d = new Date();
        showconsole("Trying to reconnecting");
        tryingToReconnect = true;
    });

    $.connection.hub.reconnected(function () {
        var d = new Date();
        showconsole("Reconnected");
        updateAllPanel();
        tryingToReconnect = false;
    });

    $.connection.hub.disconnected(function () {
        if (tryingToReconnect) {

            setTimeout(function () {
                $.connection.hub.qs = { "Authorization": $.session.get('Token') };
                $.connection.hub.start();
            }, 2000); // Restart connection after 2 seconds.
        }
    });

    $.connection.hub.error(function (error) {
        showconsole('SignalR error: ' + error);
    });

    //{ transport: ['webSockets', 'serverSentEvents'] }
    //$.connection.dashboardHub.client.ClientOnConnected = function (message){
   
    counter.client.tokenupdated = function (message) {
        showconsole("-Client-" + message);
    };

   // $.connection.hub.logging = true;

    date = new Date();
    TodayDate = $.datepicker.formatDate('yy/mm/dd', date) + ' ' + date.getHours() + ':' + date.getMinutes();
    TodayDateOnly = $.datepicker.formatDate('dd/mm/yy', date);

    $('#txtFromDate,#txtToDate,#txtFromDateCA,#txtToDateCA,#txtFromDateLocation,#txtToDateLocation').val(TodayDateOnly);
    $('#txtFromDateRanking,#txtToDateRanking,#txtTLineFromDate,#txtTLineToDate').val(TodayDateOnly);

    //==========================================================================================================================================
    //TODO : Datepicker 
    var cDate = new Date();
    $('#txtFromDate,#txtToDate,#txtFromDateCA,#txtToDateCA,#txtFromDateRanking,#txtToDateRanking').datetimepicker({
        timepicker: false,
        format: 'd/m/Y',
        maxDate: TodayDate,
    }).datepicker("setDate", TodayDate);

    $('#txtFromDateLocation,#txtToDateLocation,#txtTLineFromDate,#txtTLineToDate').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    }).datepicker("setDate", TodayDate);

    //******************************************************************************
    try {
        if ($.session.get('OfficerID') != '' && $.session.get('OfficerID') != undefined) {
            $.session.set('RankingType', 'Paid');
            $.session.set('ManagerID', OfficerID);
            $.session.set('Map', 'LL');
            $('#lblLoginOfficer').html($.session.get('OfficerName'));
            //==========================================================================================================================================
            // TODO : Notification
            $('#txtRefreshTime').keydown(function (event) {
                if (event.shiftKey == true) {
                    $('#txtRefreshTime').attr('title', 'Enter numeric values');
                    return false;
                }
                if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                    return true;
                }
                else {
                    $('#txtRefreshTime').attr('title', 'Enter numeric values');
                    return false;
                }
            });

            $('#btnTLineSubmit').click(function () {
                GetTimelineResult($.trim($.session.get('TimelineOfficer')));
            });

            $('#txtareacontent').val('');

            //: Default checkbox is checked or not
            if ($("#chkIsPriority").is(':checked')) {
                max = 200;
                $('.word_count span').text(max - $.trim($('#txtareacontent').val()).length + ' characters left');
            }
            else {
                max = 900;
                $('.word_count span').text(max - $.trim($('#txtareacontent').val()).length + ' characters left');
            }

            // Checkbox change function
            $("#chkIsPriority").change(function () {
                if ($("#chkIsPriority").is(':checked')) {
                    max = 200;
                    $('.word_count span').text(max - $.trim($('#txtareacontent').val()).length + ' characters left');
                }
                else {
                    max = 900;
                    $('.word_count span').text(max - $.trim($('#txtareacontent').val()).length + ' characters left');
                }
            });

            $("#txtareacontent").bind('paste', function (e) {
                if ($("#chkIsPriority").is(':checked')) {
                    max = 200;
                    $('#txtareacontent').attr('maxlength', '200');
                    setTimeout(function () {
                        if ($('#txtareacontent').val().length >= max) {
                            $('.word_count span').text(' Character limit exceeded').attr('style', 'font-weight: bold');
                        }
                        else {
                            $('.word_count span').text(max - $('#txtareacontent').val().length + ' characters left');
                        }
                    }, 100);
                }
                else {
                    max = 900;
                    $('#txtareacontent').attr('maxlength', '900');
                    setTimeout(function () {
                        if ($('#txtareacontent').val().length >= max) {
                            $('.word_count span').text(' Character limit exceeded').attr('style', 'font-weight: bold');
                        }
                        else {
                            $('.word_count span').text(max - $('#txtareacontent').val().length + ' characters left');
                        }
                    }, 100);
                }
            });

            // Textarea keydown function
            $('#txtareacontent').keydown(function () {
                var len = $.trim($('#txtareacontent').val()).length;// $(this).val().length;
                if ($("#chkIsPriority").is(':checked')) {
                    max = 200;
                    $('.word_count span').text(max - len + ' characters left');
                }
                else {
                    max = 900;
                    $('.word_count span').text(max - len + ' characters left');
                }
                if (len >= max) {
                    $(this).val($(this).val().substring(0, len - 1));
                    $('.word_count span').text(' Reached the limit');
                }
                else {
                    var ch = max - len;
                    $('.word_count span').text(ch + ' characters left');
                }

                if ($.trim($('#txtareacontent').val()).length > 0) {
                    $('#btnSendNotification').removeAttr('disabled');
                }
                else {
                    $('#btnSendNotification').attr('disabled', true);
                }
            });

            $('#btnSendNotification').click(function () {
                var array_of_checked_values = $("#selOfficer").multiselect("getChecked").map(function () {
                    return this.value;
                }).get();
                var notes = $("#txtareacontent").val().replace(/[^a-z0-9,£:'`&.\s]/gi, '').replace(/[_\s]/g, ' ');
                var OIDs = array_of_checked_values.toString().split(',');
                if (!(OIDs == '' || OIDs == null)) {
                    if ($("#chkIsPriority").is(':checked')) {
                        max = 200;
                        if ($.trim($('#txtareacontent').val()).length >= max) {
                            $('.word_count span').text(' Character limit exceeded').attr('style', 'font-weight: bold');
                        }
                        else {
                            $.each(OIDs, function (key, value) {
                                UpdateNotificationAlert($.session.get('OfficerID'), notes, value, $("#chkIsPriority").is(':checked'));
                            });
                            $('#txtareacontent').val('');
                            $("#chkIsPriority").prop("checked", false);
                        }
                    }
                    else {
                        max = 900;
                        if ($.trim($('#txtareacontent').val()).length >= max) {
                            $('.word_count span').text(' Character limit exceeded').attr('style', 'font-weight: bold');
                        }
                        else {
                            $.each(OIDs, function (key, value) {
                                UpdateNotificationAlert($.session.get('OfficerID'), notes, value, $("#chkIsPriority").is(':checked'));
                            });
                            $('#txtareacontent').val('');
                            $("#chkIsPriority").prop("checked", false);
                        }
                    }
                }
                else {
                    $('.word_count').hide();
                    $('#lblMsgReport').html('Please select officer(s)').show().fadeOut(4000);
                    $('#lblMsgReport').css('color', 'red');
                    $('.word_count').fadeIn(4000);
                }
            });

            //==========================================================================================================================================
            // TODO : Popup box event
            //==========================================================================================================================================
            // Target tab click
            $('#popup1Trigger').click(function () {
                $('#' + $('#popup1Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom in');
                $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
                $('#' + $('#popup3Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
                CntTab1 = 0;
                Tab = 1;
            });

            //Quick Menu tab click
            $('#popup2Trigger').click(function () {
                $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom in');
                $('#' + $('#popup1Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
                $('#' + $('#popup3Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
                if ($.session.get('FlashNews') == undefined || $.session.get('FlashNews') == 'Off') {
                    $('#DMarquee').hide();
                    $('#btnFlashOff').hide();
                    $('#btnFlashOn').show();
                }
                else {
                    $('#btnFlashOn').hide();
                    $('#DMarquee').show();
                    $('#btnFlashOff').show();
                }
                CntTab2 = 0;
                Tab = 2;
                $('#txtRefreshTime').val($.session.get('aref'));
            });

            //Logout tab click
            $('#popup3Trigger').click(function () {
                $('#' + $('#popup3Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom in');
                $('#' + $('#popup1Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
                $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');

                CntTab3 = 0;
                Tab = 3;
            });
            //==========================================================================================================================================

            $('#btnBack').click(function () {
                $('#lblSearchType').html('');
                showhidetabs('tblbodySearchType');
                $('#btnBack').hide();
                if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1)
                    $('#btnWMAOfficerBack,#tblOfficer').show();
                else
                    $('#btnWMAOfficerBack').hide();
            });

            $('#btnStatsBack').click(function () {
                $('#tblStatsManagerCount,#tblStatsOfficerCount,#btnStatsADMBack,#btnStatsBack').hide();
                $('#tblStatsADMCount').show();
                $('#lblStatsMgrName').html('').hide();
                if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 2)
                    $('#btnStatsADMBack').show();
            });

            $('#btnStatsADMBack').click(function () {
                $('#tblStatsADMCount,#tblStatsOfficerCount,#btnStatsADMBack').hide();
                $('#tblStatsManagerCount').show();
            });

            $('#btnCaseActionBack').click(function () {
                caseactionshowhidetabs();
                $('#HCaseActionNavigation,#btnCaseActionBack').hide();
                $('#divCaseActionsOfficerCount,#btnCaseActionADM').show();
                $('#lblCAOfficerName').html('');
                GetCaseActionManager(CAOFF_URL, CA_FLAG, "Officer");

                if ($.session.get('TreeLevel') == 2) {
                    $('#btnCaseActionADM').hide();
                }
            });

            $('#btnDeviationMapBack').click(function () {
                caseactionshowhidetabs();
                $('#HCaseActionNavigation,#btnDeviationMapBack').hide();
                $('#deviatedformanager').show();
                if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1)
                    $('#btnCaseActionBack').hide();
                else
                    $('#btnCaseActionBack').show();

                $('#lblCAOfficerName').html('');
            });

            $('#btnCaseActionManager').click(function () {
                caseactionshowhidetabs();
                $('#HCaseActionNavigation,#divCaseActionsOfficerCount,#btnCaseActionManager,#divCaseActionsADMCount').hide();
                $('#divCaseActionsManagerCount').show();
                $('#lblCAManagerName').html('');
                GetCaseActionManager(CAMGR_URL, CA_FLAG, "Manager");
            });

            $('#btnCaseActionADM').click(function () {
                caseactionshowhidetabs();
                $('#HCaseActionNavigation,#divCaseActionsOfficerCount,#btnCaseActionADM,#divCaseActionsManagerCount').hide();
                $('#divCaseActionsADMCount,#btnCaseActionManager').show();
                $('#lblCAADMName').html('');
                GetCaseActionManager(CAADM_URL, CA_FLAG, "ADM");
                if ($.session.get('TreeLevel') == 1) {
                    $('#btnCaseActionManager').hide();
                }
            });

            //==========================================================================================================================================
            // TODO : Map function
            //==========================================================================================================================================
            $('#btnLocationToday').click(function () {
                clearattributes_LocHBMap();
                clearattributes_LocHBMapExpand();
                $('#txtFromDateLocation').val(TodayDateOnly);
                $.session.set('LocationMode', 'S');
                $('#ALocationDate').attr('data-original-title', 'Today');
                var maptype = $.session.get('Map');
                if (maptype == 'LL') {
                    if (LocationURL != "")
                        GetLastLocation(LocationURL, 0, 1);
                }
                else if (maptype == 'LocHB')
                    $('#ALocationwithHB').trigger('click');
                else if (maptype == 'HB')
                    $('#AHeartBeat').trigger('click');
                else if (maptype == 'All')
                    $('#AShowAll').trigger('click');
                $('#btnLocationDateClose').trigger('click');
            });

            $('#ALastLocation').click(function () {
                $.session.set('Map', 'LLEnlarge');
                if ($.session.get('LocationMode') == 'N') {
                    if ($.session.get('TreeLevel') == 3)
                        LastKnownLocationExpand_Normal(0, $('#txtFromDateLocation').val(), $.session.get('ManagerID'), 0);
                    else
                        LastKnownLocationExpand_Normal($.session.get('ManagerID'), $('#txtFromDateLocation').val(), 0, 0);
                } else
                    GetLastLocation(LocationURL, 0, 1);
            });

            $('#ACloseLocation').click(function () {
                cancelTimeouts();
                if ($.session.get('TreeLevel') == '3')
                    $('#AShowAll').trigger('click');
                else {
                    clearattributes_LLMap_Expand();
                    $('#imgRefreshLocation').trigger('click');
                }
            });

            $('#AHeartBeat').click(function () {
                $.session.set('Map', 'HB');
                var l_LocationDateOk = $('#txtFromDateLocation').val();
                l_LocationDateOk = l_LocationDateOk.replace(/[^0-9\/]/g, '');
                if (l_LocationDateOk.length == 10) {
                    if (l_LocationDateOk == TodayDateOnly)
                        HeartBeat(1);
                    else
                        HeartBeat_Normal(l_LocationDateOk, $.session.get('ManagerID'), 1);
                }
            });

            $('#AHeartBeatEnlarge').click(function () {
                $.session.set('Map', 'HBEnlarge');
                var l_LocationDateOk = $('#txtFromDateLocation').val();
                l_LocationDateOk = l_LocationDateOk.replace(/[^0-9\/]/g, '');
                if (l_LocationDateOk.length == 10) {
                    if (l_LocationDateOk == TodayDateOnly)
                        HeartBeat(2);
                    else
                        HeartBeat_Normal(l_LocationDateOk, $.session.get('ManagerID'), 2);
                }
            });

            $('#ALocationwithHB').click(function () {
                $.session.set('Map', 'LocHB');
                var l_LocationDateOk = $('#txtFromDateLocation').val();
                l_LocationDateOk = l_LocationDateOk.replace(/[^0-9\/]/g, '');
                if (l_LocationDateOk.length == 10) {
                    if (l_LocationDateOk == TodayDateOnly)
                        LocationwithHB(1);
                    else
                        LocationwithHB_Normal(1);
                }
            });

            $('#ALocationwithHBEnlarge').click(function () {
                $.session.set('Map', 'LocHBEnlarge');
                var l_LocationDateOk = $('#txtFromDateLocation').val();
                l_LocationDateOk = l_LocationDateOk.replace(/[^0-9\/]/g, '');
                if (l_LocationDateOk.length == 10) {
                    if (l_LocationDateOk == TodayDateOnly)
                        LocationwithHB(2);
                    else
                        LocationwithHB_Normal(2);
                }
            });

            //==========================================================================================================================================
            // TODO : Rank more
            //==========================================================================================================================================

            $('#btnRankingDateOK').click(function () {
                var l_RankingDateOK = $('#txtFromDateRanking').val();
                l_RankingDateOK = l_RankingDateOK.replace(/[^0-9\/]/g, '');
                if (l_RankingDateOK.length == 10) {
                    if (l_RankingDateOK == TodayDateOnly) {
                        $('#btnRAToday').trigger('click');
                    }
                    else {
                        $('#ARankingDate').attr('data-original-title', l_RankingDateOK);
                        $.session.set('RankMode', 'N');

                        FillRankings_Normal($.session.get('ManagerID'), l_RankingDateOK, l_RankingDateOK, 'Paid');
                        FillRankings_Normal($.session.get('ManagerID'), l_RankingDateOK, l_RankingDateOK, 'Part Paid');
                        FillRankings_Normal($.session.get('ManagerID'), l_RankingDateOK, l_RankingDateOK, 'Returned');

                        $('#ARAClose').trigger('click');
                    }
                }
            });

            $('#btnRAToday').click(function () {
                $('#txtFromDateRanking,#txtToDateRanking').val(TodayDateOnly);
                $.session.set('RankMode', 'S');
                if (RankURL != "") {
                    rankupdated();
                }
                $('#ARankingDate').attr('data-original-title', 'Today');
                $('#ARAClose').trigger('click');
            });

            $('#ARankClose').click(function () {
                if ($.session.get('RankMode') == 'S' || $.session.get('RankMode') == undefined)
                    RA_TYPE = "Manager";
            });

            $('#btnRankADMBack').click(function () {
                $('#DRankingOfficer,#DRankingCase,#btnRankADMBack').hide();
                $('#DRankingADM').show();
                $('#lblRankADMName').html('');
                RA_TYPE = "ADM";

            });

            $('#btnRankOfficerBack').click(function () {
                if ($.session.get('TreeLevel') == 2) {
                    $('#DRankingCase, #DRankMoreNotes, #btnRankOfficerBack,#btnRankADMBack').hide();
                    $('#DRankingCase, #btnRankOfficerBack').show();
                    $('#lblRankOfficerName').html('');
                }
                else {
                    $('#DRankingCase, #DRankMoreNotes, #btnRankOfficerBack,#btnRankADMBack').hide();
                    $('#DRankingOfficer').show();
                    if ($.session.get('OfficerRole') == 9)
                        $('#btnRankADMBack').hide();
                    else
                        $('#btnRankADMBack').show();

                    $('#lblRankOfficerName').html('');
                }
                RA_TYPE = "Officer";
            });

            // paid more click
            $('#ARankingPaidMore').click(function () {
                $('#DRMPaidManager,#DRMReturnManager,#DRMPPManager').hide();
                $('#DRMPaidADM,#DRMReturnADM,#DRMPPADM').hide();
                $('#DRMPaidOfficer,#DRMReturnOfficer,#DRMPPOfficer').hide();
                $('#DRMPaidCase,#DRMReturnCase,#DRMPPCase').hide();
                $('#btnRankMoreADM,#btnRankMoreOfficer,#btnRankMoreCase,#btnRankMoreCaseNotes,#DRankMoreNotes').hide();
                $('#lblRankMore').html('Rank-Paid');
                switch ($.session.get('TreeLevel')) {
                    case '0':
                        $('#lblRankMoreManager,#lblRankMoreADM,#lblRankMoreOfficer').html('');
                        $('#DRMPaidManager').show();
                        break;
                    case '1':
                        $('#lblRankMoreADM,#lblRankMoreOfficer').html('');
                        $('#DRMPaidADM').show();
                        break;
                    case '2':
                        $('#DRMPaidOfficer').show();
                        break;
                }
            });

            // return more click
            $('#ARankingReturnedMore').click(function () {
                $('#DRMPaidManager,#DRMReturnManager,#DRMPPManager').hide();
                $('#DRMPaidADM,#DRMReturnADM,#DRMPPADM').hide();
                $('#DRMPaidOfficer,#DRMReturnOfficer,#DRMPPOfficer').hide();
                $('#DRMPaidCase,#DRMReturnCase,#DRMPPCase').hide();
                $('#btnRankMoreADM,#btnRankMoreOfficer,#btnRankMoreCase,#btnRankMoreCaseNotes,#DRankMoreNotes').hide();

                $('#lblRankMore').html('Rank-Returned');
                switch ($.session.get('TreeLevel')) {
                    case '0':
                        $('#lblRankMoreManager,#lblRankMoreADM,#lblRankMoreOfficer').html('');
                        $('#DRMReturnManager').show();
                        break;
                    case '1':
                        $('#lblRankMoreADM,#lblRankMoreOfficer').html('');
                        $('#DRMReturnADM').show();
                        break;
                    case '2':
                        $('#DRMReturnOfficer').show();
                        break;
                }
            });

            // part paid more click
            $('#ARankingPartPaidMore').click(function () {
                $('#DRMPaidManager,#DRMReturnManager,#DRMPPManager').hide();
                $('#DRMPaidADM,#DRMReturnADM,#DRMPPADM').hide();
                $('#DRMPaidOfficer,#DRMReturnOfficer,#DRMPPOfficer').hide();
                $('#DRMPaidCase,#DRMReturnCase,#DRMPPCase').hide();
                $('#btnRankMoreADM,#btnRankMoreOfficer,#btnRankMoreCase,#btnRankMoreCaseNotes,#DRankMoreNotes').hide();

                $('#lblRankMore').html('Rank-Part Paid');
                switch ($.session.get('TreeLevel')) {
                    case '0':
                        $('#lblRankMoreManager,#lblRankMoreADM,#lblRankMoreOfficer').html('');
                        $('#DRMPPManager').show();
                        break;
                    case '1':
                        $('#lblRankMoreADM,#lblRankMoreOfficer').html('');
                        $('#DRMPPADM').show();
                        break;
                    case '2':
                        $('#DRMPPOfficer').show();
                        break;
                }
            });

            $('#btnRankMoreADM').click(function () {
                $('#btnRankMoreADM,#DRMPaidADM,#DRMReturnADM,#DRMPPADM').hide();
                $('#lblRankMoreManager,#lblRankMoreOfficer').html('');
                switch ($.session.get('RankingType')) {
                    case 'Paid':
                        $('#DRMPaidManager').show();
                        break;
                    case 'Returned':
                        $('#DRMReturnManager').show();
                        break;
                    case 'Part Paid':
                        $('#DRMPPManager').show();
                        break;
                }
            });

            $('#btnRankMoreOfficer').click(function () {
                $('#btnRankMoreOfficer,#DRMPaidOfficer,#DRMReturnOfficer,#DRMPPOfficer,#DRMPaidCase,#DRMReturnCase,#DRMPPCase').hide();
                $('#lblRankMoreADM,#lblRankMoreOfficer').html('');
                switch ($.session.get('RankingType')) {
                    case 'Paid':
                        $('#DRMPaidADM').show();
                        break;
                    case 'Returned':
                        $('#DRMReturnADM').show();
                        break;
                    case 'Part Paid':
                        $('#DRMPPADM').show();
                        break;
                }

                if ($.session.get('TreeLevel') == 0)
                    $('#btnRankMoreADM').show();
                else
                    $('#btnRankMoreADM').hide();
            });

            $('#btnRankMoreCase').click(function () {
                $('#btnRankMoreCase,#DRMPaidCase,#DRMReturnCase,#DRMPPCase').hide();

                switch ($.session.get('RankingType')) {
                    case 'Paid':
                        if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1) {
                            $('#DRMPaidOfficer,#btnRankMoreOfficer').show();//
                            $('#DRMPaidCase').hide();
                        }
                        else
                            $('#DRMPaidOfficer').show();
                        break;
                    case 'Returned':
                        if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1) {
                            $('#DRMReturnOfficer,#btnRankMoreOfficer').show();//
                            $('#DRMReturnCase').hide();
                        }
                        else
                            $('#DRMReturnOfficer').show();
                        break;
                    case 'Part Paid':
                        if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1) {
                            $('#DRMPPOfficer,#btnRankMoreOfficer').show();//
                            $('#DRMPPCase').hide();
                        }
                        else
                            $('#DRMPPOfficer').show();
                        break;
                }
                $('#lblRankMoreOfficer').html('');

            });
            //==========================================================================================================================================
            // WMA More
            //==========================================================================================================================================
            $('#btnTodayWMADate').click(function () {
                $('#txtFromDate,#txtToDate').val(TodayDateOnly);
                $.session.set('WMAMode', 'S');
                if (WMAURL != "")
                    GetWMADetatil(WMAURL, WMAType);
                $('#AWMACloseDate').trigger('click');
                $('#AWMADate').attr('data-original-title', 'Today');
            });

            $('#AWMAClose').click(function () {
                if ($.session.get('WMAMode') == 'S' || $.session.get('WMAMode') == undefined) {
                    switch ($.session.get('TreeLevel')) {
                        case '0':
                        case '1':
                            GetWMADetatil(WMAMGR_URL, 'Manager');
                            break;
                        case '2':
                            GetWMADetatil(WMAADM_URL, 'ADM');
                            break;
                        case '3':
                            GetWMADetatil(WMAOFF_URL, 'Officer');
                            break;
                    }
                }
            });

            $('#AWMATab').click(function () {
                $('#ACADate').hide();
                $('#AWMADate').show();
            });

            $('#btnWMAOfficerBack').click(function () {
                $('#lblSearchType,#lblOfficerDisplay').html('');
                showhidetabs('tblWMAOfficers');
                $('#btnWMAOfficerBack').hide();
                $('#lblWMAManager').show();
                GetWMADetatil(WMAOFF_URL, 'Officer');

                if ($.session.get('TreeLevel') == 0)
                    $('#btnWMAADMBack').show();
                else
                    $('#btnWMAADMBack').hide();
            });

            $('#btnWMAADMBack').click(function () {
                $('#lblSearchType,#lblOfficerDisplay,#lblWMAADM').html('');
                showhidetabs('tblWMAADM');
                GetWMADetatil(WMAADM_URL, 'ADM');
                $('#tblWMAOfficers,#btnWMAADMBack').hide();
            });

            $('#AMoreWMA').click(function () {
                $('#lblWMAMoreOfficerName,#lblWMAMoreOfficerCaseName').html('');//#lblWMAMoreManagerName,
                $('#DWMAMoreManager,#DWMAMoreADM,#DWMAMoreOfficer,#DWMAMoreOfficerCaseCount,#DWMAMoreOfficerCase').hide();
                $('#btnWMAMoreManager,#btnWMAMoreADM,#btnWMAMoreOfficer,#btnWMAMoreOfficerCase').hide();
                switch ($.session.get('TreeLevel')) {
                    case '0':
                        $('#DWMAMoreManager').show();
                        break;
                    case '1':
                        $('#DWMAMoreADM').show();
                        break;
                    case '2':
                    case '3':
                        $('#DWMAMoreOfficer').show();
                        break;
                }
            });

            $('#btnWMAMoreManager').click(function () {
                $('#lblWMAMoreManagerName').html('');
                $('#btnWMAMoreManager,#DWMAMoreADM,#DWMAMoreOfficer').hide();
                $('#DWMAMoreManager').show();
            });

            $('#btnWMAMoreADM').click(function () {
                $('#lblWMAMoreADMName').html('');
                $('#btnWMAMoreADM,#DWMAMoreOfficer').hide();
                $('#DWMAMoreADM').show();
                if ($.session.get('TreeLevel') == 0)
                    $('#btnWMAMoreManager').show();
                else
                    $('#btnWMAMoreManager').hide();
            });

            $('#btnWMAMoreOfficer').click(function () {
                $('#lblWMAMoreOfficerName').html('');
                $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer,#btnWMAMoreManager').hide();
                if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1)
                    $('#DWMAMoreOfficer,#btnWMAMoreADM').show();
                else
                    $('#DWMAMoreOfficer').show();
            });

            $('#btnWMAMoreOfficerCase').click(function () {
                $('#lblWMAMoreOfficerCaseName').html('');
                $('#DWMAMoreOfficerCase,#btnWMAMoreOfficerCase,#btnWMAMoreOfficer').hide();
                if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1 || $.session.get('TreeLevel') == 2 || $.session.get('TreeLevel') == 3)
                    $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer').show();
                else
                    $('#DWMAMoreOfficerCaseCount').show();
            });

            $('#btnWMADateOk').click(function () {
                var l_WMADateOk = $('#txtFromDate').val();
                l_WMADateOk = l_WMADateOk.replace(/[^0-9\/]/g, '');
                if (l_WMADateOk.length == 10) {
                    if (l_WMADateOk == TodayDateOnly) {
                        $('#btnTodayWMADate').trigger('click');
                    }
                    else {
                        $('#AWMADate').attr('data-original-title', l_WMADateOk);
                        $.session.set('WMAMode', 'N');
                        if ($.session.get('TreeLevel') == 3)
                            FillWMA_Normal(0, l_WMADateOk, l_WMADateOk, $.session.get('ManagerID'));
                        else
                            FillWMA_Normal($.session.get('ManagerID'), l_WMADateOk, l_WMADateOk, 0);

                        $('#AWMACloseDate').trigger('click');
                    }
                }
            });
            //==========================================================================================================================================

            $('#btnCADateOk').click(function () {
                var l_CADateOk = $('#txtFromDateCA').val();
                l_CADateOk = l_CADateOk.replace(/[^0-9\/]/g, '');
                if (l_CADateOk.length == 10) {
                    if (l_CADateOk == TodayDateOnly) {
                        $('#btnCAToday').trigger('click');
                    }
                    else {
                        $('#ACADate').attr('data-original-title', l_CADateOk);
                        $.session.set('CaseActionMode', 'N');
                        if ($.session.get('TreeLevel') == 3)
                            GetCaseActions_Normal(0, l_CADateOk, $.session.get('ManagerID'));
                        else
                            GetCaseActions_Normal($.session.get('ManagerID'), l_CADateOk, 0);

                        $('#btnCACloseDate').trigger('click');
                    }
                }
            });

            $('#btnCAToday').click(function () {
                $('#txtFromDateCA,#txtToDateCA').val(TodayDateOnly);
                $.session.set('CaseActionMode', 'S');
                if (CaseActionURL != "") {
                    GetActionsHub(CaseActionURL);
                }
                $('#ACADate').attr('data-original-title', 'Today');
                $('#btnCACloseDate').trigger('click');
            });

            $('#ACaseActionClose').click(function () {
                if ($.session.get('CaseActionMode') == 'S' || $.session.get('CaseActionMode') == undefined) {
                    GetActionsHub(CaseActionURL);
                    CA_TYPE = "";
                    if ($.session.get('CompanyID') == 1 && $.session.get('TreeLevel') == 3) {
                        $('#paid,#partpaid,#returned,#leftletter,#revisit,#deviated,#surrenderdateagreed,#bailed,#arrested,#clamped').hide();
                        switch ($.session.get('CaseNotesReturnValue')) {
                            case '1':
                                $('#paid').show().attr('class', 'modal fade in');
                                break;
                            case '2':
                                $('#partpaid').show().attr('class', 'modal fade in');
                                break;
                            case '4':
                                $('#returned').show().attr('class', 'modal fade in');
                                break;
                            case '5':
                                $('#leftletter').show().attr('class', 'modal fade in');
                                break;
                            case '3':
                                if ($.session.get('OfficerRole') == 9)
                                    $('#bailed').show().attr('class', 'modal fade in');
                                else
                                    $('#revisit').show().attr('class', 'modal fade in');
                                break;
                            case '7':
                                $('#deviated').show().attr('class', 'modal fade in');
                                break;
                            case '8':
                                $('#arrested').show().attr('class', 'modal fade in');
                                break;
                            case '9':
                                $('#surrenderdateagreed').show().attr('class', 'modal fade in');
                                break;
                        }
                    }
                    $('#divCaseActionModal').attr('class', 'modal fade in').hide();
                    $('#Notesformanager,#HCaseNumber,#btnNotesBack').hide();
                }
            });

            $('#btnLocationDateOk').click(function () {
                clearattributes_LocHBMap();
                clearattributes_LocHBMapExpand();
                var l_LocationDateOk = $('#txtFromDateLocation').val();
                l_LocationDateOk = l_LocationDateOk.replace(/[^0-9\/]/g, '');
                if (l_LocationDateOk.length == 10) {
                    if (l_LocationDateOk == TodayDateOnly) {
                        $('#btnLocationToday').trigger('click');
                    }
                    else {
                        $('#ALocationDate').attr('data-original-title', l_LocationDateOk);
                        $.session.set('LocationMode', 'N');
                        if ($.session.get('TreeLevel') == 3)
                            LastKnownLocation_Normal(0, l_LocationDateOk, $.session.get('ManagerID'), 0);
                        else
                            LastKnownLocation_Normal($.session.get('ManagerID'), l_LocationDateOk, 0, 0);

                        $('#btnLocationDateClose').trigger('click');
                    }
                }
            });

            $('#ACATab').click(function () {
                $('#ACADate').show();
                $('#AWMADate').hide();
            });

            $('#AReturned').click(function () {
                $.session.set('RankingType', 'Returned');
            });

            $('#APaid').click(function () {
                $.session.set('RankingType', 'Paid');
            });

            $('#APartPaid').click(function () {
                $.session.set('RankingType', 'Part Paid');
            });

            $('#imgRefreshLocation').click(function () {
                $.session.set('Map', 'LL');
                if ($.session.get('LocationMode') == 'S') {
                    if ($('#lblSelectedOfficerID').html() != '0') {
                        FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), $('#lblSelectedOfficerID').html());
                    }
                    else if ($('#lblSelectedGroupID').html() != '0') {
                        FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', $('#lblSelectedGroupID').html());
                    }
                    else if ($('#lblSelectedManagerID').html() != '0') {
                        FillLastKnownLocation($('#lblSelectedManagerID').html(), $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', '0');
                    }
                    else {
                        FillLastKnownLocation(OfficerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val());
                    }
                }
                else {
                    var l_LocationDateOk = $('#txtFromDateLocation').val();

                    if ($.session.get('TreeLevel') == 3)
                        LastKnownLocation_Normal(0, l_LocationDateOk, $.session.get('ManagerID'), 0);
                    else
                        LastKnownLocation_Normal($.session.get('ManagerID'), l_LocationDateOk, 0, 0);
                }

            });

            $('.tree-toggle').click(function () {
                $(this).parent().children('ul.tree').toggle(200);
            });

            $('#lblTeamAdmin').click(function () {
                window.location.href = RedirectUrl;
            });

            $('#AMoreWorkingHours').click(function () {
                GetWorkingHoursMore($.session.get('OfficerID'), 0);
            });

            $('#btnWorkingHoursBack').click(function () {
                $('#DivWorkingHoursOfficersList,#btnWorkingHoursBack').hide();
                $('#DivWorkingHoursManagersList').show();
            });

            //==========================================================================================================================================

            $('#txtSearchOfficer').keyup(function () {
                //***********************************
                var isAtleastOneMatch = false;
                var isManagerMatch = false;
                $('#ulOfficers').find('li').css('display', 'block');
                if ($('#txtSearchOfficer').val().length > 0) {
                    $('#ulOfficers').find('li').css('display', 'none');

                    $('#ulOfficers li.Officer').each(function (key, value) {
                        if ($(this).text().toLowerCase().indexOf($('#txtSearchOfficer').val().toLowerCase()) >= 0) {
                            $(this).css('display', 'block');
                            isAtleastOneMatch = true;
                            //0- Zone;1 - Mgr; 2 - ADM; 3- Officer
                            try {
                                $adm = $(this).parent().parent();
                                $adm.css('display', 'block');
                                $adm.find('a').find('i').attr('class', 'glyphicon glyphicon-minus-sign');

                                $mgr = $adm.parent().parent();
                                $mgr.css('display', 'block');
                                $mgr.find('a').find('i').attr('class', 'glyphicon glyphicon-minus-sign');

                                $zone = $mgr.parent().parent();
                                $zone.css('display', 'block');
                            } catch (e) {

                            }
                        }
                    });

                    $('#ulOfficers li.ADM').each(function (key, value) {
                        if ($(this).text().toLowerCase().indexOf($('#txtSearchOfficer').val().toLowerCase()) >= 0) {
                            $(this).css('display', 'block');
                            isAtleastOneMatch = true;
                            try {
                                $mgr = $(this).parent().parent();
                                $mgr.css('display', 'block');
                                $mgr.find('a').find('i').attr('class', 'glyphicon glyphicon-minus-sign');

                                $zone = $mgr.parent().parent();
                                $zone.css('display', 'block');
                            } catch (e) {

                            }
                        }
                    });

                    $('#ulOfficers li.Manager').each(function (key, value) {
                        if ($(this).text().toLowerCase().indexOf($('#txtSearchOfficer').val().toLowerCase()) >= 0) {
                            isAtleastOneMatch = true;
                            $(this).css('display', 'block');
                            try {
                                $zone = $mgr.parent().parent();
                                $zone.css('display', 'block');
                            } catch (e) {

                            }
                        }
                    });
                }
                else {
                    isAtleastOneMatch = true;
                }

                if (!isAtleastOneMatch) {
                    $('#lblSearchOfficerInfo').html('No Match found');
                }
                else {
                    $('#lblSearchOfficerInfo').html('');
                    if ($('#txtSearchOfficer').val().length == 0) {
                        $('#ulOfficers li.Officer').each(function (key, value) {
                            $adm = $(this).parent().parent();
                            $adm.css('display', 'none');
                            $adm.find('a').find('i').attr('class', 'glyphicon glyphicon-plus-sign');

                            $mgr = $adm.parent().parent();
                            $mgr.find('a').find('i').attr('class', 'glyphicon glyphicon-plus-sign');
                        });
                    }
                }
            });

            GetFlashNews();

            $('#selCompanyList').change(function () {
                LoginDetails4SuperUser($('#selCompanyList').val(), $.session.get('OfficerName'));
            });

            $('#pClientName').html($.session.get('ClientName'));

            // getting month target for manager when login
            if ($.session.get('tgVal') != '') {
                $('#lblMonthTotalTarget').html($.session.get('tgVal'));
                $('#txtMonthTotalTarget').html($.session.get('tgVal'));
            }

            // TODO: Flash news
            if ($.session.get('FlashNews') == undefined || $.session.get('FlashNews') == 'Off') {
                $('#DMarquee,#btnFlashOff').hide();
                $('#btnFlashOn').show();
            }
            else {
                $('#btnFlashOn').hide();
                $('#DMarquee,#btnFlashOff').show();
            }

            //Refresh time
            if ($.session.get('aref') != undefined) {
                $('#txtRefreshTime').val($.session.get('aref'));
            }

            $(document).on({
                mouseenter: function () {
                    $(this).parent().find('.lastLeafMouse').each(function() {
                        $(this).removeClass('lastLeafMouse');
                    });
                    var l_officerid = $(this).data('id');
                    var l_selectedOfficer = $(this);
                    var l_anchorSelected = $(this).find("a");
                    l_anchorSelected.addClass('lastLeafMouse');
                    //l_anchorSelected.popover('show');

                    //****************Setting up data content for the popover *******************************
                    OfficerOnFocus = $.ajax({                        
                        url: ServiceURL + 'api/v1/officers/' + l_officerid + '/OfficerAvailability',
                        headers: {
                            "Authorization": $.session.get('TokenAuthorization'),
                            'Content-Type': 'application/json'
                        },
                        type: 'GET',
                        dataType: DataType,
                        beforeSend: function () {                            
                        },
                        success: function (data) {
                            $.each(data, function (key, value1) {
                                datacontent = '<table width="100%" class="display-status-table"><tr><td>' +
                            '<div class="block" style="margin:2px!important;padding:2px!important">' +
                            '<p class="block-heading">Availability</p><table class="table">' +
                            '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                            '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                            '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                            '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                            '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                            '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                            '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                            '</tbody></table></div></td></tr></table>';

                                datacontentforDisplay = '<table class="table table-striped table-condensed ">' +
                            '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                            '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                            '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                            '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                            '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                            '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                            '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                            '</tbody></table>';
                            });

                            l_anchorSelected.attr('data-placement', 'right');
                            l_anchorSelected.attr('data-content', datacontent);
                            l_anchorSelected.attr('panelcontent', datacontentforDisplay);                            
                        },
                        complete: function () {
                            if (l_anchorSelected.hasClass('lastLeafMouse'))
                            {
                                l_anchorSelected.popover('show');
                            }                            
                        },
                        error: function (xhr, textStatus, errorThrown) {
                        }
                    });
                },
                mouseleave: function () {
                    $(this).find('a').removeClass('lastLeafMouse');
                    $(this).find('a').popover('hide');
                    if(OfficerOnFocus)
                    {
                        OfficerOnFocus.abort();
                    }
                }
            }, '.OfficerOnFocus');

            //================================
            //TODO : Client On Connected

            counter.client.ClientOnConnected = function (message) {
                showconsole(message);
                showconsole('Call On Connected - getmanagertree');
                counter.server.getmanagertree(OfficerID);
                counter.server.getofficerstree();
                updateAllPanel();
            }

            //=======================================================================================================================
            //TODO: Officer Tree
            counter.client.getmanagertree = function (message) {
                showconsole("- get manager tree -" );
                var SelectedOfficerID = 0;
                var multiselect = true;
                message = $.parseJSON(message);
                if (!(message == "[]" || message == null || message == undefined || message == '[{"message":"NoRecords"}]')) {

                    var TreeString = $("#ulOfficers");
                    $.each(message, function (key, value) {
                        if (key == 0) {
                            $.session.set('TreeLevel', value.TreeLevel);
                            if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1)
                                $('#lblTeamAdmin').show();
                            else
                                $('#lblTeamAdmin').hide();
                        }
                        switch (value.TreeLevel) {
                            case '0':
                                if ($('#ZoneManager' + value.ZoneManagerID).length == 0) {
                                    if (multiselect) {
                                        $('#pListName').html('Zone Manager list');
                                        $('#tbodyOfficerList').append($('<tr>')
                                           .append($('<td>').append($('<a class="targetNew">').attr('id', 'AOfficerTarget')
                                               .attr('data-toggle', 'modal').html(value.ZoneManagerName + ' ' + value.ZoneManagerID))
                                               .append($('<label>').attr('id', 'lblOfficerID' + key).html(value.ZoneManagerID).hide()))
                                           .append($('<td>').append($('<input>').attr('type', 'text').attr('id', 'txtOfficerTarget' + key).attr('style', 'color:black'))))

                                        $('#txtOfficerTarget' + key).keydown(function (event) {
                                            if (event.shiftKey == true) {
                                                $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                                return false;
                                            }
                                            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                                                return true;
                                            }
                                            else {
                                                $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                                return false;
                                            }
                                        });

                                        $('#txtOfficerTarget' + key).keyup(function (event) {
                                            var TTarget = 0;
                                            $('#tbodyOfficerList tr').each(function (key, value) {
                                                if ($(this).find('input').val() > 0) {
                                                    TTarget += parseInt($(this).find('input').val());
                                                }
                                            });
                                            $('#lblTotalTarget').html(TTarget);

                                            if ($('#lblTotalTarget').html() != $('#txtMonthTotalTarget').val()) {
                                                $('#btnSubmitOfficerTotal').attr('disabled', 'disabled');
                                                $('#lblInfo').html('Total Target should match.');
                                                $("#lblInfo").show().delay(3000).fadeOut();
                                            }
                                            else {
                                                $('#btnSubmitOfficerTotal').removeAttr('disabled');
                                            }
                                        });
                                    }
                                    $("#ulOfficers").append('<li class="Zone"><a href="#" id="ZoneManager' + value.ZoneManagerID + '">' +
                                                           '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.ZoneManagerName + '</a>' +
                                                           '<ul id="ULMan' + value.ZoneManagerID + '"></ul></li>');

                                    if (key == 0) {
                                        if ($.session.get('CaseActionMode') == 'S' || $.session.get('CaseActionMode') == undefined)
                                            FillCaseActions($.session.get('OfficerID'), $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
                                        else
                                            GetCaseActions_Normal($.session.get('OfficerID'), $('#txtFromDateCA').val(), 0);

                                        if ($.session.get('WMAMode') == 'S' || $.session.get('WMAMode') == undefined)
                                            FillWMA($.session.get('OfficerID'), $('#txtFromDate').val(), $('#txtFromDate').val());
                                        else
                                            FillWMA_Normal($.session.get('OfficerID'), $('#txtFromDate').val(), $('#txtFromDate').val(), 0);

                                        GetStats($.session.get('OfficerID'), 0);
                                        if ($.session.get('LocationMode') == 'S' || $.session.get('LocationMode') == undefined)
                                            FillLastKnownLocation($.session.get('OfficerID'), $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val());
                                        else
                                            LastKnownLocation_Normal($.session.get('OfficerID'), $('#txtFromDateLocation').val(), 0, 0);

                                        if ($.session.get('RankMode') == 'S' || $.session.get('RankMode') == undefined) {
                                            FillRankings($.session.get('OfficerID'), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                            FillRankings($.session.get('OfficerID'), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                            FillRankings($.session.get('OfficerID'), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        }
                                        else {
                                            FillRankings_Normal($.session.get('OfficerID'), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                            FillRankings_Normal($.session.get('OfficerID'), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                            FillRankings_Normal($.session.get('OfficerID'), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        }
                                        GetWorkingHours($.session.get('OfficerID'));

                                        $.session.set('Availability', 0);
                                        ViewZoneMgr = value.ZoneManagerID;
                                        $('.lastLeaf').removeClass('lastLeaf');
                                        $('#ZoneManager' + value.ZoneManagerID).addClass('lastLeaf');
                                        $('#ZoneManager' + value.ZoneManagerID).find(' > i').addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');
                                        $.session.set('ManagerID', value.ZoneManagerID);
                                        $.session.set('MgrName', value.ZoneManagerName);
                                    }
                                    $('#ZoneManager' + value.ZoneManagerID).click(function () {
                                        clearmaparrays();
                                        $('.lastLeaf').removeClass('lastLeaf');
                                        $(this).addClass('lastLeaf');
                                        LinkForManager = 1;
                                        $.session.set('TreeLevel', value.TreeLevel);
                                        $.session.set('ManagerID', value.ZoneManagerID);
                                        $.session.set('MgrName', value.ZoneManagerName);
                                        $.session.set('Availability', 0);
                                        $.session.set('Map', 'LL');
                                        pLatitude = 0;
                                        pLongitude = 0;
                                        RankURL = [];
                                        RankActionText = [];
                                        RankViewType = [];
                                        officerLLArray = [];
                                        $('#lblSelectedOfficerID,#lblSelectedGroupID').html('0');
                                        $('#lblSelectedManagerID').html(value.ZoneManagerID);
                                        $('#lblCAOfficerName').html('');
                                        //TODO:  Manager 0-Zone 1-Manager 2-ADM 3-Officer
                                        if ($.session.get('CaseActionMode') == 'S' || $.session.get('CaseActionMode') == undefined)
                                            FillCaseActions(value.ZoneManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
                                        else
                                            GetCaseActions_Normal(value.ZoneManagerID, $('#txtFromDateCA').val(), 0);

                                        if ($.session.get('WMAMode') == 'S' || $.session.get('WMAMode') == undefined)
                                            FillWMA(value.ZoneManagerID, $('#txtFromDate').val(), $('#txtFromDate').val());
                                        else
                                            FillWMA_Normal(value.ZoneManagerID, $('#txtFromDate').val(), $('#txtFromDate').val(), 0);

                                        GetStats(value.ZoneManagerID, 0);

                                        if ($.session.get('LocationMode') == 'S' || $.session.get('LocationMode') == undefined)
                                            FillLastKnownLocation(value.ZoneManagerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val());
                                        else
                                            LastKnownLocation_Normal(value.ZoneManagerID, $('#txtFromDateLocation').val(), 0, 0);

                                        if ($.session.get('RankMode') == 'S' || $.session.get('RankMode') == undefined) {
                                            FillRankings(value.ZoneManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                            FillRankings(value.ZoneManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                            FillRankings(value.ZoneManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        }
                                        else {
                                            FillRankings_Normal(value.ZoneManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                            FillRankings_Normal(value.ZoneManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                            FillRankings_Normal(value.ZoneManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        }
                                        GetWorkingHours(value.ZoneManagerID);

                                        $('#AShowAll,#AShowEnlarge,#DTimeline,#DAvailability,#tblWMASearchType').hide();
                                        $('#DivRanking,#showPanel2,#imgRefreshLocation,#tblWMA').show();
                                        $('#AHeartBeat,#AHeartBeatEnlarge,#ALocationwithHB,#ALocationwithHBEnlarge').hide();
                                        $('#pMap').html('Last known location');
                                    });
                                }
                            case '1':
                                if ($('#Manager' + value.ManagerID).length == 0) {
                                    if (multiselect) {
                                        $('#pListName').html('Manager list');
                                        $('#tbodyOfficerList').append($('<tr>')
                                           .append($('<td>').append($('<a class="targetNew">').attr('id', 'AOfficerTarget')
                                               .attr('data-toggle', 'modal').html(value.ManagerName + ' ' + value.ManagerID))
                                               .append($('<label>').attr('id', 'lblOfficerID' + key).html(value.ManagerID).hide()))
                                           .append($('<td>').append($('<input>').attr('type', 'text').attr('id', 'txtOfficerTarget' + key).attr('style', 'color:black'))))

                                        $('#txtOfficerTarget' + key).keydown(function (event) {
                                            if (event.shiftKey == true) {
                                                $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                                return false;
                                            }
                                            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                                                return true;
                                            }
                                            else {
                                                $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                                return false;
                                            }
                                        });

                                        $('#txtOfficerTarget' + key).keyup(function (event) {
                                            var TTarget = 0;
                                            $('#tbodyOfficerList tr').each(function (key, value) {
                                                if ($(this).find('input').val() > 0) {
                                                    TTarget += parseInt($(this).find('input').val());
                                                }
                                            });
                                            $('#lblTotalTarget').html(TTarget);

                                            if ($('#lblTotalTarget').html() != $('#txtMonthTotalTarget').val()) {
                                                $('#btnSubmitOfficerTotal').attr('disabled', 'disabled');
                                                $('#lblInfo').html('Total Target should match.');
                                                $("#lblInfo").show().delay(3000).fadeOut();
                                            }
                                            else {
                                                $('#btnSubmitOfficerTotal').removeAttr('disabled');
                                            }
                                        });
                                    }
                                    if (value.TreeLevel == 1) {
                                        TreeString.append('<li class="Manager"><a href="#" id="Manager' + value.ManagerID + '">' +
                                                   '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.ManagerName + '</a>' +
                                                   '<ul id="ULADM' + value.ManagerID + '"></ul></li>');
                                        if ($.session.get('CaseActionMode') == 'S' || $.session.get('CaseActionMode') == undefined)
                                            FillCaseActions(value.ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                                        else
                                            GetCaseActions_Normal(value.ManagerID, $('#txtFromDateCA').val(), 0);

                                        FillWMAADM(value.ManagerID, $('#txtFromDate').val(), $('#txtFromDate').val());
                                        GetStats(value.ManagerID, 0);

                                        if ($.session.get('LocationMode') == 'S' || $.session.get('LocationMode') == undefined)
                                            FillLastKnownLocation(value.ManagerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', '0');
                                        else
                                            LastKnownLocation_Normal(value.ManagerID, $('#txtFromDateLocation').val(), 0, 0);

                                        if ($.session.get('RankMode') == 'S' || $.session.get('RankMode') == undefined) {
                                            FillRankingsForADM(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                            FillRankingsForADM(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                            FillRankingsForADM(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        }
                                        else {
                                            FillRankingsForADM_Normal(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                            FillRankingsForADM_Normal(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                            FillRankingsForADM_Normal(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        }
                                        GetWorkingHours(value.ManagerID);
                                        $.session.set('MgrName', value.ManagerName);
                                    }
                                    else {
                                        $("#ULMan" + value.ZoneManagerID).append('<li class="Manager"><a href="#" id="Manager' + value.ManagerID + '">' +
                                                       '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.ManagerName + '</a>' +
                                                       '<ul id="ULADM' + value.ManagerID + '"></ul></li>');
                                    }
                                    if (ViewZoneMgr == value.ZoneManagerID) {
                                        $("#ULMan" + value.ZoneManagerID).find(' > li').attr('style', 'display:block');
                                    }
                                    $('#Manager' + value.ManagerID).click(function () {
                                        clearmaparrays();
                                        $('.lastLeaf').removeClass('lastLeaf');
                                        $(this).addClass('lastLeaf');
                                        LinkForManager = 1;
                                        $.session.set('ManagerID', value.ManagerID);
                                        $.session.set('MgrName', value.ManagerName);
                                        $.session.set('Availability', 0);
                                        $.session.set('Map', 'LL');
                                        $.session.set('TreeLevel', 1);
                                        pLatitude = 0;
                                        pLongitude = 0;
                                        RankURL = [];
                                        RankActionText = [];
                                        RankViewType = [];
                                        $('#lblWMAManager').html(value.ManagerName);
                                        $('#lblWMAMoreManagerName,#lblRankMgrName,#lblRankMoreManager').html(' - ' + value.ManagerName);
                                        $('#lblSelectedOfficerID,#lblSelectedGroupID').html('0');
                                        $('#lblSelectedManagerID').html(value.ManagerID);
                                        $('#lblCAOfficerName').html('');

                                        if ($.session.get('CaseActionMode') == 'S' || $.session.get('CaseActionMode') == undefined)
                                            FillCaseActions(value.ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                                        else
                                            GetCaseActions_Normal(value.ManagerID, $('#txtFromDateCA').val(), 0);

                                        if ($.session.get('WMAMode') == 'S' || $.session.get('WMAMode') == undefined)
                                            FillWMAADM(value.ManagerID, $('#txtFromDate').val(), $('#txtFromDate').val());
                                        else {
                                            FillWMA_Normal(value.ManagerID, $('#txtFromDate').val(), $('#txtFromDate').val(), 0);
                                        }


                                        GetStats(value.ManagerID, 0);

                                        if ($.session.get('LocationMode') == 'S' || $.session.get('LocationMode') == undefined)
                                            FillLastKnownLocation(value.ManagerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', '0');
                                        else
                                            LastKnownLocation_Normal(value.ManagerID, $('#txtFromDateLocation').val(), 0, 0);

                                        if ($.session.get('RankMode') == 'S' || $.session.get('RankMode') == undefined) {
                                            FillRankingsForADM(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                            FillRankingsForADM(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                            FillRankingsForADM(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        }
                                        else {
                                            FillRankings_Normal(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                            FillRankings_Normal(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                            FillRankings_Normal(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        }

                                        $('#AShowAll,#AShowEnlarge,#tblWMASearchType,#DTimeline,#DAvailability').hide();
                                        $('#DivRanking,#showPanel2,#imgRefreshLocation,#tblWMA').show();
                                        $('#AHeartBeat,#AHeartBeatEnlarge,#ALocationwithHB,#ALocationwithHBEnlarge').hide();
                                        $('#pMap').html('Last known location');
                                    });
                                }
                            case '2':
                                if ($('#ADM' + value.ADMID).length == 0) {
                                    if (value.TreeLevel == 2) {
                                        TreeString.append('<li class="ADM"><a href="#" id="ADM' + value.ADMID + '">' +
                                                   '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.ADMName + '</a>' +
                                                   '<ul id="UL' + value.ADMID + '"></ul></li>');

                                        if ($.session.get('CaseActionMode') == 'S' || $.session.get('CaseActionMode') == undefined)
                                            FillCaseActions(value.ADMID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.ADMID);
                                        else
                                            GetCaseActions_Normal(value.ADMID, $('#txtFromDateCA').val(), 0);

                                        if ($.session.get('WMAMode') == 'S' || $.session.get('WMAMode') == undefined)
                                            FillWMAOfficer(value.ADMID, $('#txtFromDate').val(), $('#txtFromDate').val());
                                        else
                                            FillWMA_Normal(value.ADMID, $('#txtFromDate').val(), $('#txtFromDate').val(), 0);

                                        GetStats(value.ADMID, 0);

                                        if ($.session.get('LocationMode') == 'S' || $.session.get('LocationMode') == undefined)
                                            FillLastKnownLocation(value.ADMID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', value.GroupID);
                                        else
                                            LastKnownLocation_Normal(value.ADMID, $('#txtFromDateLocation').val(), 0, 0);

                                        if ($.session.get('RankMode') == 'S' || $.session.get('RankMode') == undefined) {
                                            FillRankingsForOfficer(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                            FillRankingsForOfficer(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                            FillRankingsForOfficer(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        }
                                        else {
                                            FillRankings_Normal(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                            FillRankings_Normal(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                            FillRankings_Normal(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        }
                                    }
                                    else {
                                        $("#ULADM" + value.ManagerID).append('<li class="ADM"><a href="#" id="ADM' + value.ADMID + '">' +
                                                       '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.ADMName + '</a>' +
                                                       '<ul id="UL' + value.ADMID + '"></ul></li>');
                                    }
                                    $('#ADM' + value.ADMID).click(function () {

                                        clearmaparrays();
                                        LinkForManager = 1;
                                        $('#lblSelectedOfficerID').html('0');
                                        $('#lblSelectedGroupID').html(value.ADMID);
                                        $('#lblSelectedManagerID').html('0');
                                        $('#lblCAOfficerName').html('');
                                        $('#lblWMAADM,#lblWMAMoreADMName,#lblRankADMName,#lblRankMoreADM').html(' - ' + value.ADMName);
                                        $('.lastLeaf').removeClass('lastLeaf');
                                        $(this).addClass('lastLeaf');
                                        $.session.set('ManagerID', value.ADMID);
                                        $.session.set('MgrName', value.ADMName);
                                        $.session.set('TreeLevel', 2);
                                        $.session.set('Availability', 0);
                                        $.session.set('Map', 'LL');
                                        pLatitude = 0;
                                        pLongitude = 0;
                                        RankURL = [];
                                        RankActionText = [];
                                        RankViewType = [];
                                        if ($.session.get('CaseActionMode') == 'S' || $.session.get('CaseActionMode') == undefined)
                                            FillCaseActions(value.ADMID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.ADMID);
                                        else
                                            GetCaseActions_Normal(value.ADMID, $('#txtFromDateCA').val(), 0);

                                        if ($.session.get('WMAMode') == 'S' || $.session.get('WMAMode') == undefined)
                                            FillWMAOfficer(value.ADMID, $('#txtFromDate').val(), $('#txtFromDate').val());
                                        else
                                            FillWMA_Normal(value.ADMID, $('#txtFromDate').val(), $('#txtFromDate').val(), 0);

                                        GetStats(value.ADMID, 0);

                                        if ($.session.get('LocationMode') == 'S' || $.session.get('LocationMode') == undefined)
                                            FillLastKnownLocation(value.ADMID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', value.GroupID);
                                        else
                                            LastKnownLocation_Normal(value.ADMID, $('#txtFromDateLocation').val(), 0, 0);

                                        if ($.session.get('RankMode') == 'S' || $.session.get('RankMode') == undefined) {
                                            FillRankingsForOfficer(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                            FillRankingsForOfficer(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                            FillRankingsForOfficer(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        }
                                        else {
                                            FillRankings_Normal(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                            FillRankings_Normal(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                            FillRankings_Normal(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        }

                                        $('#AShowAll,#AShowEnlarge,#DTimeline,#DAvailability,#tblWMASearchType').hide();
                                        $('#DivRanking,#showPanel2,#imgRefreshLocation,#tblWMA').show();
                                        $('#AHeartBeat,#AHeartBeatEnlarge,#ALocationwithHB,#ALocationwithHBEnlarge').hide();
                                        $('#pMap').html('Last known location');
                                    });
                                    if (multiselect)
                                        $('#selOfficer').append($('<optgroup>').attr('label', value.ManagerName).attr('id', 'og' + value.ManagerID))
                                }
                        }

                        if (multiselect) {
                            $('#og' + value.ManagerID)
                            .append($('<option>').attr('value', $.trim(value.OfficerId)).html(value.OfficerName + ' - ' + value.OfficerId))
                            $('#lblCurrentMonth,#lblMonthTotalTarget').hide();
                            $('#selCurrentMonth,#txtMonthTotalTarget').show();
                        }
                        $('#UL' + value.ADMID)
                              .append('<li class="Officer OfficerOnFocus" id="LI' + value.OfficerId + '" data-id="' + value.OfficerId + '">' +
                                      '<a style="cursor:pointer;"  id="ATreeOfficer' + value.OfficerId + '" rel="popover" data-original-title=""  class="a1" data-html="true" data-popover="true">' +
                                      '<i class="glyphicon glyphicon-circle-arrow-right"></i> ' + value.OfficerName +
                                      '<span class="pull-right">' + value.OfficerId + '</span>' +
                                      '<span style="padding-left:20px"><img src="images/circle-active.png" id="img' + value.OfficerId + '"></span></a> </li>');                        
                        /*
                        $('#LI' + value.OfficerId).mouseenter(function () {
                            var l_officerid = $(this).attr('id');
                            $('#ATreeOfficer' + $.trim(value.OfficerId)).attr('class', 'lastLeafMouse');
                            $('#ATreeOfficer' + $.trim(value.OfficerId)).popover('show');
                            
                            //****************Setting up data content for the popover *******************************
                            if (OfficerOnFocus>0)
                            {
                                clearTimeout(OfficerOnFocus);
                            }
                            OfficerOnFocus = setTimeout(function () {
                                $.ajax({
                                    url: ServiceURL + 'api/v1/officers/' + value.OfficerId + '/OfficerAvailability',
                                    headers: {
                                        "Authorization": $.session.get('TokenAuthorization'),
                                        'Content-Type': 'application/json'
                                    },
                                    type: 'GET',
                                    dataType: DataType,
                                    beforeSend: function () {
                                    },
                                    success: function (data) {
                                        OfficerOnFocus = 0                                        
                                        $.each(data, function (key, value1) {
                                            datacontent = '<table width="100%" class="display-status-table"><tr><td>' +
                                     '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                     '<p class="block-heading">Availability - ' + l_officerid + '</p><table class="table">' +
                                     '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                     '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '</tbody></table></div></td></tr></table>';

                                            datacontentforDisplay = '<table class="table table-striped table-condensed ">' +
                                     '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                     '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '</tbody></table>';
                                        });
                                    },
                                    complete: function () {
                                        $('#ATreeOfficer' + $.trim(value.OfficerId)).attr('data-placement', 'right');
                                        $('#ATreeOfficer' + $.trim(value.OfficerId)).removeAttr('data-content').attr('data-content', datacontent);
                                        $('#ATreeOfficer' + $.trim(value.OfficerId)).removeAttr('panelcontent').attr('panelcontent', datacontentforDisplay);
                                        OfficerOnFocus = 0;
                                    },
                                    error: function (xhr, textStatus, errorThrown) {
                                        OfficerOnFocus = 0;
                                    }
                                });
                            }, 100);
                                //****************Setting up data content for the popover *******************************                            
                        });

                        $('#LI' + value.OfficerId).mouseleave(function () {
                            //   OfficerOnFocus.abort();
                            $('#ATreeOfficer' + value.OfficerId).removeClass('lastLeafMouse');
                            $('#ATreeOfficer' + value.OfficerId).popover('hide');
                        });
                        */
                        if (SelectedOfficerID > 0) {
                            $('.activeListview').removeClass('activeListview');
                            $('#ATreeOfficer' + SelectedOfficerID).addClass('activeListview');
                        }
                        if (value.IsLogged != 0) {
                            $('#LI' + value.OfficerId).addClass('sidebar-nav-active');
                            $('#img' + value.OfficerId).show();
                        }
                        else {
                            $('#LI' + value.OfficerId).removeClass('sidebar-nav-active');
                            $('#img' + value.OfficerId).hide();
                        }

                        $('#ATreeOfficer' + value.OfficerId).click(function () {//.unbind()                            
                            clearmaparrays();
                            clearmarkers_HBMap();
                            clearmarkers_HBMapExpand();
                            clearmarkers_LLMap();
                            $.session.set('Map', 'LL');
                            LinkForManager = 2;
                            $('.lastLeaf').removeClass('lastLeaf');
                            $(this).addClass('lastLeaf');
                            //$('#ATreeOfficer' + value.OfficerId).mouseout(function () {
                            //    $(this).addClass('lastLeaf');
                            //});
                            //$('#ATreeOfficer' + value.OfficerId).mouseover(function () {
                            //    $('.lastLeaf').removeClass('lastLeaf');
                            //});
                            GetTimelineResult(value.OfficerId);
                            $('#lblSelectedOfficerID').html(value.OfficerId);
                            $('#lblSelectedGroupID,#lblSelectedManagerID').html('0');
                            $('#lblOfficerDisplay').html(value.OfficerName);

                            pLatitude = 0;
                            pLongitude = 0;
                            OffID = $(this).attr('id').substr(12);

                            $('#DivAvailability').empty();
                            $('#DivAvailability').append($('#ATreeOfficer' + value.OfficerId).attr('panelcontent'));

                            $.session.set('Map', 'LL');
                            $.session.set('ManagerID', value.OfficerId);
                            $.session.set('MgrName', value.OfficerName);
                            $.session.set('TreeLevel', 3);
                            $.session.set('Availability', 1);
                            $.session.set('TimelineOfficer', value.OfficerId);

                            if ($.session.get('CaseActionMode') == 'S' || $.session.get('CaseActionMode') == undefined)
                                FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), value.OfficerId);
                            else
                                GetCaseActions_Normal('0', $('#txtFromDateCA').val(), value.OfficerId);

                            FillWMAOfficer(value.ADMID, $('#txtFromDate').val(), $('#txtFromDate').val());
                            GetStats(value.OfficerId, 1);
                            if ($.session.get('LocationMode') == 'S' || $.session.get('LocationMode') == undefined)
                                FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), value.OfficerId);
                            else
                                LastKnownLocation_Normal(value.OfficerId, $('#txtFromDateLocation').val(), value.OfficerId, 0);

                            $('#DTimeline,#DAvailability,#tblWMA').show();
                            $('#AHeartBeat,#AHeartBeatEnlarge,#AShowAll,#AShowEnlarge,#ALocationwithHB,#ALocationwithHBEnlarge').show();
                            $('#DivRanking,#showPanel2,#imgRefreshLocation,#tblWMASearchType').hide();
                        });
                    });
                    if (multiselect) {
                        var divTarget = parseInt($('#lblMonthTotalTarget').html() / ($('#tbodyOfficerList tr').length));
                        var remTarget = parseInt($('#lblMonthTotalTarget').html() % ($('#tbodyOfficerList tr').length));

                        $('#tbodyOfficerList tr').each(function (key, value) {
                            if (key == ($('#tbodyOfficerList tr').length - 1))
                                $(this).find('input').val(divTarget + remTarget);
                            else
                                $(this).find('input').val(divTarget);
                        });

                        $('#lblTotalTarget').html($.session.get('tgVal'));

                        $('#tbodyOfficerList').append($('<tr>')
                            .append($('<td>').append($('<span>').attr('class', 'pull-right').html('Total')))
                            .append($('<td>').append($('<label>').attr('id', 'lblTotalTarget'))))
                        .append($('<tr>').append($('<td>'))
                            .append($('<td>').append($('<button>').attr('id', 'btnSubmitOfficerTotal').attr('class', 'btn-primary')
                                                                  .attr('onclick', 'SubmitOfficerTotal()').html('Submit'))))

                        $('#btnSubmitOfficerTotal').click(function () {
                            var TTargetInfo = '';
                            $('#tbodyOfficerList tr').each(function (key, value) {
                                if ($(this).find('label').html() != '' && $(this).find('label').html() != undefined && parseInt($(this).find('input').val()) > 0) {
                                    if (TTargetInfo != '') TTargetInfo += ',';
                                    TTargetInfo += $(this).find('label').html() + '|' + parseInt($(this).find('input').val());
                                }
                            });

                            // ****************************** submit functionality
                            Type = "GET";
                            var inputParams = "/UpdateOfficerTarget?TMonth=" + $('#selCurrentMonth').val() + "&TargetNo=" +
                                $('#txtMonthTotalTarget').val() + "&ManagerID=" + ManagerID + "&OfficerIDs=" + TTargetInfo;

                            Url = serviceUrl + inputParams;
                            DataType = "jsonp"; ProcessData = false;

                            $.ajax({
                                type: Type,
                                url: Url, // Location of the service
                                contentType: ContentType, // content type sent to server
                                dataType: DataType, //Expected data format from server       
                                processdata: ProcessData, //True or False      
                                async: true,
                                timeout: 20000,
                                beforeSend: function () { },
                                complete: function () { },
                                success: function (result) {//On Successfull service call  
                                    $('#lblTargetInfo').html('Target has been updated successfully.');
                                    $('#ATargetInfo').click();
                                },
                                error: function () {
                                } // When Service call fails
                            });

                        });
                    }
                    $('#AShowAll').click(function () {
                        $.session.set('Map', 'All');
                        var l_LocationDateOk = $('#txtFromDateLocation').val();
                        l_LocationDateOk = l_LocationDateOk.replace(/[^0-9\/]/g, '');
                        if (l_LocationDateOk.length == 10) {
                            if (l_LocationDateOk == TodayDateOnly) {
                                FillAllLocation(1);
                            }
                            else {
                                LastKnownLocation_Normal(0, l_LocationDateOk, $.session.get('ManagerID'), 1);
                            }
                        }
                    });
                    $('#AShowEnlarge').click(function () {
                        $.session.set('Map', 'AllEnlarge');
                        var l_LocationDateOk = $('#txtFromDateLocation').val();
                        l_LocationDateOk = l_LocationDateOk.replace(/[^0-9\/]/g, '');
                        if (l_LocationDateOk.length == 10) {
                            if (l_LocationDateOk == TodayDateOnly) {
                                FillAllLocation(2);
                            }
                            else {
                                LastKnownLocationExpand_Normal(0, l_LocationDateOk, $.session.get('ManagerID'), 1);
                            }
                        }
                    });
                    $('#selOfficer').multiselect({
                        noneSelectedText: 'Select Officers',
                        selectedList: 5,
                        multiple: true
                    }).multiselectfilter();

                    $('#selOfficer').multiselect("uncheckAll");
                }
                $('.tree li:has(ul)').addClass('parent_li');
                $('.tree li.parent_li > a').on('click', function (e) {
                    var children = $(this).parent('li.parent_li').find(' > ul > li');
                    if (children.is(":visible")) {
                        children.hide('fast');
                        $(this).find(' > i').addClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign');
                    } else {
                        children.show('fast');
                        $(this).find(' > i').addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');
                    }
                    e.stopPropagation();
                });
                $.loader('close');
            }

            //========================================================================================================================
            //TOOD: Caseaction
            //========================================================================================================================

            counter.client.getcaseactions = function (message) {
                hideOverlay("divCaseActionModaloverlaydiv");
                message = $.parseJSON(message);

                var PaidData = false;
                var PPData = false;
                var ReturnedData = false;
                var LLData = false;
                var RevisitData = false;
                var ArrestData = false;
                var SAData = false;

                $('#tbodyActionClamped').empty();
                $('#tbodyRMPaidCase,#tbodyRMReturnCase,#tbodyRMPPCase').empty();
                $('#tbodyRankOfficerPaid,#tbodyRankOfficerPartPaid,#tbodyRankOfficerReturn').empty();
                $('#tbodypaidformanager,#tbodypartpaidformanager,#tbodyreturnedformanager,#tbodyleftletterformanager,#tbodyrevisitformanager').empty();
                $('#tbodyClampedformanager,#tbodybailedformanager,#tbodyarrestedformanager,#tbodySurrenderdateagreedformanager').empty();
                $('#tbodyActionBailed,#tbodyActionArrested,#tbodyActionSurrenderDateAgreed').empty();


                if (!(message == '[]' || message == '' || message == null || message == undefined || message == '[{"message":"NoRecords"}]')) {
                    ClampMgr = 0;
                    Clamp = 0;
                    Bailed = 0;

                    var PaidNotes = '';
                    var RankPaidNotes = '';
                    var PPNotes = '';
                    var RankPPNotes = '';
                    var ReturnedNotes = '';
                    var RankReturnedNotes = '';
                    var LLNotes = '';
                    var ArrestNotes = '';
                    var SANotes = '';

                    $.each(message, function (key, value) {
                        switch (value.ActionText) {
                            case 'Paid':
                            case 'PAID':
                                PaidData = true;
                                if ($.session.get('TreeLevel') == 3) {
                                    PaidNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>";
                                    RankPaidNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 1 + ")'> ...More</label>";
                                }
                                else {
                                    PaidNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>";
                                    RankPaidNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 1 + ")'> ...More</label>";
                                }
                                $('#tblbodyActionPaid,#tbodypaidformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + PaidNotes : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                $('#tbodyRankOfficerPaid,#tbodyRMPaidCase').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + RankPaidNotes : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                break;
                            case 'Part Paid':
                            case 'PART PAID':
                                PPData = true;
                                if ($.session.get('TreeLevel') == 3) {
                                    PPNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>";
                                    RankPPNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 2 + ")'> ...More</label>";
                                }
                                else {
                                    PPNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>";
                                    RankPPNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 2 + ")'> ...More</label>";
                                }

                                $('#tbodypartpaidformanager,#tbodyActionPP').append($('<tr>')
                                        .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                        .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                        .append($('<td>').html(value.ActionText))
                                        .append($('<td>').html(value.DoorColour))
                                        .append($('<td>').html(value.HouseType))
                                        .append($('<td>').html(value.DateActioned))
                                        .append($('<td>').html("" + value.Fees))
                                        .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + PPNotes : value.Notes)))
                                        .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                $('#tbodyRankOfficerPartPaid,#tbodyRMPPCase').append($('<tr>')
                                        .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                        .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                        .append($('<td>').html(value.ActionText))
                                        .append($('<td>').html(value.DoorColour))
                                        .append($('<td>').html(value.HouseType))
                                        .append($('<td>').html(value.DateActioned))
                                        .append($('<td>').html("" + value.Fees))
                                        .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + RankPPNotes : value.Notes)))
                                        .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                break;
                            case 'Returned':
                                ReturnedData = true;
                                if ($.session.get('TreeLevel') == 3) {
                                    ReturnedNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>";
                                    RankReturnedNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 4 + ")'> ...More</label>";
                                }
                                else {
                                    ReturnedNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>";
                                    RankReturnedNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 4 + ")'> ...More</label>";
                                }

                                $('#tbodyreturnedformanager,#tbodyActionReturned').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + ReturnedNotes : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                $('#tbodyRankOfficerReturn,#tbodyRMReturnCase').append($('<tr>')
                                     .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                     .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                     .append($('<td>').html(value.ActionText))
                                     .append($('<td>').html(value.DoorColour))
                                     .append($('<td>').html(value.HouseType))
                                     .append($('<td>').html(value.DateActioned))
                                     .append($('<td>').html("" + value.Fees))
                                     .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + RankReturnedNotes : value.Notes)))
                                     .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                break;
                            case 'Left Letter':
                                LLData = true;
                                if ($.session.get('TreeLevel') == 3)
                                    LLNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>";
                                else
                                    LLNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>";

                                $('#tbodyleftletterformanager,#tbodyActionLeftLetter').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + LLNotes : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                break;
                            case '£ Collected':
                            case 'Revisit':
                                RevisitData = true;
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyrevisitformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyrevisitformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyrevisitformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Bailed':
                                $('#tbodybailedformanager,#tbodyActionBailed')
                                        .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                 '<td>' + value.Officer + '</td>' +
                                                 '<td>' + value.DateActioned + '</td>' +
                                                 '<td><a href="OptimiseImage.html" target="_blank" ><img style="padding-right:10px;" src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image"  onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                  '<img id="imgBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionBailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                                if (Bailed == 0) {
                                    ViewBailedImage(key, "'" + value.ImageURL + "'");
                                    $('#imgBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionBailedSelect');
                                    Bailed += 1;
                                }

                                break;
                            case 'Arrested':
                                ArrestData = true;
                                if ($.session.get('TreeLevel') == 3)
                                    ArrestNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>";
                                else
                                    ArrestNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 8 + ")'> ...More</label>";

                                $('#tbodyarrestedformanager,#tbodyActionArrested').append($('<tr>')
                                           .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                           .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                           .append($('<td>').html(value.ActionText))
                                           .append($('<td>').html(value.DoorColour))
                                           .append($('<td>').html(value.HouseType))
                                           .append($('<td>').html(value.DateActioned))
                                           .append($('<td>').html("" + value.Fees))
                                           .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + ArrestNotes : value.Notes)))
                                           .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                break;
                            case 'Surrender date agreed':
                                SAData = true;
                                if ($.session.get('TreeLevel') == 3)
                                    SANotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 9 + ")'> ...More</label>";
                                else
                                    SANotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 9 + ")'> ...More</label>";
                                $('#tbodySurrenderdateagreedformanager,#tbodyActionSurrenderDateAgreed').append($('<tr>')
                                      .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td>').html(value.DoorColour))
                                      .append($('<td>').html(value.HouseType))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + SANotes : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                break;
                            case 'Clamped':
                                if ($.session.get('TreeLevel') == 3) {
                                    $('#tbodyActionClamped')
                                             .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                       '<td>' + value.Officer + '</td>' +
                                                       '<td>' + value.DateActioned + '</td>' +
                                                       '<td><a href="OptimiseImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                       '<img id="imgActionClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionclampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                    if (Clamp == 0) {
                                        ClampKey = key;
                                        ViewActionClampedImage(key, value.Officer, value.CaseNumber);
                                        $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionclampSelect');
                                        Clamp += 1;
                                    }
                                }
                                else {
                                    $('#tbodyClampedformanager')
                                     .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                  '<td>' + value.Officer + '</td>' +
                                                  '<td>' + value.DateActioned + '</td>' +
                                                  '<td><a href="OptimiseImage.html" target="_blank" ><img style="padding-right:10px;" src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image"  onclick="ViewClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                  '<img id="imgClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="clampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                    if (ClampMgr == 0) {
                                        ViewClampedImage(key, value.Officer, value.CaseNumber);
                                        $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'clampSelect');
                                        ClampMgr += 1;
                                    }
                                }
                                break;
                        }
                    });

                    if (ReturnedData == false)
                        $('#tbodyreturnedformanager,#tbodyActionReturned').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                    if (PaidData == false)
                        $('#tbodypaidformanager,#tblbodyActionPaid').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                    if (PPData == false)
                        $('#tbodypartpaidformanager,#tbodyActionPP').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                    if (LLData == false)
                        $('#tbodyleftletterformanager,#tbodyActionLeftLetter').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                    if (RevisitData == false)
                        $('#tbodyrevisitformanager').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                    if (ArrestData == false)
                        $('#tbodyarrestedformanager,#tbodyActionArrested').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                    if (SAData == false)
                        $('#tbodySurrenderdateagreedformanager,#tbodyActionSurrenderDateAgreed').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                }
                else {
                    if (ReturnedData == false)
                        $('#tbodyreturnedformanager,#tbodyActionReturned,#tbodyRMReturnCase,#tbodyRankOfficerReturn').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                    if (PaidData == false)
                        $('#tbodypaidformanager,#tblbodyActionPaid,#tbodyRMPaidCase,#tbodyRankOfficerPaid').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                    if (PPData == false)
                        $('#tbodypartpaidformanager,#tbodyActionPP,#tbodyRMPPCase,#tbodyRankOfficerPartPaid').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                    if (LLData == false)
                        $('#tbodyleftletterformanager,#tbodyActionLeftLetter').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                    if (RevisitData == false)
                        $('#tbodyrevisitformanager').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                    if (ArrestData == false)
                        $('#tbodyarrestedformanager,#tbodyActionArrested').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                    if (SAData == false)
                        $('#tbodySurrenderdateagreedformanager,#tbodyActionSurrenderDateAgreed').append('<tr><td colspan="8" align="center"> No data found </td></tr>');
                }
            };

            counter.client.gethubcaseactions = function (message) {
                hideOverlay("divCaseActionModaloverlaydiv");
                message = $.parseJSON(message);
                //*****************************************************************************************************************************************
                if (message != undefined) {
                    if (message != "[]" && message != null && message != '[{"message":"NoRecords"}]') {
                        $('#tblbodyCA').empty();//,
                        if ((LinkForManager == 0 || LinkForManager == 1) && ($.session.get('RoleType') == 1)) {//($.trim($('#lblLoginOfficer').html()) == 'David Burton')
                            $.each(message, function (mkey, mvalue) {
                                $('#SpanAction1').html('Case Actions');
                                $('#tblbodyCA').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<a id="AActionModal' + mkey + '">').attr('data-toggle', 'modal')
                                                     .attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + mkey + ')').html(mvalue.ActionText)))
                                    .append($('<td>').html(mvalue.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(mvalue.Ave)))

                                if (mvalue.ActionText == 'Defendant contact')
                                    $('#AActionModal' + mkey).removeAttr('href').removeAttr('onclick').attr('style', 'cursor:none')//('href', '#defendantcontact')
                            });
                        }
                        else if ((LinkForManager == 0 || LinkForManager == 1) && ($.session.get('RoleType') != 1)) {
                            $.each(message, function (mkey, mvalue) {
                                $('#SpanAction1').html('Case Actions');
                                $('#tblbodyCA').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<a id="AActionModal' + mkey + '">').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + mkey + ')').html(mvalue.ActionText)))
                                    .append($('<td>').html(mvalue.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(mvalue.Ave)));
                                if (mvalue.ActionText == 'Defendant contact')
                                    $('#AActionModal' + mkey).attr('href', '#defendantcontact')
                            });
                        }
                        else {
                            $('#tblbodyCA').empty();
                            $('#SpanAction1').html('Case Actions');
                            $('#SpanAction2').html('Warrant Matching');
                            $.each(message, function (mkey, mvalue) {
                                $('#tblbodyCA').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<a id="AActionModal' + mkey + '">').attr('data-toggle', 'modal').attr('href', '#' + mvalue.ActionText.toString().toLowerCase().replace(" ", "").replace(" ", "")).attr('onclick', 'caseaction(' + mkey + ')').html(mvalue.ActionText)))
                                    .append($('<td>').html(mvalue.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(mvalue.Ave)))
                                if (mvalue.ActionText == 'Defendant contact')
                                    $('#AActionModal' + mkey).attr('href', '#defendantcontact')
                            });
                        }
                    }
                }
                //*****************************************************************************************************************************************
            };

            counter.client.getcaseactionmanager = function (message, flag, type) {
                hideOverlay("divCaseActionModaloverlaydiv");
                message = $.parseJSON(message);
                //*****************************************************************************************************************************************
                if (message != undefined) {
                    if (message != "[]" && message != null && message != '[{"message":"NoRecords"}]') {
                        if (type == 'Manager') $('#tbodyCaseActionsManagerCount').empty();
                        if (type == 'ADM') $('#tbodyCaseActionsADMCount').empty();
                        if (type == 'Officer') $('#tbodyCaseActionsOfficerCount').empty();
                        $.each(message, function (key, value) {
                            //*************************************************************
                            if (type == "Manager") {
                                $('#tbodyCaseActionsManagerCount')
                                .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())
                                .append($('<a>').attr('onclick', 'GetCaseActionsADMCount(' + flag + ',' + $.trim(value.OfficerID) + ',' + 0 + ',' + "'" + value.OfficerName + "'" + ')')
                                    .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                .append($('<td>').html(value.CaseCnt)))
                            }
                            else if (type == "ADM") {
                                $('#tbodyCaseActionsADMCount')
                                   .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())
                                   .append($('<a>').attr('onclick', 'GetCaseActionsOfficerCount(' + flag + ',' + $.trim(value.OfficerID) + ',' + 0 + ',' + "'" + value.OfficerName + "'" + ')')
                                   .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                   .append($('<td>').html(value.CaseCnt)))
                            }
                            else if (type == "Officer") {
                                var dateAr = $('#txtFromDateCA').val().split('/');
                                var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

                                $('#tbodyCaseActionsOfficerCount')
                                    .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())
                                    .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + newDate + "','" + newDate + "', 'CaseAction'" + ')')
                                        .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                    .append($('<td>').html(value.CaseCnt)))
                            }
                            //*************************************************************
                        });
                    }
                    else {
                        if (type == 'Manager') $('#tbodyCaseActionsManagerCount').empty().html('No records found');
                        if (type == 'ADM') $('#tbodyCaseActionsADMCount').empty().html('No records found');
                        if (type == 'Officer') $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
                    }
                }
                else {
                    if (type == 'Manager') $('#tbodyCaseActionsManagerCount').empty().html('No records found');
                    if (type == 'ADM') $('#tbodyCaseActionsADMCount').empty().html('No records found');
                    if (type == 'Officer') $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
                }
            };

            //========================================================================================================================
            //TODO: WMA 
            //========================================================================================================================

            counter.client.getwmadetail = function (message, type) {
                hideOverlay("windowTitleDialogoverlaydiv");
                if (message == '[]') {
                    if (type == 'ADM') {
                        $('#AMoreWMA').hide();
                        $('#tbodyWMA').empty();
                    }
                    else if (type == 'Officer') {
                        $('#AMoreWMA').hide();
                        $('#tbodyWMA').empty();
                    }
                }
                else {
                    message = $.parseJSON(message);
                    if (type == 'Manager') {
                        $('#tbodyWMA').empty();
                        $('#tbodyWMAMore').empty();
                    }
                    else if (type == 'ADM') {
                        switch ($.session.get('TreeLevel')) {
                            case '0':
                                $('#tbodyWMAADM,#tbodyWMAMoreADM').empty();
                                break;
                            case '1':
                                $('#tbodyWMA,#tbodyWMAMoreADM').empty();
                                break;
                        }
                    }
                    else if (type == 'Officer') {
                        switch ($.session.get('TreeLevel')) {
                            case '0':
                                $('#tbodyWMAOfficers,#tbodyWMAMoreOfficer').empty();
                                break;
                            case '1':
                                $('#tbodyWMAOfficers,#tbodyWMAMoreOfficer').empty();
                                break;
                            case '2':
                                $('#tbodyWMA,#tbodyWMAMoreOfficer').empty();
                                break;
                            case '3':
                                $('#tbodyWMA,#tbodyWMAMoreOfficer').empty();
                                break;
                        }
                    }

                    $.each(message, function (key, value) {
                        //*************************************************************
                        if (type == "Manager") {
                            $('#AMoreWMA').show();
                            if (key <= 4) {
                                $('#tbodyWMA')
                                    .append('<tr><td><i class="icon24"><img src="images/Case-ico.png" alt="icon"></i></td>' +
                                    '<td><a id="AMGR' + value.OfficerID + '" onclick="GetManager(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',0" + ')" data-toggle="modal" href="#windowTitleDialog">' + value.OfficerName + '</a></td>' +
                                    '<td>' + value.CaseCnt + '</td></tr>');
                            }
                            $('#tbodyWMAMore')
                                .append('<tr><td><a onclick="GetManager(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',1" + ')" data-toggle="modal" href="#">' + value.OfficerName + '</a></td>' +
                                '<td>' + value.CaseCnt + '</td></tr>');
                        }
                        else if (type == "ADM") {
                            $('#AMoreWMA').show();
                            switch ($.session.get('TreeLevel')) {
                                case '0':
                                    $('#tbodyWMAADM')
                                         .append('<tr><td><a onclick="GetAdm(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',0" + ')" data-toggle="modal" style="cursor:pointer;">' + value.OfficerName + '</a></td>' +
                                    '<td>' + value.CaseCnt + '</td></tr>');
                                    break;
                                case '1':
                                    if (key <= 4) {
                                        $('#tbodyWMA')
                                               .append('<tr><td><i class="icon24"><img src="images/Case-ico.png" alt="icon"></i></td>' +
                                               '<td><a onclick="GetAdm(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',0" + ')" data-toggle="modal" href="#windowTitleDialog" >' + value.OfficerName + '</a></td>' +
                                               '<td>' + value.CaseCnt + '</td></tr>');
                                    }
                                    break;
                            }
                            $('#tbodyWMAMoreADM')
                                .append('<tr><td><a href="#" onclick="GetAdm(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',1" + ')" style="cursor:pointer;">' + value.OfficerName + ' </a></td>' +
                                '<td>' + value.CaseCnt + '</td></tr>');
                        }
                        else if (type == "Officer") {
                            $('#AMoreWMA').show();
                            switch ($.session.get('TreeLevel')) {
                                case '0':
                                case '1':
                                    $('#tbodyWMAOfficers').append('<tr><td><a onclick="GetOfficer(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',0" + ')" data-toggle="modal" style="cursor:pointer;">' + value.OfficerName + '</a></td>' +
                                       '<td>' + value.CaseCnt + '</td></tr>');
                                    break;
                                case '2':
                                    if (key <= 4) {
                                        $('#tbodyWMA')
                                           .append('<tr><td><i class="icon24"><img src="images/Case-ico.png" alt="icon"></i></td>' +
                                           '<td><a onclick="GetOfficer(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',0" + ')" data-toggle="modal" href="#windowTitleDialog">' + value.OfficerName + '</a></td>' +
                                           '<td>' + value.CaseCnt + '</td></tr>');
                                    }
                                    break;
                                case '3':
                                    if (key <= 4) {
                                        $('#tbodyWMA')
                                           .append('<tr><td><i class="icon24"><img src="images/Case-ico.png" alt="icon"></i></td>' +
                                           '<td><a onclick="GetOfficer(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',0" + ')" data-toggle="modal" href="#windowTitleDialog">' + value.OfficerName + '</a></td>' +
                                           '<td>' + value.CaseCnt + '</td></tr>');
                                    }
                                    break;
                            }
                            $('#tbodyWMAMoreOfficer')
                                .append('<tr><td><a href="#" onclick="GetOfficer(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',1" + ')" style="cursor:pointer;">' + value.OfficerName + ' </a></td>' +
                                '<td>' + value.CaseCnt + '</td></tr>');
                        }
                        //*************************************************************
                    });
                }
            };

            counter.client.getwmalogdetail = function (message, type) {
                hideOverlay("windowTitleDialogoverlaydiv");
                message = $.parseJSON(message);
                //*****************************************************************************************************************************************
                if (message != undefined) {
                    if (message != "[]" && message != null && message != '[{"message":"NoRecords"}]') {
                        $('#tblbodySearchType').empty();
                        $('#tbodyWMAMoreOfficerCaseCount').empty();

                        showhidetabs('tblbodySearchType');
                        $('#lblSearchType').html('');
                        $('#btnBack,#btnWMAOfficerBack,#tblWMAADM').hide();
                        $('#tblbodyWMASearchType').empty();

                        if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1) {
                            $('#btnWMAOfficerBack').show();
                        }
                        else {
                            $('#btnWMAOfficerBack').hide();
                        }
                        $.each(message, function (key, value) {
                            $('#tblbodySearchType')
                                .append('<tr><td><a style="cursor:pointer" onclick="GetWMACase(' + value.OfficerID + ',' + "'" + value.FromDate + "'" + ',' + "'" + value.LogType + "',0" + ')">' + value.LogType + '</a></td>' +
                                '<td>' + value.Case + '</td></tr>');

                            $('#tbodyWMAMoreOfficerCaseCount')
                             .append('<tr><td><a style="cursor:pointer" onclick="GetWMACase(' + value.OfficerID + ',' + "'" + value.FromDate + "'" + ',' + "'" + value.LogType + "',1" + ')">' + value.LogType + '</a></td>' +
                             '<td>' + value.Case + '</td></tr>');
                        });
                    }
                }
                //*****************************************************************************************************************************************
            };

            counter.client.getwmacasedetail = function (message) {
                hideOverlay("windowTitleDialogoverlaydiv");
                var WMACase = message.split('||')[0];
                $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase').empty();
                if (!(WMACase == undefined || WMACase == '[]')) {
                    message = $.parseJSON(WMACase);
                    $.each(message, function (key, value) {
                        $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase')
                            .append('<tr><td>' + value.SearchText + '</td>' +
                                '<td>' + value.TotalRecordCount + '</td>' +
                                '<td>' + value.ActionedDate + '</td></tr>');
                    });
                }
                else {
                    $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase').html('No warrant matching activity found for ' + message.split('||')[1]);
                }
            };

            //========================================================================================================================
            //TODO: Stats
            //========================================================================================================================

            counter.client.gethubstats = function (message, link) {
                message = $.parseJSON(message);
                $('#tbodyStats').empty();
                $.each(message, function (key, value) {
                    var StatFormula = '<table width="100%" class="display-status-table"><tr><td>' +
                                               '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                               '<a class="close" data-dismiss="modal" href="#">X</a>' +
                                               '<p class="block-heading">Percentage calculation</p><table class="">' +
                                               '<tbody>' +
                                               '<tr><td>Formula for ' + value.Description + ' : ' + value.Formula + '</td></tr>' +
                                               '<tr><td>' + '</td></tr>' +
                                               '<tr><td>Calculation for ' + value.Description + ' : ' + value.Calculation + '</td></tr>' +
                                              '</tbody></table></div></td>' +
                                              '</td></tr></table>';

                    $('#tbodyStats').append($('<tr>')
                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                        .append($('<td>').append($('<a>').attr('id', 'AStat' + key).attr('style', 'cursor:pointer;')
                                            .attr('onclick', 'GetStatsOnClick(' + key + ')').html(value.Description)
                                            .attr('data-original-title', '').attr('rel', 'popover').attr('class', 'a1').attr('data-html', 'true')))
                        .append($('<td id="tdbodyStats' + key + '">').html(value.DisplayValue)))

                    $('#AStat' + key).mouseover(function () {
                        $('#AStat' + key).attr('data-content', StatFormula);
                        $('#AStat' + key).attr('data-placement', 'left');
                        $('#AStat' + key).popover('show');
                    });
                    $('#AStat' + key).mouseout(function () {
                        $('#AStat' + key).popover('hide');
                    });

                    if (link == 1) {
                        if ($.trim(value.Description) == "Case holdings") {
                            $('#tdbodyStats' + key).empty();//data-toggle="modal" href="#DivLogout"
                            $('#tdbodyStats' + key).append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseSearchByOfficerID').attr('onclick', 'GetSatasCaseActions(' + $.session.get('ManagerID') + ')').html(value.DisplayValue));
                        }
                    }
                });
                $('.close').click(function () { $('.popover').remove(); });
            };

            counter.client.getstatscaseactions = function (message) {
                message = $.parseJSON(message);
                $('#tbodyCaseSearchByOfficerID').empty();
                var HTMLString = '';
                $.each(message, function (key, value) {
                    HTMLString += '<tr><td>' + value.CaseNumber + '</td><td>' + value.DebtName + '</td><td>' + value.ClientName + '</td><td>' + value.ContactFirstLine + ', ' + value.ContactAddress + ', ' + value.PostCode + '</td>' +
                            '<td>' + value.CaseStatus + '</td><td>' + value.OfficerFirstName + ' ' + value.OfficerLastName + '</td><td>' + value.Ageing + '</td><td>' + value.IssueDate + '</td><td>' + value.DateActioned + '</td></tr>';
                });
                $('#tbodyCaseSearchByOfficerID').html(HTMLString);

                $('#tblCaseSearchByOfficerID').dataTable({
                    "sScrollY": "370px",
                    "bAutoWidth": false,
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "bLengthChange": false,
                    "bScrollCollapse": true,
                    "sInfo": true,
                    "iDisplayLength": 5
                });
            };

            //========================================================================================================================
            //TODO: Lastlocation
            //========================================================================================================================

            counter.client.getlastlocation = function (message, showAll, Condition) {
                hideOverlay("DLastLocationoverlaydiv");
                if (message != undefined && message != '[]' && message != '[{"Result":"NoRecords"}]') {
                    $('#lblDisplay').html('').hide();
                    result = JSON.parse(message);
                    var iconImg1;
                    var maptype = $.session.get('Map');
                    if (maptype == 'LL') {
                        Latitude = [];
                        Longitude = [];
                        Icon = [];
                        Content = [];
                        pLatitude = 0;
                        pLongitude = 0;
                        pContentkey;
                        pLatMap = 0;
                        pLongMap = 0;

                        if (Object.keys(officerLLArray).length <= 0) {
                            llmapinit();
                        }
                        else {
                            //clearmarkers_LLMap();
                        }

                        var infowindow = new google.maps.InfoWindow();
                        var l_newitem = 0;
                        var pLatValue = "";
                        var pLangValue = "";
                        var iconImg1 = "";
                        $.each(result, function (key, value) {
                            var l_latlngarray = value.html.split('</b>');
                            var l_latlngarray = l_latlngarray[0].split('[');
                            var l_latlngarray = l_latlngarray[1].split(']');
                            var l_officerid = l_latlngarray[0];
                            l_newitem++;
                            officerLLArray[l_officerid] = value.latitude + "," + value.longitude;

                            latLng = new google.maps.LatLng(value.latitude, value.longitude);

                            switch (value.IconType) {
                                case '1': // UTTC
                                    iconImg1 = "images/map-icon-yellow.png";
                                    break;
                                case '2':  // Returned, DROPPED
                                    iconImg1 = "images/map-icon-red.png";
                                    break;
                                case '3':  // Revisit,TCG
                                    iconImg1 = "images/map-icon-floresent.png";
                                    break;
                                case '5':  //  Paid , Part paid , TCG PP,TCG Paid 
                                    iconImg1 = "images/map-icon-orange.png";
                                    break;
                                case '6':  // Login
                                    iconImg1 = "images/map-icon-blue.png";
                                    break;
                                case '7':  // ARR
                                    iconImg1 = "images/map-icon-navyblue.png";
                                    break;
                                case '8':  // DEP
                                    iconImg1 = "images/map-icon-lightvio.png";
                                    break;
                                case '9':  // Logout
                                    iconImg1 = "images/map-icon-grey.png";
                                    break;
                                default:
                                    iconImg1 = "images/map-icon-white.png";
                            }

                            marker = new google.maps.Marker({
                                'position': latLng,
                                'map': officerLLMap,
                                'icon': iconImg1
                            });
                            if (llmarkers.hasOwnProperty(l_officerid)) {
                                llmarkers[l_officerid].setMap(null);
                                delete llmarkers[l_officerid];
                            }
                            llmarkers[l_officerid] = marker;
                            LL_Bounds.extend(marker.position);
                            google.maps.event.addListener(llmarkers[l_officerid], 'click', function (e) {
                                infowindow.setContent(value.html);
                                infowindow.open(officerLLMap, this);
                            });

                        });
                        if (l_newitem > 0) {
                            officerLLMap.fitBounds(LL_Bounds);
                        }
                    }
                    else {
                        Latitude = [];
                        Longitude = [];
                        Icon = [];
                        Content = [];
                        pLatitude = 0;
                        pLongitude = 0;
                        pContentkey;
                        pLatMap = 0;
                        pLongMap = 0;
                        if (Object.keys(officerLLArray_Expand).length <= 0) {
                            llmapexpandinit();
                        }
                        else {
                            //clearmarkers_LLMap();
                        }

                        var infowindow = new google.maps.InfoWindow();
                        var l_newitem = 0;
                        var iconImg1 = "";
                        $.each(result, function (key, value) {
                            var l_latlngarray = value.html.split('</b>');
                            var l_latlngarray = l_latlngarray[0].split('[');
                            var l_latlngarray = l_latlngarray[1].split(']');
                            var l_officerid = l_latlngarray[0];
                            l_newitem++;

                            officerLLArray_Expand[l_officerid] = value.latitude + "," + value.longitude;

                            latLng = new google.maps.LatLng(value.latitude, value.longitude);

                            switch (value.IconType) {
                                case '1': // UTTC
                                    iconImg1 = "images/map-icon-yellow.png";
                                    break;
                                case '2':  // Returned, DROPPED
                                    iconImg1 = "images/map-icon-red.png";
                                    break;
                                case '3':  // Revisit,TCG
                                    iconImg1 = "images/map-icon-floresent.png";
                                    break;
                                case '5':  //  Paid , Part paid , TCG PP,TCG Paid 
                                    iconImg1 = "images/map-icon-orange.png";
                                    break;
                                case '6':  // Login
                                    iconImg1 = "images/map-icon-blue.png";
                                    break;
                                case '7':  // ARR
                                    iconImg1 = "images/map-icon-navyblue.png";
                                    break;
                                case '8':  // DEP
                                    iconImg1 = "images/map-icon-lightvio.png";
                                    break;
                                case '9':  // Logout
                                    iconImg1 = "images/map-icon-grey.png";
                                    break;
                                default:
                                    iconImg1 = "images/map-icon-white.png";
                            }

                            marker = new google.maps.Marker({
                                'position': latLng,
                                'map': officerLLMap_Expand,
                                'icon': iconImg1
                            });
                            if (llmarkers_expand.hasOwnProperty(l_officerid)) {
                                llmarkers_expand[l_officerid].setMap(null);
                                delete llmarkers_expand[l_officerid];
                            }
                            llmarkers_expand[l_officerid] = marker;
                            LL_Bounds_Expand.extend(marker.position);
                            google.maps.event.addListener(llmarkers_expand[l_officerid], 'click', function (e) {
                                infowindow.setContent(value.html);
                                infowindow.open(officerLLMap_Expand, this);
                            });
                        });
                        if (l_newitem > 0) {
                            officerLLMap_Expand.fitBounds(LL_Bounds_Expand);
                        }

                    }
                }
                else {
                    $('#lblDisplay').html('No activity recorded for today!').show();
                    center = new google.maps.LatLng('52.8849565', '-1.9770329');
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP,
                        'fullscreenControl': true
                    };
                    var maptype = $.session.get('Map');

                    if (maptype == 'LL') {
                        llmapinit();
                    }
                    else {
                        llmapexpandinit();
                    }
                }
            };

            counter.client.getheartbeat = function (message, Condition) {
                var maptype = $.session.get('Map');
                $('#map_canvas,#DLocationHBMap').hide();
                $('#lblDisplay').html('').hide();
                if (maptype == 'HB') {
                    Condition = 1;
                    $('#DHeartBeatMap').show();
                }
                else if (maptype == 'HBEnlarge') {
                    Condition = 2;
                }
                if (message != undefined && message != '[]' && message != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(message);
                    if (Condition == 1) {
                        if (Object.keys(officerHBArray).length <= 0) {
                            hbmapinit();
                        }
                        else {
                        }
                        var infowindow = new google.maps.InfoWindow();
                        var l_newitem = 0;
                        $.each(result, function (key, value) {
                            var l_latlngarray = value.html.split('<br>');
                            var l_latlngtime = l_latlngarray[1].replace(/[^0-9]/g, '');
                            if (officerHBArray.hasOwnProperty(l_latlngtime)) {
                            }
                            else {
                                l_newitem++;
                                officerHBArray[l_latlngtime] = value.latitude + "," + value.longitude;

                                latLng = new google.maps.LatLng(value.latitude, value.longitude);
                                marker = new google.maps.Marker({
                                    'position': latLng,
                                    'map': officerHBMap,
                                    'icon': "images/map-icon-pink.png"
                                });
                                hbmarkers.push(marker);
                                HB_bounds.extend(marker.position);
                                officerHBMap.panTo(latLng);
                                var path = HB_polyline.getPath().getArray();
                                path.push(latLng);
                                HB_polyline.setPath(path);

                                google.maps.event.addListener(hbmarkers[hbmarkers.length - 1], 'click', function (e) {
                                    infowindow.setContent(value.html + '<br>' + value.latitude + ',' + value.longitude);
                                    infowindow.open(officerHBMap, this);
                                });
                            }
                        });
                        if (l_newitem > 0) {
                        }
                    }
                    else {
                        if (officerHBExpandArray.length <= 0) {
                            hbExpandMapinit();
                        }

                        var infowindow = new google.maps.InfoWindow();
                        var l_newitem = 0;

                        $.each(result, function (key, value) {
                            var l_latlngarray = value.html.split('<br>');
                            var l_latlngtime = l_latlngarray[1].replace(/[^0-9]/g, '');
                            if (officerHBExpandArray.hasOwnProperty(l_latlngtime)) {
                            }
                            else {
                                l_newitem++;
                                officerHBExpandArray[l_latlngtime] = value.latitude + "," + value.longitude;

                                latLng = new google.maps.LatLng(value.latitude, value.longitude);
                                marker = new google.maps.Marker({
                                    'position': latLng,
                                    'map': officerHBMapExpand,
                                    'icon': "images/map-icon-pink.png"
                                });
                                hbexpandmarkers.push(marker);
                                HBExpand_bounds.extend(marker.position);
                                officerHBMapExpand.panTo(latLng);
                                var path = HB_Expand_polyline.getPath().getArray();
                                path.push(latLng);
                                HB_Expand_polyline.setPath(path);
                                google.maps.event.addListener(hbexpandmarkers[hbexpandmarkers.length - 1], 'click', function (e) {
                                    infowindow.setContent(value.html + '<br>' + value.latitude + ',' + value.longitude);
                                    infowindow.open(HBExpand_bounds, this);
                                });
                            }
                        });
                        if (l_newitem > 0) {
                        }
                    }
                }
                else {
                    $('#lblDisplay').html('No activity recorded for today!').show();
                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 5,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP,
                        'fullscreenControl': true
                    };
                    if (Condition == 1) {
                        map = new google.maps.Map(document.getElementById("DHeartBeatMap"), options);
                    }
                    else {
                        map = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);
                    }
                }

                hbtimer = setTimeout(function () {
                    callHB();
                }, 30000);
            };

            counter.client.getlocationheartbeat = function (message, Condition) {
                var maptype = $.session.get('Map');
                $('#map_canvas,#DHeartBeatMap').hide();
                $('#lblDisplay').html('').hide();

                if (maptype == 'LocHB') {
                    Condition = 1;
                    $('#DLocationHBMap').show();
                }
                else if (maptype == 'LocHBEnlarge') {
                    Condition = 2;
                }

                if (message != undefined && message != '[]' && message != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(message);
                    var iconImg1;

                    if (Condition == 1) {

                        if (Object.keys(officerLocHBArray).length <= 0) {
                            LocHBMapInit();
                        }
                        else {
                        }

                        var infowindow = new google.maps.InfoWindow();
                        var l_newitem = 0;
                        $.each(result, function (key, value) {
                            var l_latlngarray = value.html.split('<br>');
                            var l_latlngtime = l_latlngarray[1].replace(/[^0-9]/g, '');
                            if (officerLocHBArray.hasOwnProperty(key + '' + l_latlngtime)) {
                            }
                            else {
                                l_newitem++;
                                officerLocHBArray[key + '' + l_latlngtime] = value.latitude + "," + value.longitude;
                                switch (value.IconType) {
                                    case '1': // Paid , Part paid
                                        iconImg1 = "images/map-icon-yellow.png";
                                        break;
                                    case '2':  // Returned
                                        iconImg1 = "images/map-icon-red.png";
                                        break;
                                    case '3':  // Revisit
                                        iconImg1 = "images/map-icon-green.png";
                                        break;
                                    case '5': // HeartBeat
                                        iconImg1 = "images/map-icon-pink.png";
                                        break;
                                    default:
                                        iconImg1 = "images/map-icon-white.png";
                                }
                                latLng = new google.maps.LatLng(value.latitude, value.longitude);
                                marker = new google.maps.Marker({
                                    'position': latLng,
                                    'map': officerLocHBMap,
                                    'icon': iconImg1
                                });
                                LocHB_markers.push(marker);
                                LocHB_bounds.extend(marker.position);
                                officerLocHBMap.panTo(latLng);
                                google.maps.event.addListener(LocHB_markers[LocHB_markers.length - 1], 'click', function (e) {
                                    infowindow.setContent(value.html + '<br>' + value.latitude + ", " + value.longitude);
                                    infowindow.open(officerLocHBMap, this);
                                });
                                //  showconsole(value.latitude + ' - ' + value.longitude + ' - ' + key + '' + l_latlngtime + ' - ' + value.html + ' - ' + value.IconType + ' - ' + iconImg1);

                            }
                            officerLocHBMap.fitBounds(LocHB_bounds);
                            officerLocHBMap.panToBounds(LocHB_bounds);
                        });
                    }
                    else {
                        //if (Object.keys(officerLocHBArray_Expand).length <= 0) {
                        //    LocHBMapExpandInit();
                        //}
                        //else {
                        //}
                        LocHBMapExpandInit();

                        var infowindow = new google.maps.InfoWindow();
                        var l_newitem = 0;
                        $.each(result, function (key, value) {
                            var l_latlngarray = value.html.split('<br>');
                            var l_latlngtime = l_latlngarray[1].replace(/[^0-9]/g, '');
                            if (officerLocHBArray_Expand.hasOwnProperty(value.IconType + '' + l_latlngtime)) {
                            }
                            else {
                                l_newitem++;
                                officerLocHBArray_Expand[value.IconType + '' + l_latlngtime] = value.latitude + "," + value.longitude;
                                switch (value.IconType) {
                                    case '1': // Paid , Part paid
                                        iconImg1 = "images/map-icon-yellow.png";
                                        break;
                                    case '2':  // Returned
                                        iconImg1 = "images/map-icon-red.png";
                                        break;
                                    case '3':  // Revisit
                                        iconImg1 = "images/map-icon-green.png";
                                        break;
                                    case '5': // HeartBeat
                                        iconImg1 = "images/map-icon-pink.png";
                                        break;
                                    default:
                                        iconImg1 = "images/map-icon-white.png";
                                }
                                latLng = new google.maps.LatLng(value.latitude, value.longitude);
                                marker = new google.maps.Marker({
                                    'position': latLng,
                                    'map': officerLocHBMap_Expand,
                                    'icon': iconImg1
                                });
                                LocHB_markers_Expand.push(marker);
                                LocHB_bounds_Expand.extend(marker.position);
                                officerLocHBMap_Expand.panTo(latLng);
                                google.maps.event.addListener(LocHB_markers_Expand[LocHB_markers_Expand.length - 1], 'click', function (e) {
                                    infowindow.setContent(value.html + '<br>' + value.latitude + ", " + value.longitude);
                                    infowindow.open(officerLocHBMap_Expand, this);
                                });
                            }
                            officerLocHBMap_Expand.fitBounds(LocHB_bounds_Expand);
                            officerLocHBMap_Expand.panToBounds(LocHB_bounds_Expand);
                        });
                    }
                }
                else {
                    $('#lblDisplay').html('No activity recorded for today!').show();
                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP,
                        'fullscreenControl': true
                    };
                    if (Condition == 1)
                        map = new google.maps.Map(document.getElementById("DLocationHBMap"), options);
                    else {
                        map = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);
                    }
                }

            };
            //========================================================================================================================
            //TODO: Ranking
            //========================================================================================================================

            counter.client.getrankingmanager = function (message, flag, type) {
                hideOverlay("divOfficerRankoverlaydiv");
                message = $.parseJSON(message);

                //*****************************************************************************************************************************************
                if (message != undefined) {
                    if (message != "[]" && message != null && message != '[{"message":"NoRecords"}]') {
                        if (type == 'Manager') {
                            switch (flag) {
                                case '1':
                                    $('#tblbodyPaid,#tbodyRMPaidManager').empty();
                                    break;
                                case '2':
                                    $('#tblbodyReturned,#tbodyRMReturnManager').empty();
                                    break;
                                case '3':
                                    $('#tblbodyPP,#tbodyRMPPManager').empty();
                                    break;
                            }
                        }
                        if (type == 'ADM') {
                            switch (flag) {
                                case '1':
                                    if ($.session.get('TreeLevel') == '0')
                                        $('#tbodyADMRank,#tbodyRMPaidADM').empty();
                                    else
                                        $('#tbodyADMRank,#tblbodyPaid,#tbodyRMPaidADM').empty();
                                    break;
                                case '4':
                                    if ($.session.get('TreeLevel') == '0')
                                        $('#tbodyADMRank,#tbodyRMReturnADM').empty();
                                    else
                                        $('#tbodyADMRank,#tblbodyReturned,#tbodyRMReturnADM').empty();

                                    break;
                                case '2':
                                    if ($.session.get('TreeLevel') == '0')
                                        $('#tbodyADMRank,#tbodyRMPPADM').empty();
                                    else
                                        $('#tbodyADMRank,#tblbodyPP,#tbodyRMPPADM').empty();
                                    break;
                            }
                        }
                        if (type == 'Officer') {
                            switch (flag) {
                                case '1':
                                    if ($.session.get('TreeLevel') == '2')
                                        $('#tbodyOfficerRank,#tblbodyPaid,#tbodyRMPaidOfficer').empty();
                                    else
                                        $('#tbodyOfficerRank,#tbodyRMPaidOfficer').empty();
                                    break;
                                case '4':
                                    if ($.session.get('TreeLevel') == '2')
                                        $('#tbodyOfficerRank,#tblbodyReturned,#tbodyRMReturnOfficer').empty();
                                    else
                                        $('#tbodyOfficerRank,#tbodyRMReturnOfficer').empty();
                                    break;
                                case '2':
                                    if ($.session.get('TreeLevel') == '2')
                                        $('#tbodyOfficerRank,#tblbodyPP,#tbodyRMPPOfficer').empty();
                                    else
                                        $('#tbodyOfficerRank,#tbodyRMPPOfficer').empty();
                                    break;
                            }
                        }

                        $.each(message, function (key, value) {
                            //*************************************************************
                            if (type == 'Manager') {
                                switch (value.ActionText) {
                                    case 'Paid':
                                        if (key <= 4) {
                                            $('#tblbodyPaid').append($('<tr>')
                                                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingSelection(1,' + value.OfficerID + ',1,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                        .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        }
                                        $('#tbodyRMPaidManager')
                                            .append($('<tr>')
                                                .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#').attr('onclick', 'FillRankingSelection(1,' + value.OfficerID + ',5,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                .append($('<td>').append($('<label>').html(value.CaseCnt))))

                                        break;
                                    case 'Returned':
                                        if (key <= 4) {
                                            $('#tblbodyReturned').append($('<tr>')
                                                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingSelection(2,' + value.OfficerID + ',1,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                        .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        }
                                        $('#tbodyRMReturnManager')
                                           .append($('<tr>')
                                               .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#').attr('onclick', 'FillRankingSelection(2,' + value.OfficerID + ',5,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                               .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        break;
                                    case 'Part Paid':
                                        if (key <= 4) {
                                            $('#tblbodyPP').append($('<tr>')
                                                      .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                      .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingSelection(3,' + value.OfficerID + ',1,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                      .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        }
                                        $('#tbodyRMPPManager')
                                            .append($('<tr>')
                                                .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#').attr('onclick', 'FillRankingSelection(3,' + value.OfficerID + ',5,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        break;
                                }
                            }
                            else if (type == "ADM") {
                                switch (value.ActionText) {
                                    case 'Paid':
                                        if ($.session.get('TreeLevel') == '1') {
                                            $('#tblbodyPaid').append($('<tr>')
                                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingSelection(1,' + value.OfficerID + ',2,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                            .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        }
                                        else {
                                            $('#tbodyADMRank').append($('<tr>')
                                                 .append($('<td>').append($('<a>').attr('onclick', 'FillRankingSelection(1,' + value.OfficerID + ',2,"' + value.OfficerName + '","' + value.ActionText + '")').attr('href', '#paid').html(value.OfficerName)))
                                                 .append($('<td>').html(value.CaseCnt)))
                                        }
                                        $('#tbodyRMPaidADM').append($('<tr>').append($('<td>')
                                                   .append($('<a>').attr('onclick', 'FillRankingSelection(1,' + value.OfficerID + ',6,"' + value.OfficerName + '","' + value.ActionText + '")')
                                                      .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                                   .append($('<td>').html(value.CaseCnt)))
                                        break;
                                    case 'Returned':
                                        if ($.session.get('TreeLevel') == '1') {
                                            $('#tblbodyReturned').append($('<tr>')
                                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingSelection(2,' + value.OfficerID + ',2,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                            .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        }
                                        else {
                                            $('#tbodyADMRank').append($('<tr>')
                                               .append($('<td>').append($('<a>').attr('onclick', 'FillRankingSelection(2,' + value.OfficerID + ',2,"' + value.OfficerName + '","' + value.ActionText + '")').attr('href', '#paid').html(value.OfficerName)))
                                               .append($('<td>').html(value.CaseCnt)))
                                        }

                                        $('#tbodyRMReturnADM').append($('<tr>')
                                                     .append($('<td>').append($('<a>').attr('onclick', 'FillRankingSelection(2,' + value.OfficerID + ',6,"' + value.OfficerName + '","' + value.ActionText + '")')
                                                        .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                                     .append($('<td>').html(value.CaseCnt)))
                                        break;
                                    case 'Part Paid':
                                        if ($.session.get('TreeLevel') == '1') {
                                            $('#tblbodyPP').append($('<tr>')
                                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingSelection(3,' + value.OfficerID + ',2,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                            .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        }
                                        else {
                                            $('#tbodyADMRank').append($('<tr>')
                                                  .append($('<td>').append($('<a>').attr('onclick', 'FillRankingSelection(3,' + value.OfficerID + ',2,"' + value.OfficerName + '","' + value.ActionText + '")').attr('href', '#paid').html(value.OfficerName)))
                                                  .append($('<td>').html(value.CaseCnt)))
                                        }
                                        $('#tbodyRMPPADM').append($('<tr>')
                                                    .append($('<td>').append($('<a>').attr('onclick', 'FillRankingSelection(3,' + value.OfficerID + ',6,"' + value.OfficerName + '","' + value.ActionText + '")')
                                                       .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                                    .append($('<td>').html(value.CaseCnt)))
                                        break;
                                }
                            }
                            else if (type == "Officer") {
                                switch (value.ActionText) {
                                    case 'Paid':
                                        if ($.session.get('TreeLevel') == '2') {
                                            if (key <= 4) {
                                                $('#tblbodyPaid').append($('<tr>')
                                                          .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                          .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                    .attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',0" + ')').html(value.OfficerName)))
                                                          .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                            }
                                        }
                                        else {
                                            $('#tbodyOfficerRank').append($('<tr>').append($('<td>')
                                               .append($('<a>').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',0" + ')')
                                                .attr('href', '#paid').html(value.OfficerName)))
                                               .append($('<td>').html(value.CaseCnt)))
                                        }
                                        $('#tbodyRMPaidOfficer').append($('<tr>')
                                                .append($('<td>').append($('<a style="cursor:pointer;">').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',1" + ')')
                                                .html(value.OfficerName)))
                                                .append($('<td>').html(value.CaseCnt)))
                                        break;
                                    case 'Returned':
                                        if ($.session.get('TreeLevel') == '2') {
                                            if (key <= 4) {
                                                $('#tblbodyReturned').append($('<tr>')
                                                          .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                          .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                    .attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',0" + ')').html(value.OfficerName)))
                                                          .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                            }
                                        }
                                        else {
                                            $('#tbodyOfficerRank').append($('<tr>')
                                                 .append($('<td>').append($('<a style="cursor:pointer">').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',0" + ')').html(value.OfficerName)))
                                                 .append($('<td>').html(value.CaseCnt)))
                                        }

                                        $('#tbodyRMReturnOfficer').append($('<tr>')
                                                 .append($('<td>').append($('<a style="cursor:pointer;">').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',1" + ')')
                                                .html(value.OfficerName)))
                                             .append($('<td>').html(value.CaseCnt)))
                                        break;
                                    case 'Part Paid':
                                        if ($.session.get('TreeLevel') == '2') {
                                            if (key <= 4) {
                                                $('#tblbodyPP').append($('<tr>')
                                                          .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                          .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',0" + ')').html(value.OfficerName)))
                                                          .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                            }
                                        }
                                        else {
                                            $('#tbodyOfficerRank').append($('<tr>')
                                                 .append($('<td>').append($('<a style="cursor:pointer">').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',0" + ')').html(value.OfficerName)))
                                                 .append($('<td>').html(value.CaseCnt)))
                                        }

                                        $('#tbodyRMPPOfficer').append($('<tr>')
                                            .append($('<td>').append($('<a style="cursor:pointer;">').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',1" + ')')
                                                .html(value.OfficerName)))
                                            .append($('<td>').html(value.CaseCnt)))

                                        break;
                                }
                            }

                            //*************************************************************

                        });
                    }
                    else {
                        if (type == 'Manager') {
                            switch (flag) {
                                case '1':
                                    $('#tblbodyPaid,#tbodyRankMoreManager').empty().html('No rankings for paid actions.');
                                    break;
                                case '2':
                                    $('#tblbodyReturnedMore,#tblbodyReturned').empty().html('No rankings for returned actions.');
                                    break;
                                case '3':
                                    $('#tblbodyPPRanking,#tblbodyPP').empty().html('No rankings for part paid actions.');
                                    break;
                            }
                        }
                        if (type == 'ADM') $('#tbodyCaseActionsADMCount').empty().html('No records found');
                        if (type == 'Officer') $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
                    }
                }
                else {
                    if (type == 'Manager') {
                        switch (flag) {
                            case '1':
                                $('#tblbodyPaid,#tbodyRankMoreManager').empty().html('No rankings for paid actions.');
                                break;
                            case '2':
                                $('#tblbodyReturnedMore,#tblbodyReturned').empty().html('No rankings for returned actions.');
                                break;
                            case '3':
                                $('#tblbodyPPRanking,#tblbodyPP').empty().html('No rankings for part paid actions.');
                                break;
                        }
                    }
                    if (type == 'ADM') $('#tbodyCaseActionsADMCount').empty().html('No records found');
                    if (type == 'Officer') $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
                }
            };
            //========================================================================================================================
            //TODO: broadcast Message | token update
            //========================================================================================================================

            counter.client.broadcastMessage = function (message) {
                showconsole(message);
                if (message == "LL") {
                    locationupdated();
                    locationhbupdated();
                }
                else if (message == "CA") {
                    caseactionupdated();
                    rankupdated();
                }
                else if (message == "WMA") {
                    WMAupdated();
                }
                else if (message == "HB") {
                    heartbeatupdated();
                }
                else if (message == "ST") {
                    StatsURLupdated();
                }
            };

            counter.client.updateYourself = function (message) {
                showconsole(message);
                if (message == "LL") {
                    locationupdated();
                    locationhbupdated();
                }
                else if (message == "CA") {
                    caseactionupdated();
                    rankupdated();
                }
                else if (message == "WMA") {
                    WMAupdated();
                }
                else if (message == "HB") {
                    heartbeatupdated();
                }
                else if (message == "ST") {
                    StatsURLupdated();
                }
                else if (message == "ALL") {
                    updateAllPanel();
                }
            };

            counter.client.actionlogupdate = function (message) {
                showconsole("-Client-" + message);
            };
            //========================================================================================================================
        }
        else {
            window.location.href = "sign-in.html";
        }

    } catch (e) {
    }

    $.connection.hub.start({ transport: ['webSockets', 'serverSentEvents'] }).done(function () {
        $.session.set("signalrconnected", 1);
        signalrconnected = true;

        showconsole("Connected, transport = " + $.connection.hub.transport.name);

        l_connectionid = $.connection.hub.id;
        showconsole('Hub Start - connect ID : ' + l_connectionid);

        /*
        if ($.connection.hub && $.connection.hub.state !== $.signalR.connectionState.disconnected) {
            showconsole("connection live");
            if ($.connection.hub && $.connection.hub.state !== $.signalR.connectionState.disconnected) {                
                counter.server.getmanagertree(OfficerID);
                showconsole("calling getmanagertree");
            }

            if ($.connection.hub && $.connection.hub.state !== $.signalR.connectionState.disconnected) {
                counter.server.getofficerstree();
                showconsole("calling getofficerstree");
            }  
            
            updateAllPanel();
        }
        else {
            showconsole("connection disconnected");
        }*/

    }).fail(function () {
        $.loader('close');
        showconsole("SignalR DISCONNECTED..!");
    });

    //******************************************************************************
});
//==========================================================================================================================================
// TODO: Hub connection
//==========================================================================================================================================

function GetActionsHub(parameter) {    
    CaseActionURL = parameter;
    try {
        if (signalrconnected) {
            showOverlay("divCaseActionModaloverlaydiv");
            counter.server.gethubcaseactions(parameter);
        }
    } catch (e) {
        hideOverlay("divCaseActionModaloverlaydiv");
    }
}

//==========================================================================================================================================
//TODO: Action Manager/ADM/Officer-Start 
//==========================================================================================================================================

function GetManager(OfficerID, OfficerName, FromDate, Flag) {
    if (Flag == 0) {
        $('#tblWMAADM,#tblWMAOfficers,#tblOfficer,#show-search-details,#btnWMAADMBack,#btnWMAOfficerBack,#btnBack').hide();
        $('#lblWMAADM,#lblOfficerDisplay,#lblSearchType').html('');
        if ($.session.get('TreeLevel') == 0) {
            $('#lblWMAManager').html(OfficerName);
            $('#tblWMAADM').show();
            FillWMAADM(OfficerID, FromDate, FromDate);
        }
        else if ($.session.get('TreeLevel') == 1)
            FillWMAOfficer(OfficerID, FromDate, FromDate);
        else
            FillWMADetailL1(OfficerID, FromDate, FromDate);
    }
    else {
        $('#DWMAMoreADM,#btnWMAMoreManager').show();
        $('#DWMAMoreManager').hide();
        $('#lblWMAMoreManagerName').html(' - ' + OfficerName);
        if ($.session.get('TreeLevel') == 0) {
            $('#tblWMAADM').show();
            FillWMAADM(OfficerID, FromDate, FromDate);
        }
        else
            FillWMADetailL1(OfficerID, FromDate, FromDate);
    }
}

function GetAdm(OfficerID, OfficerName, FromDate, Flag) {
    if (Flag == 0) {
        $('#tblWMAOfficers').show();
        switch ($.session.get('TreeLevel')) {
            case '0':
                $('#lblWMAADM').html(' - ' + OfficerName);
                $('#btnWMAADMBack').show();
                $('#tblWMAADM').hide();
                break;
            case '1':
                $('#lblWMAADM').html(' - ' + OfficerName);
                $('#tblWMA').show();
                $('#tblWMAADM,#tblOfficer,#btnWMAADMBack').hide();
                break;
            case '2':
                break;
        }
        FillWMAOfficer(OfficerID, FromDate, FromDate);
    }
    else {
        $('#DWMAMoreOfficer,#btnWMAMoreADM').show();
        $('#DWMAMoreADM,#btnWMAMoreManager').hide();
        $('#lblWMAMoreADMName').html(' - ' + OfficerName);
        FillWMAOfficer(OfficerID, FromDate, FromDate);
    }
}

function GetOfficer(OfficerID, OfficerName, FromDate, Flag) {
    if (Flag == 0) {
        $('#tblWMAOfficers,#btnWMAADMBack').hide();
        $('#tblOfficer').show();
        $('#lblOfficerDisplay').html(' - ' + OfficerName);
        switch ($.session.get('TreeLevel')) {
            case '0':
                $('#tblbodySearchType').show();
                break;
            case '1':
                break;
            case '2':
                $('#tblWMAADM').hide();
                $('#tblbodySearchType').show();
                break;
        }
        FillWMADetailL1(OfficerID, FromDate, FromDate);
    }
    else {
        $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer').show();
        $('#DWMAMoreOfficer,#btnWMAMoreADM').hide();
        $('#lblWMAMoreOfficerName').html(' - ' + OfficerName);
        FillWMADetailL1(OfficerID, FromDate, FromDate);
    }
}

//==========================================================================================================================================
//TODO: Action Manager/ADM/Officer-End
//==========================================================================================================================================

function GetStats(OfficerID, link) {
    var inputParams = OfficerID + "," + $.session.get('CompanyID');
    StatsURL = inputParams;
    try {
        if (signalrconnected) {
            counter.server.gethubstats(inputParams, link);
        }
    } catch (e) {
    }
}

function GetSatasCaseActions(parameter) {
    try {
        if (signalrconnected) {
            counter.server.getstatscaseactions(parameter);
        }
    } catch (e) {
    }
}
//==========================================================================================================================================
//TODO : Client search function -Start
//==========================================================================================================================================

function ClientSearch() {
    $('#lblClientSearch').attr('data-toggle', 'modal').attr('href', '#ClientSearch');
    $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
    GetClientList();
}

function GetClientList() {
    Type = "GET";
    var inputParams = "/GetClientList?ClientName=";
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () {
            if ($.session.get('ClientId') == undefined || $.session.get('ClientId') == 0) {
                $.session.set('ClientId', 0);
            }
        },
        success: function (result) {//On Successfull service call
            if (result != undefined) {
                $('#tblClientList').dataTable().fnClearTable();
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        $('#tbodyClientList').append('<tr style="border-bottom: 1px solid;"><td>' + value.ClientName + '</td>' +
                            '<td><img src="images/Disactive.png" class="Unselect" style="width: 24px; cursor: pointer;" title="Select client" onclick="ClientSelect(' + key + ',' + value.ClientId + ',' + "'" + value.ClientName + "'" + ')" id="imgClientview' + key + '" /></td></tr>');
                    });
                    $('#tblClientList').dataTable({
                        "sScrollY": "auto",
                        "bPaginate": true,
                        "bDestroy": true,
                        "bSort": false,
                        "sPaginationType": "full_numbers",
                        "bLengthChange": false,
                        "sPageButton": "paginate_button",
                        "sPageButtonActive": "paginate_active",
                        "sPageButtonStaticDisabled": "paginate_button",
                        "bFilter": true,
                        "bInfo": false,
                        "iDisplayLength": 5
                    });
                    if (!($.session.get('ClientKey') == undefined || $.session.get('ClientKey') == '')) {
                        $('#imgClientview' + $.session.get('ClientKey')).attr('src', 'images/Active.png').attr('class', 'Select');
                    }
                }
                else {
                    $('#tblClientList').dataTable({
                        "sScrollY": "auto",
                        "bPaginate": false,
                        "bDestroy": true,
                        "bSort": false,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bInfo": false,
                        "iDisplayLength": 1
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function ClientSelect(key, ClientId, Name) {
    $.session.set('ClientKey', key);
    $.session.set('ClientId', ClientId);
    $.session.set('ClientName', Name);
    $('#pClientName').html(Name);
    $('#DClient').show();

    $('.Select').removeClass('Select').addClass('Unselect').attr('src', 'images/Disactive.png');
    if ($('#imgClientview' + key).attr('src') == 'images/Disactive.png')
        $('#imgClientview' + key).attr('src', 'images/Active.png').attr('class', 'Select');
    else
        $('#imgClientview' + key).attr('src', 'images/Disactive.png').attr('class', 'Unselect');
}

function ShowAllClient() {
    $.session.set('ClientId', 0);
    $.session.set('ClientName', '');
    $.session.set('ClientKey', '');
    $('#pClientName').html('');
    $('#DClient').hide();
}
//==========================================================================================================================================
//TODO : Client search function -End
//==========================================================================================================================================
//TODO : Case action function - Start
//==========================================================================================================================================

function FillCaseActions(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;
    GetActionsHub(ManagerID.toString() + ',' + FromDate.toString() + ',' + OfficerID.toString() + ',' + GroupID.toString() + ',' + $.session.get('OfficerRole') + ',' + $.session.get('CompanyID'));
}

function FillCaseDetailsActionForManager(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Bailed = 0;
    Clamp = 0;
    $('#tblbodyActionPaid,#tbodyActionPP,#tbodyActionReturned,#tbodyActionLeftLetter').empty();
    $('#tbodyActionEnforcementStart,#tbodyActionRevisit,#tbodyActionEnforcementEnd,#tbodyActionArrested').empty();
    $('#tbodyActionSurrenderDateAgreed').empty();

    $('#tbodypaidformanager,#tbodypartpaidformanager,#tbodyreturnedformanager').empty();
    $('#tbodyleftletterformanager,#tbodyrevisitformanager,#tbodyClampedformanager').empty();
    $('#tbodybailedformanager,#tbodyarrestedformanager,#tbodySurrenderdateagreedformanager').empty();
    $('#tbodyRankOfficerPaid,#tbodyRankOfficerPartPaid,#tbodyRankOfficerReturn').empty();
    // TODO : Rank more
    $('#tbodyRMPaidCase,#tbodyRMReturnCase,#tbodyRMPPCase').empty();

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;

    var inputParams = $.trim(ManagerID) + "," + FromDate + "," + ToDate + "," + OfficerID + "," + GroupID + "," + $.session.get('CompanyID');
    GetCaseActionsOfficer(inputParams);
}

function FullDescription(key, val) {
    $('#btnCaseActionBack,#divCaseActionsManagerCount,#divCaseActionsADMCount,#divCaseActionsOfficerCount,#HCaseActionNavigation').hide();
    if ($.session.get('CompanyID') == 1) {
        switch (val) {
            case 1:
                $('#paidformanager,#paid').hide();
                break;
            case 2:
                $('#partpaidformanager,#partpaid').hide();
                break;
            case 4:
                $('#returnedformanager,#returned').hide();
                break;
            case 5:
                $('#leftletterformanager,#leftletter,#bailedformanager,#bailed').hide();
                break;
            case 6:
                $('#Clampedformanager,#clamped').hide();
                break;
            case 3:
                $('#revisitformanager,#revisit').hide();
                break;
            case 7:
                $('#deviatedformanager,#deviated').hide();
                break;
            case 8:
                $('#arrestedformanager,#arrested').hide();
                break;
            case 9:
                $('#Surrenderdateagreedformanager,#surrenderdateagreed').hide();
                break;
        }
    }
    $('#divCaseActionModal').attr('class', 'modal fade in').show();
    $('#Notesformanager').empty().html($('#tdFullDesc' + key).html()).show();
    $('#HCaseNumber').empty().html(' :  ' + $.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html() + " - " + $('#tdCaseOfficer' + key).html()).show();
    $('#btnNotesBack').show().click(function () {
        if ($.session.get('CompanyID') == 1) {
            $('#paidformanager,#partpaidformanager,#returnedformanager,#leftletterformanager,#Clampedformanager,#revisitformanager,#bailedformanager,#arrestedformanager,#Surrenderdateagreedformanager,#deviated').hide();
            switch (val) {
                case 1:
                    $('#paidformanager').show();
                    break;
                case 2:
                    $('#partpaidformanager').show();
                    break;
                case 3:
                    $('#revisitformanager').show();
                    break;
                case 4:
                    $('#returnedformanager').show();
                    break;
                case 5:
                    $('#leftletterformanager').show();
                    break;
                case 6:
                    $('#Clampedformanager').show();
                    break;
                case 7:
                    $('#deviated').show();
                    break;
                case 8:
                    $('#arrestedformanager').show();
                    break;
                case 9:
                    $('#Surrenderdateagreedformanager').show();
                    break;
            }
        }
        $('#divCaseActionModal').attr('class', 'modal fade in').show();
        $('#divOfficerRank').attr('class', 'modal fade').hide();
        $('#Notesformanager,#HCaseNumber,#btnNotesBack').hide();
        $('#btnCaseActionBack').show();
    });
}

function FullDescriptionOfficer(key, val) {
    $('#btnCaseActionBack,#divCaseActionsManagerCount,#divCaseActionsADMCount,#divCaseActionsOfficerCount').hide();
    $('#btnCaseActionBack,#Notesformanager,#btnNotesBack').hide();
    $.session.set('CaseNotesReturnValue', val);
    if ($.session.get('CompanyID') == 1) {
        switch (val) {
            case 1:
                $('#paidformanager,#paid').hide();
                break;
            case 2:
                $('#partpaidformanager,#partpaid').hide();
                break;
            case 3:
                $('#revisitformanager,#revisit,#bailedformanager,#bailed').hide();
                break;
            case 4:
                $('#returnedformanager,#returned').hide();
                break;
            case 5:
                $('#leftletterformanager,#leftletter').hide();
                break;
            case 6:
                $('#Clampedformanager,#clamped').hide();
                break;
            case 7:
                $('#deviatedformanager,#deviated').hide();
                break;
            case 8:
                $('#arrestedformanager,#arrested').hide();
                break;
            case 9:
                $('#Surrenderdateagreedformanager,#surrenderdateagreed').hide();
                break;
        }
    }
    $('#divCaseActionModal').attr('class', 'modal fade in').show();
    $('#Notesformanager').empty().html($('#tdFullDesc' + key).html()).show();
    $('#HCaseNumber').empty().html(' :  ' + $.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html() + " - " + $('#tdCaseOfficer' + key).html()).show();
    $('#btnNotesBack').show().click(function () {
        if ($.session.get('CompanyID') == 1) {
            $('#paid,#partpaid,#returned,#leftletter,#revisit,#deviated,#surrenderdateagreed,#bailed,#arrested,#clamped').hide();
            switch (val) {
                case 1:
                    if ($.session.get('TreeLevel') == 3)
                        $('#paid').show().attr('class', 'modal fade in');
                    else
                        $('#paid').show().attr('class', 'modal fade').hide();
                    break;
                case 2:
                    if ($.session.get('TreeLevel') == 3)
                        $('#partpaid').show().attr('class', 'modal fade in');
                    else
                        $('#partpaid').show().attr('class', 'modal fade').hide();
                    break;
                case 3:
                    if ($.session.get('OfficerRole') == 9) {
                        if ($.session.get('TreeLevel') == 3)
                            $('#bailed').show().attr('class', 'modal fade in');
                        else
                            $('#bailed').show().attr('class', 'modal fade').hide();
                    }
                    else {
                        if ($.session.get('TreeLevel') == 3)
                            $('#revisit').show().attr('class', 'modal fade in');
                        else
                            $('#revisit').show().attr('class', 'modal fade').hide();
                    }
                    break;
                case 4:
                    if ($.session.get('TreeLevel') == 3)
                        $('#returned').show().attr('class', 'modal fade in');
                    else
                        $('#returned').show().attr('class', 'modal fade').hide();
                    break;
                case 5:
                    if ($.session.get('TreeLevel') == 3)
                        $('#leftletter').show().attr('class', 'modal fade in');
                    else
                        $('#leftletter').show().attr('class', 'modal fade').hide();
                    break;
                case 7:
                    if ($.session.get('TreeLevel') == 3)
                        $('#deviated').show().attr('class', 'modal fade in');
                    else
                        $('#deviated').show().attr('class', 'modal fade').hide();
                    break;
                case 8:
                    if ($.session.get('TreeLevel') == 3)
                        $('#arrested').show().attr('class', 'modal fade in');
                    else
                        $('#arrested').show().attr('class', 'modal fade').hide();
                    break;
                case 9:
                    if ($.session.get('TreeLevel') == 3)
                        $('#surrenderdateagreed').show().attr('class', 'modal fade in');
                    else
                        $('#surrenderdateagreed').show().attr('class', 'modal fade').hide();
                    break;

            }
        }
        $('#divCaseActionModal').attr('class', 'modal fade in').hide();
        $('#Notesformanager,#HCaseNumber,#btnNotesBack').hide();
    });
}

function caseaction(Flag) {
    showOverlay("divCaseActionModaloverlaydiv");
    $('#btnCaseActionBack,#btnCaseActionADM,#btnCaseActionManager,#Notesformanager,#btnNotesBack,#divCaseActionsOfficerCount,#HCaseNumber').hide();
    caseactionshowhidetabs();
    $('#divCaseActionsManagerCount').show();
    $('#lblCAZoneName,#lblCAManagerName,#lblCAADMName,#lblCAOfficerName').html('');
    Flag += 1;

    if ($.session.get('CompanyID') == 1) {
        switch (Flag) {
            case 1:
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#HCaseActionTitle').html($.session.get('OfficerRole') == 9 ? 'Left Letter' : 'Part Paid');
                break;
            case 3:
                $('#HCaseActionTitle').html($.session.get('OfficerRole') == 9 ? 'Bailed' : 'Returned');
                break;
            case 4:
                $('#HCaseActionTitle').html($.session.get('OfficerRole') == 9 ? 'Arrested' : 'Left Letter');
                break;
            case 5:
                $('#HCaseActionTitle').html($.session.get('OfficerRole') == 9 ? 'Surrender date agreed' : 'Defendant Contact');
                break;
            case 6:
                $('#HCaseActionTitle').html($.session.get('OfficerRole') == 9 ? 'Returned' : 'Clamped');
                break;
            case 7:
                $('#HCaseActionTitle').html('Deviation');
                break;
        }
    }
    switch ($.session.get('TreeLevel')) {
        case '0':
            $('#divCaseActionsOfficerCount,#divCaseActionsADMCount').hide();
            $('#divCaseActionsManagerCount').show();
            if ($.session.get('CaseActionMode') == 'S' || $.session.get('CaseActionMode') == undefined) {
                GetCaseActionsManagerCount(Flag, $.session.get('ManagerID'), 0, $.session.get('MgrName'));
            }
            else {
                GetCaseActionsType_Normal(Flag, $.session.get('ManagerID'), 'Manager', $.session.get('MgrName'));
            }

            $('#lblCAZoneName').html(' - ' + $.session.get('MgrName'));
            break;
        case '1':
            $('#divCaseActionsOfficerCount,#divCaseActionsManagerCount').hide();
            $('#divCaseActionsADMCount').show();
            if ($.session.get('CaseActionMode') == 'S' || $.session.get('CaseActionMode') == undefined) {
                GetCaseActionsADMCount(Flag, $.session.get('ManagerID'), 0, $.session.get('MgrName'));
            }
            else {
                GetCaseActionsType_Normal(Flag, $.session.get('ManagerID'), 'ADM', $.session.get('MgrName'));
            }

            if ($.session.get('TreeLevel') == 1)
                $('#btnCaseActionManager').hide();
            else
                $('#lblCAManagerName').show();
            break;
        case '2':
            if ($.session.get('CaseActionMode') == 'S' || $.session.get('CaseActionMode') == undefined) {
                GetCaseActionsOfficerCount(Flag, $.session.get('ManagerID'), 0, $.session.get('MgrName'));
            }
            else {
                GetCaseActionsType_Normal(Flag, $.session.get('ManagerID'), 'Officer', $.session.get('MgrName'));
            }
            $('#divCaseActionsOfficerCount').show();
            $('#divCaseActionsManagerCount,#divCaseActionsADMCount').hide();
            break;
        case '3':
            if ($('#txtFromDateCA').val() == '') {
                var date = new Date();
                var TDate = $.datepicker.formatDate('dd/mm/yy', date);
                $('#txtFromDateCA').val(TDate);
            }
            var dateAr = $('#txtFromDateCA').val().split('/');
            var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
            if ($.session.get('CaseActionMode') == 'S' || $.session.get('CaseActionMode') == undefined) {
                FillCaseDetailsActionForManager($.session.get('ManagerID'), newDate, newDate, $.session.get('ManagerID'), 0);
            }
            else {
                var CaseDate = newDate.replace('/', '-').replace('/', '-');
                GetCaseDetail_Normal($.session.get('ManagerID'), CaseDate);
            }
            break;
    }
    hideOverlay("divCaseActionModaloverlaydiv");
}

function GetCaseActionsManagerCount(Flag, MgrID, GrpID, MgrName) {
    if ($('#txtFromDateCA').val() == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        $('#txtFromDateCA').val(TDate);
    }
    var dateAr = $('#txtFromDateCA').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

    var inputParams = $('#HCaseActionTitle').html() + "," + newDate + "," + newDate + "," + $.session.get('CompanyID') + "," + MgrID + "," + GrpID + "," + $.session.get('OfficerRole');
    CAMGR_URL = inputParams;
    CA_FLAG = Flag;
    CA_TYPE = "Manager";
    GetCaseActionManager(inputParams, Flag, "Manager");
}

function GetCaseActionsADMCount(Flag, MgrID, GrpID, MgrName) {
    showOverlay("divCaseActionModaloverlaydiv");
    $('#btnCaseActionBack,#Notesformanager,#btnNotesBack,#divCaseActionsManagerCount,#divCaseActionsOfficerCount').hide();
    caseactionshowhidetabs();
    $('#divCaseActionsADMCount').show();
    if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1)
        $('#btnCaseActionManager').show();
    else
        $('#btnCaseActionManager').show();
    $('#lblCAManagerName').html(' - ' + MgrName);

    if ($('#txtFromDateCA').val() == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        $('#txtFromDateCA').val(TDate);
    }
    var dateAr = $('#txtFromDateCA').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

    var inputParams = $('#HCaseActionTitle').html() + "," + newDate + "," + newDate + "," + $.session.get('CompanyID') + "," + MgrID + "," + GrpID + "," + $.session.get('OfficerRole');
    CAADM_URL = inputParams;
    CA_FLAG = Flag;
    CA_TYPE = "ADM";

    GetCaseActionManager(inputParams, Flag, "ADM");
    hideOverlay("divCaseActionModaloverlaydiv");
}

function GetCaseActionsOfficerCount(Flag, MgrID, GrpID, MgrName) {
    showOverlay("divCaseActionModaloverlaydiv");
    $('#btnCaseActionBack,#Notesformanager,#btnNotesBack,#divCaseActionsManagerCount,#divCaseActionsADMCount,#btnCaseActionManager').hide();
    caseactionshowhidetabs();
    $('#divCaseActionsOfficerCount').show();
    if ($.session.get('TreeLevel') == 1 || $.session.get('TreeLevel') == 0) {
        $('#btnCaseActionADM').show();
        $('#lblCAADMName').html(' - ' + MgrName);
    }
    if ($.session.get('TreeLevel') == 2)
        $('#lblCAADMName').html((MgrName != null && MgrName != undefined && MgrName != 'undefined' && MgrName != '') ? ' - ' + MgrName : '');

    if ($('#txtFromDateCA').val() == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        $('#txtFromDateCA').val(TDate);
    }
    var dateAr = $('#txtFromDateCA').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

    var inputParams = $('#HCaseActionTitle').html() + "," + newDate + "," + newDate + "," + $.session.get('CompanyID') + "," + MgrID + "," + GrpID + "," + $.session.get('OfficerRole');
    CAOFF_URL = inputParams;
    CA_FLAG = Flag;
    CA_TYPE = "Officer";
    GetCaseActionManager(inputParams, Flag, "Officer");

}

function clickoncaseactionmanager(caseid, key, OfficerID, OfficerName, FromDate, ToDate, Module) {
    caseactionshowhidetabs();
    var Flag, ActionText;
    $('#HCaseActionNavigation').show();
    $('#btnCaseActionBack').show();
    $('#divCaseActionsManagerCount,#divCaseActionsOfficerCount,#btnCaseActionManager,#btnCaseActionADM').hide();
    $('#DRankingOfficer').hide();
    if (Module == 'CaseAction') {
        if ($.session.get('CaseActionMode') == 'S') {// || $.session.get('CaseActionMode') == undefined) {
            FillCaseDetailsActionForManager($('#HCaseActionManagerID' + key).html(), FromDate, ToDate, OfficerID, '0');
        }
        else {
            GetCaseDetail_Normal(OfficerID, FromDate);
        }
    }
    else if (Module == 'Ranking') {
        if ($.session.get('RankMode') == 'S') {// || $.session.get('CaseActionMode') == undefined) {
            FillCaseDetailsActionForManager($('#HCaseActionManagerID' + key).html(), FromDate, ToDate, OfficerID, '0');
        }
        else {
            GetCaseDetail_Normal(OfficerID, FromDate);
        }
    }


    //TODO: Rank more
    $('#DRankMorePaid,#DRankMorePaidOfficer,#btnPaidOfficerBack').hide();
    $('#DRankMoreReturned,#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').hide();
    $('#DRankMorePP,#DRankMorePPOfficer,#btnPPOfficerBack').hide();

    $('#lblCAOfficerName,#lblRankOfficerName,#lblRankPaidOfficer,#lblRankReturnedOfficer,#lblRankPPOfficer').html(' - ' + OfficerName);
    $('#tblRankOfficerReturn,#tblRankOfficerPartPaid,#tblRankOfficerPaid,#btnRankADMBack,#btnRankOfficerBack').hide();
    if ($.session.get('CompanyID') == 1) {
        switch (caseid) {
            case 1:
                $('#lblRankStatus').html('Ranking - Paid');
                $('#paidformanager,#DRankingCase,#tblRankOfficerPaid,#DRankMorePaidCaseList,#btnPaidCaseBack').show();
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#lblRankStatus').html('Ranking - Part Paid');
                $('#DRankingCase,#tblRankOfficerPartPaid,#DRankMorePPCaseList,#btnPPCaseBack').show();
                if ($.session.get('OfficerRole') == 9) {
                    $('#leftletterformanager').show();
                    $('#HCaseActionTitle').html('Left Letter');
                }
                else {
                    $('#partpaidformanager').show();
                    $('#HCaseActionTitle').html('Part Paid');
                }
                break;
            case 3:
                if ($.session.get('OfficerRole') == 9) {
                    $('#bailedformanager').show();
                    $('#HCaseActionTitle').html('Bailed');
                }
                else {
                    $('#returnedformanager').show();
                    $('#HCaseActionTitle').html('Returned');
                }
                break;
            case 4:
                $('#lblRankStatus').html('Ranking - Returned');
                $('#DRankingCase,#tblRankOfficerReturn,#DRankMoreReturnedCaseList,#btnReturnedCaseBack').show();
                if ($.session.get('OfficerRole') == 9) {
                    $('#arrestedformanager').show();
                    $('#HCaseActionTitle').html('Arrested');
                }
                else {
                    $('#leftletterformanager').show();
                    $('#HCaseActionTitle').html('Left Letter');
                }
                break;
            case 5:
                if ($.session.get('OfficerRole') == 9) {
                    $('#Surrenderdateagreedformanager').show();
                    $('#HCaseActionTitle').html('Surrender date agreed');
                }
                else {
                    $('#leftletterformanager').show();
                    $('#HCaseActionTitle').html('Left Letter');
                }
                break;
            case 6:
                if ($.session.get('OfficerRole') == 9) {
                    $('#HCaseActionTitle').html('Returned');
                    $('#returnedformanager').show();
                }
                else {
                    $('#Clampedformanager').show();
                    $('#HCaseActionTitle').html('Clamped');
                }
                Clamp = 0;
                break;
            case 7:
                $('#deviatedformanager').show();
                $('#HCaseActionTitle').html('Deviation');
                break;
        }
    }
}

// TODO : Normal case Search
function GetCaseActions_Normal(ManagerID, FromDate, OfficerID) {
    var SDate = FromDate;
    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    FromDate = newDate;
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + ManagerID + '/' + FromDate + '/' + FromDate + '/CaseActions?OfficerID=' + OfficerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader9').show();
            $('#changeclass1 div').css('background', '#F9F9F9');
        },
        success: function (data) {
            if (!(data == '' || data == null || data == '[]')) {
                $('#tblbodyCA').empty();
                $('#SpanAction1').html('Case Actions');
                if ($.session.get('TreeLevel') == 3) {
                    $.each(data, function (key, value) {
                        $('#tblbodyCA').append($('<tr>')
                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                            .append($('<td>').append($('<a id="AActionModal' + key + '">').attr('data-toggle', 'modal')
                                .attr('href', '#' + value.ActionText.toString().toLowerCase().replace(" ", "").replace(" ", ""))
                                .attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                            .append($('<td>').html(value.Case))
                            .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                        if (value.ActionText == 'Defendant contact')
                            $('#AActionModal' + key).attr('href', '#defendantcontact')
                    });
                }
                else {
                    $.each(data, function (key, value) {
                        $('#tblbodyCA').append($('<tr>')
                                      .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                      .append($('<td>').append($('<a id="AActionModal' + key + '">').attr('data-toggle', 'modal')
                                                       .attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                      .append($('<td>').html(value.Case))
                                      .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))

                        if (value.ActionText == 'Defendant contact')
                            $('#AActionModal' + key).removeAttr('href').removeAttr('onclick').attr('style', 'cursor:none')
                    });
                }

            }
        },
        complete: function () {
            $('#imgLoader9').fadeOut(1000);
            $('#changeclass1 div').css('background', '');
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function GetCaseActionsType_Normal(Flag, ManagerID, Type, ManagerName) {
    if (Type == 'ADM') {
        $('#btnCaseActionBack,#Notesformanager,#btnNotesBack,#divCaseActionsManagerCount,#divCaseActionsOfficerCount').hide();
        caseactionshowhidetabs();
        $('#divCaseActionsADMCount').show();
        if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1)
            $('#btnCaseActionManager').show();
        else
            $('#btnCaseActionManager').show();

        $('#lblCAManagerName').html(' - ' + ManagerName);
    }
    else if (Type == 'Officer') {
        $('#btnCaseActionBack,#Notesformanager,#btnNotesBack,#divCaseActionsManagerCount,#divCaseActionsADMCount,#btnCaseActionManager').hide();
        caseactionshowhidetabs();
        $('#divCaseActionsOfficerCount').show();
        if ($.session.get('TreeLevel') == 1 || $.session.get('TreeLevel') == 0) {
            $('#btnCaseActionADM').show();
            $('#lblCAADMName').html(' - ' + ManagerName);
        }
        if ($.session.get('TreeLevel') == 2)
            $('#lblCAADMName').html((ManagerName != null && ManagerName != undefined && ManagerName != 'undefined' && ManagerName != '') ? ' - ' + ManagerName : '');

    }
    var dateAr = $('#txtFromDateCA').val().split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + $('#HCaseActionTitle').html() + '/' + newDate + '/' + newDate + '/CaseActionssDetailForManager?ManagerID=' + ManagerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader1').show();
        },
        success: function (data) {
            if (!(data == '' || data == null || data == '[]')) {
                switch (Type) {
                    case 'Manager':
                        $('#tbodyCaseActionsManagerCount').empty();
                        $.each(data, function (key, value) {
                            $('#tbodyCaseActionsManagerCount')
                                .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())
                                .append($('<a>').attr('onclick', 'GetCaseActionsType_Normal(' + Flag + ',' + $.trim(value.OfficerID) + ',' + "'ADM'" + ",'" + value.OfficerName + "'" + ')')
                                    .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                .append($('<td>').html(value.CaseCnt)))
                        });
                        break;
                    case 'ADM':
                        $('#tbodyCaseActionsADMCount').empty();
                        $.each(data, function (key, value) {
                            $('#tbodyCaseActionsADMCount')
                                      .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())
                                      .append($('<a>').attr('onclick', 'GetCaseActionsType_Normal(' + Flag + ',' + $.trim(value.OfficerID) + ',' + "'Officer'" + ",'" + value.OfficerName + "'" + ')')
                                      .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                      .append($('<td>').html(value.CaseCnt)))
                        });
                        break;
                    case 'Officer':
                        $('#tbodyCaseActionsOfficerCount').empty();
                        $.each(data, function (key, value) {
                            $('#tbodyCaseActionsOfficerCount')
                                        .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())
                                        .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + newDate + "','" + newDate + "', 'CaseAction'" + ')')
                                            .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                        .append($('<td>').html(value.CaseCnt)))
                        });
                        break;
                }
            }
            else {
                $('#tbodyCaseActionsManagerCount').empty().html('No records found');
                $('#tbodyCaseActionsADMCount').empty().html('No records found');
                $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
            }
        },
        complete: function () {
            $('#imgLoader1').fadeOut(1000);
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function GetCaseDetail_Normal(ManagerID, FromDate) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + ManagerID + '/' + FromDate + '/' + FromDate + '/CaseActionssDetail',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader8').show();
            ClampMgr = 0;
            BailedMgr = 0;
        },
        success: function (data) {//On Successfull service call         
            if (!(data == '' || data == null || data == '[]')) {
                $('#tblbodyActionPaid,#tbodyActionPP,#tbodyActionReturned,#tbodyActionLeftLetter,#tbodyActionClamped').empty();
                $('#tbodyActionRevisit,#tbodyActionBailed,#tbodyActionArrested,#tbodyActionSurrenderDateAgreed').empty();
                $('#tbodyActionEnforcementStart,#tbodyActionEnforcementEnd').empty();
                $('#tbodyRankOfficerPaid,#tbodyRankOfficerPartPaid,#tbodyRankOfficerReturn').empty();
                $('#tbodypaidformanager,#tbodypartpaidformanager,#tbodyreturnedformanager').empty();
                $('#tbodyleftletterformanager,#tbodyClampedformanager,#tbodybailedformanager').empty();
                $('#tbodyarrestedformanager,#tbodySurrenderdateagreedformanager').empty();
                var PaidNotes = '';
                var PPNotes = '';
                var ReturnedNotes = '';
                var LLNotes = '';
                var ArrestNotes = '';
                var SANotes = '';

                $.each(data, function (key, value) {
                    switch (value.ActionText) {
                        case 'Paid':
                        case 'PAID':
                            if ($.session.get('TreeLevel') == 3)
                                PaidNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>";
                            else
                                PaidNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>";

                            $('#tbodypaidformanager,#tblbodyActionPaid').append($('<tr>')
                               .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                               .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                               .append($('<td>').html(value.ActionText))
                               .append($('<td>').html(value.DoorColour))
                               .append($('<td>').html(value.HouseType))
                               .append($('<td>').html(value.DateActioned))
                               .append($('<td>').html("" + value.Fees))
                               .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + PaidNotes : value.Notes)))
                               .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                            break;
                        case 'Part Paid':
                        case 'PART PAID':
                            if ($.session.get('TreeLevel') == 3)
                                PPNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>";
                            else
                                PPNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>";

                            $('#tbodypartpaidformanager,#tbodyActionPP').append($('<tr>')
                               .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                               .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                               .append($('<td>').html(value.ActionText))
                               .append($('<td>').html(value.DoorColour))
                               .append($('<td>').html(value.HouseType))
                               .append($('<td>').html(value.DateActioned))
                               .append($('<td>').html("" + value.Fees))
                               .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + PPNotes : value.Notes)))
                               .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                            break;
                        case 'Returned':
                            if ($.session.get('TreeLevel') == 3)
                                ReturnedNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>";
                            else
                                ReturnedNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>";

                            $('#tbodyreturnedformanager,#tbodyActionReturned').append($('<tr>')
                               .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                               .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                               .append($('<td>').html(value.ActionText))
                               .append($('<td>').html(value.DoorColour))
                               .append($('<td>').html(value.HouseType))
                               .append($('<td>').html(value.DateActioned))
                               .append($('<td>').html("" + value.Fees))
                               .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + ReturnedNotes : value.Notes)))
                               .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                            break;
                        case 'Left Letter':
                            if ($.session.get('TreeLevel') == 3)
                                LLNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>";
                            else
                                LLNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>";

                            $('#tbodyleftletterformanager,#tbodyActionLeftLetter').append($('<tr>')
                               .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                               .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                               .append($('<td>').html(value.ActionText))
                               .append($('<td>').html(value.DoorColour))
                               .append($('<td>').html(value.HouseType))
                               .append($('<td>').html(value.DateActioned))
                               .append($('<td>').html("" + value.Fees))
                               .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + LLNotes : value.Notes)))
                               .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                            break;
                        case 'Clamped':
                            $('#tbodyClampedformanager,#tbodyActionClamped')
                            .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseImage.html" target="_blank" ><img style="padding-right:10px;" src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image"  onclick="ViewClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                   '<img id="imgClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="clampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                            if (ClampMgr == 0) {
                                ViewClampedImage(key, value.Officer, value.CaseNumber);
                                $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'clampSelect');
                                ClampMgr += 1;
                            }
                            break;
                        case 'Bailed':
                            $('#tbodybailedformanager,#tbodyActionBailed').append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseImage.html" target="_blank" ><img style="padding-right:10px;" src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image"  onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                    '<img id="imgBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="BailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                            if (BailedMgr == 0) {
                                $('#imgBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'BailedSelect');
                                ViewBailedImage(key, "'" + value.ImageURL + "'");
                                BailedMgr += 1;
                            }
                            break;
                        case 'Arrested':
                            if ($.session.get('TreeLevel') == 3)
                                ArrestNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>";
                            else
                                ArrestNotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 8 + ")'> ...More</label>";

                            $('#tbodyarrestedformanager,#tbodyActionArrested').append($('<tr>')
                               .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                               .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                               .append($('<td>').html(value.ActionText))
                               .append($('<td>').html(value.DoorColour))
                               .append($('<td>').html(value.HouseType))
                               .append($('<td>').html(value.DateActioned))
                               .append($('<td>').html("" + value.Fees))
                               .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + ArrestNotes : value.Notes)))
                               .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                            break;
                        case 'Surrender date agreed':
                            if ($.session.get('TreeLevel') == 3)
                                SANotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 9 + ")'> ...More</label>";
                            else
                                SANotes = "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 9 + ")'> ...More</label>";

                            $('#tbodySurrenderdateagreedformanager,#tbodyActionSurrenderDateAgreed').append($('<tr>')
                               .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                               .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                               .append($('<td>').html(value.ActionText))
                               .append($('<td>').html(value.DoorColour))
                               .append($('<td>').html(value.HouseType))
                               .append($('<td>').html(value.DateActioned))
                               .append($('<td>').html("" + value.Fees))
                               .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + " " + SANotes : value.Notes)))
                               .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                            break;
                        case 'EnforcementStart':
                            $('#tbodyActionEnforcementStart').append($('<tr>')
                               .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                               .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                               .append($('<td>').html(value.ActionText))
                               .append($('<td>').html(value.DoorColour))
                               .append($('<td>').html(value.HouseType))
                               .append($('<td>').html(value.DateActioned))
                               .append($('<td>').html("" + value.Fees))
                               .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                               .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                            break;
                        case '£ Collected':
                        case 'Revisit':
                            $('#tbodyActionRevisit').append($('<tr>')
                               .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                               .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                               .append($('<td>').html(value.ActionText))
                               .append($('<td>').html(value.DoorColour))
                               .append($('<td>').html(value.HouseType))
                               .append($('<td>').html(value.DateActioned))
                               .append($('<td>').html("" + value.Fees))
                               .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                               .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                            break;
                        case 'EnforcementEnd':
                            $('#tbodyActionEnforcementEnd').append($('<tr>')
                               .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                               .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                               .append($('<td>').html(value.ActionText))
                               .append($('<td>').html(value.DoorColour))
                               .append($('<td>').html(value.HouseType))
                               .append($('<td>').html(value.DateActioned))
                               .append($('<td>').html("" + value.Fees))
                               .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                               .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                            break;
                    }
                });
            }
            else {
                $('#tblbodyActionPaid,#tbodyActionPP,#tbodyActionReturned').empty().html('No records found');
                $('#tbodyActionLeftLetter,#tbodyActionEnforcementStart,#tbodyActionRevisit').empty().html('No records found');
                $('#tbodyActionEnforcementEnd,#tbodyActionClamped,#tbodyActionBailed').empty().html('No records found');
                $('#tbodyActionArrested,#tbodyActionSurrenderDateAgreed').empty().html('No records found');
            }
        },
        complete: function () {
            $('#imgLoader8').fadeOut(1000);
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}


function FillCaseActionDetailsOnClick_Normal(Action, OfficerID, OfficerName, Check) {
    $('#HCaseActionNavigation').show();
    $('#btnCaseActionBack').show();
    $('#divCaseActionsManagerCount,#divCaseActionsADMCount,#divCaseActionsOfficerCount,#btnCaseActionManager').hide();
    $('#DRankingOfficer,#DRankMorePaidOfficer,#btnPaidOfficerBack,#DRankMoreReturnedOfficer,#btnReturnedOfficerBack,#DRankMorePPOfficer,#btnPPOfficerBack,#btnRankADMBack,#btnRankOfficerBack').hide();
    $('#tblRankOfficerReturn,#tblRankOfficerPartPaid,#tblRankOfficerPaid').hide();
    $('#lblCAOfficerName,#lblRankOfficerName,#lblRankPaidOfficer,#lblRankReturnedOfficer,#lblRankPPOfficer').html(' - ' + OfficerName);
    $('#DRMPaidOfficer,#DRMReturnOfficer,#DRMPPOfficer').hide();
    if (Check == 0) {
        switch (Action) {
            case 1:
                $('#lblRankStatus').html('Ranking - Paid');
                $('#DRankingCase,#tblRankOfficerPaid,#DRMPaidCase').show();//,#btnRankOfficerBack
                if ($.session.get('TreeLevel') == 2)
                    $('#btnRankOfficerBack').hide();
                else
                    $('#btnRankOfficerBack').show();

                $('#HCaseActionTitle').html('Paid');
                FillCaseDetailsAction_Normal(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 2:
                $('#lblRankStatus').html('Ranking - Returned');
                $('#DRankingCase,#tblRankOfficerReturn,#DRMReturnCase').show();

                if ($.session.get('TreeLevel') == 2)
                    $('#btnRankOfficerBack').hide();
                else
                    $('#btnRankOfficerBack').show();

                $('#HCaseActionTitle').html('Returned');
                FillCaseDetailsAction_Normal(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 3:
                $('#lblRankStatus').html('Ranking - Part Paid');
                $('#DRankingCase,#tblRankOfficerPartPaid,#DRMPPCase').show();
                if ($.session.get('TreeLevel') == 2)
                    $('#btnRankOfficerBack').hide();
                else
                    $('#btnRankOfficerBack').show();
                $('#HCaseActionTitle').html('Part Paid');
                FillCaseDetailsAction_Normal(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 4:
                FillCaseDetailsAction_Normal(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
        }
    }
    else {
        switch (Action) {
            case 1:
                FillCaseDetailsAction_Normal(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 2:
                FillCaseDetailsAction_Normal(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 3:
                FillCaseDetailsAction_Normal(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 4:
                FillCaseDetailsAction_Normal(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
        }
    }
}

function FillCaseDetailsAction_Normal(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Clamp = 0;
    Bailed = 0;

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    FromDate = newDate;

    var dateAr = ToDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    ToDate = newDate;
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + ManagerID + '/' + FromDate + '/' + ToDate + '/' + 'CaseActionssDetail?OfficerID=' + OfficerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader8').show();
        },
        complete: function () {
            $('#imgLoader8').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    $('#tblbodyActionPaid,#tbodyActionPP,#tbodyActionReturned,#tbodyActionLeftLetter').empty();
                    $('#tbodyActionEnforcementStart,#tbodyActionRevisit,#tbodyActionEnforcementEnd').empty();
                    $('#tbodyActionClamped,#tbodyActionBailed,#tbodyActionArrested,#tbodyActionSurrenderDateAgreed').empty();
                    $('#tbodyActionTCG,#tbodyActionTCGPAID,#tbodyActionTCGPP,#tbodyActionUTTC').empty();
                    $('#tbodyActionDROPPED,#tbodyActionOTHER').empty();
                    $('#tbodyRMPaidCase,#tbodyRMReturnCase,#tbodyRMPPCase').empty();
                    $('#tbodyRankOfficerPaid,#tbodyRankOfficerPartPaid,#tbodyRankOfficerReturn').empty();

                    $.each(result, function (key, value) {
                        switch (value.ActionText) {
                            case 'Paid':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerPaid,#tbodyRMPaidCase').append($('<tr style = "background:#66FF99">')
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerPaid,#tbodyRMPaidCase').append($('<tr style = "background:#FFCCFF">')
                                     .append($('<td>').html(''))
                                     .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                     .append($('<td>').html(value.ActionText))
                                     .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                     .append($('<td>').html(value.DateActioned))
                                     .append($('<td>').html("" + value.Fees))
                                     .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                     .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tblbodyActionPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                    $('#tbodyRankOfficerPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                    $('#tbodyRMPaidCase').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Part Paid':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerPartPaid,#tbodyRMPPCase').append($('<tr style = "background:#66FF99">')
                                    .append($('<td>').html(''))
                                    .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                    .append($('<td>').html(value.ActionText))
                                    .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                    .append($('<td>').html(value.DateActioned))
                                    .append($('<td>').html("" + value.Fees))
                                    .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                    .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)));

                                        $('#tbodyRankOfficerPartPaid,#tbodyRMPPCase').append($('<tr style = "background:#FFCCFF">')
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                    $('#tbodyRankOfficerPartPaid').append($('<tr>')
                                      .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td>').html(value.DoorColour))
                                      .append($('<td>').html(value.HouseType))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                    $('#tbodyRMPPCase').append($('<tr>')
                                                                          .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                                                          .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                                                          .append($('<td>').html(value.ActionText))
                                                                          .append($('<td>').html(value.DoorColour))
                                                                          .append($('<td>').html(value.HouseType))
                                                                          .append($('<td>').html(value.DateActioned))
                                                                          .append($('<td>').html("" + value.Fees))
                                                                          .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                                                          .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Returned':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionReturned').append($('<tr style = "background:#66FF99">')//,#tblbodyReturned
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerReturn,#tbodyRMReturnCase').append($('<tr style = "background:#66FF99">')//,#tblbodyReturned
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionReturned').append($('<tr style = "background:#FFCCFF">')//,#tblbodyReturned
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerReturn,#tbodyRMReturnCase').append($('<tr style = "background:#FFCCFF">')//,#tblbodyReturned
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionReturned').append($('<tr>')//,#tblbodyReturned
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                    $('#tbodyRankOfficerReturn').append($('<tr>')//,#tblbodyReturned
                                    .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                    .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                    .append($('<td>').html(value.ActionText))
                                    .append($('<td>').html(value.DoorColour))
                                    .append($('<td>').html(value.HouseType))
                                    .append($('<td>').html(value.DateActioned))
                                    .append($('<td>').html("" + value.Fees))
                                    .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                    .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                    $('#tbodyRMReturnCase').append($('<tr>')//,#tblbodyReturned
                                    .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                    .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                    .append($('<td>').html(value.ActionText))
                                    .append($('<td>').html(value.DoorColour))
                                    .append($('<td>').html(value.HouseType))
                                    .append($('<td>').html(value.DateActioned))
                                    .append($('<td>').html("" + value.Fees))
                                    .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                    .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                }
                                break;
                            case 'Left Letter':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionLeftLetter').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionLeftLetter').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionLeftLetter').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Clamped':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionClamped')
                                         .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                   '<img id="imgActionClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionclampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                        if (Clamp == 0) {
                                            ClampKey = key;
                                            ViewActionClampedImage(key, value.Officer, value.CaseNumber);
                                            $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionclampSelect');
                                            Clamp += 1;
                                        }
                                    }
                                    else {
                                        $('#tbodyActionClamped')
                                        .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                   '<img id="imgActionClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionclampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                        if (Clamp == 0) {
                                            ClampKey = key;
                                            ViewActionClampedImage(key, value.Officer, value.CaseNumber);
                                            $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionclampSelect');
                                            Clamp += 1;
                                        }
                                    }
                                }
                                else {
                                    $('#tbodyActionClamped')
                                       .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                   '<img id="imgActionClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionclampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                    if (Clamp == 0) {
                                        ClampKey = key;
                                        ViewActionClampedImage(key, value.Officer, value.CaseNumber);
                                        $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionclampSelect');
                                        Clamp += 1;
                                    }
                                }
                                break;
                            case 'Bailed':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionBailed')
                                     .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                    '<td>' + value.Officer + '</td>' +
                                                    '<td>' + value.DateActioned + '</td>' +
                                                    '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                     '<img id="imgActionBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionBailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                                        if (Bailed == 0) {
                                            BailedKey = key;
                                            $('#imgActionBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionBailedSelect');
                                            ViewActionBailedImage(key, "'" + value.ImageURL + "'");
                                            Bailed += 1;
                                        }
                                    }
                                    else {
                                        $('#tbodyActionBailed')
                                     .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                    '<td>' + value.Officer + '</td>' +
                                                    '<td>' + value.DateActioned + '</td>' +
                                                    '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                     '<img id="imgActionBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionBailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                                        if (Bailed == 0) {
                                            BailedKey = key;
                                            $('#imgActionBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionBailedSelect');
                                            ViewActionBailedImage(key, "'" + value.ImageURL + "'");
                                            Bailed += 1;
                                        }
                                    }
                                }
                                else {
                                    $('#tbodyActionBailed')
                                    .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                    '<img id="imgActionBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionBailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                                    if (Bailed == 0) {
                                        BailedKey = key;
                                        if ($.session.get('TreeLevel') == '3') {
                                            $('#imgActionBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionBailedSelect');
                                            ViewActionBailedImage(key, "'" + value.ImageURL + "'");
                                        }
                                        Bailed += 1;
                                    }
                                }
                                break;
                            case 'Arrested':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionArrested').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionArrested').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionArrested').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Surrender date agreed':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionSurrenderDateAgreed').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionSurrenderDateAgreed').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionSurrenderDateAgreed').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'EnforcementStart':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionEnforcementStart').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionEnforcementStart').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionEnforcementStart').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case '£ Collected':
                            case 'Revisit':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionRevisit').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionRevisit').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionRevisit').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'EnforcementEnd':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionEnforcementEnd').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionEnforcementEnd').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionEnforcementEnd').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;

                            case 'TCG':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionTCG').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'TCG PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPAID').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'TCG PP':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tblbodyActionPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'PART PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'UTTC':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionUTTC').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'DROPPED':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionDROPPED').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'OTHER':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionOTHER').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                        }
                    });
                }
                else {
                    $('#tblbodyActionPaid,#tbodyActionPP,#tbodyActionReturned').empty().html('No records found');
                    $('#tbodyActionLeftLetter,#tbodyActionEnforcementStart,#tbodyActionRevisit').empty().html('No records found');
                    $('#tbodyActionEnforcementEnd,#tbodyActionClamped,#tbodyActionBailed').empty().html('No records found');
                    $('#tbodyActionArrested,#tbodyActionSurrenderDateAgreed').empty().html('No records found');
                    $('#tbodyActionTCG,#tbodyActionTCGPAID,#tbodyActionTCGPP').empty().html('No records found');
                    $('#tbodyActionUTTC,#tbodyActionDROPPED,#tbodyActionOTHER').empty().html('No records found');
                }
            }
        },
        error: function () {
        } // When Service call fails
    });
}
//==========================================================================================================================================
//TODO : Case action function - End
//==========================================================================================================================================
//TODO : InfoWindow for map -Start 
//==========================================================================================================================================

function addInfoWindow(marker, message, showAll) {
    var infoWindow = new google.maps.InfoWindow({
        content: message
    });

    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.open(map, marker);
    });

    if (showAll == 1)
        markerCluster = new MarkerClusterer(map, marker, { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
}

function addExpandInfoWindow(marker, message, showAll) {
    var infoWindow = new google.maps.InfoWindow({
        content: message
    });

    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.open(map, marker);
    });

    if (showAll == 1)
        markerCluster = new MarkerClusterer(map, marker, { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
}

//==========================================================================================================================================
//TODO : InfoWindow for map -End 
//==========================================================================================================================================

// TODO: SignalR
function GetCaseActionsOfficer(parameter) {
    try {
        if (signalrconnected) {
            showOverlay("divCaseActionModaloverlaydiv");
            counter.server.getcaseactions(parameter);
        }
    } catch (e) {
        hideOverlay("divCaseActionModaloverlaydiv");
    }
}

function GetCaseActionManager(parameter, flag, ltype) {
    CA_TYPE = ltype;
    try {
        if (signalrconnected) {
            showOverlay("divCaseActionModaloverlaydiv");
            counter.server.getcaseactionmanager(parameter, flag, ltype);
        }
    } catch (e) {
        hideOverlay("divCaseActionModaloverlaydiv");
    }
}

// TODO : Not Use
function clickoncaseactionofficer(caseid, key, OfficerID, OfficerName, FromDate, ToDate) {
    caseactionshowhidetabs();

    FillCaseDetailsActionForManager($('#HCaseActionManagerID' + key).html(), FromDate, ToDate, OfficerID, '0');
    $('#HCaseActionNavigation').show();
    $('#btnCaseActionBack').show();
    $('#divCaseActionsManagerCount,#divCaseActionsOfficerCount,#btnCaseActionManager').hide();
    $('#DRankingOfficer').hide();

    //TODO: Rank more
    $('#DRankMorePaid,#DRankMorePaidOfficer,#btnPaidOfficerBack').hide();
    $('#DRankMoreReturned,#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').hide();
    $('#DRankMorePP,#DRankMorePPOfficer,#btnPPOfficerBack').hide();

    $('#lblCAOfficerName,#lblRankOfficerName,#lblRankPaidOfficer,#lblRankReturnedOfficer,#lblRankPPOfficer').html(' - ' + OfficerName);
    $('#tblRankOfficerReturn,#tblRankOfficerPartPaid,#tblRankOfficerPaid,#btnRankADMBack,#btnRankOfficerBack').hide();
    if ($.session.get('CompanyID') == 1) {
        switch (caseid) {
            case 1:
                $('#lblRankStatus').html('Ranking - Paid');
                $('#paidformanager,#DRankingCase,#tblRankOfficerPaid,#DRankMorePaidCaseList,#btnPaidOfficerBack').show();
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#lblRankStatus').html('Ranking - Part Paid');
                $('#partpaidformanager,#DRankingCase,#tblRankOfficerPartPaid,#DRankMorePPCaseList,#btnPPOfficerBack').show();
                $('#HCaseActionTitle').html('Part Paid');
                break;
            case 3:
                if ($.session.get('OfficerRole') == 9) {
                    $('#bailedformanager').show();
                    $('#HCaseActionTitle').html('Bailed');
                }
                else {
                    $('#revisitformanager').show();
                    $('#HCaseActionTitle').html('£ Collected');
                }
                break;
            case 4:
                $('#lblRankStatus').html('Ranking - Returned');
                $('#DRankingCase,#tblRankOfficerReturn,#DRankMoreReturnedCaseList,#btnReturnedOfficerBack').show();
                if ($.session.get('OfficerRole') == 9) {
                    $('#arrestedformanager').show();
                    $('#HCaseActionTitle').html('Arrested');
                }
                else {
                    $('#returnedformanager').show();
                    $('#HCaseActionTitle').html('Returned');
                }
                break;
            case 5:
                if ($.session.get('OfficerRole') == 9) {
                    $('#Surrenderdateagreedformanager').show();
                    $('#HCaseActionTitle').html('Surrender date agreed');
                }
                else {
                    $('#leftletterformanager').show();
                    $('#HCaseActionTitle').html('Left Letter');
                }
            case 6:
                $('#Clampedformanager').show();
                $('#HCaseActionTitle').html('Clamped');
                break;
            case 7:
                $('#deviatedformanager').show();
                $('#HCaseActionTitle').html('Deviation');
                break;
        }
    }
}

//==========================================================================================================================================
//TODO : Warrent matching function -Start
//==========================================================================================================================================

function FillWMA(ManagerID, FromDate, ToDate) {
    var inputParams = ManagerID.toString() + ',' + FromDate.toString() + ',' + $.session.get('OfficerRole') + ',' + $.session.get('CompanyID');
    WMAMGR_URL = inputParams;
    GetWMADetatil(WMAMGR_URL, "Manager");
}

function FillWMAADM(ManagerID, FromDate, ToDate) {
    var inputParams = ManagerID.toString() + ',' + FromDate.toString() + ',' + $.session.get('OfficerRole') + ',' + $.session.get('CompanyID');
    WMAADM_URL = inputParams;
    GetWMADetatil(WMAADM_URL, "ADM");
}

function FillWMAOfficer(ManagerID, FromDate, ToDate) {
    var inputParamsOfficer = ManagerID.toString() + ',' + FromDate.toString() + ',' + $.session.get('OfficerRole') + ',' + $.session.get('CompanyID');
    WMAOFF_URL = inputParamsOfficer;
    GetWMADetatil(WMAOFF_URL, "Officer");
}

function GetWMACase(OfficerID, FromDate, LogType, Flag) {
    if (Flag == 0) {
        $('#lblSearchType').html(' - ' + LogType);
        FillWMADetailL2(OfficerID, FromDate, LogType);
        showhidetabs('show-search-details');
        $('#btnBack').show();
        $('#btnWMAOfficerBack').hide();
    }
    else {
        $('#lblWMAMoreOfficerCaseName').html(' - ' + LogType);
        $('#DWMAMoreOfficerCase,#btnWMAMoreOfficerCase').show();
        $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer').hide();
        FillWMADetailL2(OfficerID, FromDate, LogType);
    }
}

function FillWMADetailL1(OfficerID, FromDate, ToDate) {
    var inputParams = OfficerID.toString() + ',' + FromDate.toString();
    GetWMALogDetail(inputParams, OfficerID);
}

function FillWMADetailL2(OfficerID, FromDate, LogType) {
    var inputParams = OfficerID.toString() + ',' + FromDate.toString() + ',' + LogType;
    GetWMACaseDetail(inputParams);
}

//TODO: SignalR

function GetWMADetatil(parameter, ltype) {
    WMAURL = parameter;
    WMAType = ltype;
    try {
        if (signalrconnected) {
            showOverlay("windowTitleDialogoverlaydiv");
            counter.server.getwmadetail(parameter, ltype);
        }
    } catch (e) {
        hideOverlay("windowTitleDialogoverlaydiv");
    }
}

function GetWMALogDetail(parameter, ltype) {
    try {
        if (signalrconnected) {
            showOverlay("windowTitleDialogoverlaydiv");
            counter.server.getwmalogdetail(parameter, ltype);
        }
    } catch (e) {
        hideOverlay("windowTitleDialogoverlaydiv");
    }
}

function GetWMACaseDetail(parameter) {

    try {
        if (signalrconnected) {
            showOverlay("windowTitleDialogoverlaydiv");
            counter.server.getwmacasedetail(parameter);
        }
    } catch (e) {
        hideOverlay("windowTitleDialogoverlaydiv");
    }
}

// TODO : Fill WMA Normal search
function FillWMA_Normal(ManagerID, FromDate, ToDate, OfficerID) {
    var SDate = FromDate;
    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    FromDate = newDate;
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + ManagerID + '/' + FromDate + '/' + FromDate + '/WMADashboard?OfficerID=' + OfficerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#tbodyWMA,#tbodyWMAMore,#tbodyWMAForOfficer').empty();
            $('#imgLoader10').show();
            $('#changeclass1 div').css('background', '#F9F9F9');
        },
        success: function (data) {
            if (data != undefined) {
                if (data != "[]" && data != null && data[0].Result != 'NoRecords') {
                    $('#AMoreWMA').show();
                    $('#tbodyWMA,#tbodyWMAMore').empty();
                    $('#tbodyWMAForOfficer,#tbodyWMAMoreOfficer,#tbodyWMAMoreADM').empty();
                    $.each(data, function (key, value) {
                        if (key <= 4) {
                            $('#tbodyWMA').append($('<tr>')
                                  .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                  .append($('<td>').append($('<a>').attr('id', 'AMGR' + value.OfficerId).attr('data-toggle', 'modal')
                                  .attr('href', '#windowTitleDialog').html(value.OfficerName))).append($('<td>').html(value.Activities)));
                        }

                        $('#tbodyWMAMore').append($('<tr>').append($('<td>').append($('<a>').attr('id', 'AMoreWMA' + value.OfficerId).attr('data-toggle', 'modal')
                              .attr('href', '#').html(value.OfficerName)))//windowTitleDialog
                            .append($('<td>').html(value.Activities)));

                        $('#tbodyWMAMoreADM').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">')
                            .attr('href', '#').attr('id', 'WMAMoreOfficer' + value.OfficerId)//.attr('data-toggle', 'modal')
                           .html(value.OfficerName))).append($('<td>').html(value.Activities)));

                        $('#tbodyWMAMoreOfficer').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">')
                            .attr('href', '#').attr('id', 'WMAMoreOfficerCase' + value.OfficerId)//.attr('data-toggle', 'modal')
                            .html(value.OfficerName))).append($('<td>').html(value.Activities)))

                        $('#WMAMoreOfficerCase' + value.OfficerId).click(function () {
                            if ($.session.get('TreeLevel') == 1) {
                                $('#DWMAMoreOfficer,#btnWMAMoreADM').show();
                                $('#DWMAMoreADM,#btnWMAMoreManager').hide();
                                $('#lblWMAMoreADMName').html(' - ' + $(this).html());
                                FillWMAOfficer_Normal(value.OfficerId, FromDate, FromDate, 0, 0);
                            }
                            else {
                                $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer').show();
                                $('#DWMAMoreOfficer,#btnWMAMoreADM').hide();
                                $('#lblWMAMoreOfficerName').html(' - ' + $(this).html());
                                FillWMADetailL1_Normal(value.OfficerId, FromDate, FromDate);
                            }
                        });

                        $('#WMAMoreOfficer' + value.OfficerId).click(function () {
                            if ($.session.get('TreeLevel') == 1) {
                                $('#DWMAMoreOfficer,#btnWMAMoreADM').show();
                                $('#DWMAMoreADM,#btnWMAMoreManager').hide();
                                $('#lblWMAMoreADMName').html(' - ' + $(this).html());
                                FillWMAOfficer_Normal(value.OfficerId, FromDate, FromDate, 0, 0);
                            }
                            else {
                                $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer').show();
                                $('#DWMAMoreOfficer,#btnWMAMoreADM').hide();
                                $('#lblWMAMoreOfficerName').html(' - ' + $(this).html());
                                FillWMADetailL1_Normal(value.OfficerId, FromDate, FromDate);
                            }
                        });


                        $('#AMGR' + value.OfficerId).click(function () {
                            $('#tblWMAADM,#btnWMAADMBack,#btnWMAOfficerBack,#btnBack').hide();
                            $('#lblWMAADM,#lblOfficerDisplay,#lblSearchType').html('');
                            $('#lblWMAManager').html($(this).html());
                            if ($.session.get('TreeLevel') == 0) {
                                $('#tblWMAADM').show();
                                FillWMAADM_Normal(value.OfficerId, FromDate, FromDate, OfficerID, 0);
                            }
                            else if ($.session.get('TreeLevel') == 1) {
                                FillWMAOfficer_Normal(value.OfficerId, FromDate, FromDate, OfficerID, 0);
                            }
                            else {
                                FillWMADetailL1_Normal(value.OfficerId, FromDate, FromDate);
                            }
                        });

                        $('#AMoreWMA' + value.OfficerId).click(function () {
                            $('#DWMAMoreADM,#btnWMAMoreManager').show();
                            $('#DWMAMoreManager').hide();
                            $('#lblWMAMoreManagerName').html(' - ' + $(this).html());
                            if ($.session.get('TreeLevel') == 0) {
                                $('#tblWMAADM').show();
                                FillWMAADM_Normal(value.OfficerId, FromDate, FromDate, OfficerID, 0);
                            }
                            else {
                                FillWMADetailL1_Normal(value.OfficerId, FromDate, FromDate);
                            }
                        });
                    });
                }
                else
                    $('#AMoreWMA').hide();
            }
            else
                $('#AMoreWMA').hide();
        },
        complete: function () {
            $('#imgLoader10').fadeOut(1000);
            $('#changeclass1 div').css('background', '');
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function FillWMAMore_Normal(ManagerID, FromDate, ToDate, OfficerID, GroupID) {

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    FromDate = newDate;
    ToDate = FromDate;

    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + ManagerID + '/' + FromDate + '/' + FromDate + '/WMADashboard?OfficerID=' + OfficerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#tbodyWMAMore').empty();
            $('#imgLoader3').show();
        },
        complete: function () {
            $('#imgLoader3').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    $('#tbodyWMAMore').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyWMAMore').append($('<tr>').append($('<td>').append($('<a>').attr('id', 'AMoreWMA' + value.OfficerId).attr('data-toggle', 'modal')
                              .attr('href', '#').html(value.OfficerName)))//windowTitleDialog
                            .append($('<td>').html(value.Activities)))
                        switch ($.session.get('TreeLevel')) {
                            case '0':
                                FillWMAADM_Normal($.session.get('ManagerID'), FromDate, ToDate, OfficerID, GroupID);
                                break;
                            case '1':
                                FillWMAADM_Normal($.session.get('ManagerID'), FromDate, ToDate, OfficerID, GroupID);
                                break;
                            case '2':
                            case '3':
                                FillWMAOfficer_Normal($.session.get('ManagerID'), FromDate, ToDate, 0, 0);
                                break;
                        }

                        $('#AMoreWMA' + value.OfficerId).click(function () {
                            $('#DWMAMoreADM,#btnWMAMoreManager').show();
                            $('#DWMAMoreManager').hide();
                            $('#lblWMAMoreManagerName').html(' - ' + $(this).html());
                            if ($.session.get('TreeLevel') == 0) {
                                $('#tblWMAADM').show();
                                FillWMAADM_Normal(value.OfficerId, FromDate, ToDate, OfficerID, GroupID);
                            }
                            else {
                                FillWMADetailL1_Normal($(this).attr('id'), FromDate, ToDate);
                            }
                        });
                    });
                }
            }
            else {
                $('#tblbodyWMASearchType').empty();
            }
        },
        error: function () {
        } // When Service call fails
    });
}

function FillWMAADM_Normal(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + ManagerID + '/' + FromDate + '/' + ToDate + '/WMADashboard?OfficerID=' + OfficerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#tbodyWMAForOfficer').empty();
            $('#tbodyWMAADM').empty();
            $('#tbodyWMAMoreADM').empty();
            $('#show-search-details').hide();
            $('#tblOfficer').hide();
            $('#imgLoader3,#imgLoader4, #imgLoader9').show();
            $('#tblWMAOfficers,#btnWMAADMBack').hide();
        },
        complete: function () {
            $('#imgLoader3, #imgLoader4, #imgLoader9').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    $('#tbodyWMAForOfficer,#tbodyWMAADM,#tbodyWMAMoreADM').empty();
                    $('#lblWMAManager').show();
                    $.each(result, function (key, value) {
                        $('#tbodyWMAADM').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">').attr('id', 'AADM' + value.OfficerId).attr('data-toggle', 'modal')
                            .html(value.OfficerName))).append($('<td>').html(value.Activities)));

                        $('#tbodyWMAForOfficer').append($('<tr>').append($('<td>').append($('<label>').html(value.OfficerName)))
                            .append($('<td>').html(value.Activities)));

                        $('#tbodyWMAMoreADM').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">').attr('href', '#').attr('id', 'WMAMoreOfficer' + value.OfficerId)//.attr('data-toggle', 'modal')
                            .html(value.OfficerName))).append($('<td>').html(value.Activities)));

                        $('#AADM' + value.OfficerId).click(function () {
                            $('#tblWMAADM').hide();
                            $('#lblWMAManager').show();
                            $('#lblWMAADM').html(' - ' + $(this).html());
                            if (OfficerID > 0) {
                                FillWMADetailL3_Normal(OfficerID, FromDate, ToDate);
                            }
                            else {
                                $('#btnWMAADMBack').show();
                                FillWMAOfficer_Normal(value.OfficerId, FromDate, ToDate, 0, 0);
                            }
                        });

                        $('#WMAMoreOfficer' + value.OfficerId).click(function () {
                            $('#DWMAMoreOfficer,#btnWMAMoreADM').show();
                            $('#DWMAMoreADM,#btnWMAMoreManager').hide();
                            $('#lblWMAMoreADMName').html(' - ' + $(this).html());
                            if (OfficerID > 0) {
                                FillWMADetailL3_Normal(OfficerID, FromDate, ToDate);
                            }
                            else {
                                FillWMAOfficer_Normal(value.OfficerId, FromDate, ToDate, 0, 0);
                            }
                        });
                    });
                }
                else {
                    if ($.session.get('CompanyID') == 1) {
                        $('#tblbodyWMASearchType').empty();//-- not comment
                    }
                    $('#tbodyWMAMoreOfficer').empty();
                    $('#tbodyWMAForOfficer').empty();
                    $('#AMoreWMA').hide();
                }
            }
        },
        error: function () {
        } // When Service call fails
    });
}

function FillWMAOfficer_Normal(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + ManagerID + '/' + FromDate + '/' + ToDate + '/WMADashboard?OfficerID=' + OfficerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#show-search-details').hide();
            $('#tblOfficer').hide();
            $('#imgLoader3,#imgLoader4').show();
            $('#tblWMAOfficers').show();
        },
        complete: function () {
            $('#imgLoader3,#imgLoader4').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call 
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    $('#tbodyWMAForOfficer').empty();
                    $('#tbodyWMAOfficers').empty();
                    $('#tbodyWMAMoreOfficer').empty();

                    $.each(result, function (key, value) {
                        $('#tbodyWMAOfficers').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">')
                            .attr('id', value.OfficerId).attr('data-toggle', 'modal')
                            .html(value.OfficerName))).append($('<td>').html(value.Activities)))

                        $('#tbodyWMAForOfficer').append($('<tr>').append($('<td>').append($('<label>').html(value.OfficerName)))
                            .append($('<td>').html(value.Activities)))

                        $('#tbodyWMAMoreOfficer').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">').attr('href', '#')
                            .attr('id', 'WMAMoreOfficer' + value.OfficerId)
                            .html(value.OfficerName))).append($('<td>').html(value.Activities)))


                        $('#' + value.OfficerId).click(function () {
                            $('#lblWMAManager').show();
                            $('#tblWMAOfficers,#btnWMAADMBack').hide();
                            $('#tblOfficer').show();
                            $('#lblOfficerDisplay').html(' - ' + $(this).html());
                            if (OfficerID > 0) {
                                FillWMADetailL3_Normal(OfficerID, FromDate, ToDate);
                            }
                            else {
                                FillWMADetailL1_Normal(value.OfficerId, FromDate, ToDate);
                            }
                        });
                        $('#WMAMoreOfficer' + value.OfficerId).click(function () {
                            $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer').show();
                            $('#DWMAMoreOfficer,#btnWMAMoreADM').hide();
                            $('#lblWMAMoreOfficerName').html(' - ' + $(this).html());
                            if (OfficerID > 0) {
                                FillWMADetailL3_Normal(OfficerID, FromDate, ToDate);
                            }
                            else {
                                FillWMADetailL1_Normal(value.OfficerId, FromDate, ToDate);
                            }
                        });
                    });
                }
                else {
                    if ($.session.get('CompanyID') == 1) {
                        $('#tbodyWMA').empty();
                        $('#tblbodyWMASearchType').empty();//-- not comment
                    }
                    $('#tbodyWMAOfficers').empty().html('No records found');
                    $('#tbodyWMAMoreOfficer').empty().html('No records found');
                    $('#tbodyWMAForOfficer').empty().html('No records found');
                    $('#AMoreWMA').hide();
                }
            }
        },
        error: function () {

        } // When Service call fails
    });
}

function FillWMADetailL1_Normal(OfficerID, FromDate, ToDate) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + OfficerID + '/' + FromDate + '/' + ToDate + '/WMADetail1Dashboard',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader3,#imgLoader4').show();
            $('#tblWMAADM,#tblWMAOfficers,#btnWMAADMBack').hide();
            $('#tblOfficer').show();
        },
        complete: function () {
            $('#imgLoader3,#imgLoader4').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    $('#tblbodySearchType').empty();
                    $('#tbodyWMAMoreOfficerCaseCount').empty();

                    showhidetabs('tblbodySearchType');
                    $('#lblSearchType').html('');
                    $('#btnBack,#btnWMAOfficerBack').hide();
                    $('#tblbodyWMASearchType').empty();
                    if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1) {
                        $('#btnWMAOfficerBack').show();
                    }
                    else {
                        $('#btnWMAOfficerBack').hide();
                    }
                    $.each(result, function (key, value) {
                        $('#tblbodySearchType').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">').attr('id', 'AOfficer' + value.LogType).html(value.LogType)))
                            .append($('<td>').html(value.Activities)))

                        $('#tbodyWMAMoreOfficerCaseCount').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">').attr('id', 'WMAMoreOfficerCase' + value.LogType)
                            .attr('href', '#').html(value.LogType)))
                          .append($('<td>').html(value.Activities)))

                        $('#tblbodyWMASearchType').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">').attr('id', 'A' + value.LogType)
                            .attr('data-toggle', 'modal').attr('href', '#windowTitleDialog').html(value.LogType))).append($('<td>').html(value.Activities)))

                        $('#A' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2_Normal(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                            $('#btnWMAOfficerBack').hide();
                        });


                        $('#WMAMoreOfficerCase' + value.LogType).click(function () {
                            $('#lblWMAMoreOfficerCaseName').html(' - ' + $(this).html());
                            $('#DWMAMoreOfficerCase').show();
                            $('#DWMAMoreOfficerCaseCount').hide();
                            FillWMADetailL2_Normal(OfficerID, FromDate, ToDate, value.LogType);
                            $('#btnWMAMoreOfficerCase').show();
                            $('#btnWMAMoreOfficer').hide();
                        });

                        $('#AOfficer' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2_Normal(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                            $('#btnWMAOfficerBack').hide();
                        })
                    });
                }
            }

        },
        error: function () {
        } // When Service call fails
    });
}

function FillWMADetailL2_Normal(OfficerID, FromDate, ToDate, LogType) {

    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + OfficerID + '/' + FromDate + '/' + ToDate + '/' + LogType + '/WMADetail2Dashboard',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase').empty();
            $('#imgLoader3,#imgLoader4').show();
        },
        complete: function () {
            $('#imgLoader3,#imgLoader4').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase').empty();
                    $.each(result, function (key, value) {
                        $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase').append($('<tr>')
                            .append($('<td>').html(value.SearchKey))
                            .append($('<td>').html(value.ResultReturned))
                            .append($('<td>').html(value.ActionedDate)))
                    });
                }
                else {
                    $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase').append($('<tr>').append($('<td colspan="3">').html('No warrant matching activity found for ' + LogType)));
                }
            }
        },
        error: function () {
        } // When Service call fails
    });

}

function FillWMADetailL3_Normal(OfficerID, FromDate, ToDate) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + OfficerID + '/' + FromDate + '/' + ToDate + '/WMADetail1Dashboard',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#tblbodySearchType').empty();
            $('#tblbodyWMASearchType').empty();
        },
        complete: function () {
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    $('#tblbodySearchType').empty();
                    showhidetabs('tblbodySearchType');
                    $('#lblSearchType').html('');
                    $('#btnBack').hide();
                    $('#tblbodyWMASearchType').empty();
                    $('#tblWMASearchType').show();
                    $('#tblWMA').hide();

                    $.each(result, function (key, value) {
                        $('#tblbodySearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', value.LogType).html(value.LogType)))
                            .append($('<td>').html(value.Activities)))

                        $('#tblbodyWMASearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', 'A' + value.LogType)
                            .attr('data-toggle', 'modal').attr('href', '#windowTitleDialog').html(value.LogType))).append($('<td>').html(value.Activities)))

                        $('#A' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2_Normal(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        });

                        $('#' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2_Normal(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        })
                    });
                }
            }
        },
        error: function () {
        } // When Service call fails
    });
}

//==========================================================================================================================================
//TODO : Warrent matching function -End
//==========================================================================================================================================
//TODO : Working Hours function

function GetWorkingHours(OfficerID) {
    $.ajax({
        url: ServiceURL + 'api/v1/officers/OfficersWorkingHours',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'GET',
        dataType: DataType,
        data: JSON.stringify({ 'OfficerID': OfficerID, 'ShowAll': 0 }),
        beforeSend: function () {
        },
        success: function (data) {
            if (!(data == '' || data == null || data == '[]')) {
                $('#AMoreWorkingHours').show();
                $('#tbodyWorkingHours').empty();
                $.each(data, function (key, value) {
                    if (value.CallId == '9999' || value.CallId == '99991') {
                        $('#thWorkingName').html('Manager Name');
                        $('#tbodyWorkingHours').append($('<tr>').append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#DivWorkingHoursMore')
                                                                    .attr('onclick', 'GetWorkingHoursMore(' + value.OfficerID + ',0' + ')').html(value.OfficerName)))
                                                                .append($('<td id="tdbodyWorkingHours' + key + '">').html(value.AvgHours))
                                                                .append($('<td>').html(value.PredictedHours)))
                    }
                    else {
                        $('#thWorkingName').html('Officer Name');
                        $('#tbodyWorkingHours').append($('<tr>').append($('<td>').html(value.OfficerName))
                                                           .append($('<td id="tdbodyWorkingHours' + key + '">').html(value.AvgHours))
                                                           .append($('<td>').html(value.PredictedHours)))
                    }
                });
            }
            else {
                $('#tbodyWorkingHours').empty().append('<tr><td colspan="3">No data found</td></tr>');
                $('#AMoreWorkingHours').hide();
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function GetWorkingHoursMore(OfficerID, Flag) {
    $('#DivWorkingHoursOfficersList').hide();
    $('#DivWorkingHoursManagersList').show();
    $('#btnWorkingHoursBack').hide();
    $.ajax({
        url: ServiceURL + 'api/v1/officers/OfficersWorkingHours',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'GET',
        dataType: DataType,
        data: JSON.stringify({ 'OfficerID': OfficerID, 'ShowAll': Flag }),
        beforeSend: function () {
        },
        success: function (data) {
            $('#tbodyWorkingHoursManagersList').empty();
            if (!(data == '' || data == null || data == '[]')) {
                $.each(data, function (key, value) {
                    if (value.CallId == '9999' || value.CallId == '99991') {
                        $('#thWorkingName').html('Manager Name');
                        $('#tbodyWorkingHoursManagersList').append($('<tr>').append($('<td>').append($('<a>').attr('style', 'cursor:pointer;')
                                                                .attr('onclick', 'GetWorkingHoursForOfficers(' + value.OfficerID + ')').html(value.OfficerName)))
                                                           .append($('<td id="tdbodyWorkingHoursManagerList' + key + '">').html(value.AvgHours))
                                                           .append($('<td>').html(value.PredictedHours)))
                    }
                    else {
                        $('#thWorkingName').html('Officer Name');
                        $('#tbodyWorkingHoursManagersList').append($('<tr>').append($('<td>').html(value.OfficerName))
                                                       .append($('<td id="tdbodyWorkingHoursManagerList' + key + '">').html(value.AvgHours))
                                                       .append($('<td>').html(value.PredictedHours)))
                    }
                });
            }
            else {
                $('#tbodyWorkingHoursManagersList').append('<tr><td colspan="3">No data found </td></tr>');
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

function GetWorkingHoursForOfficers(OfficerID) {
    $('#DivWorkingHoursOfficersList').show();
    $('#DivWorkingHoursManagersList').hide();
    $('#btnWorkingHoursBack').show();

    $.ajax({
        url: ServiceURL + 'api/v1/officers/OfficersWorkingHours',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'GET',
        dataType: DataType,
        data: JSON.stringify({ 'OfficerID': OfficerID, 'ShowAll': 0 }),
        beforeSend: function () {
        },
        success: function (data) {
            $('#tbodyWorkingHoursManagersList').empty();
            if (!(data == '' || data == null || data == '[]')) {
                $('#tbodyWorkingHoursOfficersList').empty();
                $.each(data, function (key, value) {
                    $('#tbodyWorkingHoursOfficersList').append($('<tr>').append($('<td>').html(value.OfficerName))
                                                            .append($('<td id="tdbodyWorkingHoursOfficerList' + key + '">').html(value.AvgHours))
                                                            .append($('<td>').html(value.PredictedHours)))
                });
            }
            else {
                $('#tbodyWorkingHoursOfficersList').append('<tr><td colspan="3">No data found </td></tr>');
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

//==========================================================================================================================================
//TODO : Map functionality -Start
//==========================================================================================================================================

function FillLastKnownLocation(ManagerID, FromDate, ToDate, OfficerID, GroupID, ShowAll) {
    if (ManagerID == $.session.get('OfficerID') && OfficerID == undefined) ManagerID = $.session.get('ManagerID');
    MID = ManagerID;

    if (OfficerID == undefined) {
        if ($('#lblSelectedOfficerID').html() == undefined || $.trim($('#lblSelectedOfficerID').html()) == '')
            OfficerID = 0;
        else
            OfficerID = $('#lblSelectedOfficerID').html();
    }
    OID = OfficerID;
    GID = GroupID;
    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;
    if (ShowAll == undefined) ShowAll = 0;

    FD = FromDate;
    TD = FromDate;
    MapFillLastKnownLocation(MID, FD, TD, OID, GID, ShowAll, 1);
}

function MapFillLastKnownLocation(mID, fDate, tDate, oID, gID, ShowAll, Condition) {
    $('#DHeartBeatMap,#DLocationHBMap').hide();
    if (Condition == 1) {
        $('#map_canvas').show();
    }
    else {
        $('#map_canvas_Expand').show();
    }

    if (gID == undefined) gID = 0;
    var inputParams = mID + ',' + fDate + ',' + $.trim(oID) + ',' + $.trim(gID) + ',' + $.session.get('CompanyID') + ',' + $.session.get('OfficerRole');

    if (Condition == 2) {
        setTimeout(function () {
            GetLastLocation(inputParams, ShowAll, Condition);
        }, 1000);
    }
    else {
        GetLastLocation(inputParams, ShowAll, Condition);
    }
}

function HeartBeat(Condition) {
    var dateAr = $('#txtFromDateLocation').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    var inputParams = OffID + ',' + newDate + ',' + $.session.get('CompanyID') + ',' + $.session.get('OfficerRole');
    GetHeartBeat(inputParams, Condition);
}

function LocationwithHB(Condition) {
    var dateAr = $('#txtFromDateLocation').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    var inputParams = OffID + ',' + newDate + ',' + $.session.get('CompanyID') + ',' + $.session.get('OfficerRole');
    GetLocationHB(inputParams, Condition);
}

function LocationwithHB_Normal(Condition) {
    var FDate, TDate = "";
    FDate = $.trim($('#txtFromDateLocation').val().replace(/[^0-9\/]/g, '').replace(/[\/]/g, '-'));
    GetLocationHB_Normal(FDate, FDate, OffID, Condition);
}

function FillAllLocation(Condition) {
    var maptype = $.session.get('Map');
    if (maptype == 'All') {
        Condition = 1;
    }
    else if (maptype == 'AllEnlarge') {
        Condition = 2;
    }
    var FDate = $.trim($('#txtFromDateLocation').val().replace(/[^0-9\/]/g, '').replace(/[\/]/g, '/'));
    if (Condition == 1)
        LastKnownLocation_Normal(0, FDate, OID, 1);
    else
        LastKnownLocationExpand_Normal(0, FDate, OID, 1);
}

function GetLocationHB_Normal(fDate, tDate, oID, Condition) {
    var maptype = $.session.get('Map');
    if (maptype == 'LocHB') {
        Condition = 1;
    }
    else if (maptype == 'LocHBEnlarge') {
        Condition = 2;
    }
    $('#map_canvas,#DHeartBeatMap').hide();
    $('#DLocationHBMap').show();
    var Parameter = "OfficerID=" + oID + "&FromDate=" + fDate + "&ToDate=" + tDate + "&IsSc=0";
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/LocationHB?' + Parameter,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader12').show();
            $('#lblDisplay').html('').hide();
            markers = [];
        },
        success: function (data, textStatus, xhr) {
            if (!(data == '' || data == null || data == '[]' || data == '[{"Result":"NoRecords"}]')) {
                var bounds = new google.maps.LatLngBounds();
                var iconImg1;
                center = new google.maps.LatLng(51.699467, 0.109348);
                options = {
                    'zoom': 5,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP,
                    'fullscreenControl': true
                };
                if (Condition == 1) {
                    map = new google.maps.Map(document.getElementById("DLocationHBMap"), options);
                    infowindow = new google.maps.InfoWindow();
                    $.each(data, function (key, value) {
                        switch (value.IconType) {
                            case 1: // Paid , Part paid
                                iconImg1 = "images/map-icon-yellow.png";
                                break;
                            case 2:  // Returned
                                iconImg1 = "images/map-icon-red.png";
                                break;
                            case 3:  // Revisit
                                iconImg1 = "images/map-icon-green.png";
                                break;
                            case 5: // HeartBeat
                                iconImg1 = "images/map-icon-pink.png";
                                break;
                            default:
                                iconImg1 = "images/map-icon-white.png";
                        }
                        latLng = new google.maps.LatLng(value.latitude, value.longitude);
                        marker = new google.maps.Marker({
                            'position': latLng,
                            'map': map,
                            'icon': iconImg1
                        });
                        markers.push(marker);
                        bounds.extend(marker.position);
                        google.maps.event.addListener(markers[key], 'click', function (e) {
                            infowindow.setContent(value.html);
                            infowindow.open(map, this);
                        });
                        map.fitBounds(bounds);
                    });
                }
                else {
                    map = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);
                    infowindow = new google.maps.InfoWindow();
                    $.each(data, function (key, value) {
                        switch (value.IconType) {
                            case 1: // Paid , Part paid
                                iconImg1 = "images/map-icon-yellow.png";
                                break;
                            case 2:  // Returned
                                iconImg1 = "images/map-icon-red.png";
                                break;
                            case 3:  // Revisit
                                iconImg1 = "images/map-icon-green.png";
                                break;
                            case 5: // HeartBeat
                                iconImg1 = "images/map-icon-pink.png";
                                break;
                            default:
                                iconImg1 = "images/map-icon-white.png";
                        }
                        latLng = new google.maps.LatLng(value.latitude, value.longitude);
                        marker = new google.maps.Marker({
                            'position': latLng,
                            'map': map,
                            'icon': iconImg1
                        });
                        markers.push(marker);
                        bounds.extend(marker.position);
                        google.maps.event.addListener(markers[key], 'click', function (e) {
                            infowindow.setContent(value.html);
                            infowindow.open(map, this);
                        });
                        map.fitBounds(bounds);
                    });
                }
            }
            else {
                $('#lblDisplay').html('No activity recorded for today!').show();
                center = new google.maps.LatLng(52.8849565, -1.9770329);
                options = {
                    'zoom': 6,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP,
                    'fullscreenControl': true
                };
                if (Condition == 1)
                    map = new google.maps.Map(document.getElementById("DLocationHBMap"), options);
                else {
                    map = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);
                }
            }
        },
        complete: function () {
            $('#imgLoader12').fadeOut(1000);
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function GetAllLocationCluster(fDate, tDate, oID, Condition) {
    $('#DLocationHBMap,#DHeartBeatMap').hide();
    $('#map_canvas').show();
    var Parameter = "OfficerID=" + oID + "&FromDate=" + fDate + "&ToDate=" + tDate + "&IsSc=0";

    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/LocationHB?' + Parameter,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader12').show();
            $('#lblDisplay').html('').hide();
            markers = [];
        },
        success: function (data, textStatus, xhr) {
            if (!(data == '' || data == null || data == '[]' || data == '[{"Result":"NoRecords"}]')) {
                var bounds = new google.maps.LatLngBounds();
                var iconImg1;
                center = new google.maps.LatLng(51.699467, 0.109348);
                options = {
                    'zoom': 5,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP,
                    'fullscreenControl': true
                };
                if (Condition == 1) {
                    map = new google.maps.Map(document.getElementById("map_canvas"), options);
                    infowindow = new google.maps.InfoWindow();
                    $.each(data, function (key, value) {
                        if (value.IconType != 5) {
                            switch (value.IconType) {
                                case '1': // UTTC
                                    iconImg1 = "images/map-icon-yellow.png";
                                    break;
                                case '2':  // Returned, DROPPED
                                    iconImg1 = "images/map-icon-red.png";
                                    break;
                                case '3':  // Revisit,TCG
                                    iconImg1 = "images/map-icon-floresent.png";
                                    break;
                                case '6':  // Login
                                    iconImg1 = "images/map-icon-blue.png";
                                    break;
                                case '7':  // ARR
                                    iconImg1 = "images/map-icon-navyblue.png";
                                    break;
                                case '8':  // DEP
                                    iconImg1 = "images/map-icon-lightvio.png";
                                    break;
                                case '9':  // Logout
                                    iconImg1 = "images/map-icon-grey.png";
                                    break;
                                default:
                                    iconImg1 = "images/map-icon-white.png";
                            }
                            latLng = new google.maps.LatLng(value.latitude, value.longitude);
                            marker = new google.maps.Marker({
                                'position': latLng,
                                'map': map,
                                'icon': iconImg1
                            });
                            markers.push(marker);
                            bounds.extend(marker.position);
                            google.maps.event.addListener(markers[key], 'click', function (e) {
                                infowindow.setContent(value.html);
                                infowindow.open(map, this);
                            });
                            map.fitBounds(bounds);
                        }
                    });
                }

            }
            else {
                $('#lblDisplay').html('No activity recorded for today!').show();
                center = new google.maps.LatLng(52.8849565, -1.9770329);
                options = {
                    'zoom': 6,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP,
                    'fullscreenControl': true
                };
                if (Condition == 1)
                    map = new google.maps.Map(document.getElementById("DLocationHBMap"), options);
                else {
                    map = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);
                }
            }
        },
        complete: function () {
            $('#imgLoader12').fadeOut(1000);
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

//TODO: SignalR
function GetLastLocation(parameter, showAll, Condition) {
    showOverlay("DLastLocationoverlaydiv");

    LocationURL = parameter;

    try {
        if (signalrconnected) {
            counter.server.getlastlocation(parameter, showAll, Condition);
        }
    }
    catch (e) {
    }
}

function GetHeartBeat(parameter, Condition) {
    try {
        if (signalrconnected) {
            counter.server.getheartbeat(parameter, Condition);
        }
    }
    catch (e) {
    }
}

function GetLocationHB(parameter, Condition) {
    try {
        if (signalrconnected) {
            counter.server.getlocationheartbeat(parameter, Condition);
        }
    }
    catch (e) {
    }
}

//Normal case last location
function LastKnownLocation_Normal(mID, fDate, OfficerID, Type) {
    $('#DHeartBeatMap,#DLocationHBMap').hide();
    $('#map_canvas').show();
    var dateAr = fDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + mID + '/' + newDate + '/' + newDate + '/LastLocation?OfficerID=' + OfficerID + '&ShowAll=' + Type,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader12').show();
            $('#map_canvas').empty();

            markers = [];
            Latitude = [];
            Longitude = [];
            Icon = [];
            Content = [];
            pLatitude = 0;
            pLongitude = 0;
            pLatMap = 0;
            pLongMap = 0;
        },
        success: function (data) {
            if (!(data == '' || data == null || data == '[]' || data["Result"] == 'NoRecords')) {
                if (Type == 1) {// Cluster
                    $('#lblDisplay').html('').hide();
                    var iconImg1;
                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP,
                        'fullscreenControl': true
                    };
                    var RBounds = new google.maps.LatLngBounds();
                    map = new google.maps.Map(document.getElementById("map_canvas"), options);
                    infowindow = new google.maps.InfoWindow();

                    $.each(data, function (key, value) {
                        switch (value.IconType) {
                            case 1: // UTTC
                                iconImg1 = "images/map-icon-yellow.png";
                                break;
                            case 2:  // Returned, DROPPED
                                iconImg1 = "images/map-icon-red.png";
                                break;
                            case 3:  // Revisit,TCG
                                iconImg1 = "images/map-icon-floresent.png";
                                break;
                            case 5:  //  Paid , Part paid , TCG PP,TCG Paid 
                                iconImg1 = "images/map-icon-orange.png";
                                break;
                            case 6:  // Login
                                iconImg1 = "images/map-icon-blue.png";
                                break;
                            case 7:  // ARR
                                iconImg1 = "images/map-icon-navyblue.png";
                                break;
                            case 8:  // DEP
                                iconImg1 = "images/map-icon-lightvio.png";
                                break;
                            case 9:  // Logout
                                iconImg1 = "images/map-icon-grey.png";
                                break;
                            default:
                                iconImg1 = "images/map-icon-white.png";
                        }
                        Latitude[key] = value.latitude;
                        Longitude[key] = value.longitude;
                        if (Latitude[key] != pLatMap && Longitude[key] != pLongMap) {
                            Icon[key] = iconImg1;
                            Content[key] = value.html;
                            pContentkey = key;
                        }
                        else {
                            Content[pContentkey] = Content[pContentkey] + '<br><hr>' + value.html;
                        }
                        pLatMap = value.latitude;
                        pLongMap = value.longitude;

                    });
                    for (var i = 0; i < data.length; i++) {
                        if (Latitude[i] != pLatitude && Longitude[i] != pLongitude) {
                            latLng = new google.maps.LatLng(Latitude[i], Longitude[i]);
                            marker = new google.maps.Marker({
                                'position': latLng,
                                'map': map,
                                'icon': Icon[i]
                            });
                            markers.push(marker);
                            RBounds.extend(marker.position);
                            if (Content[i] != undefined) {
                                addInfoWindow(marker, Content[i]);
                            }
                            map.fitBounds(RBounds);
                        }
                        pLatitude = Latitude[i];
                        pLongitude = Longitude[i];
                    }
                    markerCluster = new MarkerClusterer(map, markers, { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
                }
                else {// Last location
                    $('#lblDisplay').html('').hide();
                    var iconImg1;
                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP,
                        'fullscreenControl': true
                    };
                    var RBounds = new google.maps.LatLngBounds();
                    map = new google.maps.Map(document.getElementById("map_canvas"), options);

                    infowindow = new google.maps.InfoWindow();

                    $.each(data, function (key, value) {
                        switch (value.IconType) {
                            case 1: // UTTC
                                iconImg1 = "images/map-icon-yellow.png";
                                break;
                            case 2:  // Returned, DROPPED
                                iconImg1 = "images/map-icon-red.png";
                                break;
                            case 3:  // Revisit,TCG
                                iconImg1 = "images/map-icon-floresent.png";
                                break;
                            case 5:  //  Paid , Part paid , TCG PP,TCG Paid 
                                iconImg1 = "images/map-icon-orange.png";
                                break;
                            case 6:  // Login
                                iconImg1 = "images/map-icon-blue.png";
                                break;
                            case 7:  // ARR
                                iconImg1 = "images/map-icon-navyblue.png";
                                break;
                            case 8:  // DEP
                                iconImg1 = "images/map-icon-lightvio.png";
                                break;
                            case 9:  // Logout
                                iconImg1 = "images/map-icon-grey.png";
                                break;
                            default:
                                iconImg1 = "images/map-icon-white.png";
                        }
                        Latitude[key] = value.latitude;
                        Longitude[key] = value.longitude;
                        if (Latitude[key] != pLatMap && Longitude[key] != pLongMap) {
                            Icon[key] = iconImg1;
                            Content[key] = value.html;
                            pContentkey = key;
                        }
                        else {
                            Content[pContentkey] = Content[pContentkey] + '<br><hr>' + value.html;
                        }
                        pLatMap = value.latitude;
                        pLongMap = value.longitude;
                    });

                    for (var i = 0; i < data.length; i++) {
                        if (Latitude[i] != pLatitude && Longitude[i] != pLongitude) {
                            latLng = new google.maps.LatLng(Latitude[i], Longitude[i]);
                            marker = new google.maps.Marker({
                                'position': latLng,
                                'map': map,
                                'icon': Icon[i]
                            });
                            markers.push(marker);
                            RBounds.extend(marker.position);
                            if (Content[i] != undefined) {
                                addInfoWindow(marker, Content[i]);
                            }
                            map.fitBounds(RBounds);
                        }
                        pLatitude = Latitude[i];
                        pLongitude = Longitude[i];
                    }
                }

            }
            else {
                $('#lblDisplay').html('No activity recorded for today!').show();
                center = new google.maps.LatLng(52.8849565, -1.9770329);
                options = {
                    'zoom': 6,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById("map_canvas"), options);
            }
        },
        complete: function () {
            $('#imgLoader12').fadeOut(1000);
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function LastKnownLocationExpand_Normal(mID, fDate, OfficerID, Type) {
    $('#DHeartBeatMap,#DLocationHBMap').hide();
    $('#map_canvas_Expand').show();
    var dateAr = fDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + mID + '/' + newDate + '/' + newDate + '/LastLocation?OfficerID=' + OfficerID + '&ShowAll=' + Type,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader12').show();
            $('#map_canvas_Expand').empty();

            markers = [];
            Latitude = [];
            Longitude = [];
            Icon = [];
            Content = [];
            pLatitude = 0;
            pLongitude = 0;
            pLatMap = 0;
            pLongMap = 0;
        },
        success: function (data) {
            if (!(data == '' || data == null || data == '[]' || data["Result"] == 'NoRecords')) {
                if (Type == 1) {// Cluster

                    $('#lblDisplay').html('').hide();
                    var iconImg1;
                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP
                    };
                    var RBounds = new google.maps.LatLngBounds();
                    map = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);

                    infowindow = new google.maps.InfoWindow();

                    $.each(data, function (key, value) {
                        switch (value.IconType) {
                            case 1: // UTTC
                                iconImg1 = "images/map-icon-yellow.png";
                                break;
                            case 2:  // Returned, DROPPED
                                iconImg1 = "images/map-icon-red.png";
                                break;
                            case 3:  // Revisit,TCG
                                iconImg1 = "images/map-icon-floresent.png";
                                break;
                            case 5:  //  Paid , Part paid , TCG PP,TCG Paid 
                                iconImg1 = "images/map-icon-orange.png";
                                break;
                            case 6:  // Login
                                iconImg1 = "images/map-icon-blue.png";
                                break;
                            case 7:  // ARR
                                iconImg1 = "images/map-icon-navyblue.png";
                                break;
                            case 8:  // DEP
                                iconImg1 = "images/map-icon-lightvio.png";
                                break;
                            case 9:  // Logout
                                iconImg1 = "images/map-icon-grey.png";
                                break;
                            default:
                                iconImg1 = "images/map-icon-white.png";
                        }
                        Latitude[key] = value.latitude;
                        Longitude[key] = value.longitude;
                        if (Latitude[key] != pLatMap && Longitude[key] != pLongMap) {
                            Icon[key] = iconImg1;
                            Content[key] = value.html;
                            pContentkey = key;
                        }
                        else {
                            Content[pContentkey] = Content[pContentkey] + '<br><hr>' + value.html;
                        }
                        pLatMap = value.latitude;
                        pLongMap = value.longitude;

                    });
                    for (var i = 0; i < data.length; i++) {
                        if (Latitude[i] != pLatitude && Longitude[i] != pLongitude) {
                            latLng = new google.maps.LatLng(Latitude[i], Longitude[i]);
                            marker = new google.maps.Marker({
                                'position': latLng,
                                'map': map,
                                'icon': Icon[i]
                            });
                            markers.push(marker);
                            RBounds.extend(marker.position);
                            if (Content[i] != undefined) {
                                addInfoWindow(marker, Content[i]);
                            }
                            map.fitBounds(RBounds);
                        }
                        pLatitude = Latitude[i];
                        pLongitude = Longitude[i];
                    }
                    markerCluster = new MarkerClusterer(map, markers, { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
                }
                else {

                    $('#lblDisplay').html('').hide();
                    var iconImg1;
                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP,
                        'fullscreenControl': true
                    };
                    var RBounds = new google.maps.LatLngBounds();
                    map = new google.maps.Map(document.getElementById("DHeartBeatMap"), options);
                    map = new google.maps.Map(document.getElementById("DLocationHBMap"), options);
                    map = new google.maps.Map(document.getElementById("map_canvas"), options);
                    map = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);

                    infowindow = new google.maps.InfoWindow();

                    $.each(data, function (key, value) {
                        switch (value.IconType) {
                            case 1: // UTTC
                                iconImg1 = "images/map-icon-yellow.png";
                                break;
                            case 2:  // Returned, DROPPED
                                iconImg1 = "images/map-icon-red.png";
                                break;
                            case 3:  // Revisit,TCG
                                iconImg1 = "images/map-icon-floresent.png";
                                break;
                            case 5:  //  Paid , Part paid , TCG PP,TCG Paid 
                                iconImg1 = "images/map-icon-orange.png";
                                break;
                            case 6:  // Login
                                iconImg1 = "images/map-icon-blue.png";
                                break;
                            case 7:  // ARR
                                iconImg1 = "images/map-icon-navyblue.png";
                                break;
                            case 8:  // DEP
                                iconImg1 = "images/map-icon-lightvio.png";
                                break;
                            case 9:  // Logout
                                iconImg1 = "images/map-icon-grey.png";
                                break;
                            default:
                                iconImg1 = "images/map-icon-white.png";
                        }
                        Latitude[key] = value.latitude;
                        Longitude[key] = value.longitude;
                        if (Latitude[key] != pLatMap && Longitude[key] != pLongMap) {
                            Icon[key] = iconImg1;
                            Content[key] = value.html;
                            pContentkey = key;
                        }
                        else {
                            Content[pContentkey] = Content[pContentkey] + '<br><hr>' + value.html;
                        }
                        pLatMap = value.latitude;
                        pLongMap = value.longitude;
                    });

                    for (var i = 0; i < data.length; i++) {
                        if (Latitude[i] != pLatitude && Longitude[i] != pLongitude) {
                            latLng = new google.maps.LatLng(Latitude[i], Longitude[i]);
                            marker = new google.maps.Marker({
                                'position': latLng,
                                'map': map,
                                'icon': Icon[i]
                            });
                            markers.push(marker);
                            RBounds.extend(marker.position);
                            if (Content[i] != undefined) {
                                addInfoWindow(marker, Content[i]);
                            }
                            map.fitBounds(RBounds);
                        }
                        pLatitude = Latitude[i];
                        pLongitude = Longitude[i];
                    }
                }
            }
            else {
                $('#lblDisplay').html('No activity recorded for today!').show();
                center = new google.maps.LatLng(52.8849565, -1.9770329);
                options = {
                    'zoom': 6,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP,
                    'fullscreenControl': true
                };
                map = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);

            }
        },
        complete: function () {
            $('#imgLoader12').fadeOut(1000);
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function HeartBeat_Normal(fDate, OfficerID, Type) {
    var dateAr = fDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + OfficerID + '/OfficerHeartBeat?FromDate=' + newDate + '&ToDate=' + newDate,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader12').show();
            $('#map_canvas,#DLocationHBMap').hide();
            $('#DHeartBeatMap').show();
        },
        success: function (data) {
            markers = [];
            if (!(data == '' || data == null || data == '[]' || data["Result"] == 'NoRecords')) {
                center = new google.maps.LatLng(51.699467, 0.109348);
                options = {
                    'zoom': 5,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP,
                    'fullscreenControl': true
                };

                if (Type == 1) {
                    map = new google.maps.Map(document.getElementById("DHeartBeatMap"), options);
                    var polyline = new google.maps.Polyline({
                        path: [],
                        strokeColor: "#FF0000",
                        strokeOpacity: 1.0,
                        strokeWeight: 8,
                        map: map
                    });

                    var Hbounds = new google.maps.LatLngBounds();

                    infowindow = new google.maps.InfoWindow();
                    $.each(data, function (key, value) {
                        latLng = new google.maps.LatLng(value.latitude, value.longitude);
                        marker = new google.maps.Marker({
                            'position': latLng,
                            'map': map,
                            'icon': "images/map-icon-pink.png"
                        });
                        markers.push(marker);
                        Hbounds.extend(marker.position);
                        map.panTo(latLng);
                        var path = polyline.getPath().getArray();
                        path.push(latLng);
                        polyline.setPath(path);
                        google.maps.event.addListener(markers[key], 'click', function (e) {
                            infowindow.setContent(value.html + '<br>' + value.latitude + ',' + value.longitude);
                            infowindow.open(map, this);
                        });
                        map.fitBounds(Hbounds);
                    });
                }
                else {
                    map = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);
                    var Hbounds = new google.maps.LatLngBounds();
                    infowindow = new google.maps.InfoWindow();
                    var polyline = new google.maps.Polyline({
                        path: [],
                        strokeColor: "#FF0000",
                        strokeOpacity: 1.0,
                        strokeWeight: 8,
                        map: map
                    });
                    $.each(data, function (key, value) {
                        latLng = new google.maps.LatLng(value.latitude, value.longitude);
                        marker = new google.maps.Marker({
                            'position': latLng,
                            'map': map,
                            'icon': "images/map-icon-pink.png"
                        });
                        markers.push(marker);
                        Hbounds.extend(marker.position);
                        map.panTo(latLng);
                        var path = polyline.getPath().getArray();
                        path.push(latLng);
                        polyline.setPath(path);

                        google.maps.event.addListener(markers[key], 'click', function (e) {
                            infowindow.setContent(value.html + '<br>' + value.latitude + ',' + value.longitude);
                            infowindow.open(map, this);
                        });
                        map.fitBounds(Hbounds);
                    });
                }
            }
            else {
                $('#lblDisplay').html('No activity recorded for today!').show();
                center = new google.maps.LatLng(52.8849565, -1.9770329);
                options = {
                    'zoom': 5,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP,
                    'fullscreenControl': true
                };
                if (Type == 1) {
                    map = new google.maps.Map(document.getElementById("DHeartBeatMap"), options);
                }
                else {
                    map = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);
                    map = new google.maps.Map(document.getElementById("DHeartBeatMap"), options);
                }
            }
        },
        complete: function () {
            $('#imgLoader12').fadeOut(1000);
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}
//==========================================================================================================================================
//TODO : Map functionality -End
//==========================================================================================================================================
//TODO: Ranking -Start 
//==========================================================================================================================================

function GetRanking(parameter, flag, ltype) {
    RankURL.push(parameter);
    RankActionText.push(flag);
    RankViewType.push(ltype);
    try {
        if (signalrconnected) {
            showOverlay("divOfficerRankoverlaydiv");
            counter.server.getrankingmanager(parameter, flag, ltype);
        }
    } catch (e) {
        hideOverlay("divOfficerRankoverlaydiv");
    }
}

function FillRankingSelection(Action, key, Condition, MgrName, ActionText) {
    switch (Condition) {
        case 1: //TODO: List level (Manager select)
            $('#DRankingADM').show();
            $('#lblRankMgrName').html(' - ' + MgrName);
            $('#lblRankADMName,#lblRankOfficerName').html('');
            $('#DRankingOfficer,#DRankingCase,#DRankOfficerNotes,#btnRankADMBack,#btnRankOfficerBack,#btnRankOfficerNotesBack,#DRankMoreNotes').hide();

            FillRankingsForADM(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), $.session.get('RankingType'));
            break;
        case 2:// Manager select adm 
            $('#DRankingADM,#DRankingCase,#DRankOfficerNotes, #btnRankOfficerBack,#DRankMoreNotes,#btnRankOfficerNotesBack').hide();
            if ($.session.get('TreeLevel') == '1')
                $('#btnRankADMBack').hide();
            else
                $('#btnRankADMBack').show();

            $('#DRankingOfficer').show();
            $('#lblRankOfficerName').html('');
            $('#lblRankADMName').html(' - ' + MgrName);
            FillRankingsForOfficer(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), $.session.get('RankingType'));
            break;
        case 5://// Ranking More Manager list
            $('#DRMPaidManager,#DRMReturnManager,#DRMPPManager,#DRankMoreNotes,#DRankOfficerNotes, #btnRankOfficerNotesBack').hide();
            $('#lblRankMoreManager').html(' - ' + MgrName);
            $('#btnRankMoreADM').show();

            switch (Action) {
                case 1://Paid
                    $('#DRMPaidADM').show();
                    break;
                case 2://Returned
                    $('#DRMReturnADM').show();
                    break;
                case 3://Part Paid
                    $('#DRMPPADM').show();
                    break;
            }
            FillRankingsForADM(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), ActionText);
            break;
        case 6:// Ranking More ADM list
            $('#btnRankMoreADM,#DRMPaidManager,#DRMPaidADM,#DRMReturnManager,#DRMReturnADM,#DRMPPManager,#DRMPPADM').hide();
            $('#lblRankMoreADM').html(' - ' + MgrName);
            $('#btnRankMoreOfficer').show();
            switch (Action) {
                case 1://Paid
                    $('#DRMPaidOfficer').show();
                    break;
                case 2://Returned
                    $('#DRMReturnOfficer').show();
                    break;
                case 3://Part Paid
                    $('#DRMPPOfficer').show();
                    break;
            }
            FillRankingsForOfficer(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), ActionText);;
            break;
        case 7:
            break;
    }
}

function FillRankingCase(caseid, key, OfficerID, OfficerName, FromDate, RankType) {
    if (RankType == 0) {
        caseactionshowhidetabs();
        $('#HCaseActionNavigation').show();
        $('#btnCaseActionBack').show();
        $('#divCaseActionsManagerCount,#divCaseActionsOfficerCount,#btnCaseActionManager,#btnCaseActionADM,#DRankMoreNotes,#btnRankOfficerNotesBack').hide();
        $('#DRankingOfficer').hide();
        var dateAr = FromDate.split('/');
        var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
        FillCaseDetailsActionForManager(OfficerID, newDate, newDate, OfficerID, '0');
        //TODO: Rank more
        $('#DRankMorePaid,#DRankMorePaidOfficer,#btnPaidOfficerBack').hide();
        $('#DRankMoreReturned,#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').hide();
        $('#DRankMorePP,#DRankMorePPOfficer,#btnPPOfficerBack,#DRankMoreNotes,#btnRankOfficerNotesBack').hide();

        $('#DRankingADM').hide();

        $('#lblCAOfficerName,#lblRankOfficerName,#lblRankPaidOfficer,#lblRankReturnedOfficer,#lblRankPPOfficer').html(' - ' + OfficerName);
        $('#tblRankOfficerReturn,#tblRankOfficerPartPaid,#tblRankOfficerPaid,#btnRankADMBack,#DRankMoreNotes,#btnRankOfficerNotesBack').hide();
        if ($.session.get('TreeLevel') == 2)
            $('#btnRankOfficerBack').hide();
        else
            $('#btnRankOfficerBack').show();
        switch (caseid) {
            case 1:
                $('#lblRankStatus').html('Ranking - Paid');
                $('#paidformanager,#DRankingCase,#tblRankOfficerPaid,#DRankMorePaidCaseList,#btnPaidCaseBack').show();
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#lblRankStatus').html('Ranking - Part Paid');
                $('#DRankingCase,#tblRankOfficerPartPaid,#DRankMorePPCaseList,#btnPPCaseBack').show();
                break;

            case 4:
                $('#lblRankStatus').html('Ranking - Returned');
                $('#DRankingCase,#tblRankOfficerReturn,#DRankMoreReturnedCaseList,#btnReturnedCaseBack').show();
                break;
        }
    }
    else {
        $('#DRMPaidOfficer,#DRMReturnOfficer,#DRMPPOfficer,#DRankMoreNotes,#btnRankOfficerNotesBack').hide();
        $('#btnRankMoreOfficer').hide();

        switch ($.session.get('RankingType')) {
            case 'Paid':
                $('#DRMPaidCase').show();
                break;
            case 'Returned':
                $('#DRMReturnCase').show();
                break;
            case 'Part Paid':
                $('#DRMPPCase').show();
                break;
        }

        $('#btnRankMoreCase').show();

        $('#lblRankMoreOfficer').html(' - ' + OfficerName);
        var dateAr = FromDate.split('/');
        var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

        FillCaseDetailsActionForManager(OfficerID, newDate, newDate, OfficerID, '0');
    }
}

function FullDescriptionOfficerRank(key, val) {
    $('#btnRankOfficerBack,#DRankingADM,#DRankingOfficer,#DRankingCase,#DRankMoreNotes,#btnRankOfficerNotesBack').hide();
    $('#DRankOfficerNotes').empty().html($('#tdFullDesc' + key).html()).show();
    $('#HCaseNumber').empty().html(' :  ' + $.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html() + " - " + $('#tdCaseOfficer' + key).html()).show();
    $('#btnRankOfficerNotesBack').show().click(function () {
        $('#DRankOfficerNotes,#HCaseNumber,#btnRankOfficerNotesBack').hide();
        $('#DRankingCase').show();
        if ($.session.get('TreeLevel') == '2')
            $('#btnRankOfficerBack').hide();
        else
            $('#btnRankOfficerBack').show();
    });
}

function FullDescriptionRank(key, val) {
    $('#DRankingCase, #DRMReturnCase,#DRMPPCase,#DRankMoreNotes,#btnRankOfficerNotesBack, #btnRankMoreOfficer').hide();
    $('#btnRankMoreCase, #btnRankOfficerBack,#DRMPaidCase').hide();
    switch ($.session.get('RankingType')) {
        case 'Paid':
            $('#DRankMoreNotes,#DRankOfficerNotes').empty().html($('#tdFullDesc' + key).html()).show();
            $('#btnRankOfficerNotesBack,#btnRankMoreCaseNotes').show().click(function () {
                $('#btnRankMoreCase,#DRankingCase,#DRMPaidCase, #btnRankOfficerBack').show();
                $('#btnRankMoreCaseNotes,#DRMReturnCase,#DRMPPCase,#DRankMoreNotes,#DRankOfficerNotes,#btnRankOfficerNotesBack').hide();
            });
            break;
        case 'Returned':
            $('#DRankMoreNotes,#DRankOfficerNotes').empty().html($('#tdFullDesc' + key).html()).show();
            $('#btnRankOfficerNotesBack,#btnRankMoreCaseNotes').show().click(function () {
                $('#btnRankMoreCase,#DRMReturnCase, #btnRankOfficerBack').show();
                $('#btnRankMoreCaseNotes,#DRMPaidCase,#DRMPPCase,#DRankMoreNotes,#DRankOfficerNotes,#btnRankOfficerNotesBack').hide();
            });
            break;
        case 'Part Paid':
            $('#DRankMoreNotes,#DRankOfficerNotes').empty().html($('#tdFullDesc' + key).html()).show();
            $('#btnRankOfficerNotesBack,#btnRankMoreCaseNotes').show().click(function () {
                $('#btnRankMoreCase,#DRMPPCase,#btnRankOfficerBack').show();
                $('#btnRankMoreCaseNotes,#DRMPaidCase,#DRMReturnCase,#DRankMoreNotes,#DRankOfficerNotes,#btnRankOfficerNotesBack').hide();
            });
            break;
    }
}

//TODO : Ranking normal function
function GetFillRankings_Normal(ManagerID, FromDate, ActionText) {

    var dateAr = FromDate.split('/');
    var FromDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    var ToDate = FromDate;

    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + ActionText + '/' + FromDate + '/' + ToDate + '/' + 'CaseActionssDetailForManager?ManagerID=' + ManagerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader11').show();
            $('#changeclass1 div').css('background', '#F9F9F9');
        },
        success: function (result) {
            var cPaid = false;
            var cReturn = false;
            var cPartpaid = false;
            if (result != undefined) {
                $('#tblbodyPaid,#tblbodyReturned,#tblbodyPP').empty();
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    $('#tbodyRMPaidManager').empty();
                    $('#tbodyRMReturnManager').empty();
                    $('#tbodyRMPPManager').empty();

                    $.each(result, function (key, value) {
                        switch (ActionText) {
                            case 'Paid':
                                cPaid = true;
                                $('#ARankingPaidMore').show();
                                if (key <= 4) {
                                    $('#tblbodyPaid').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPaidOfficerID' + key).html(value.OfficerID))
                                    .append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                        .attr('onclick', 'FillOfficerRankOnClick(1,' + value.OfficerID + ',1,"' + value.OfficerName + '","' + ActionText + '")').html(value.OfficerName)))
                                    .append($('<td>')
                                        .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionmanager(' + 1 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "', 'Ranking'" + ')')
                                            .attr('href', '#divOfficerRank').html(value.CaseCnt))
                                        .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())));
                                }
                                $('#tbodyRMPaidManager').append($('<tr>').append($('<td>')
                                             .append($('<a>').attr('onclick', 'FillOfficerRankOnClick(1,' + value.OfficerID + ',4,"' + value.OfficerName + '","' + ActionText + '")')
                                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                             .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                             .append($('<td>').html(value.CaseCnt)));
                                break;
                            case 'Returned':
                                cReturn = true;
                                $('#ARankingReturnedMore').show();
                                if (key <= 4) {
                                    $('#tblbodyReturned').append($('<tr>')
                                                .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblReturnOfficerID' + key).html(value.OfficerID))
                                                .append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                    .attr('onclick', 'FillOfficerRankOnClick(2,' + value.OfficerID + ',1,"' + value.OfficerName + '","' + ActionText + '")').html(value.OfficerName)))
                                                .append($('<td>')
                                                    .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionmanager(' + 3 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "', 'Ranking'" + ')')
                                                        .attr('href', '#divOfficerRank').html(value.CaseCnt))
                                                    .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())));
                                }
                                $('#tbodyRMReturnManager').append($('<tr>').append($('<td>')
                                             .append($('<a>').attr('onclick', 'FillOfficerRankOnClick(2,' + value.OfficerID + ',4,"' + value.OfficerName + '","' + ActionText + '")')
                                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                             .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                             .append($('<td>').html(value.CaseCnt)));
                                break;
                            case 'Part Paid':
                                cPartpaid = true;
                                $('#ARankingPartPaidMore').show();
                                if (key <= 4) {
                                    $('#tblbodyPP').append($('<tr>')
                                              .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                              .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPartPaidOfficerID' + key).html(value.OfficerID))
                                              .append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                  .attr('onclick', 'FillOfficerRankOnClick(3,' + value.OfficerID + ',1,"' + value.OfficerName + '","' + ActionText + '")').html(value.OfficerName)))
                                              .append($('<td>')
                                                  .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionmanager(' + 2 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "', 'Ranking'" + ')')
                                                      .attr('href', '#divOfficerRank').html(value.CaseCnt))
                                                  .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())))
                                }

                                $('#tbodyRMPPManager').append($('<tr>').append($('<td>')
                                            .append($('<a>').attr('onclick', 'FillOfficerRankOnClick(3,' + value.OfficerID + ',4,"' + value.OfficerName + '","' + ActionText + '")')
                                               .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                            .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                            .append($('<td>').html(value.CaseCnt)));
                                break;
                        }
                    });
                    if (cPaid == false) {
                        $('#ARankingPaidMore').hide();
                        $('#tblbodyPaid').empty();
                        $('#tblbodyPaid').append($('<tr>').append($('<td colspan="2">').html('No rankings for paid actions.')));
                    }
                    if (cReturn == false) {
                        $('#ARankingReturnedMore').hide();
                        $('#tblbodyReturned').empty();
                        $('#tblbodyReturned').append($('<tr>').append($('<td colspan="2">').html('No rankings for returned actions.')));
                    }
                    if (cPartpaid == false) {
                        $('#ARankingPartPaidMore').hide();
                        $('#tblbodyPP').empty();
                        $('#tblbodyPP').append($('<tr>').append($('<td colspan="2">').html('No rankings for part paid actions.')));
                    }
                }
                else {
                    if (cPaid == false) {
                        $('#ARankingPaidMore').hide();
                        $('#tblbodyPaid').empty();
                        $('#tblbodyPaid').append($('<tr>').append($('<td colspan="2">').html('No rankings for paid actions.')));
                    }
                    if (cReturn == false) {
                        $('#ARankingReturnedMore').hide();
                        $('#tblbodyReturned').empty();
                        $('#tblbodyReturned').append($('<tr>').append($('<td colspan="2">').html('No rankings for returned actions.')));
                    }
                    if (cPartpaid == false) {
                        $('#ARankingPartPaidMore').hide();
                        $('#tblbodyPP').empty();
                        $('#tblbodyPP').append($('<tr>').append($('<td colspan="2">').html('No rankings for part paid actions.')));
                    }
                }
            }
            else {
            }
        },
        complete: function () {
            $('#imgLoader11').fadeOut(1000);
            $('#changeclass1 div').css('background', '');
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function FillRankings(ManagerID, FromDate, ToDate, ActionText) {
    var inputParams = ActionText + "," + FromDate + "," + ToDate + "," + $.session.get('CompanyID') + "," + ManagerID + "," + $.session.get('OfficerRole');
    RA_TYPE = "Manager";

    switch (ActionText) {
        case 'Paid':
            GetRanking(inputParams, 1, "Manager");
            break;
        case 'Returned':
            GetRanking(inputParams, 2, "Manager");
            break;
        case 'Part Paid':
            GetRanking(inputParams, 3, "Manager");
            break;
    }

}

function FillRankingsForADM(ManagerID, FromDate, ToDate, ActionText) {
    var Flag;
    switch (ActionText) {
        case 'Paid':
            $('#lblRankStatus').html('Ranking - Paid');
            $('#HCaseActionTitle').html('Paid');
            Flag = 1;
            break;
        case 'Returned':
            $('#lblRankStatus').html('Ranking - Returned');
            $('#HCaseActionTitle').html('Returned');
            Flag = 4;
            break;
        case 'Part Paid':
            $('#lblRankStatus').html('Ranking - Part Paid');
            $('#HCaseActionTitle').html('Part Paid');
            Flag = 2;
            break;
    }
    var inputParams = ActionText + "," + FromDate + "," + ToDate + "," + $.session.get('CompanyID') + "," + ManagerID + "," + $.session.get('OfficerRole');
    RA_TYPE = "ADM";

    RankADMURL.push(inputParams);
    RankADMActionText.push(Flag);
    RankADMViewType.push("ADM");
    GetRanking(inputParams, Flag, "ADM");
}

function FillRankingsForOfficer(ManagerID, FromDate, ToDate, ActionText) {
    var Flag;
    switch (ActionText) {
        case 'Paid':
            $('#lblRankStatus').html('Ranking - Paid');
            $('#HCaseActionTitle').html('Paid');
            Flag = 1;
            break;
        case 'Returned':
            $('#lblRankStatus').html('Ranking - Returned');
            $('#HCaseActionTitle').html('Returned');
            Flag = 4;
            break;
        case 'Part Paid':
            $('#lblRankStatus').html('Ranking - Part Paid');
            $('#HCaseActionTitle').html('Part Paid');
            Flag = 2;
            break;
    }
    var inputParams = ActionText + "," + FromDate + "," + ToDate + "," + $.session.get('CompanyID') + "," + ManagerID + "," + $.session.get('OfficerRole');
    //Ranking SignalR Method
    RA_TYPE = "Officer";
    RankOFFURL.push(inputParams);
    RankOFFActionText.push(Flag);
    RankOFFViewType.push("Officer");
    GetRanking(inputParams, Flag, "Officer");
}

function FillRankings_Normal(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    FromDate = newDate;

    var dateAr = ToDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    ToDate = newDate;

    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + ActionText + '/' + FromDate + '/' + ToDate + '/CaseActionssDetailForManager?ManagerID=' + ManagerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader11').show();
            $('#DivRanking div').css('background', '#F9F9F9');
        },
        complete: function () {
            $('#imgLoader11').fadeOut(1000);
            $('#DivRanking div').css('background', '');
        },
        success: function (result) {//On Successfull service call  
            var cPaid = false;
            var cReturn = false;
            var cPartpaid = false;
            if (result != undefined) {

                switch (ActionText) {
                    case 'Paid':
                        if (cPaid == false) {
                            $('#tbodyRMPaidManager,#tbodyRMPaidOfficer').empty();
                            $('#tblbodyPaid,#tbodyRMPaidADM').empty();
                        }
                        break;
                    case 'Returned':
                        if (cReturn == false) {
                            $('#tbodyRMReturnManager,#tbodyRMReturnOfficer').empty();
                            $('#tblbodyReturned,#tbodyRMReturnADM').empty();
                        }
                        break;
                    case 'Part Paid':
                        if (cPartpaid == false) {
                            $('#tbodyRMPPManager,#tbodyRMPPOfficer').empty();
                            $('#tblbodyPP,#tbodyRMPPADM').empty();
                        }
                        break;
                }
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    $.each(result, function (key, value) {
                        switch (ActionText) {
                            case 'Paid':
                                cPaid = true;
                                $('#ARankingPaidMore').show();
                                $('#tbodyRMPaidManager')
                                           .append($('<tr>')
                                               .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#')
                                                .attr('onclick', 'FillOfficerRankOnClick_Normal(1,' + value.OfficerID + ',5,"' + value.OfficerName + '","' + ActionText + '")').html(value.OfficerName)))
                                               .append($('<td>').append($('<label>').html(value.CaseCnt))));

                                $('#tbodyRMPaidADM').append($('<tr>').append($('<td>')
                                               .append($('<a>').attr('onclick', 'FillOfficerRankOnClick_Normal(1,' + value.OfficerID + ',6,"' + value.OfficerName + '","' + ActionText + '")')
                                                  .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                               .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                               .append($('<td>').html(value.CaseCnt)));

                                $('#tbodyRMPaidOfficer').append($('<tr>').append($('<td>')
                                           .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick_Normal(1,' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                           .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                           .append($('<td>').html(value.CaseCnt)));
                                if (key <= 4) {
                                    $('#tblbodyPaid').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPaidOfficerID' + key).html(value.OfficerID))
                                    .append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                    .attr('onclick', 'FillOfficerRankOnClick_Normal(1,' + value.OfficerID + ',1,"' + value.OfficerName + '","' + ActionText + '")').html(value.OfficerName)))
                                    .append($('<td>')
                                    .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionmanager(' + 1 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "', 'Ranking'" + ')')
                                    .attr('href', '#divOfficerRank').html(value.CaseCnt))
                                    .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())));
                                }
                                break;
                            case 'Returned':
                                cReturn = true;
                                $('#ARankingReturnedMore').show();

                                $('#tbodyRMReturnManager').append($('<tr>')
                                .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#')
                                .attr('onclick', 'FillOfficerRankOnClick_Normal(2,' + value.OfficerID + ',5,"' + value.OfficerName + '","' + ActionText + '")').html(value.OfficerName)))
                                .append($('<td>').append($('<label>').html(value.CaseCnt))));

                                $('#tbodyRMReturnADM').append($('<tr>').append($('<td>')
                                                 .append($('<a>').attr('onclick', 'FillOfficerRankOnClick_Normal(2,' + value.OfficerID + ',6,"' + value.OfficerName + '","' + ActionText + '")')
                                                    .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                                 .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                                 .append($('<td>').html(value.CaseCnt)));

                                $('#tbodyRMReturnOfficer').append($('<tr>').append($('<td>')
                                                          .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick_Normal(2,' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                                          .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                                          .append($('<td>').html(value.CaseCnt)));
                                if (key <= 4) {
                                    $('#tblbodyReturned').append($('<tr>')
                                               .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                               .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblReturnOfficerID' + key).html(value.OfficerID))
                                               .append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                   .attr('onclick', 'FillOfficerRankOnClick_Normal(2,' + value.OfficerID + ',1,"' + value.OfficerName + '","' + ActionText + '")').html(value.OfficerName)))
                                               .append($('<td>')
                                                   .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionmanager(' + 3 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "', 'Ranking'" + ')')
                                                       .attr('href', '#divOfficerRank').html(value.CaseCnt))
                                                   .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())));
                                }
                                break;
                            case 'Part Paid':
                                cPartpaid = true;
                                $('#ARankingPartPaidMore').show();

                                $('#tbodyRMPPManager').append($('<tr>')
                                .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#')
                                .attr('onclick', 'FillOfficerRankOnClick_Normal(3,' + (value.OfficerID) + ',5,"' + value.OfficerName + '","' + ActionText + '")').html(value.OfficerName)))
                                .append($('<td>').append($('<label>').html(value.CaseCnt))))

                                $('#tbodyRMPPADM').append($('<tr>').append($('<td>')
                                               .append($('<a>').attr('onclick', 'FillOfficerRankOnClick_Normal(3,' + value.OfficerID + ',6,"' + value.OfficerName + '","' + ActionText + '")')
                                                  .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                               .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                               .append($('<td>').html(value.CaseCnt)))

                                $('#tbodyRMPPOfficer').append($('<tr>').append($('<td>')
                                         .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick_Normal(3,' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                         .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                         .append($('<td>').html(value.CaseCnt)));

                                if (key <= 4) {
                                    $('#tblbodyPP').append($('<tr>')
                                              .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                              .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPartPaidOfficerID' + key).html(value.OfficerID))
                                              .append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                  .attr('onclick', 'FillOfficerRankOnClick_Normal(3,' + value.OfficerID + ',1,"' + value.OfficerName + '","' + ActionText + '")').html(value.OfficerName)))
                                              .append($('<td>')
                                                  .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionmanager(' + 2 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "', 'Ranking'" + ')')
                                                      .attr('href', '#divOfficerRank').html(value.CaseCnt))
                                                  .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())));
                                }
                                break;
                        }
                    });

                    switch (ActionText) {
                        case 'Paid':
                            if (cPaid == false) {
                                $('#ARankingPaidMore').hide();
                                $('#tblbodyPaid').empty();
                                $('#tblbodyPaid').append($('<tr>').append($('<td colspan="2">').html('No rankings for paid actions.')));
                            }
                            break;
                        case 'Returned':
                            if (cReturn == false) {
                                $('#ARankingReturnedMore').hide();
                                $('#tblbodyReturned').empty();
                                $('#tblbodyReturned').append($('<tr>').append($('<td colspan="2">').html('No rankings for returned actions.')));
                            }
                            break;
                        case 'Part Paid':
                            if (cPartpaid == false) {
                                $('#ARankingPartPaidMore').hide();
                                $('#tblbodyPP').empty();
                                $('#tblbodyPP').append($('<tr>').append($('<td colspan="2">').html('No rankings for part paid actions.')));
                            }
                            break;
                    }
                }
                else {
                    switch (ActionText) {
                        case 'Paid':
                            if (cPaid == false) {
                                $('#ARankingPaidMore').hide();
                                $('#tblbodyPaid').empty();
                                $('#tblbodyPaid').append($('<tr>').append($('<td colspan="2">').html('No rankings for paid actions.')));
                            }
                            break;
                        case 'Returned':
                            if (cReturn == false) {
                                $('#ARankingReturnedMore').hide();
                                $('#tblbodyReturned').empty();
                                $('#tblbodyReturned').append($('<tr>').append($('<td colspan="2">').html('No rankings for returned actions.')));
                            }
                            break;
                        case 'Part Paid':
                            if (cPartpaid == false) {
                                $('#ARankingPartPaidMore').hide();
                                $('#tblbodyPP').empty();
                                $('#tblbodyPP').append($('<tr>').append($('<td colspan="2">').html('No rankings for part paid actions.')));
                            }
                            break;
                    }
                }
            }
            else {
            }
        },
        error: function () {
        } // When Service call fails
    });
}

function FillOfficerRankOnClick_Normal(Action, key, Condition, MgrName, ActionText) {
    switch (Condition) {
        case 1: //Ranking ADM list
            //Tree level click function
            switch ($.session.get('TreeLevel')) {
                case '0':// Manager click: Shown ADM list
                    $('#DRankingOfficer,#DRankingCase,#btnRankADMBack,#btnRankOfficerBack, #DRankOfficerNotes').hide();
                    $('#DRankingADM').show();
                    $('#lblRankOfficerName,#lblRankMgrName,#lblRankADMName').html('');
                    $('#lblRankMgrName').html(' - ' + MgrName);
                    FillRankingsForADM_Normal(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), $.session.get('RankingType'), 0);
                    break;
                case '1'://ADM click: Shown Officer list
                    $('#DRankingADM,#DRankingCase,#btnRankADMBack,#btnRankOfficerBack,#btnRankADMBack, #DRankOfficerNotes').hide();
                    $('#DRankingOfficer').show();
                    $('#lblRankOfficerName').html('');
                    $('#lblRankADMName').html(' - ' + MgrName);
                    FillRankingsForOfficer_Normal(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), $.session.get('RankingType'), 0);

                    break;
                case '2'://Officer click: Shown case list
                    $('#DRankingADM,#DRankingOfficer,#DRankingCase,#btnRankADMBack,#btnRankOfficerBack, #DRankOfficerNotes').hide();
                    FillCaseActionDetailsOnClick_Normal(Action, key, MgrName, 0);
                    break;
            }
            break;
        case 2: //Ranking More ADM list
            switch (Action) {
                case 1://Paid
                    $('#DRankMorePaid,#DRankMorePaidCaseList').hide();
                    $('#DRankMorePaidADM,#btnPaidADMBack').show();
                    $('#lblRankPaidManager').html(' - ' + MgrName);
                    break;
                case 2://Returned
                    $('#DRankMoreReturned,#DRankMoreReturnedCaseList').hide();
                    $('#DRankMoreReturnedADM,#btnReturnedADMBack').show();
                    $('#lblRankReturnedManager').html(' - ' + MgrName);
                    break;
                case 3://Part Paid
                    $('#DRankMorePP,#DRankMorePPCaseList').hide();
                    $('#DRankMorePPADM,#btnPPADMBack').show();
                    $('#lblRankPPManager').html(' - ' + MgrName);
                    break;
            }
            FillRankingsForADM_Normal(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), $.session.get('RankingType'), 0);

            break;
        case 3: // Ranking Officer list
            $('#DRankingADM,#DRankingCase,#btnRankADMBack,#btnRankOfficerBack').hide();
            $('#DRankingOfficer,#btnRankADMBack').show();
            $('#lblRankOfficerName').html('');
            $('#lblRankADMName').html(' - ' + MgrName);
            FillRankingsForOfficer_Normal(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), $.session.get('RankingType'), 0);

            break;
        case 4:// Ranking More Officer list
            switch (Action) {
                case 1://Paid
                    $('#DRankMorePaid,#DRankMorePaidCaseList,#DRankMorePaidADM,#btnPaidADMBack').hide();
                    $('#DRankMorePaidOfficer,#btnPaidOfficerBack').show();
                    $('#lblRankPaidADM').html(' - ' + MgrName);
                    break;
                case 2://Returned
                    $('#DRankMoreReturned,#DRankMoreReturnedCaseList,#DRankMoreReturnedADM,#btnReturnedADMBack').hide();
                    $('#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').show();
                    $('#lblRankReturnedADM').html(' - ' + MgrName);
                    break;
                case 3://Part Paid
                    $('#DRankMorePP,#DRankMorePPCaseList,#DRankMorePPADM,#btnPPADMBack').hide();
                    $('#DRankMorePPOfficer,#btnPPOfficerBack').show();
                    $('#lblRankPPADM').html(' - ' + MgrName);
                    break;
            }
            FillRankingsForOfficer_Normal(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), $.session.get('RankingType'), 0);

            break;
        case 5://// Ranking More Manager list
            $('#DRMPaidManager,#DRMReturnManager,#DRMPPManager,#DRankMoreNotes,#btnRankOfficerNotesBack').hide();
            $('#lblRankMoreManager').html(' - ' + MgrName);
            $('#btnRankMoreADM').show();

            switch (Action) {
                case 1://Paid
                    $('#DRMPaidADM').show();
                    break;
                case 2://Returned
                    $('#DRMReturnADM').show();
                    break;
                case 3://Part Paid
                    $('#DRMPPADM').show();
                    break;
            }
            FillRankingsForADM_Normal(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), ActionText);
            break;
        case 6:// Ranking More ADM list
            $('#btnRankMoreADM,#DRMPaidManager,#DRMPaidADM,#DRMReturnManager,#DRMReturnADM,#DRMPPManager,#DRMPPADM').hide();
            $('#lblRankMoreADM').html(' - ' + MgrName);
            $('#btnRankMoreOfficer').show();
            switch (Action) {
                case 1://Paid
                    $('#DRMPaidOfficer').show();
                    break;
                case 2://Returned
                    $('#DRMReturnOfficer').show();
                    break;
                case 3://Part Paid
                    $('#DRMPPOfficer').show();
                    break;
            }
            FillRankingsForOfficer_Normal(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), ActionText);;
            break;
    }
}

// TODO : Ranking more details ( Manager list)
function FillRankingsMore_Normal(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    FromDate = newDate;

    var dateAr = ToDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    ToDate = newDate;

    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + ActionText + '/' + FromDate + '/' + ToDate + '/CaseActionssDetailForManager?ManagerID=' + ManagerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader5,#imgLoader6,#imgLoader7').show();
        },
        complete: function () {
            $('#imgLoader5,#imgLoader6,#imgLoader7').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    $('#tblbodyRAMore').empty();
                    $('#tblbodyReturnedMore').empty();
                    $('#tblbodyPPRanking').empty();
                    $.each(result, function (key, value) {
                        switch (value.ActionText) {
                            case 'Paid':
                                $('#tblbodyRAMore')
                                .append($('<tr>')
                                    .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPaidOfficerID' + key).html(value.OfficerID))
                                        .append($('<a>').attr('data-toggle', 'modal').attr('href', '#')
                                        .attr('onclick', 'FillRankingMoreOnClick_Normal(1,' + value.OfficerID + ',2,"' + value.OfficerName + '")').html(value.OfficerName)))
                                    .append($('<td>')
                                    .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionofficer(' + 1 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                            .attr('href', '#').html(value.CaseCnt))
                                        .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())))
                                if ($.session.get('TreeLevel') == 1)
                                    FillRankingMoreOnClick_Normal(1, $.session.get('ManagerID'), 2, $.session.get('MgrName'));
                                break;
                            case 'Returned':
                                $('#tblbodyReturnedMore')
                                .append($('<tr>')
                                    .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPaidOfficerID' + key).html(value.OfficerID))
                                        .append($('<a>').attr('data-toggle', 'modal').attr('href', '#')
                                        .attr('onclick', 'FillRankingMoreOnClick_Normal(2,' + value.OfficerID + ',2,"' + value.OfficerName + '")').html(value.OfficerName)))
                                    .append($('<td>')
                                        .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionofficer(' + 3 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                            .attr('href', '#').html(value.CaseCnt))
                                        .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())))
                                if ($.session.get('TreeLevel') == 1)
                                    FillRankingMoreOnClick_Normal(2, $.session.get('ManagerID'), 2, $.session.get('MgrName'));
                                break;
                            case 'Part Paid':
                                $('#tblbodyPPRanking')
                                         .append($('<tr>')
                                             .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPaidOfficerID' + key).html(value.OfficerID))
                                                 .append($('<a>').attr('data-toggle', 'modal').attr('href', '#')
                                                 .attr('onclick', 'FillRankingMoreOnClick_Normal(3,' + value.OfficerID + ',2,"' + value.OfficerName + '")').html(value.OfficerName)))
                                             .append($('<td>')
                                                .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionofficer(' + 2 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                                    .attr('href', '#').html(value.CaseCnt))
                                                .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())))
                                if ($.session.get('TreeLevel') == 1)
                                    FillRankingMoreOnClick_Normal(3, $.session.get('ManagerID'), 2, $.session.get('MgrName'));
                                break;
                        }
                    });
                }
                else {
                    switch (ActionText) {
                        case 'Paid':
                            $('#tblbodyRAMore').empty();
                            $('#tblbodyRAMore').append($('<tr>').append($('<td colspan="2">').html('No rankings for paid actions.')));
                            break;
                        case 'Returned':
                            $('#tblbodyReturnedMore').empty();
                            $('#tblbodyReturnedMore').append($('<tr>').append($('<td colspan="2">').html('No rankings for returned actions.')));
                            break;
                        case 'Part Paid':
                            $('#tblbodyPPRanking').empty();
                            $('#tblbodyPPRanking').append($('<tr>').append($('<td colspan="2">').html('No rankings for rart paid actions.')));
                            break;
                    }
                }
            }
        },
        error: function () {
        } // When Service call fails
    });


}

function FillRankingMoreOnClick_Normal(Action, key, Condition, MgrName) {
    //Ranking More ADM list
    switch (Action) {
        case 1://Paid
            $('#DRankMorePaid,#DRankMorePaidCaseList').hide();
            $('#DRankMorePaidADM,#btnPaidADMBack').show();
            $('#lblRankPaidManager').html(' - ' + MgrName);
            break;
        case 2://Returned
            $('#DRankMoreReturned,#DRankMoreReturnedCaseList').hide();
            $('#DRankMoreReturnedADM,#btnReturnedADMBack').show();
            $('#lblRankReturnedManager').html(' - ' + MgrName);
            break;
        case 3://Part Paid
            $('#DRankMorePP,#DRankMorePPCaseList').hide();
            $('#DRankMorePPADM,#btnPPADMBack').show();
            $('#lblRankPPManager').html(' - ' + MgrName);
            break;
    }
    switch (Action) {
        case 1:
            FillRankingsForADM_Normal(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', 0, true);//$('#lblPaidOfficerID' + key).html()
            break;
        case 2:
            FillRankingsForADM_Normal(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', 0, true);//$('#lblReturnOfficerID' + key).html()
            break;
        case 3:
            FillRankingsForADM_Normal(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', 0, true);//$('#lblPartPaidOfficerID' + key).html()
            break;
    }
}

function FillRankingsForADM_Normal(ManagerID, FromDate, ToDate, ActionText, GroupID, ShowAll) {
    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    FromDate = newDate;

    var dateAr = ToDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    ToDate = newDate;

    switch (ActionText) {
        case 'Paid':
            $('#lblRankStatus').html('Ranking - Paid');
            $('#HCaseActionTitle').html('Paid');
            Flag = 1;
            break;
        case 'Returned':
            $('#lblRankStatus').html('Ranking - Returned');
            $('#HCaseActionTitle').html('Returned');
            Flag = 4;
            break;
        case 'Part Paid':
            $('#lblRankStatus').html('Ranking - Part Paid');
            $('#HCaseActionTitle').html('Part Paid');
            Flag = 2;
            break;
    }
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + ActionText + '/' + FromDate + '/' + ToDate + '/CaseActionssDetailForManager?ManagerID=' + ManagerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader5,#imgLoader6,#imgLoader7,#imgLoader8').show();
        },
        complete: function () {
            $('#imgLoader5,#imgLoader6,#imgLoader7,#imgLoader8').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                $('#tbodyADMRank').empty();
                //TODO: Rank more
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    $('#tbodyRankMorePaidADM,#tbodyRMPaidADM').empty();
                    $('#tbodyRankMoreReturnedADM,#tbodyRMReturnADM').empty();
                    $('#tbodyRankMorePPADM,#tbodyRMPPADM').empty();
                    $.each(result, function (key, value) {
                        switch (ActionText) {//
                            case 'Paid':
                                $('#tbodyRankMorePaidADM').append($('<tr>').append($('<td>')
                                             .append($('<a>').attr('onclick', 'FillOfficerRankOnClick_Normal(1,' + value.OfficerID + ',4,"' + value.OfficerName + '","' + ActionText + '")')
                                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                             .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                             .append($('<td>').html(value.CaseCnt)))
                                $('#tbodyRMPaidADM').append($('<tr>').append($('<td>')
                                            .append($('<a>').attr('onclick', 'FillOfficerRankOnClick_Normal(1,' + value.OfficerID + ',6,"' + value.OfficerName + '","' + ActionText + '")')
                                               .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                            .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                            .append($('<td>').html(value.CaseCnt)))
                                $('#tbodyADMRank').append($('<tr>').append($('<td>')
                                       .append($('<a>').attr('onclick', 'FillOfficerRankOnClick_Normal(1,' + value.OfficerID + ',3,"' + value.OfficerName + '","' + ActionText + '")').attr('href', '#paid').html(value.OfficerName))
                                       .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                       .append($('<td>').html(value.CaseCnt)));

                                break;
                            case 'Returned':
                                $('#tbodyRankMoreReturnedADM').append($('<tr>').append($('<td>')
                                             .append($('<a>').attr('onclick', 'FillOfficerRankOnClick_Normal(2,' + value.OfficerID + ',4,"' + value.OfficerName + '","' + ActionText + '")')
                                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                             .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                             .append($('<td>').html(value.CaseCnt)))
                                $('#tbodyRMReturnADM').append($('<tr>').append($('<td>')
                                             .append($('<a>').attr('onclick', 'FillOfficerRankOnClick_Normal(2,' + value.OfficerID + ',6,"' + value.OfficerName + '","' + ActionText + '")')
                                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                             .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                             .append($('<td>').html(value.CaseCnt)))
                                $('#tbodyADMRank').append($('<tr>').append($('<td>')
                                      .append($('<a>').attr('onclick', 'FillOfficerRankOnClick_Normal(2,' + value.OfficerID + ',3,"' + value.OfficerName + '","' + ActionText + '")').attr('href', '#paid').html(value.OfficerName))
                                      .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                      .append($('<td>').html(value.CaseCnt)));
                                break;
                            case 'Part Paid':
                                $('#tbodyRankMorePPADM').append($('<tr>').append($('<td>')
                                            .append($('<a>').attr('onclick', 'FillOfficerRankOnClick_Normal(3,' + value.OfficerID + ',4,"' + value.OfficerName + '","' + ActionText + '")')
                                               .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                            .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                            .append($('<td>').html(value.CaseCnt)))
                                $('#tbodyRMPPADM').append($('<tr>').append($('<td>')
                                            .append($('<a>').attr('onclick', 'FillOfficerRankOnClick_Normal(3,' + value.OfficerID + ',6,"' + value.OfficerName + '","' + ActionText + '")')
                                               .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                            .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                            .append($('<td>').html(value.CaseCnt)))
                                $('#tbodyADMRank').append($('<tr>').append($('<td>')
                                      .append($('<a>').attr('onclick', 'FillOfficerRankOnClick_Normal(3,' + value.OfficerID + ',3,"' + value.OfficerName + '","' + ActionText + '")').attr('href', '#paid').html(value.OfficerName))
                                      .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                      .append($('<td>').html(value.CaseCnt)));
                                break;
                        }
                    });
                }
                else {
                    $('#tbodyADMRank').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                    //TODO: Rank more
                    switch (ActionText) {
                        case 'Paid':
                            $('#tbodyRankMorePaidOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                        case 'Returned':
                            $('#tbodyRankMoreReturnedOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                        case 'Part Paid':
                            $('#tbodyRankMorePPOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                    }
                }
            }
        },
        error: function () {
        } // When Service call fails
    });
}

function FillRankingsForOfficer_Normal(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    FromDate = newDate;

    var dateAr = ToDate.split('/');
    var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    ToDate = newDate;

    var Flag;
    switch (ActionText) {
        case 'Paid':
            $('#lblRankStatus').html('Ranking - Paid');
            $('#HCaseActionTitle').html('Paid');
            Flag = 1;
            break;
        case 'Returned':
            $('#lblRankStatus').html('Ranking - Returned');
            $('#HCaseActionTitle').html('Returned');
            Flag = 4;
            break;
        case 'Part Paid':
            $('#lblRankStatus').html('Ranking - Part Paid');
            $('#HCaseActionTitle').html('Part Paid');
            Flag = 2;
            break;
    }
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + ActionText + '/' + FromDate + '/' + ToDate + '/CaseActionssDetailForManager?ManagerID=' + ManagerID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader5,#imgLoader6,#imgLoader7,#imgLoader8').show();
        },
        complete: function () {
            $('#imgLoader5,#imgLoader6,#imgLoader7,#imgLoader8').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                $('#tbodyOfficerRank').empty();
                //TODO: Rank more
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    $('#tbodyRankMorePaidOfficer,#tbodyRMPaidOfficer').empty();
                    $('#tbodyRankMoreReturnedOfficer,#tbodyRMReturnOfficer').empty();
                    $('#tbodyRankMorePPOfficer,#tbodyRMPPOfficer').empty();
                    $.each(result, function (key, value) {
                        switch (ActionText) {
                            case 'Paid':
                                $('#tbodyRankMorePaidOfficer').append($('<tr>').append($('<td>')
                                        .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "', 'Ranking'" + ')')
                                            .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                        .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                        .append($('<td>').html(value.CaseCnt)));
                                $('#tbodyRMPaidOfficer').append($('<tr>').append($('<td>')
                                       .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick_Normal(1,' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                       .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                       .append($('<td>').html(value.CaseCnt)));
                                $('#tbodyOfficerRank').append($('<tr>').append($('<td>')
                                       .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick_Normal(1,' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                       .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                       .append($('<td>').html(value.CaseCnt)));
                                break;
                            case 'Returned':
                                $('#tbodyRankMoreReturnedOfficer').append($('<tr>').append($('<td>')
                                             .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "', 'Ranking'" + ')')
                                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                             .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                             .append($('<td>').html(value.CaseCnt)));
                                $('#tbodyRMReturnOfficer').append($('<tr>').append($('<td>')
                                      .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick_Normal(2,' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                      .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                      .append($('<td>').html(value.CaseCnt)))
                                $('#tbodyOfficerRank').append($('<tr>').append($('<td>')
                                      .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick_Normal(2,' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                      .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                      .append($('<td>').html(value.CaseCnt)));
                                break;
                            case 'Part Paid':
                                $('#tbodyRankMorePPOfficer').append($('<tr>').append($('<td>')
                                            .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "', 'Ranking'" + ')')
                                               .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                            .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                            .append($('<td>').html(value.CaseCnt)));
                                $('#tbodyRMPPOfficer').append($('<tr>').append($('<td>')
                                      .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick_Normal(3,' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                      .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                      .append($('<td>').html(value.CaseCnt)));
                                $('#tbodyOfficerRank').append($('<tr>').append($('<td>')
                                      .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick_Normal(3,' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                      .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide()))
                                      .append($('<td>').html(value.CaseCnt)));
                                break;
                        }
                    });
                }
                else {
                    $('#tbodyOfficerRank').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                    //TODO: Rank more
                    switch (ActionText) {
                        case 'Paid':
                            $('#tbodyRankMorePaidOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                        case 'Returned':
                            $('#tbodyRankMoreReturnedOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                        case 'Part Paid':
                            $('#tbodyRankMorePPOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                    }
                }
            }
        },
        error: function () {
        } // When Service call fails
    });
}

function FullDescriptionRank_Normal(key, val) {
    $('#btnPaidCaseBack,#DRankMorePaid,#DRankMorePaidADM,#DRankMorePaidOfficer').hide();
    $('#btnReturnedCaseBack,#DRankMoreReturned,#DRankMoreReturnedADM,#DRankMoreReturnedOfficer').hide();
    $('#btnPPCaseBack,#DRankMorePP,#DRankMorePPADM,#DRankMorePPOfficer').hide();
    if ($.session.get('CompanyID') == 1) {
        switch (val) {
            case 1:
                $('#DRankMorePaidCaseList').hide();
                break;
            case 2:
                $('#DRankMorePPCaseList').hide();
                break;
            case 4:
                $('#DRankMoreReturnedCaseList').hide();
                break;
        }
    }
    else {
        switch (val) {
            case 1:
                $('#paidformanager,#paid').hide();
                break;
            case 2:
                $('#partpaidformanager,#partpaid').hide();
                break;
            case 3:
                $('#tcgformanager,#tcg').hide();
                break;
            case 4:
                $('#tcgpaidformanager,#tcgpaid').hide();
                break;
            case 5:
                $('#tcgppformanager,#tcgpp').hide();
                break;
            case 6:
                $('#uttcformanager,#uttc').hide();
                break;
            case 7:
                $('#droppedformanager,#dropped').hide();
                break;
            case 8:
                $('#otherformanager,#other').hide();
                break;
        }
    }
    switch ($.session.get('RankingType')) {
        case 'Paid':
            $('#DivRankingPaidMore').attr('class', 'modal fade in').show();
            $('#DRankMorePaidNotes').empty().html($('#tdFullDesc' + key).html()).show();
            $('#HCaseNumber').empty().html(' :  ' + $.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html() + " - " + $('#tdCaseOfficer' + key).html()).show();
            $('#btnPaidCaseNotesBack').show().click(function () {
                $('#DRankMorePaidCaseList').show();
                $('#DivRankingPaidMore').attr('class', 'modal fade in').show();
                $('#DRankMorePaidNotes,#HCaseNumber,#btnPaidCaseNotesBack').hide();
                $('#btnPaidCaseBack').show();
            });
            break;
        case 'Returned':
            $('#DivRankingReturnedMore').attr('class', 'modal fade in').show();
            $('#DRankMoreReturnedNotes').empty().html($('#tdFullDesc' + key).html()).show();
            $('#HCaseNumber').empty().html(' :  ' + $.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html() + " - " + $('#tdCaseOfficer' + key).html()).show();
            $('#btnReturnedCaseNotesBack').show().click(function () {
                $('#DRankMoreReturnedCaseList').show();
                $('#DivRankingReturnedMore').attr('class', 'modal fade in').show();
                $('#DRankMoreReturnedNotes,#HCaseNumber,#btnReturnedCaseNotesBack').hide();
                $('#btnReturnedCaseBack').show();
            });
            break;
        case 'Part Paid':
            $('#DivRankingPartPaidMore').attr('class', 'modal fade in').show();
            $('#DRankMorePPNotes').empty().html($('#tdFullDesc' + key).html()).show();
            $('#HCaseNumber').empty().html(' :  ' + $.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html() + " - " + $('#tdCaseOfficer' + key).html()).show();
            $('#btnPPCaseNotesBack').show().click(function () {
                $('#DRankMorePPCaseList').show();
                $('#DivRankingPartPaidMore').attr('class', 'modal fade in').show();
                $('#DRankMorePPNotes,#HCaseNumber,#btnPPCaseNotesBack').hide();
                $('#btnPPCaseBack').show();
            });
            break;
    }

    if ($.session.get('CompanyID') == 2) {
        $('#paidformanager,#partpaidformanager,#tcgformanager,#tcgpaidformanager,#tcgppformanager,#uttcformanager,#droppedformanager,#otherformanager').hide();
        switch (val) {
            case 1:
                $('#paidformanager').show();
                break;
            case 2:
                $('#partpaidformanager').show();
                break;
            case 3:
                $('#tcgformanager').show();
                break;
            case 4:
                $('#tcgpaidformanager').show();
                break;
            case 5:
                $('#tcgppformanager').show();
                break;
            case 6:
                $('#uttcformanager').show();
                break;
            case 7:
                $('#droppedformanager').show();
                break;
            case 8:
                $('#otherformanager').show();
                break;
        }
    }

}

//==========================================================================================================================================
//TODO: Ranking  -End 
//==========================================================================================================================================
function GetStatsOnClick(key) {
    var Flag = key + 1;
    $('#tblStatsManagerCount,#tblStatsADMCount,#tblStatsOfficerCount').hide();
    $('#AStat' + key).attr('data-toggle', 'modal').attr('href', '#divStats');
    $('#lblStatsMgrName').html('');
    switch (Flag) {
        case 1:
            $('#HStatsTitle').html('Productivity');
            break;
        case 2:
            $('#HStatsTitle').html('Efficiency');
            break;
        case 3:
            $('#HStatsTitle').html('Conversion');
            break;
        case 4:
            $('#HStatsTitle').html('Case Holdings');
            break;
        case 5:
            $('#HStatsTitle').html('Paids Vs Targets');
            break;
        case 6:
            $('#HStatsTitle').html('Predicted Efficiency');
            break;
    }
    switch ($.session.get('TreeLevel')) {
        case '0':
            StatsManager($.session.get('ManagerID'), Flag);
            $('#tblStatsManagerCount').show();
            break;
        case '1':
            StatsADM($.session.get('ManagerID'), Flag);
            $('#tblStatsADMCount').show();
            $('#btnStatsADMBack').hide();
            break;
        case '2':
        case '3':
            $('#btnStatsBack').hide();
            $('#tblStatsOfficerCount').show();
            StatsOfficer($.session.get('ManagerID'), Flag);
            break;
    }
}

function StatsManager(ID, Flag) {
    showOverlay("divStatsoverlaydiv");

    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + ID + '/' + Flag + '/StatsOfficerCount',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader2').show();
        },
        success: function (data) {
            hideOverlay("divStatsoverlaydiv");
            $('#tbodyStatsManagerCount').empty();
            if (!(data == '' || data == null || data == '[]')) {
                $.each(data, function (key, value) {
                    $('#tbodyStatsManagerCount').append('<tr><td><a style="cursor:pointer" onclick="StatsADM(' + value.OfficerID + ',' + Flag + ',' + "'" + value.OfficerName + "'" + ')" >' + value.OfficerName + '</a></td>' +
                                                        '<td>' + value.Cnt + '</td></tr>');
                });
            }
            else {
                $('#tbodyStatsManagerCount').append($('<tr>').append($('<td colspan="2">').html('No data found.')));
            }
        },
        complete: function () {
            hideOverlay("divStatsoverlaydiv");
            $('#imgLoader2').fadeOut(1000);//.hide();
        },
        error: function (xhr, textStatus, errorThrown) {
            hideOverlay("divStatsoverlaydiv");
        }
    });
}

function StatsADM(OfficerID, Flag, StatsMgrName) {
    showOverlay("divStatsoverlaydiv");
    $('#tblStatsManagerCount,#tblStatsADMCount,#tblStatsOfficerCount,#btnStatsADMBack,#btnStatsBack').hide();
    $('#tblStatsADMCount').show();
    if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1) {
        $('#btnStatsADMBack').show();
        $('#lblStatsMgrName').html(' - ' + StatsMgrName);
    }
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + ID + '/' + Flag + '/StatsOfficerCount',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader2').show();
        },
        success: function (data) {
            hideOverlay("divStatsoverlaydiv");
            $('#tbodyStatsADMCount').empty();
            if (!(data == '' || data == null || data == '[]')) {
                $.each(data, function (key, value) {
                    $('#tbodyStatsADMCount').append('<tr><td><a style="cursor:pointer" onclick="StatsOfficer(' + value.OfficerID + ',' + Flag + ')" >' + value.OfficerName + '</a></td>' +
                                                            '<td>' + value.Cnt + '</td></tr>');
                });
            }
            else {
                $('#tbodyStatsADMCount').append($('<tr>').append($('<td>').html('No data found.')));
            }
        },
        complete: function () {
            hideOverlay("divStatsoverlaydiv");
            $('#imgLoader2').fadeOut(1000);//.hide();
        },
        error: function (xhr, textStatus, errorThrown) {
            hideOverlay("divStatsoverlaydiv");
        }
    });

}

function StatsOfficer(ID, Flag) {
    showOverlay("divStatsoverlaydiv");
    $('#tblStatsManagerCount,#tblStatsADMCount,#tblStatsOfficerCount,#btnStatsADMBack,#btnStatsBack').hide();
    $('#tblStatsOfficerCount').show();
    if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1) {
        $('#btnStatsBack').show();
    }
    switch (Flag) {
        case 1:
            $('#HStatsTitle').html('Productivity');
            break;
        case 2:
            $('#HStatsTitle').html('Efficiency');
            break;
        case 3:
            $('#HStatsTitle').html('Conversion');
            break;
        case 4:
            $('#HStatsTitle').html('Case Holdings');
            break;
        case 5:
            $('#HStatsTitle').html('Paids Vs Targets');
            break;
        case 6:
            $('#HStatsTitle').html('Predicted Efficiency');
            break;
    }
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + ID + '/' + Flag + '/StatsOfficerCount',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () {
            $('#imgLoader2').show();
        },
        success: function (data) {
            hideOverlay("divStatsoverlaydiv");
            $('#tbodyStatsOfficerCount').empty();
            if (!(data == '' || data == null || data == '[]')) {
                $.each(data, function (key, value) {
                    $('#tbodyStatsOfficerCount').append($('<tr>').append($('<td>').html(value.OfficerName)).append($('<td>').html(value.Cnt)))
                });
            }
            else {
                $('#tbodyStatsOfficerCount').append($('<tr>').append($('<td>').html('No data found.')));
            }
        },
        complete: function () {
            hideOverlay("divStatsoverlaydiv");
            $('#imgLoader2').fadeOut(1000);//.hide();            
        },
        error: function (xhr, textStatus, errorThrown) {
            hideOverlay("divStatsoverlaydiv");
        }
    });
}

//==========================================================================================================================================
//TODO : Clamp function

function ViewClampedImage(key, OfficerID, CaseNo) {

    $.session.set('ImageType', 'C');
    $.session.set('ClampCaseNo', CaseNo);
    $.session.set('ClampOfficerID', OfficerID);
    $('#iframeClampImage').attr('src', 'OptimiseImage.html');
    $('#iframeActionClampImage').attr('src', 'OptimiseImage.html');

    $('.clampSelect').removeClass('clampSelect').addClass('clampUnselect').attr('src', 'images/ViewImage_24_24_2.png');

    if ($('#imgClampImage' + key).attr('src') == 'images/ViewImage_24_24_2.png')
        $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'clampSelect');
    else
        $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_2.png').attr('class', 'clampUnselect');
}

function ViewActionClampedImage(key, OfficerID, CaseNo) {

    $.session.set('ImageType', 'C');
    $.session.set('ClampCaseNo', CaseNo);
    $.session.set('ClampOfficerID', OfficerID);
    $('#iframeActionClampImage').attr('src', 'OptimiseImage.html');

    $('.ActionclampSelect').removeClass('ActionclampSelect').addClass('ActionclampUnselect').attr('src', 'images/ViewImage_24_24_2.png');

    if ($('#imgActionClampImage' + key).attr('src') == 'images/ViewImage_24_24_2.png')
        $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionclampSelect');
    else
        $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_2.png').attr('class', 'ActionclampUnselect');
}

//==========================================================================================================================================
//TODO : Bail function

function ViewBailedImage(Bkey, BailedImageURL) {
    $.session.set('ImageType', 'B');
    $.session.set('BailedImageURL', BailedImageURL);
    $('#iframeBailedImage,#iframeActionBailedImage').attr('src', 'OptimiseImage.html');
    $('.ActionBailedSelect').removeClass('ActionBailedSelect').addClass('ActionBailedUnselect').attr('src', 'images/ViewImage_24_24_2.png');

    if ($('#imgBailedImage' + Bkey).attr('src') == 'images/ViewImage_24_24_2.png')
        $('#imgBailedImage' + Bkey).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionBailedSelect');
    else
        $('#imgBailedImage' + Bkey).attr('src', 'images/ViewImage_24_24_2.png').attr('class', 'ActionBailedUnselect');
}

function ViewActionBailedImage(Bkey, BailedImageURL) {
    $.session.set('ImageType', 'B');
    $.session.set('BailedImageURL', BailedImageURL);

    if ($.session.get('TreeLevel') == '3') {
        $('#iframeActionBailedImage').attr('src', 'OptimiseImage.html');
        $('.ActionBailedSelect').removeClass('ActionBailedSelect').addClass('ActionBailedUnselect').attr('src', 'images/ViewImage_24_24_2.png');

        if ($('#imgActionBailedImage' + Bkey).attr('src') == 'images/ViewImage_24_24_2.png')
            $('#imgActionBailedImage' + Bkey).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionBailedSelect');
        else
            $('#imgActionBailedImage' + Bkey).attr('src', 'images/ViewImage_24_24_2.png').attr('class', 'ActionBailedUnselect');
    }
}

//==========================================================================================================================================
//TODO : Timeline

function GetTimelineResult(OfficerID) {
    var Fdate = $.trim($('#txtTLineFromDate').val()) == '' ? '1/1/1900' : $.trim($('#txtTLineFromDate').val());
    var dateT1 = Fdate.split('/');
    var newDateFrom = dateT1[2] + '-' + dateT1[1] + '-' + dateT1[0];
    $.ajax({
        url: ServiceURL + 'api/v1/officers/' + OfficerID + '/' + newDateFrom + '/' + newDateFrom + '/TimelineDetails',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'GET',
        dataType: DataType,
        beforeSend: function () {
            $('#selCompanyList').empty();
        },
        success: function (data) {
            $('#cd-timeline').empty();
            if (!(data == '' || data == null || data == '[]')) {
                $.each(data, function (key, value) {
                    $('#cd-timeline').append('<div class="cd-timeline-block">' +
                                             '<div id="DTimeLinePicture' + key + '" ></div>' +
                                             '<div class="cd-timeline-content"><h2>' + value.TTitle + '</h2> <p>' + value.TDescription + '</p>' +
                                             '<span class="cd-date">' + value.TDate + '</span></div></div>');

                    switch (value.TTitle) {
                        case 'LOGIN':
                        case 'ARR':
                        case 'DEP':
                            $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-picture');
                            break;
                        case 'Returned':
                            $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-movie');
                            break;
                        case 'Left Letter':
                            $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-location');
                            break;
                        case 'Paid':
                        case 'Part Paid':
                            $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-location');
                            break;
                    }
                });
            }
            else {
                $('#cd-timeline').append('<div class="cd-timeline-block">' +
                                           '<div class="cd-timeline-img cd-location" ></div>' +
                                           '<div class="cd-timeline-content"><h2>No data found...</h2></div></div>');
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

//==========================================================================================================================================
//TODO : Notification function

function Notification() {
    $('#lblSendNotification').attr('data-toggle', 'modal').attr('href', '#Notification');
    $('#popup2Trigger').trigger('click');
}

function UpdateNotificationAlert(ManagerID, MessageText, OfficerID, IsPriority) {
    if (OfficerID == undefined) OfficerID = 0;
    $.ajax({
        url: ServiceURL + 'api/v1/officers/' + ManagerID + '/' + MessageText.replace('&', 'ybcc') + '/' + $.trim(OfficerID) + '/' + IsPriority + '/Notification',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'PUT',
        dataType: 'json',
        beforeSend: function () {
            $('.word_count').hide();
            $('#imgLoading').show();
            $('#btnSendNotification').hide();
        },
        success: function (data) {
        },
        complete: function () {
            if ($("#chkIsPriority").is(':checked')) {
                $('.word_count span').text('200 characters left');
            }
            else {
                $('.word_count span').text('900 characters left');
            }
            $('#lblMsgReport').html('Message sent successfully').show().fadeOut(3000);
            $('#lblMsgReport').css('color', 'green');
            $('.word_count').fadeIn(4000);
            $('#imgLoading').hide();
            $('#btnSendNotification').show();

            $('#selOfficer').multiselect({
                noneSelectedText: 'Select Officers',
                selectedList: 5,
                multiple: true
            }).multiselectfilter();

            $('#selOfficer').multiselect("uncheckAll");
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

//==========================================================================================================================================
//TODO : Officer Activity status

function OfficerActivityStatusClick() {
    OfficerActivityStatusInit();
    $('#AOfficerActivityStatus').click();
}

function GetOfficerActivityStatus() {
    $.ajax({
        url: ServiceURL + 'api/v1/officers/' + tableIndex + '/' + $.session.get('OfficerID') + '/OfficerActivityStatus',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'GET',
        dataType: 'json',
        beforeSend: function () {
            $('#popup2Trigger').trigger('click');
        },
        success: function (data) {
            $('#DivOfficerStatusContent').empty();
            $('#DivOfficerStatusContent').append('<div id="tdCont"></div>');
            $('#DivOfficerStatusContent').append('<div id="GMAPOAS" class="map" style="width:1150px; height: 370px;"></div>')
            if (data != undefined) {
                if (data != "[]" && data != null) {
                    $.each(data, function (key, value) {
                        tableIndex = value.IndexNo;
                        $('#lblManagerNameActivityStatus').html(value.ManagerName);
                        $('#tdGray').html(value.Darkgray);
                        $('#tdGreen').html(value.Green);
                        $('#tdRed').html(value.Red);
                        $('#tdAmber').html(value.Amber);

                        if ($('#tbl' + $.trim(value.ManagerId)).length == 0) {
                            $('#tdCont').append('<div id="tbl' + $.trim(value.ManagerId) + '"></div>')
                        }

                        if ($('#tbl' + $.trim(value.ManagerId) + ' tr').length == 0) {
                            $('#tbl' + $.trim(value.ManagerId)).append($('<div class="col-md-4 col-sm-4">').append($('<div class="bubble ' + value.Status + 'Bub">')
                                    .html(value.OfficerName).attr('id', 'OAS' + value.OfficerID)))
                        }
                        else {
                            if ($('#tbl' + $.trim(value.ManagerId) + ' tr:last' + ' td').length >= 3) {
                                $('#tbl' + $.trim(value.ManagerId)).append($('<tr>'))
                            }
                            $('#tbl' + $.trim(value.ManagerId) + ' tr:last').append($('<td style="padding:10px !important;">')
                                .append($('<div class="bubble ' + value.Status + 'Bub">').html(value.OfficerName).attr('id', 'OAS' + value.OfficerID)));
                        }

                        $('#OAS' + value.OfficerID).click(function () {
                            $('#btnBacktoStatus').show();
                            $('#lblOfficerNameMapview').html(value.OfficerName);
                            $('#lblOfficerNameMapview').show();
                            OffID = $(this).attr('id').split('OAS')[1];
                            HeartBeat(1);
                            $('#tdPrev,#tdNext,#tdCont').hide();
                            $('#divMapOAS').show();
                            $('#divOfficerActivityStatus').css('width', '87%').css('height', '95%');
                            $('#btnBacktoStatus').unbind().click(function () {
                                $('#divMapOAS').hide();
                                $('#tdCont,#tdPrev,#tdNext').show();
                                $('#lblOfficerNameMapview').hide();
                                $('#divOfficerActivityStatus').css('width', '').css('height', '');
                                $(this).hide();
                                tableIndex = tableIndex - 1;
                                GetOfficerActivityStatus();
                            });
                        });
                    });
                    $('#lblManagerNameActivityStatus').html($('#tdCont table:eq(' + tableIndex + ')').attr('mname'));
                    tableIndex = tableIndex + 1;
                }
            }

            $('#DivOfficerStatusContent').append($('<table style="width:100%">')
           .append($('<tr >').append($('<td style="width:50%">').attr('id', 'tdPrev'))
           .append($('<td style="width:50%;text-align:right;">').attr('id', 'tdNext'))))

            $('#tdPrev').append('<img src="images/Previous_W.png" style="cursor:pointer;" /><br><span style="cursor:pointer;">Previous</span>');
            $('#tdNext').append('<img src="images/Next_W.png" style="cursor:pointer;" /><br><span style="cursor:pointer;">Next</span>');

            $('#tdPrev').unbind().click(function () {

                tableIndex = tableIndex - 2;

                if (tableIndex <= -1) tableIndex = 0;
                GetOfficerActivityStatus();
            });

            $('#tdNext').unbind().click(function () {

                GetOfficerActivityStatus();

                clearInterval(IntervalvalueOfficerStatus);
                IntervalvalueOfficerStatus = setInterval(function () {
                    GetOfficerActivityStatus();
                }, 180000);
            });
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

function OfficerActivityStatusInit() {
    GetOfficerActivityStatus();
}

//==========================================================================================================================================
//TODO : Flash news function

function FlashOn() {
    $('#btnFlashOn').hide();
    $('#DMarquee,#btnFlashOff').show();
    $.session.set('FlashNews', 'On');
}

function FlashOff() {
    $('#DMarquee,#btnFlashOff').hide();
    $('#btnFlashOn').show();
    $.session.set('FlashNews', 'Off');
    $('#popup2Trigger').trigger('click');
}

function GetFlashNews() {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + $.session.get('CompanyID') + '/GetFlashNews',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () { },
        success: function (data) {
            if (data != undefined) {
                if (data != "[]" && data != null && data[0].Result != 'NoRecords') {
                    $('#DMarquee').empty().append('<marquee behavior="scroll" direction="left" scrollamount="6">' + data + '</marquee>')
                }
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

//==========================================================================================================================================
//TODO : Company function

function GetCompany() {
    $.ajax({
        url: ServiceURL + 'api/v1/companies',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'GET',
        dataType: DataType,
        beforeSend: function () {
            $('#selCompanyList').empty();
        },
        success: function (data) {
            $.each(data, function (key, value) {
                $('#selCompanyList').append($('<option>').attr('value', $.trim(value.CompanyID)).html(value.CompanyName));
            });
            $('#selCompanyList').val($.session.get('CompanyID')).attr("selected", "selected");
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

function SubmitOfficerTotal() {
    var TTargetInfo = '';
    $('#tbodyOfficerList tr').each(function (key, value) {
        if ($(this).find('label').html() != '' && $(this).find('label').html() != undefined && parseInt($(this).find('input').val()) > 0) {
            if (TTargetInfo != '') TTargetInfo += ',';
            TTargetInfo += $(this).find('label').html() + '|' + parseInt($(this).find('input').val());
        }
    });

    $.ajax({
        url: ServiceURL + 'api/v1/officers/' + $('#selCurrentMonth').val() + '/' + $('#selCurrentMonth').val() +
            '/' + $('#txtMonthTotalTarget').val() + '/' + ManagerID + '/' + TTargetInfo + '/OfficerTarget',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'PUT',
        dataType: DataType,
        beforeSend: function () {

        },
        success: function (data) {
            if (data == true) {
                $('#lblTargetInfo').html('Target has been updated successfully.');
                $('#ATargetInfo').click();
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

//==========================================================================================================================================
//TODO: Page link -Start
//==========================================================================================================================================

function TeamAdmin() {
    window.location.href = TeamAdminUrl;
}

function ReturnAction() {
    window.location.href = ReturnActionUrl;
}

function CaseSearchAction() {
    window.location.href = CaseSearchUrl;
}

function ApproveRequest() {
    window.location.href = ApproveRequestUrl;
}

function HPICheckRequest() {
    window.location.href = HPICheckRequestUrl;
}

function PaymentReport() {
    window.location.href = PaymentReportUrl;
}

function CaseUpload() {
    window.location.href = CaseuploadUrl;
}

function HRview() {
    window.location.href = HRUrl;
}

function SpecialCase() {
    window.location.href = SCUrl;
}

function OptimiseHome() {
    window.location.href = OptimiseHomeUrl;
}

function CaseGuidance() {
    window.location.href = CGAdminUrl;
}

function ClampedImageSearch() {
    window.location.href = ClampedImageUrl;
}

function BailedImageSearch() {
    window.location.href = BailedImageSearchUrl;
}

function GetManageCaseReturn() {
    window.location.href = CaseReturnUrl;
}

function SCAdmin() {
    window.location.href = SCAdminUrl;
}
//==========================================================================================================================================
//TODO: Page link -End
//==========================================================================================================================================
//TODO: panel update 
//==========================================================================================================================================

function showhidetabs(tabname) {
    $('#show-search-details,#tblbodySearchType').hide();//,#tblWMAADM
    $('#' + tabname).show();
}

function updateConnection() {
    if (signalrconnected) {
        $.connection.hub.stop();
        signalrconnected = false;
        var d = new Date();
        $.connection.hub.qs = { "Authorization": $.session.get('Token') };
        $.connection.hub.start().done(function () {
            signalrconnected = true;
            counter.server.tokenupdated();
            updateAllPanel();
        });
    }
}

function caseactionupdated() {
    var d = new Date();
    if (signalrconnected) {
        if (CaseActionURL != "") {
            if ($.session.get('CaseActionMode') == 'S' || $.session.get('CaseActionMode') == undefined)
                GetActionsHub(CaseActionURL);
            if (!(CA_TYPE == null || CA_TYPE == "")) {
                switch (CA_TYPE) {
                    case 'Manager':
                        GetCaseActionManager(CAMGR_URL, CA_FLAG, "Manager");
                        break;
                    case 'ADM':
                        GetCaseActionManager(CAADM_URL, CA_FLAG, "ADM");
                        break;
                    case 'Officer':
                        GetCaseActionManager(CAOFF_URL, CA_FLAG, "Officer");
                        break;
                }
            }
        }
    }
}

function WMAupdated() {
    var d = new Date();
    if (signalrconnected) {
        if (WMAURL != "") {
            if ($.session.get('WMAMode') == 'S' || $.session.get('WMAMode') == undefined) {
                GetWMADetatil(WMAURL, WMAType);
            }
        }
    }
}

function StatsURLupdated() {
    var d = new Date();
    if (signalrconnected) {
        if (StatsURL != "") {
            if ($.session.get('TreeLevel') == '3')
                GetStats(StatsURL, 1);
            else
                GetStats(StatsURL, 0);
        }
    }
}

function locationupdated() {
    var maptype = $.session.get('Map');
    if ((signalrconnected) && ((maptype == 'LL') || (maptype == 'LLEnlarge'))) {
        if (LocationURL != "") {
            if ($.session.get('LocationMode') == 'S' || $.session.get('LocationMode') == undefined)
                GetLastLocation(LocationURL, 0, 1);
        }
    }
}

function locationhbupdated() {
    var maptype = $.session.get('Map');
    if ((signalrconnected) && ((maptype == 'LocHB') || (maptype == 'LocHBEnlarge'))) {
        if (LocationURL != "") {
            if ($.session.get('LocationMode') == 'S' || $.session.get('LocationMode') == undefined) {
                if ($.session.get('TreeLevel') == 3)
                    LocationwithHB(1);
            }
        }
    }
}

function heartbeatupdated() {
    var maptype = $.session.get('Map');
    if ((signalrconnected) && ((maptype == 'HB') || (maptype == 'HBEnlarge'))) {
        if (LocationURL != "") {
            if ($.session.get('LocationMode') == 'S' || $.session.get('LocationMode') == undefined) {
                if ($.session.get('TreeLevel') == 3)
                    HeartBeat(1);
            }
        }
    }
}

function rankupdated() {
    var d = new Date();
    if (signalrconnected) {
        if (RankURL != "") {
            if ($.session.get('RankMode') == 'S' || $.session.get('RankMode') == undefined) {
                if (RA_TYPE == "Manager") {
                    for (var i = 0; i <= 2; i++) {
                        GetRanking(RankURL[i], RankActionText[i], RankViewType[i]);
                    }
                }
                else if (RA_TYPE == "ADM") {
                    for (var i = 0; i <= 2; i++) {
                        GetRanking(RankADMURL[i], RankADMActionText[i], RankADMViewType[i]);
                    }
                }
                else {
                    for (var i = 0; i <= 2; i++) {
                        GetRanking(RankOFFURL[i], RankOFFActionText[i], RankOFFViewType[i]);
                    }
                }
            }
        }
    }
}

function updateAllPanel() {
    caseactionupdated();
    WMAupdated();
    StatsURLupdated();
    locationupdated();
    rankupdated();
    locationhbupdated();
    heartbeatupdated();
}
//==========================================================================================================================================
// TODO : HeartBeat normal MAP Initialize
//==========================================================================================================================================
function hbmapinit() {
    clearattributes_HBMap();
    center = new google.maps.LatLng(51.699467, 0.109348);
    options = {
        'zoom': 16,
        'center': center,
        'mapTypeId': google.maps.MapTypeId.ROADMAP,
        'fullscreenControl': true
    };

    officerHBMap = new google.maps.Map(document.getElementById("DHeartBeatMap"), options);

    HB_polyline = new google.maps.Polyline({
        path: [],
        strokeColor: "#FF0000",
        strokeOpacity: 1.0,
        strokeWeight: 8,
        map: officerHBMap
    });
}

function clearattributes_HBMap() {
    officerHBArray = new Array();
    HB_bounds = new google.maps.LatLngBounds();
    clearmarkers_HBMap();
}

function clearmarkers_HBMap() {
    for (var key in hbmarkers) {
        hbmarkers[key].setMap(null);
        delete hbmarkers[key];
    };
    hbmarkers = [];
}

function LocHBMapInit() {
    clearattributes_LocHBMap();
    center = new google.maps.LatLng(51.699467, 0.109348);
    options = {
        'zoom': 10,
        'center': center,
        'mapTypeId': google.maps.MapTypeId.ROADMAP,
        'fullscreenControl': true
    };

    officerLocHBMap = new google.maps.Map(document.getElementById("DLocationHBMap"), options);

    LocHB_polyline = new google.maps.Polyline({
        path: [],
        strokeColor: "#FF0000",
        strokeOpacity: 1.0,
        strokeWeight: 8,
        map: officerLocHBMap
    });
}

function clearattributes_LocHBMap() {
    officerLocHBArray = new Array();
    LocHB_bounds = new google.maps.LatLngBounds();
    clearmarkers_LocHBMap();
}

function clearmarkers_LocHBMap() {
    for (var key in LocHB_markers) {
        LocHB_markers[key].setMap(null);
        delete LocHB_markers[key];
    };
    LocHB_markers = [];
}

function LocHBMapExpandInit() {
    clearattributes_LocHBMapExpand();
    center = new google.maps.LatLng(51.699467, 0.109348);
    options = {
        'zoom': 10,
        'center': center,
        'mapTypeId': google.maps.MapTypeId.ROADMAP,
        'fullscreenControl': true
    };

    officerLocHBMap_Expand = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);

    LocHB_polyline_Expand = new google.maps.Polyline({
        path: [],
        strokeColor: "#FF0000",
        strokeOpacity: 1.0,
        strokeWeight: 8,
        map: officerLocHBMap_Expand
    });
}

function clearattributes_LocHBMapExpand() {
    officerLocHBArray_Expand = new Array();
    LocHB_bounds_Expand = new google.maps.LatLngBounds();
    clearmarkers_LocHBMapExpand();
}

function clearmarkers_LocHBMapExpand() {
    for (var key in LocHB_markers_Expand) {
        LocHB_markers_Expand[key].setMap(null);
        delete LocHB_markers_Expand[key];
    };
    LocHB_markers_Expand = [];
}

// HeartBeat EXPAND MAP Initialize
function hbExpandMapinit() {
    clearattributes_HBMapExpand();
    center = new google.maps.LatLng(51.699467, 0.109348);
    options = {
        'zoom': 16,
        'center': center,
        'mapTypeId': google.maps.MapTypeId.ROADMAP,
        'fullscreenControl': true
    };

    officerHBMapExpand = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);

    HB_Expand_polyline = new google.maps.Polyline({
        path: [],
        strokeColor: "#FF0000",
        strokeOpacity: 1.0,
        strokeWeight: 8,
        map: officerHBMapExpand
    });
}

function clearattributes_HBMapExpand() {
    officerHBExpandArray = new Array();
    HBExpand_bounds = new google.maps.LatLngBounds();
    clearmarkers_HBMapExpand();
}

function clearmarkers_HBMapExpand() {
    for (var key in hbexpandmarkers) {
        hbexpandmarkers[key].setMap(null);
        delete hbexpandmarkers[key];
    };
    hbexpandmarkers = [];
}

// Last Location NORMAL MAP Initialize
function llmapinit() {
    clearattributes_LLMap();
    center = new google.maps.LatLng(51.699467, 0.109348);
    options = {
        'zoom': 4,
        'center': center,
        'mapTypeId': google.maps.MapTypeId.ROADMAP,
        'fullscreenControl': true
    };
    officerLLMap = new google.maps.Map(document.getElementById("map_canvas"), options);
}

function clearattributes_LLMap() {
    officerLLArray = new Array();
    LL_Bounds = new google.maps.LatLngBounds();
    clearmarkers_LLMap();
}

function clearmarkers_LLMap() {
    for (var key in llmarkers) {
        llmarkers[key].setMap(null);
        delete llmarkers[key];
    };
    llmarkers = [];
}

// Last Location EXPAND MAP Initialize
function llmapexpandinit() {
    clearattributes_LLMap_Expand();
    center = new google.maps.LatLng(51.699467, 0.109348);
    options = {
        'zoom': 4,
        'center': center,
        'mapTypeId': google.maps.MapTypeId.ROADMAP,
        'fullscreenControl': true
    };
    officerLLMap_Expand = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);
}

function clearattributes_LLMap_Expand() {
    officerLLArray_Expand = new Array();
    LL_Bounds_Expand = new google.maps.LatLngBounds();
    clearmarkers_LLMap_Expand();
}

function clearmarkers_LLMap_Expand() {
    for (var key in llmarkers_expand) {
        llmarkers_expand[key].setMap(null);
        delete llmarkers_expand[key];
    };
    llmarkers_expand = [];
}

function connectionStateChanged(state) {
    var stateConversion = { 0: 'connecting', 1: 'connected', 2: 'reconnecting', 4: 'disconnected' };
    showconsole('SignalR state changed from: ' + stateConversion[state.oldState]
     + ' to: ' + stateConversion[state.newState]);
}

function clearmaparrays() {
    officerHBArray = new Array();
    HB_bounds = new google.maps.LatLngBounds();

    officerHBExpandArray = new Array();
    HBExpand_bounds = new google.maps.LatLngBounds();

    officerLLArray = new Array();
    LL_Bounds = new google.maps.LatLngBounds();

    officerLLArray_Expand = new Array();
    LL_Bounds_Expand = new google.maps.LatLngBounds();

    officerLocHBArray = new Array();
    LocHB_bounds = new google.maps.LatLngBounds();

    officerLocHBArray_Expand = new Array();
    LocHB_bounds_Expand = new google.maps.LatLngBounds();

    // TODO: Fancybox close 
    fancyboxClose();
}

function callHB() {
    var maptype = $.session.get('Map');
    if ($("#DHeartBeatMap").css('display') != 'none') {
        if (maptype == 'HB')
            $('#AHeartBeat').trigger('click');
    }
    else if ($("#map_canvas_Expand").css('display') != 'none') {
        if (maptype == 'HBEnlarge')
            $('#AHeartBeatEnlarge').trigger('click');
    }
    else {
        //showconsole('DHeartBeatMap is hidden');
        //showconsole('map_canvas_Expand is hidden');
    }
}

function callLOCHB() {
    var maptype = $.session.get('Map');
    if ((maptype == 'LocHB') || (maptype == 'LocHBEnlarge')) {
        $('#ALocationwithHB').trigger('click');
    }
}
//==========================================================================================================================================

function cancelTimeouts() {
    clearTimeout(hbtimer);
    clearTimeout(lochbtimer);
}

function stopHub() {
    showconsole("Stop Hub called");
    $.connection.hub.stop();
    $.connection.hub.stop().done(function () {
        showconsole("Stop Hub stopped");
    });
}

function fancyboxClose() {
    $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom').attr('style', 'display:none');
    $('#' + $('#popup1Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom').attr('style', 'display:none');
    $('#' + $('#popup3Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom').attr('style', 'display:none');
}

function showhidetabs(tabname) {
    $('#show-search-details,#tblbodySearchType').hide();//,#tblWMAADM
    $('#' + tabname).show();
}

function caseactionshowhidetabs() {
    $('#leftletterformanager,#revisitformanager,#partpaidformanager,#returnedformanager,#paidformanager,#deviatedformanager,#DeviatedGPS').hide();
    $('#bailedformanager,#arrestedformanager,#Surrenderdateagreedformanager').hide();
    $('#tcgpaidformanager,#tcgppformanager,#tcgformanager,#uttcformanager,#droppedformanager,#otherformanager,#Clampedformanager').hide();
}

//==========================================================================================================================================
